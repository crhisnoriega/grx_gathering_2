package grx.tracking.gui.views;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.persistence.ContainerInfo;

public class ContainerRegisterView extends VerticalLayout implements View {

	private Binder<ContainerInfo> binder;

	public ContainerRegisterView() {
		this.binder = new Binder<ContainerInfo>(ContainerInfo.class);

		{
			GridLayout form = this.createForm();

			this.addComponent(form);
		}
		this.setWidth("100%");
	}

	private GridLayout createForm() {
		GridLayout gl = new GridLayout();

		{
			Label title = new Label(VaadinIcons.TRUCK.getHtml() + " Cadastro de Caçambas", ContentMode.HTML);
			title.addStyleName(ValoTheme.LABEL_LARGE);
			title.addStyleName(ValoTheme.LABEL_BOLD);
			title.setHeight("40px");

			gl.addComponent(title);
		}

		{
			TextField field = new TextField("Código da Caçamba");
			field.addStyleName("textfield-large");

			this.binder.forField(field).asRequired().bind("code");

			gl.addComponent(field);
		}

		{
			TextField field = new TextField("Descrição");
			field.setWidth("100%");

			this.binder.forField(field).asRequired().bind("description");

			gl.addComponent(field);
		}

		gl.addStyleName("my-card");
		gl.setMargin(true);
		gl.setWidth("100%");

		return gl;

	}

	private Grid<ContainerInfo> createGrid() {
		Grid<ContainerInfo> grid = new Grid<>();

		return grid;
	}
}
