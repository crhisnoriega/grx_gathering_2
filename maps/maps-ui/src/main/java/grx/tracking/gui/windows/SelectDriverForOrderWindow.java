package grx.tracking.gui.windows;

import java.util.List;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.UserId;

public class SelectDriverForOrderWindow extends Window {

	private static final long serialVersionUID = 1L;

	private SelectedDriverListener listener;

	private UserId user;

	public SelectDriverForOrderWindow(OrderInfo order, List<UserId> userIds) {
		super("Enviar pedido para Motorista");
		VerticalLayout mainLayout = new VerticalLayout();
		{
			TextField orderfield = new TextField("Numero do Pedido");

			orderfield.setValue(
					order.getOrderId().getOrder_id() + " - " + order.getAddress().getAddress().asAddressString());

			mainLayout.addComponent(orderfield);

		}
		{
			Grid<UserId> gridUsers = new Grid<>();
			gridUsers.addColumn(user -> user.getUser_id()).setCaption("Motorista disponível").setWidth(400d);

			gridUsers.setItems(userIds);

			gridUsers.addSelectionListener(new SelectionListener<UserId>() {

				@Override
				public void selectionChange(SelectionEvent<UserId> event) {
					user = event.getFirstSelectedItem().get();
				}
			});

			gridUsers.setSizeFull();

			gridUsers.setHeight("200px");

			mainLayout.addComponent(gridUsers);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfim = new Button("Confirmar");
			btnConfim.addClickListener(event -> {
				if (listener != null) {
					listener.process(user);
				}

			});
			buttons.addComponent(btnConfim);

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(event -> close());
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		setContent(mainLayout);

		setWidth("500px");
		setHeight("400px");
		setClosable(false);
		setResizable(false);

	}

	public void setListener(SelectedDriverListener listener) {
		this.listener = listener;
	}

	public static interface SelectedDriverListener {
		public void process(UserId userId);

	};

}
