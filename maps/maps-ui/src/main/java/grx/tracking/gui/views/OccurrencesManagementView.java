package grx.tracking.gui.views;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_STATUS;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_TYPE;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.util.MyConfig;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyDialogs;
import grx.tracking.gui.windows.MyNotification;
import grx.tracking.gui.windows.ResolveOccurrenceWindow;
import grx.tracking.gui.windows.ResolveOccurrenceWindow.OptionListener;
import grx.tracking.gui.windows.SelectDriverForOrderWindow;
import grx.tracking.gui.windows.SelectDriverForOrderWindow.SelectedDriverListener;

public class OccurrencesManagementView extends VerticalLayout implements View {

	public static Logger logger = Logger.getLogger(OccurrencesManagementView.class.getCanonicalName());

	private static final long serialVersionUID = 1L;

	private UI ui;

	private GRXTrackingSession session;

	private Grid<OccurrenceInfo> ocurrenceGrid;
	// public List<OccurrenceInfo> okOcurrs;

	private DateField date;

	private MyConfig config;
	private Timer timer;

	private List<OccurrenceInfo> ocurrences;

	public OccurrencesManagementView(UI ui) {
		super();
		this.ui = ui;
		this.config = new MyConfig();
		this.session = CurrentUser.get();
		// this.okOcurrs = new ArrayList<>();
		{
			HorizontalLayout buttons = new HorizontalLayout();

			this.date = new DateField();
			this.date.setWidth("150px");
			this.date.setDateFormat("dd/MM/yyyy");
			this.date.setValue(LocalDate.now());
			this.date.addValueChangeListener(new ValueChangeListener<LocalDate>() {

				@Override
				public void valueChange(ValueChangeEvent<LocalDate> event) {
					filter();
				}
			});
			buttons.addComponent(this.date);

			Button btnRefresh = new Button(VaadinIcons.REFRESH);
			btnRefresh.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					filter();
				}
			});
			buttons.addComponent(btnRefresh);

			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);

		}

		{
			this.ocurrenceGrid = this.createGrid();
			this.ocurrenceGrid.setSizeFull();
			this.ocurrenceGrid.addSelectionListener(new SelectionListener<OccurrenceInfo>() {

				private static final long serialVersionUID = 1L;

				@Override
				public void selectionChange(SelectionEvent<OccurrenceInfo> event) {

				}
			});

			addComponent(this.ocurrenceGrid);
			setExpandRatio(this.ocurrenceGrid, 0.2f);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnShow = new Button("Resolver Ocorrência");
			btnShow.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					Set<OccurrenceInfo> selected = ocurrenceGrid.getSelectedItems();

					if (!selected.isEmpty()) {
						OccurrenceInfo ocurr = selected.iterator().next();

						ResolveOccurrenceWindow window = new ResolveOccurrenceWindow(ocurr, session);
						window.setModal(true);
						window.setListener(new OptionListener() {

							@Override
							public void cancelRoute(String route_id, UserId userId) {
								try {
									EJBLocator.refGRXTrackingService().cancelRoute(route_id, userId, true, true);
									ocurr.setStatus(OCCURRENCE_STATUS.CLOSED);
									session.saveEntity(ocurr.getId(), ocurr);
									window.close();

									filter();

								} catch (Exception e) {
									e.printStackTrace();
								}

							}

							@Override
							public void cancelOrder(OrderId orderId, String route_id, UserId userId) {
								try {
									EJBLocator.refGRXTrackingService().cancelOrder(orderId, route_id, userId, true);
									ocurr.setStatus(OCCURRENCE_STATUS.CLOSED);
									session.saveEntity(ocurr.getId(), ocurr);
									window.close();

									filter();
								} catch (Exception e) {
									e.printStackTrace();
								}

								try {
									OrderInfo order = session.findEntity(OrderInfo.class, orderId);
									List<UserId> userIds = new ArrayList<>();

									GRXTrackingService grxservice = EJBLocator.refGRXTrackingService();

									Future<List<UserId>> future = grxservice.getSessionsRemote(null, USER_ROLE.DRIVER);
									List<UserId> drivers = future.get();
									drivers.forEach(driver -> {
										try {
											if (grxservice.getUserSessionEJB(driver).getLastLocation() != null) {
												userIds.add(driver);
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									});

									SelectDriverForOrderWindow driverWindow = new SelectDriverForOrderWindow(order,
											userIds);
									driverWindow.setListener(new SelectedDriverListener() {

										@Override
										public void process(UserId userId) {
											try {
												GRXTrackingSession session1 = EJBLocator.refGRXTrackingService()
														.getUserSessionEJB(userId);
												if (session1.getCurrentRoute() == null) {
													throw new Exception("Motorista nao tem rota designada");
												}

												if (session1 != null) {
													session1.addOrderToRoute(userId, order);
												}
												driverWindow.close();
											} catch (Exception e) {
												e.printStackTrace();
												MyDialogs.showError("Ocorrência", "Erro: " + e.getLocalizedMessage());
											}
										}
									});
									driverWindow.setModal(true);
									ui.addWindow(driverWindow);
								} catch (Exception e) {
									e.printStackTrace();
								}

							}
						});
						ui.addWindow(window);
					}

				}
			});

			Button btnRemove = new Button("Cancelar Ocorrencia");
			btnRemove.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						Set<OccurrenceInfo> selected = ocurrenceGrid.getSelectedItems();

						if (!selected.isEmpty()) {
							OccurrenceInfo ocurr = selected.iterator().next();
							ocurr.setStatus(OCCURRENCE_STATUS.CLOSED);
							session.saveEntity(ocurr.getId(), ocurr);
						}
						ocurrenceGrid.getDataProvider().refreshAll();
						filter();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			buttons.addComponent(btnShow);
			buttons.addComponent(btnRemove);
			buttons.setSizeUndefined();

			setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		filter();

		setSizeFull();

	}

	private Grid<OccurrenceInfo> createGrid() {
		Grid<OccurrenceInfo> grid = new Grid<>();

		/*
		 * grid.addComponentColumn(ocurr -> { CheckBox check = new CheckBox();
		 * check.addValueChangeListener(new ValueChangeListener<Boolean>() {
		 * 
		 * private static final long serialVersionUID = 1L;
		 * 
		 * @Override public void valueChange(ValueChangeEvent<Boolean> event) { if
		 * (event.getValue() == false && okOcurrs.contains(ocurr)) {
		 * okOcurrs.remove(ocurr); }
		 * 
		 * if (event.getValue() == true && !okOcurrs.contains(ocurr)) {
		 * okOcurrs.add(ocurr); } } }); check.setValue(okOcurrs.contains(ocurr)); return
		 * check; }).setCaption("#").setWidth(60d);
		 */

		grid.addColumn(ocurr -> ocurr.getId().split("-")[0]).setCaption("ID da Rota");
		grid.addColumn(ocurr -> ocurr.getStatus().getName()).setCaption("Status");
		grid.addColumn(ocurr -> ocurr.getDate()).setCaption("Data Atualização");
		grid.addColumn(ocurr -> {
			if (ocurr.getType() != null) {
				return ocurr.getType().getName();
			} else {
				return "";
			}
		}).setCaption("Tipo");
		grid.addColumn(ocurr -> {
			if (ocurr.getUserId() != null) {
				return ocurr.getUserId().getUser_id();
			}
			return "";
		}).setCaption("Motorista ID");

		grid.addColumn(ocurr -> {
			if (OCCURRENCE_TYPE.TRUCK_BREAK.equals(ocurr.getType()) && ocurr.getRoute_id() != null) {
				return "Rota: " + ocurr.getRoute_id().split("-")[0];
			}

			if (OCCURRENCE_TYPE.ORDER_PROBLEM.equals(ocurr.getType()) && ocurr.getOrderId() != null) {
				return "Pedido: " + ocurr.getOrderId().getOrder_id();
			}

			return "";
		}).setCaption("Rota/Pedido").setWidth(250d);

		return grid;
	}

	private void filter() {
		try {
			Date date1 = Date.from(this.date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("date1", date1);
			this.ocurrences = this.session.findEntities(
					"select o from OccurrenceInfo as o where o.date >= :date1 order by o.date desc",
					OccurrenceInfo.class, params);
			this.ocurrenceGrid.setItems(this.ocurrences);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		logger.info("enter view event.getParameters(): " + event.getParameters());
		String ocurr_id = event.getParameters();

		this.ui.access(new Runnable() {

			@Override
			public void run() {
				filter();

				OccurrenceInfo select = new OccurrenceInfo();
				select.setId(ocurr_id);
				ocurrenceGrid.select(select);
			}
		});

	}

	public void updateOccurrTable() {
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				ui.access(new Runnable() {
					@Override
					public void run() {
						
						filter();
					}
				});
			}
		});
		t.start();

	}
}
