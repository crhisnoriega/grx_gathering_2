package grx.tracking.gui.windows;

import org.vaadin.inputmask.InputMask;

import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.ContainerCTRInfo;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_TYPE;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.TrashType;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;

public class TrashCapacitiyWindow extends Window {

	private static final long serialVersionUID = 1L;

	private Binder<ContainerCTRInfo> containerBinder;

	public TrashCapacitiyWindow() {
		super("Tipos de Residuos");

		this.containerBinder = new Binder<>(ContainerCTRInfo.class);

		VerticalLayout mainLayout = new VerticalLayout();
		{
			GridLayout grid = new GridLayout(3, 10);

			TrashType[] trashes = TrashType.values();

			for (int i = 1; i < trashes.length; i++) {
				TrashType trash = trashes[i];

				Label label = new Label(trash.getName());
				label.setWidth("400px");
				grid.addComponent(label);

				TextField value = new TextField();
				grid.addComponent(value);

				grid.setComponentAlignment(value, Alignment.MIDDLE_CENTER);
				{
					InputMask numericInputMask = new InputMask("999999,99");
					numericInputMask.setNumericInput(true);
					numericInputMask.setPlaceholder(" ");
					numericInputMask.extend(value);
				}

				String bindname = trash.toString();

				this.containerBinder.forField(value).bind(bindname);

				Label cap = new Label(" " + trash.getUnid());
				grid.addComponent(cap);
				grid.setComponentAlignment(cap, Alignment.MIDDLE_CENTER);

				VerticalLayout vDivider = new VerticalLayout();
				vDivider.setWidth("100%"); // I need 2px separator
				vDivider.setHeight(10, Unit.PIXELS);
				// vDivider.addStyleName("color_ligth"); // Ligth color for separator

				vDivider.setSpacing(false);
				vDivider.setMargin(false);

				grid.addComponent(vDivider);
				grid.addComponent(new Label());
				grid.addComponent(new Label());

			}

			grid.setColumnExpandRatio(1, 3);
			grid.setColumnExpandRatio(2, 1);
			grid.setColumnExpandRatio(3, 1);
			mainLayout.addComponent(grid);

		}

		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfim = new Button("Confirmar");
			btnConfim.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnConfim.addClickListener(event -> {
				try {
					ContainerCTRInfo bean = new ContainerCTRInfo();

					containerBinder.writeBean(bean);

					if (listener != null) {
						listener.confirm(bean);
					}
					
					close();
				} catch (ValidationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			buttons.addComponent(btnConfim);

			Button btnCancel = new Button("Cancelar");
			btnCancel.addStyleName(ValoTheme.BUTTON_DANGER);
			btnCancel.addClickListener(event -> close());
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		setContent(mainLayout);

		center();

		setWidth("700px");
		setHeight("700px");
		setClosable(false);
		setResizable(false);

		ContainerCTRInfo bean = new ContainerCTRInfo();
		this.containerBinder.readBean(bean);

	}

	private ComboBox<String> createComboOptions(String[] options) {

		ComboBox<String> optCombo = new ComboBox<>("O que deseja fazer?");
		optCombo.setItems(options);
		optCombo.setEmptySelectionAllowed(false);
		optCombo.setSelectedItem(options[0]);
		optCombo.setWidth("250px");
		optCombo.addSelectionListener((event -> {
		}));

		return optCombo;

	}

	private Listener listener;

	public void setListener(Listener listener) {
		this.listener = listener;
	}

	public static interface Listener {
		public void confirm(ContainerCTRInfo container);

		public void cancel();
	}
}
