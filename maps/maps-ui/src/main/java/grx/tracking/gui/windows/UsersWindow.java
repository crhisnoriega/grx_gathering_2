package grx.tracking.gui.windows;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.UserInfo;

public class UsersWindow extends Window {

	private static final long serialVersionUID = 1L;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	// private Hashtable<RoutePointInfo, OrderInfo> orders;

	private List<UserInfo> users;
	// private RouteInfo routeInfo;
	private GRXTrackingSession session;

	private SelectUser listener;

	private Grid<UserInfo> grid;

	public UsersWindow(GRXTrackingSession session) {
		super("Usuarios Cadastrados");

		this.session = session;

		{
			try {
				Hashtable<String, Object> params = new Hashtable<>();

				this.users = session.findEntities("select t from UserInfo as t", UserInfo.class, params);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		VerticalLayout mainLayout = new VerticalLayout();
		{

		}

		this.grid = new Grid<>();
		{
			grid.addColumn(user -> {
				return user.getUserId().getUser_id();
			}).setCaption("Login").setWidth(200d);

			grid.addColumn(user -> {
				return user.getName();
			}).setWidth(200d).setCaption("Nome");

			grid.addColumn(user -> {
				return Arrays.asList(user.getRoles());
			}).setWidth(100d).setCaption("Perfil");

			grid.addColumn(user -> {
				if (user.getTokenDate() != null) {
					return user.getTokenDate().toString();
				}
				return "";
			}).setWidth(100d).setCaption("Data");

			grid.addColumn(user -> {
				return user.getUserId().getSector_id();
			}).setWidth(100d).setCaption("Base");

			grid.setItems(this.users);
			grid.setSizeFull();

			mainLayout.addComponent(grid);
			mainLayout.setExpandRatio(grid, 0.2f);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Selecionar");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						Set<UserInfo> sets = grid.getSelectedItems();
						if (!sets.isEmpty()) {
							if (listener != null) {
								listener.confirm(sets.iterator().next());
							}
						}
						close();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});

			// buttons.addComponent(btnShow);
			buttons.addComponent(btnConfirm);
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		mainLayout.setSizeFull();
		setContent(mainLayout);

		setWidth("800px");
		setHeight("640px");
		setClosable(false);

	}

	private void filter() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setListener(SelectUser listener) {
		this.listener = listener;
	}

	public static interface SelectUser {
		public void confirm(UserInfo truck);
	}
}
