package grx.tracking.gui;

import static com.github.appreciated.app.layout.builder.Section.HEADER;

import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.annotation.WebServlet;

import com.github.appreciated.app.layout.AppLayout;
import com.github.appreciated.app.layout.behaviour.AppLayoutComponent;
import com.github.appreciated.app.layout.behaviour.Behaviour;
import com.github.appreciated.app.layout.builder.Section;
import com.github.appreciated.app.layout.builder.design.AppLayoutDesign;
import com.github.appreciated.app.layout.builder.elements.builders.SubmenuBuilder;
import com.github.appreciated.app.layout.builder.entities.DefaultBadgeHolder;
import com.github.appreciated.app.layout.builder.entities.DefaultBadgeHolder.BadgeListener;
import com.github.appreciated.app.layout.builder.entities.DefaultNotification;
import com.github.appreciated.app.layout.builder.entities.DefaultNotificationHolder;
import com.github.appreciated.app.layout.builder.entities.NotificationHolder.NotificationClickListener;
import com.github.appreciated.app.layout.component.MenuHeader;
import com.github.appreciated.app.layout.component.button.AppBarNotificationButton;
import com.github.appreciated.app.layout.interceptor.DefaultViewNameInterceptor;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.annotations.Widgetset;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.PushStateNavigation;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.listener.GRXUIEventListener;
import grx.tracking.core.maps.MapModel;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_STATUS;
import grx.tracking.gui.listener.GRXUIListener;
import grx.tracking.gui.samples.authentication.AccessControl;
import grx.tracking.gui.samples.authentication.BasicAccessControl;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.samples.authentication.LoginScreen;
import grx.tracking.gui.samples.authentication.LoginScreen.LoginListener;
import grx.tracking.gui.samples.authentication.ProfileScreen;
import grx.tracking.gui.samples.authentication.ProfileScreen.ProfileSelected;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.views.SectorRegisterView;
import grx.tracking.gui.views.RequestCTRView;
import grx.tracking.gui.views.CTRManagementView;
import grx.tracking.gui.views.ContainerRegisterView;
import grx.tracking.gui.views.DriverRegisterView;
import grx.tracking.gui.views.MapperView;
import grx.tracking.gui.views.OccurrencesManagementView;
import grx.tracking.gui.views.RoutesManagementView;
import grx.tracking.gui.views.TruckRegisterView;
import grx.tracking.gui.views.OrdersManagementView;
import grx.tracking.gui.windows.MyNotification;
import grx.tracking.gui.windows.WelcomeView;

/**
 * Main UI class of the application that shows either the login screen or the
 * main view of the application depending on whether a user is signed in.
 *
 * The @Viewport annotation configures the viewport meta tags appropriately on
 * mobile devices. Instead of device based scaling (default), using responsive
 * layouts.
 */
@Push
@PushStateNavigation
@Viewport("user-scalable=no,initial-scale=1.0")
@Theme("mytheme")
@Widgetset("grx.tracking.gui.MyAppWidgetset")
public class MyUI extends UI {

	private static final long serialVersionUID = 1L;

	private AccessControl accessControl = new BasicAccessControl();

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		Responsive.makeResponsive(this);
		VaadinSession.getCurrent().getSession().setMaxInactiveInterval(30 * 60);
		setLocale(vaadinRequest.getLocale());
		getPage().setTitle("GRX Gathering");

		// setContent(new SolicitacaoCTRView(this));

		/*
		 * MapModel mapModel = new MapModel(); mapModel.setObjects(new
		 * ArrayList<>()); MapperView map = new MapperView(this, mapModel);
		 * setContent(map);
		 */

		if (!accessControl.isUserSignedIn()) {
			setContent(new LoginScreen(MyUI.this, accessControl, new LoginListener() {
				@Override
				public void loginSuccessful() {
					// showMainView(Behaviour.LEFT_RESPONSIVE);
					ProfileSelected ddd = new ProfileSelected() {

						@Override
						public void select(String profile) {
							showMainView(Behaviour.LEFT_RESPONSIVE, profile);

						}
					};
					ProfileScreen profile = new ProfileScreen(MyUI.this, ddd);
					setContent(profile);
				}
			}));
		} else {
			ProfileSelected ddd = new ProfileSelected() {

				@Override
				public void select(String profile) {
					showMainView(Behaviour.LEFT_RESPONSIVE, profile);

				}
			};
			ProfileScreen profile = new ProfileScreen(MyUI.this, ddd);
			setContent(profile);
		}

	}

	private MapperView mapView;
	private RoutesManagementView routeAproval;
	private GRXUIListener stubListener;

	private SectorRegisterView baseView;

	private OrdersManagementView uploaded;
	private DefaultNotificationHolder notifHolder;

	private OccurrencesManagementView ocurrence;

	private TruckRegisterView truckers;

	private DriverRegisterView users;

	private CTRManagementView manageCTR;

	protected void showMainView(Behaviour b, String profile) {
		addStyleName(ValoTheme.UI_WITH_MENU);

		this.notifHolder = new DefaultNotificationHolder();
		this.notifHolder.addNotificationClickedListener(new NotificationClickListener<DefaultNotification>() {

			@Override
			public void onNotificationClicked(DefaultNotification newStatus) {
				System.out.println("newStatus: " + newStatus.getClass());
				if (newStatus instanceof MyNotification) {
					MyNotification mynotif = (MyNotification) newStatus;
					mynotif.setUnread(false);

					notifHolder.removeNotification(mynotif);

					getNavigator().navigateTo("ocorrences/" + mynotif.getOcurrence_id());
				}
			}
		});

		try {
			Hashtable<String, Object> params = new Hashtable<>();
			params.put("status", OCCURRENCE_STATUS.RECEIVED);
			List<OccurrenceInfo> ocurrences = EJBLocator.refGRXTrackingService().query(OccurrenceInfo.class,
					"select o from OccurrenceInfo as o where o.status = :status", params);

			ocurrences.forEach(ocurr -> {
				MyNotification notification = new MyNotification(ocurr.getType().getName(), ocurr.getDescription());
				notification.setOcurrence_id(ocurr.getId());

				LocalDateTime ldt = LocalDateTime.ofInstant(ocurr.getDate().toInstant(), ZoneId.systemDefault());

				notification.setTime1(ldt);
				notifHolder.addNotification(notification);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		DefaultBadgeHolder badge = new DefaultBadgeHolder();
		badge.setCount(2);

		badge.addListener(new BadgeListener() {

			@Override
			public void onChange(DefaultBadgeHolder newStatus) {

			}
		});

		MenuHeader menuHeader = new MenuHeader("", new ThemeResource("img/grx_logo_transparente.png"));
		// menuHeader.setHeight("400px");
		menuHeader.addStyleName("app-icon");

		GRXTrackingSession session = CurrentUser.get();

		try {
			if (!session.isAlive()) {
				VaadinSession.getCurrent().getSession().invalidate();
				Page.getCurrent().reload();
				return;
			}
		} catch (Exception e) {
			VaadinSession.getCurrent().getSession().invalidate();
			Page.getCurrent().reload();
			return;
		}

		// MapModel mapModel = null;

		/*
		 * try { Hashtable<String, Object> params = new Hashtable<>();
		 * params.put("userId", session.getUser().getUserId());
		 * params.put("date", Calendar.getInstance().getTime()); mapModel =
		 * EJBLocator.refGRXEventService().getMapModel(params);
		 * System.out.println("map model: " + mapModel.getObjects()); } catch
		 * (Exception e1) { e1.printStackTrace(); }
		 */

		this.mapView = new MapperView(this, null);
		this.routeAproval = new RoutesManagementView(this);
		this.baseView = new SectorRegisterView(this);
		this.uploaded = new OrdersManagementView(this.mapView, this);

		this.manageCTR = new CTRManagementView(mapView, this);

		this.ocurrence = new OccurrencesManagementView(this);
		this.truckers = new TruckRegisterView(this);
		this.users = new DriverRegisterView(this);

		try {
			if (this.stubListener != null) {
				session.unRegisterListener(stubListener);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			this.stubListener = new GRXUIListener(this.mapView, this.ocurrence, this.routeAproval, this.notifHolder);
			GRXUIEventListener listener = (GRXUIEventListener) UnicastRemoteObject.exportObject(stubListener, 0);
			session.registerListener(listener);
		} catch (Exception e) {
			e.printStackTrace();
		}

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////

		RequestCTRView requestCTR = new RequestCTRView(this);
		// notifications
		AppBarNotificationButton notifButton = new AppBarNotificationButton(this.notifHolder, true);

		// create main view and menu
		AppLayoutComponent layout = AppLayout.getDefaultBuilder(b).withTitle("GRX Gathering")

				.addToAppBar(notifButton).withViewNameInterceptor(new DefaultViewNameInterceptor())
				.withDefaultNavigationView(WelcomeView.class).withDesign(AppLayoutDesign.MATERIAL)
				.add(menuHeader, HEADER).add("Solicitação CTR", "requestctr", VaadinIcons.HOME, badge, requestCTR)
				.add("Envio de CTR", "managectr", VaadinIcons.HOME, badge, manageCTR)
				.add("Localizaço de Caçambas", "locations", VaadinIcons.HOME, null, this.mapView)

				.add(SubmenuBuilder.get("Cadastro", VaadinIcons.ALARM)
						.add("Caçambas", "container", VaadinIcons.TRUCK, ContainerRegisterView.class).build())

				.addClickable("Sair", VaadinIcons.SIGN_OUT, clickEvent -> {
					System.out.println("clickEvent:" + clickEvent);
					try {
						CurrentUser.get().killSession("OK");
					} catch (Exception e) {
						e.printStackTrace();
					}
					VaadinSession.getCurrent().getSession().invalidate();
					Page.getCurrent().reload();
				} , Section.FOOTER)

				.build();

		setContent(layout);
		getNavigator().navigateTo("locations");

	}

	public static MyUI get() {
		return (MyUI) UI.getCurrent();
	}

	public AccessControl getAccessControl() {
		return accessControl;
	}

	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
	public static class MyUIServlet extends VaadinServlet {
	}
}
