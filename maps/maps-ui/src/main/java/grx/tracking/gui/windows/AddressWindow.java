package grx.tracking.gui.windows;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import elemental.json.JsonObject;
import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.gui.jsmaps.MapListener;
import grx.tracking.gui.jsmaps.MyMaps;

public class AddressWindow extends Window {

	private static final long serialVersionUID = 1L;
	private AddressSelect listener;
	private MyMaps map;
	protected GeoLocation location;
	private GoogleMapsServices services;
	private String address_str;

	public AddressWindow(String address_str) {
		super("Localização");

		this.services = new GoogleMapsServices();

		this.address_str = address_str;

		try {
			this.location = this.services.geoDecoding(Address.extractAddressSimpleWithException(this.address_str));
		} catch (Exception e) {
			this.location = new GeoLocation();
			this.location.setLatitude(-23.5489);
			this.location.setLongitude(-46.6388);
		}

		setWidth("800px");
		setHeight("600px");
		setClosable(false);

		VerticalLayout verticalLayout = new VerticalLayout();
		{
			this.map = new MyMaps();
			map.setId("mymap");
			map.setSizeFull();

			map.setListener(new MapListener() {

				@Override
				public void mapLoaded() {
				}

				@Override
				public void functionCalled(String functionName, String id, JsonObject jsonObject) {
					double lat = jsonObject.getNumber("lat");
					double lng = jsonObject.getNumber("lng");

					AddressWindow.this.location = new GeoLocation();
					AddressWindow.this.location.setLatitude(lat);
					AddressWindow.this.location.setLongitude(lng);

					System.out.println("lat: " + lat + " lng:" + lng);
				}
			});

			map.addDropMarker("add_info", "", location, "");

			verticalLayout.addComponent(map);
			verticalLayout.setExpandRatio(map, 0.2f);
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Confirmar Localização");
			btnConfirm.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					if (listener != null) {
						try {
							Address address = services.reverseDecoding(location);
							listener.confirm(address, address.asAddressString(), location);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					close();
				}
			});
			buttons.addComponent(btnConfirm);

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});
			buttons.addComponent(btnCancel);
			buttons.setSizeUndefined();
			verticalLayout.addComponent(buttons);
			verticalLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);

		}

		verticalLayout.setSizeFull();
		setContent(verticalLayout);
		center();

	}

	public void setListener(AddressSelect listener) {
		this.listener = listener;
	}

	public interface AddressSelect {
		public void confirm(Address address, String address_str, GeoLocation location);
	}

}
