package grx.tracking.gui.util;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;

public class MyDialogs {

	public static void showError(String title, String message) {
		Notification notification = new Notification(title, message, Notification.Type.ERROR_MESSAGE);
		notification.setDelayMsec(2000);
		notification.show(Page.getCurrent());
	}
}
