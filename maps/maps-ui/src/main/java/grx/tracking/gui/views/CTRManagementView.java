package grx.tracking.gui.views;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.jboss.logging.Logger;
import org.vaadin.addons.searchbox.SearchBox;
import org.vaadin.addons.searchbox.event.SearchEvent;
import org.vaadin.addons.searchbox.event.SearchListener;

import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.event.selection.MultiSelectionEvent;
import com.vaadin.event.selection.MultiSelectionListener;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.SerializableSupplier;
import com.vaadin.server.Setter;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Grid.FetchItemsCallback;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.StyleGenerator;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.GridDragSource;
import com.vaadin.ui.components.grid.GridDropTarget;
import com.vaadin.ui.components.grid.GridSelectionModel;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.ComponentRenderer;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.fileimport.ExcelFileImport;
import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.CTRCreatorInfo;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.CustomerId;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyDialogs;
import grx.tracking.gui.windows.MapSelectWindow;
import grx.tracking.gui.windows.MapSelectWindow.MapSelectListener;
import grx.tracking.gui.windows.MyNotification;

public class CTRManagementView extends GridLayout implements View {

	private static final long serialVersionUID = 1L;

	private ByteArrayOutputStream uploadedFile;
	// private GridLayout orderGrid;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	public List<OrderInfo> ordersList;
	public List<OrderInfo> okOrders;
	private GRXTrackingSession session;
	private GoogleMapsServices services;

	private TextField orders_qtde;
	private OrderInfo dragged;

	private int qtde_validate_orders;
	private SectorInfo empty;
	private List<SectorInfo> sectors;
	private Hashtable<String, SectorInfo> sectorsTable;

	private SearchBox searchBox;

	private GridLayout mainList;

	public CTRManagementView(MapperView mapView, UI ui) {
		super(1, 30);
		{

			this.searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			this.searchBox.setStyleName("filter-textfield");
			this.searchBox.setPlaceholder("digite nome do usuario");
			this.searchBox.setWidth("400px");

			this.searchBox.addSearchListener(new SearchListener<String>() {

				@Override
				public void search(SearchEvent<String> event) {
					System.out.println(event.getSearchTerm());
					try {
						filter(event.getSearchTerm());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});

			GridLayout gl = new GridLayout();
			gl.addComponent(this.searchBox);
			gl.setMargin(true);

			addComponent(gl);
			setComponentAlignment(gl, Alignment.MIDDLE_RIGHT);
		}
		{
			try {
				List<CTRInfo> ctrs = CurrentUser.get().findEntities("select c from CTRInfo as c", CTRInfo.class,
						new Hashtable<>());

				this.mainList = this.createList(ctrs);

				this.addComponent(this.mainList);
				// addComponent(this.orderGrid);
				// setExpandRatio(this.orderGrid, 0.2f);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		setWidth("100%");
		setHeightUndefined();

	}

	private GridLayout createList(List<CTRInfo> ctrs) {
		System.out.println("ctrs: " + ctrs);

		GridLayout list = new GridLayout();
		list.setWidth("100%");
		list.setHeightUndefined();

		for (CTRInfo ctr : ctrs) {
			try {
				CTRCreatorInfo creator = CurrentUser.get().findEntity(CTRCreatorInfo.class, ctr.getGeradorId());
				CTRDetailsCardComponent card = new CTRDetailsCardComponent(ctr, creator);
				card.setMargin(true);
				card.show();
				card.setWidth("100%");
				list.addComponent(card);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}

	private GridLayout createGrid(List<CTRInfo> ctrs) {
		GridLayout grid = new GridLayout();

		for (CTRInfo ctr : ctrs) {
			try {
				CTRCreatorInfo creator = CurrentUser.get().findEntity(CTRCreatorInfo.class, ctr.getGeradorId());
				CTRDetailsCardComponent card = new CTRDetailsCardComponent(ctr, creator);
				card.show();
				card.setHeightUndefined();
				card.setWidth("100%");
				grid.addComponent(card);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return grid;
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

	private void filter(String query) throws Exception {
		GRXTrackingSession ejb = CurrentUser.get();

		List<CTRInfo> ctrs = new ArrayList<>();

		Hashtable<String, Object> params = new Hashtable<>();
		params.put("name", "%" + query + "%");
		params.put("cpf", "%" + query + "%");

		List<CTRCreatorInfo> creators = ejb.findEntities(
				"select g from CTRCreatorInfo as g where g.name like :name or g.cpf like :cpf", CTRCreatorInfo.class,
				params);

		creators.forEach(creator -> {
			try {
				Hashtable<String, Object> params1 = new Hashtable<>();
				params1.put("id", creator.getId());
				List<CTRInfo> tmp = ejb.findEntities("select c from CTRInfo as c where c.geradorId = :id",
						CTRInfo.class, params1);
				ctrs.addAll(tmp);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});

		this.mainList.removeAllComponents();

		this.mainList = this.createList(ctrs);

		this.addComponent(this.mainList);

	}

	private class FileReceiver implements Receiver, SucceededListener {

		private static final long serialVersionUID = 1L;

		@Override
		public OutputStream receiveUpload(String filename, String mimeType) {
			uploadedFile = new ByteArrayOutputStream();
			return uploadedFile;
		}

		@Override
		public void uploadSucceeded(SucceededEvent event) {
			if (uploadedFile != null) {
				try {

				} catch (Exception e) {
					e.printStackTrace();
					MyDialogs.showError("Processando Planilha",
							"Erro processando planilha: " + e.getLocalizedMessage());
				}
			}

		}

	}

	public ComboBox<SectorInfo> createComboSector() {

		ComboBox<SectorInfo> comboSector = new ComboBox<>();
		try {
			// sectors.add(0, this.empty);
			if (!sectors.isEmpty()) {
				empty = sectors.get(0);
			}
			comboSector.setItems(sectors);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		comboSector.setItemCaptionGenerator(SectorInfo::getName);
		comboSector.setEmptySelectionAllowed(false);

		comboSector.setWidth("300px");

		return comboSector;

	}
}
