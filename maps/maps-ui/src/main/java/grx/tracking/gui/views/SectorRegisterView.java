package grx.tracking.gui.views;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;
import org.vaadin.addons.searchbox.SearchBox;
import org.vaadin.addons.searchbox.event.SearchEvent;
import org.vaadin.addons.searchbox.event.SearchListener;

import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.gui.samples.ResetButtonForTextField;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.windows.AddressWindow;
import grx.tracking.gui.windows.SectorWindow;
import grx.tracking.gui.windows.SectorWindow.SelectSector;
import grx.tracking.gui.windows.UsersWindow;
import grx.tracking.gui.windows.AddressWindow.AddressSelect;
import grx.tracking.gui.windows.UsersWindow.SelectUser;

public class SectorRegisterView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	private Binder<SectorInfo> binder;
	private SectorInfo currentSector;
	private GRXTrackingSession session;

	private SearchBox searchBox;
	private UI ui;
	private boolean newBase = true;
	private TextField endereco;

	public SectorRegisterView(UI ui) {
		this.binder = new Binder<>();
		this.session = CurrentUser.get();
		this.currentSector = this.createEmptySector();
		this.ui = ui;
		{
			HorizontalLayout topLayout = new HorizontalLayout();

			// Label lblSearch = new Label("Procurar Caminhão");
			// lblSearch.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);

			this.searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			this.searchBox.setStyleName("filter-textfield");
			this.searchBox.setPlaceholder("nome da base");
			this.searchBox.setWidth("400px");
			// this.searchBox.addValueChangeListener(event -> filter());

			this.initSearchBox(this.searchBox);

			this.searchBox.focus();

			// topLayout.addComponent(lblSearch);
			// topLayout.setComponentAlignment(lblSearch,
			// Alignment.MIDDLE_RIGHT);

			Button mostrarTodos = new Button("Mostrar Todos");
			mostrarTodos.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					SectorWindow window = new SectorWindow(session);
					window.setModal(true);
					window.setListener(new SelectSector() {

						@Override
						public void confirm(SectorInfo sector) {
							showSelect(sector);
						}
					});
					ui.addWindow(window);
				}
			});

			topLayout.addComponent(this.searchBox);
			topLayout.setComponentAlignment(searchBox, Alignment.MIDDLE_RIGHT);

			topLayout.addComponent(mostrarTodos);
			topLayout.setComponentAlignment(mostrarTodos, Alignment.MIDDLE_RIGHT);

			topLayout.setWidth("100%");
			topLayout.setExpandRatio(searchBox, 1);
			// topLayout.setExpandRatio(lblSearch, 1);
			topLayout.setStyleName("top-bar");

			addComponent(topLayout);
		}
		{
			TextField baseName = new TextField("Nome da Base");
			baseName.setWidth("300px");
			this.binder.forField(baseName).asRequired("Nome invalido").withValidator(name -> {
				if (newBase) {
					try {
						Hashtable<String, Object> params = new Hashtable<>();
						params.put("name", name);
						List<SectorInfo> ll = session.findEntities("select t from SectorInfo as t where t.name = :name",
								SectorInfo.class, params);
						return ll == null || ll.isEmpty();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return true;
			} , "Base j� existe").bind(SectorInfo::getName, SectorInfo::setName);
			addComponent(baseName);
		}

		{
			TextArea descricao = new TextArea("Descri��o");
			descricao.setWidth("600px");
			this.binder.forField(descricao).bind(SectorInfo::getDescription, SectorInfo::setDescription);
			addComponent(descricao);
		}
		{
			this.endereco = new TextField("Endere�o (formato: Logradouro, Nro - Bairro - Cidade)");
			this.endereco.setWidth("600px");

			this.binder.forField(this.endereco).withValidator(address_str1 -> {
				try {
					Address.extractAddressSimpleWithException(address_str1);

					return true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return false;
			} , "Formato invalido").bind(sector -> {
				if (sector.getAddress() != null) {
					return sector.getAddress().asAddressString();
				} else {
					return "";
				}
			} , (sector, address_str) -> {
				try {
					Address address = Address.extractAddressSimpleWithException(address_str);
					sector.setAddress(address);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});

			addComponent(this.endereco);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnLocation = new Button("Procurar Endere�o");
			btnLocation.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					String address_str = endereco.getValue();
					AddressWindow mapSelect = new AddressWindow(address_str);
					mapSelect.setModal(true);
					mapSelect.setListener(new AddressSelect() {

						@Override
						public void confirm(Address address, String address_str, GeoLocation location) {
							endereco.setValue(address_str);
							currentSector.setAddress(address);
							currentSector.setLocation(location);
						}

					});
					ui.addWindow(mapSelect);
				}
			});

			Button btnNew = new Button("Nova Base");
			btnNew.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.readBean(createEmptySector());
					endereco.setValue("");
					newBase = true;

					currentSector = createEmptySector();
				}
			});
			btnNew.setWidth("150px");

			Button btnSave = new Button("Salvar");
			btnSave.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnSave.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					// BinderValidationStatus<SectorInfo> results =
					// binder.validate();

					try {

						binder.writeBean(currentSector);

						if (currentSector.getLocation() == null) {
							AddressWindow mapSelect = new AddressWindow(currentSector.getAddress().asAddressString());
							mapSelect.setModal(true);
							mapSelect.setListener(new AddressSelect() {

								@Override
								public void confirm(Address address, String address_str, GeoLocation location) {
									endereco.setValue(address_str);
									currentSector.setAddress(address);
									currentSector.setLocation(location);

									try {
										if (currentSector.getSectorId() == null) {
											currentSector.setSectorId("sector_" + UUID.randomUUID().toString());
										}
										session.saveEntity(currentSector.getSectorId(), currentSector);

										binder.readBean(createEmptySector());
										endereco.setValue("");
										currentSector = createEmptySector();
									} catch (Exception e) {
										e.printStackTrace();
									}

								}

							});
							ui.addWindow(mapSelect);
							return;
						}

						if (currentSector.getSectorId() == null) {
							currentSector.setSectorId("sector_" + UUID.randomUUID().toString());
						}
						session.saveEntity(currentSector.getSectorId(), currentSector);

						currentSector = createEmptySector();
						binder.readBean(currentSector);
						endereco.setValue("");

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
			btnSave.setWidth("150px");

			buttons.addComponent(btnLocation);
			buttons.addComponent(btnNew);
			buttons.addComponent(btnSave);

			buttons.setSizeUndefined();

			// setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		this.binder.readBean(this.currentSector);
	}

	private void initSearchBox(SearchBox searchBox2) {
		searchBox2.setSuggestionGenerator(new SuggestionGenerator<SectorInfo>() {

			@Override
			public List<SectorInfo> apply(String query, Integer limit) {
				List<SectorInfo> suggestions = new ArrayList<>();

				binder.readBean(createEmptySector());

				if (query.length() >= 0) {
					try {
						Hashtable<String, Object> params = new Hashtable<>();
						params.put("name", "%" + query + "%");
						List<SectorInfo> sectors = session.findEntities(
								"select u from SectorInfo as u where u.name like :name", SectorInfo.class, params);
						sectors.forEach(sector -> suggestions.add(sector));
					} catch (Exception e) {

					}
				}
				return suggestions;
			}
		}, new SuggestionValueConverter<SectorInfo>() {

			@Override
			public String apply(SectorInfo suggestion) {
				return suggestion.getName();
			}
		}, new SuggestionCaptionConverter<SectorInfo>() {

			@Override
			public String apply(SectorInfo sector, String query) {
				return sector.getName();
			}
		});

		searchBox2.addSearchListener(new SearchListener<SectorInfo>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void search(SearchEvent<SectorInfo> event) {
				SectorInfo sector = event.getSelectedItem().get();
				showSelect(sector);
			}

		});

	}

	private void filter_not_use() {
		try {
			String valueToFind = this.searchBox.getSearchField().getValue();

			if (valueToFind.isEmpty()) {
				this.binder.readBean(this.createEmptySector());
				return;
			}

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("name", "%" + valueToFind + "%");
			List<SectorInfo> sectors = session.findEntities("select u from SectorInfo as u where u.name like :name",
					SectorInfo.class, params);

			newBase = false;

			if (!sectors.isEmpty()) {
				for (SectorInfo s : sectors) {
					if (s.getName().equals(valueToFind)) {
						this.currentSector = s;
						break;
					} else {
						this.currentSector = sectors.get(0);
					}
				}
				this.endereco.setValue(this.currentSector.getAddress().asAddressString());
				this.binder.readBean(this.currentSector);
			} else {
				this.binder.readBean(this.createEmptySector());
				this.endereco.setValue("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private SectorInfo createEmptySector() {
		SectorInfo sector = new SectorInfo();
		return sector;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		View.super.enter(event);
	}

	private void showSelect(SectorInfo sector) {
		try {
			currentSector = sector;
			binder.readBean(currentSector);

			newBase = false;
		} catch (Exception e) {
			e.printStackTrace();
			binder.readBean(createEmptySector());
		}

		UI.getCurrent().access(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				searchBox.getSearchField().setValue("");
			}
		});
	}
}
