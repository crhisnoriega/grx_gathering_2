package grx.tracking.gui.views;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;
import org.vaadin.addons.searchbox.SearchBox;
import org.vaadin.addons.searchbox.event.SearchEvent;
import org.vaadin.addons.searchbox.event.SearchListener;
import org.vaadin.inputmask.InputMask;

import com.vaadin.data.Binder;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.UserError;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.CTRCreatorInfo;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.CTRStatus;
import grx.tracking.core.persistence.CTRStatus.CTR_STATUS;
import grx.tracking.core.persistence.ContainerCTRInfo;
import grx.tracking.core.persistence.ContainerType;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyValidator;
import grx.tracking.gui.windows.TrashCapacitiyWindow;

public class RequestCTRView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	private SearchBox searchBox;

	private GRXTrackingSession session;

	private Binder<CTRCreatorInfo> creatorBinder;
	private Binder<Address> creatorAdrBinder;
	private Binder<Address> entregaAdrBinder;

	private TextField username;

	private UI ui;

	private boolean newUser = true;
	private boolean firstTime = true;

	private CTRCreatorInfo mainBean;
	private ContainerCTRInfo currentContainer;

	private RadioButtonGroup<String> single;

	public RequestCTRView(UI ui) {

		this.ui = ui;
		this.session = CurrentUser.get();

		this.creatorBinder = new Binder<>();
		this.creatorAdrBinder = new Binder<>();
		this.entregaAdrBinder = new Binder<>();

		{
			HorizontalLayout topLayout = new HorizontalLayout();

			// Label lblSearch = new Label("Procurar Usuario");
			// lblSearch.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);

			this.searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			this.searchBox.setStyleName("filter-textfield");
			this.searchBox.setPlaceholder("digite nome do usuario");
			this.searchBox.setWidth("400px");
			// this.searchBox.addValueChangeListener(event -> filter());

			this.initSearchBox(this.searchBox);

			this.searchBox.focus();

			// topLayout.addComponent(lblSearch);
			// topLayout.setComponentAlignment(lblSearch,
			// Alignment.MIDDLE_RIGHT);

			topLayout.addComponent(this.searchBox);
			topLayout.setComponentAlignment(searchBox, Alignment.MIDDLE_RIGHT);

			topLayout.setWidth("100%");
			topLayout.setExpandRatio(searchBox, 1);
			// topLayout.setExpandRatio(lblSearch, 1);
			topLayout.setStyleName("top-bar");

			addComponent(topLayout);
		}

		{
			Component card1 = createDadosGerador();
			card1.setWidth("100%");
			addComponent(card1);
		}

		{
			Component card1 = createEnderecoObra();
			card1.setWidth("100%");
			addComponent(card1);

		}

		{
			Component card1 = createDadosCacamba();
			card1.setWidth("100%");
			addComponent(card1);
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();
			{
				Button btnSave = new Button("Salvar Solicitação CTR");
				btnSave.addStyleName(ValoTheme.BUTTON_PRIMARY);
				btnSave.addClickListener(new ClickListener() {

					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							doSave();
						} catch (Exception e) {
							e.printStackTrace();
							Notification.show("error: " + e.getLocalizedMessage(), Type.ERROR_MESSAGE);
						}
					}
				});
				btnSave.setWidth("200px");
				buttons.addComponent(btnSave);

			}

			{
				Button btnSave = new Button("Salvar CTR e Enviar Caçamba à Obra");
				btnSave.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnSave.addClickListener(new ClickListener() {

					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							doSave();
						} catch (Exception e) {
							e.printStackTrace();
							Notification.show("error: " + e.getLocalizedMessage(), Type.ERROR_MESSAGE);
						}
					}
				});
				btnSave.setWidth("300px");
				buttons.addComponent(btnSave);

			}

			{
				Button btnCancel = new Button("Cancelar");
				btnCancel.addStyleName(ValoTheme.BUTTON_DANGER);
				btnCancel.addClickListener(new ClickListener() {

					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {

					}
				});
				btnCancel.setWidth("150px");
				buttons.addComponent(btnCancel);

			}

			buttons.setSizeUndefined();

			// setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		{
			this.init();
		}
		// this.binder.readBean(this.curretUser);

	}

	private void init() {
		this.firstTime = true;

		this.mainBean = new CTRCreatorInfo();
		this.mainBean.setAddress(new Address());

		this.creatorBinder.readBean(this.mainBean);

		this.creatorAdrBinder.readBean(new Address());

		Address address = new Address();
		this.entregaAdrBinder.readBean(address);

		if (this.single != null) {
			this.single.setSelectedItem(null);
		}

	}

	public Component createDadosGerador() {
		GridLayout grid = new GridLayout();
		grid.addStyleName("my-card");

		Label title = new Label(VaadinIcons.USER.getHtml() + " Dados do Gerador", ContentMode.HTML);
		title.addStyleName(ValoTheme.LABEL_LARGE);
		title.addStyleName(ValoTheme.LABEL_BOLD);
		title.setHeight("40px");

		grid.addComponent(title);

		{
			HorizontalLayout row1 = new HorizontalLayout();

			{
				TextField tfCpf = new TextField("CNPJ/CPF");

				{
					InputMask cpfmask = new InputMask("99999999999999");
					cpfmask.setAutoUnmask(true);
					cpfmask.setPlaceholder(" ");
					cpfmask.extend(tfCpf);
				}

				this.creatorBinder.forField(tfCpf).asRequired("CNPJ/CPF obrigatorio").withValidator(value -> {
					return value.length() == 11 || value.length() == 14;
				} , "CNPJ/CPF inválido").withValidationStatusHandler(statusChange -> {
					if (statusChange.isError()) {
						// emailStatus.setValue(statusChange.getMessage().orElse("error"));
						tfCpf.setComponentError(new UserError(statusChange.getMessage().orElse("error")));
					} else {
						tfCpf.setComponentError(null);
						// emailStatus.setValue("");
					}

				}).bind(CTRCreatorInfo::getCpf, CTRCreatorInfo::setCpf);

				tfCpf.addValueChangeListener(new ValueChangeListener<String>() {

					@Override
					public void valueChange(ValueChangeEvent<String> event) {
						String cpf = event.getValue();
						if (cpf.length() == 11 || cpf.length() == 14) {
							try {
								CTRCreatorInfo ger = CurrentUser.get().findEntity(CTRCreatorInfo.class, cpf);
								if (ger != null) {
									creatorBinder.readBean(ger);
									creatorAdrBinder.readBean(ger.getAddress());
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
				});
				row1.addComponent(tfCpf);
			}
			{

				TextField tfNome = new TextField("Nome");
				tfNome.addStyleName("textfield-large");

				this.creatorBinder.forField(tfNome).asRequired().bind(CTRCreatorInfo::getName, CTRCreatorInfo::setName);

				row1.addComponent(tfNome);

			}

			{
				DateField dfBorn = new DateField("Data Nascimento");

				this.creatorBinder.bind(dfBorn, gerador -> {
					return LocalDate.now();
				} , (gerador, value) -> {

				});

				row1.addComponent(dfBorn);

			}

			grid.addComponent(row1);
		}

		{
			HorizontalLayout row2 = new HorizontalLayout();
			{
				TextField tfCep = new TextField("CEP");
				tfCep.addStyleName("textfield-short");

				InputMask cepmask = new InputMask("99999-999");
				cepmask.setAutoUnmask(true);
				cepmask.setPlaceholder(" ");
				cepmask.extend(tfCep);

				tfCep.addValueChangeListener(new ValueChangeListener<String>() {

					@Override
					public void valueChange(ValueChangeEvent<String> event) {
						String cep = event.getValue();

						if (cep.length() >= 8) {

							try {
								Address address = EJBLocator.refGRXTrackingService().findAddressForCEP(cep);
								creatorAdrBinder.readBean(address);

								// geradorBinder.readBean(bean1);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});

				this.creatorAdrBinder.forField(tfCep).asRequired().bind(Address::getCEP, Address::setCEP);

				row2.addComponent(tfCep);
			}

			{
				TextField tfLgr = new TextField("Logradouro");
				tfLgr.addStyleName("textfield-large");

				this.creatorAdrBinder.forField(tfLgr).asRequired().bind(Address::getEndereco, Address::setEndereco);

				row2.addComponent(tfLgr);
			}

			{
				TextField tfNro = new TextField("Número");
				tfNro.addStyleName("textfield-short");

				this.creatorAdrBinder.forField(tfNro).asRequired().bind(Address::getNro, Address::setNro);

				row2.addComponent(tfNro);
			}
			{
				TextField tfCompl = new TextField("Complemento");
				tfCompl.addStyleName("textfield-short");

				this.creatorAdrBinder.forField(tfCompl).asRequired().bind(Address::getXCpl, Address::setXCpl);

				row2.addComponent(tfCompl);
			}

			grid.addComponent(row2);
		}

		{
			HorizontalLayout row3 = new HorizontalLayout();
			{
				TextField tfBairro = new TextField("Bairro");
				tfBairro.addStyleName("textfield-large");

				this.creatorAdrBinder.forField(tfBairro).asRequired().bind(Address::getXBairro, Address::setXBairro);

				row3.addComponent(tfBairro);
			}

			{
				TextField tfCidade = new TextField("Cidade");
				tfCidade.addStyleName("textfield-large");

				this.creatorAdrBinder.forField(tfCidade).asRequired().bind(Address::getCidade, Address::setCidade);

				row3.addComponent(tfCidade);
			}

			{
				TextField tfUF = new TextField("UF");
				tfUF.addStyleName("textfield-short");

				InputMask m = new InputMask("AA");
				m.setAutoUnmask(true);
				m.setPlaceholder(" ");
				m.extend(tfUF);

				this.creatorAdrBinder.forField(tfUF).asRequired().bind(Address::getUF, Address::setUF);

				row3.addComponent(tfUF);
			}

			grid.addComponent(row3);
		}

		{
			HorizontalLayout row4 = new HorizontalLayout();
			{
				TextField tfFone1 = new TextField("Telefone 1");

				InputMask fonemask = new InputMask("(99) 9999-9999");
				fonemask.setAutoUnmask(true);
				fonemask.setPlaceholder(" ");
				fonemask.extend(tfFone1);

				this.creatorBinder.forField(tfFone1).asRequired().bind(CTRCreatorInfo::getFone1,
						CTRCreatorInfo::setFone1);

				row4.addComponent(tfFone1);
			}

			{
				TextField tfFone1 = new TextField("Contato");

				this.creatorBinder.forField(tfFone1).asRequired().bind(CTRCreatorInfo::getContato,
						CTRCreatorInfo::setContato);

				row4.addComponent(tfFone1);
			}

			{
				TextField tfFone1 = new TextField("Telefone 2");

				InputMask fonemask = new InputMask("(99) 9999-9999");
				fonemask.setAutoUnmask(true);
				fonemask.setPlaceholder(" ");
				fonemask.extend(tfFone1);

				this.creatorBinder.forField(tfFone1).bind(CTRCreatorInfo::getFone2, CTRCreatorInfo::setFone2);

				row4.addComponent(tfFone1);
			}

			{
				TextField tfFone1 = new TextField("Celular");

				InputMask fonemask = new InputMask("(99) 99999-9999");
				fonemask.setAutoUnmask(true);
				fonemask.setPlaceholder(" ");
				fonemask.extend(tfFone1);

				this.creatorBinder.forField(tfFone1).bind(CTRCreatorInfo::getFone_celular,
						CTRCreatorInfo::setFone_celular);

				row4.addComponent(tfFone1);
			}

			grid.addComponent(row4);
		}

		{
			HorizontalLayout row5 = new HorizontalLayout();
			{
				TextField tfFone1 = new TextField("Email");
				tfFone1.addStyleName("textfield-large");

				this.creatorBinder.forField(tfFone1).withValidator(new EmailValidator("email invalido")).asRequired()
						.bind(CTRCreatorInfo::getEmail, CTRCreatorInfo::setEmail);

				row5.addComponent(tfFone1);
			}

			grid.addComponent(row5);
		}
		return grid;
	}

	private Component createEnderecoObra() {
		GridLayout grid = new GridLayout();
		grid.addStyleName("my-card");

		Label title = new Label(VaadinIcons.BUILDING.getHtml() + " Endereço da Obra", ContentMode.HTML);

		title.addStyleName(ValoTheme.LABEL_LARGE);
		title.addStyleName(ValoTheme.LABEL_BOLD);
		title.setHeight("40px");

		grid.addComponent(title);
		{
			HorizontalLayout row2 = new HorizontalLayout();
			this.single = new RadioButtonGroup<>("Utilizar o mesmo endereço do cadastro?");
			this.single.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			this.single.setItems("Sim", "Não");

			this.single.addSelectionListener(new SingleSelectionListener<String>() {

				@Override
				public void selectionChange(SingleSelectionEvent<String> event) {
					if ("Sim".equals(event.getValue())) {
						try {
							Address address = new Address();
							creatorAdrBinder.writeBean(address);
							entregaAdrBinder.readBean(address);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
						try {
							entregaAdrBinder.readBean(new Address());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

				}
			});

			row2.addComponent(single);
			grid.addComponent(row2);
		}

		{
			HorizontalLayout row2 = new HorizontalLayout();
			{
				TextField tfCep = new TextField("CEP");
				tfCep.addStyleName("textfield-short");

				InputMask cepmask = new InputMask("99999-999");
				cepmask.setAutoUnmask(true);
				cepmask.setPlaceholder(" ");
				cepmask.extend(tfCep);

				this.entregaAdrBinder.forField(tfCep).asRequired().bind(Address::getCEP, Address::setCEP);

				tfCep.addValueChangeListener(new ValueChangeListener<String>() {

					@Override
					public void valueChange(ValueChangeEvent<String> event) {
						String cep = event.getValue();

						if (cep.length() >= 8) {

							try {
								Address address = EJBLocator.refGRXTrackingService().findAddressForCEP(cep);
								entregaAdrBinder.readBean(address);
								// geradorBinder.readBean(bean1);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});

				row2.addComponent(tfCep);
			}

			{
				TextField tfLgr = new TextField("Logradouro");
				tfLgr.addStyleName("textfield-large");

				this.entregaAdrBinder.forField(tfLgr).asRequired().bind(Address::getEndereco, Address::setEndereco);

				row2.addComponent(tfLgr);
			}

			{
				TextField tfNro = new TextField("Número");
				tfNro.addStyleName("textfield-short");

				this.entregaAdrBinder.forField(tfNro).asRequired().bind(Address::getNro, Address::setNro);

				row2.addComponent(tfNro);
			}
			{
				TextField tfCompl = new TextField("Complemento");
				tfCompl.addStyleName("textfield-short");

				this.entregaAdrBinder.forField(tfCompl).asRequired().bind(Address::getXCpl, Address::setXCpl);

				row2.addComponent(tfCompl);
			}

			grid.addComponent(row2);
		}

		{
			HorizontalLayout row3 = new HorizontalLayout();
			{
				TextField tfBairro = new TextField("Bairro");
				tfBairro.addStyleName("textfield-large");

				this.entregaAdrBinder.forField(tfBairro).asRequired().bind(Address::getXBairro, Address::setXBairro);

				row3.addComponent(tfBairro);
			}

			{
				TextField tfCidade = new TextField("Cidade");
				tfCidade.addStyleName("textfield-large");

				this.entregaAdrBinder.forField(tfCidade).asRequired().bind(Address::getCidade, Address::setCidade);

				row3.addComponent(tfCidade);
			}

			{
				TextField tfUF = new TextField("UF");
				tfUF.addStyleName("textfield-short");

				this.entregaAdrBinder.forField(tfUF).asRequired().bind(Address::getUF, Address::setUF);

				row3.addComponent(tfUF);
			}

			grid.addComponent(row3);
		}

		return grid;
	}

	private Component createDadosCacamba() {
		GridLayout grid = new GridLayout();
		grid.addStyleName("my-card");

		Label title = new Label(VaadinIcons.CAR.getHtml() + " Dados da Caçamba", ContentMode.HTML);

		title.addStyleName(ValoTheme.LABEL_LARGE);
		title.addStyleName(ValoTheme.LABEL_BOLD);
		title.setHeight("40px");

		grid.addComponent(title);

		{
			ComboBox<ContainerType> comboContainer = new ComboBox<>("Tipo da Caçamba");
			comboContainer.setItems(ContainerType.values());
			comboContainer.setItemCaptionGenerator(ContainerType::getName);
			comboContainer.setEmptySelectionAllowed(false);
			this.creatorBinder.forField(comboContainer).bind(gerador -> ContainerType.CONTAINER_1, (gerador, type) -> {
			});

			comboContainer.addSelectionListener(event -> {
				if (!firstTime) {
					TrashCapacitiyWindow window = new TrashCapacitiyWindow();
					window.setModal(true);
					window.setListener(new TrashCapacitiyWindow.Listener() {

						@Override
						public void confirm(ContainerCTRInfo container) {
							RequestCTRView.this.currentContainer = container;
						}

						@Override
						public void cancel() {
							RequestCTRView.this.currentContainer = null;
						}
					});
					ui.addWindow(window);
				}

				firstTime = false;
			});

			comboContainer.addStyleName("textfield-large");
			grid.addComponent(comboContainer);
		}

		return grid;
	}

	private void initSearchBox(SearchBox searchBox2) {
		searchBox2.setSuggestionGenerator(new SuggestionGenerator<UserInfo>() {

			@Override
			public List<UserInfo> apply(String query, Integer limit) {
				List<UserInfo> suggestions = new ArrayList<>();

				return suggestions;
			}
		}, new SuggestionValueConverter<UserInfo>() {

			@Override
			public String apply(UserInfo suggestion) {
				return suggestion.getUserId().getUser_id();
			}
		}, new SuggestionCaptionConverter<UserInfo>() {

			@Override
			public String apply(UserInfo user, String query) {
				return user.getUserId().getUser_id() + " - " + user.getName();
			}
		});

		searchBox2.addSearchListener(new SearchListener<UserInfo>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void search(SearchEvent<UserInfo> event) {
				UserInfo user = event.getSelectedItem().get();
				// showSelect(user);
			}

		});

	}

	@Override
	public void enter(ViewChangeEvent event) {
		View.super.enter(event);
	}

	public void doSave() throws Exception {
		GRXTrackingService service = EJBLocator.refGRXTrackingService();

		try {
			Address creatorAdr = new Address();
			creatorAdrBinder.writeBean(creatorAdr);

			// save ger

			CTRCreatorInfo creator = new CTRCreatorInfo();
			creator.setAddress(new Address());
			creatorBinder.writeBean(creator);

			creator.setAddress(creatorAdr);
			creator.setId(creator.getCpf());

			CurrentUser.get().saveEntity(creator.getCpf(), creator);

			// entrega
			Address entregaAdr = new Address();
			entregaAdrBinder.writeBean(entregaAdr);

			CTRInfo info = new CTRInfo();
			{
				info.setId("ctr_" + UUID.randomUUID().toString());
				info.setGeradorId(creator.getId());
				info.setEntrega(entregaAdr);
				info.setContainer(currentContainer);
			}
			{
				CTRStatus status = new CTRStatus();
				status.setDate(Calendar.getInstance().getTime());
				status.setStatus(CTR_STATUS.NEW);
				info.setStatus(status);
			}
			{
				GoogleMapsServices services = new GoogleMapsServices();
				GeoLocation location = services.geoDecoding(info.getEntrega());
				info.setLocation(location);
			}
			{
				MapObject mapobj = new MapObject();
				mapobj.setLocation(info.getLocation());
			}

			CurrentUser.get().saveEntity(info.getId(), info);

			Notification.show("CTR enviado com sucesso", Type.HUMANIZED_MESSAGE);

			this.init();

		} catch (Exception e) {
			e.printStackTrace();
			Notification.show("Erro enviado CTR", e.getLocalizedMessage(), Type.ERROR_MESSAGE);
		}
	}
}
