package grx.tracking.gui.windows;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class WelcomeView extends VerticalLayout implements View{

	
	public WelcomeView() {
		GridLayout grid = new GridLayout();
		
		grid.addStyleName("my-card");
		grid.setWidth("100%");
		grid.setHeight("20%");
		
		grid.addComponent(new Label("Bem-vindo"));
		
		addComponent(grid);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		View.super.enter(event);
	}
}
