package grx.tracking.gui.samples.authentication;

import java.io.Serializable;

import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.gui.jsmaps.MyMaps;
import grx.tracking.gui.windows.MapSelectWindow;

public class ProfileScreen extends CssLayout {

	private static final long serialVersionUID = 1L;

	private UI ui;
	private ProfileSelected listener;

	public ProfileScreen(UI ui, ProfileSelected listener) {
		this.ui = ui;
		this.listener = listener;
		buildUI();

	}

	private void buildUI() {
		addStyleName("login-screen");

		// login form, centered in the available part of the screen
		Component transportador = buildLoginForm("Transportador");
		Component coletor = buildLoginForm("Controlador");

		// layout to center login form when there is sufficient screen space
		// - see the theme for how this is made responsive for various screen
		// sizes
		VerticalLayout centeringLayout = new VerticalLayout();
		centeringLayout.setMargin(false);
		centeringLayout.setSpacing(false);
		centeringLayout.setStyleName("centering-layout");

		HorizontalLayout modules = new HorizontalLayout();
		modules.addComponent(transportador);
		modules.addComponent(coletor);

		centeringLayout.addComponent(modules);

		centeringLayout.setComponentAlignment(modules, Alignment.MIDDLE_CENTER);

		Image image = new Image("", new ThemeResource("img/grx_logo_transparente.png"));
		image.setWidth("200px");
		// image.setHeight("200px");

		centeringLayout.addComponent(image);
		centeringLayout.setComponentAlignment(image, Alignment.TOP_CENTER);

		// information text about logging in
		// CssLayout loginInformation = buildLoginInformation();

		addComponent(centeringLayout);
		// addComponent(loginInformation);

	}

	private Component buildLoginForm(String perfil) {
		VerticalLayout loginForm = new VerticalLayout();

		loginForm.addStyleName("login-form");
		loginForm.setHeight("200px");
		loginForm.setSizeUndefined();
		// loginForm.setMargin(false);

		{
			if ("Transportador".equals(perfil)) {
				Image image = new Image("", new ThemeResource("img/delivery-truck.png"));
				image.setWidth("100px");

				loginForm.addComponent(image);
				loginForm.setComponentAlignment(image, Alignment.BOTTOM_CENTER);
			} else {

				Image image = new Image("", new ThemeResource("img/container.png"));
				image.setWidth("100px");

				loginForm.addComponent(image);
				loginForm.setComponentAlignment(image, Alignment.BOTTOM_CENTER);
			}
		}

		CssLayout buttons = new CssLayout();
		buttons.setStyleName("buttons");
		loginForm.addComponent(buttons);
		loginForm.setComponentAlignment(buttons, Alignment.BOTTOM_CENTER);

		Button login;
		buttons.addComponent(login = new Button(perfil));
		login.setDisableOnClick(true);
		login.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				listener.select(perfil);
			}
		});
		login.setWidth("150px");

		login.addStyleName(ValoTheme.BUTTON_FRIENDLY);

		return loginForm;
	}

	private CssLayout buildLoginInformation() {
		CssLayout loginInformation = new CssLayout();

		Image image = new Image("", new ThemeResource("img/grx_image.jpg"));
		image.setWidth("100%");
		loginInformation.addComponent(image);

		loginInformation.setStyleName("login-information");

		Label loginInfoText = new Label("<h1>GRX Tracking</h1>" + "Sistema de rastreamento e designação de tarefas",
				ContentMode.HTML);
		loginInfoText.setSizeFull();
		loginInformation.addComponent(loginInfoText);

		return loginInformation;
	}

	private void login() {

	}

	private void showNotification(Notification notification) {
		// keep the notification visible a little while after moving the
		// mouse, or until clicked
		notification.setDelayMsec(2000);
		notification.show(Page.getCurrent());
	}

	public interface LoginListener extends Serializable {
		void loginSuccessful();
	}

	public static interface ProfileSelected {
		public void select(String profile);
	}
}
