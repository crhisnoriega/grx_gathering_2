package grx.tracking.gui.jsmaps;

import elemental.json.JsonObject;

public interface MapListener {

	public void mapLoaded();

	public void functionCalled(String functionName, String id, JsonObject jsonObject);

}
