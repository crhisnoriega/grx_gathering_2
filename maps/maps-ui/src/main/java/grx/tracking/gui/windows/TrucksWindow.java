package grx.tracking.gui.windows;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;
import org.vaadin.addons.searchbox.SearchBox;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.GridDragSource;
import com.vaadin.ui.components.grid.GridDropTarget;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.util.MyConvertArray;

public class TrucksWindow extends Window {

	private static final long serialVersionUID = 1L;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	// private Hashtable<RoutePointInfo, OrderInfo> orders;

	private List<TruckInfo> trucks;
	// private RouteInfo routeInfo;
	private GRXTrackingSession session;

	private SelectTruck listener;
	private RoutePointInfo dragged;
	private Grid<TruckInfo> grid;

	public TrucksWindow(GRXTrackingSession session) {
		super("Caminhoes Cadastrados");

		this.session = session;

		{
			try {
				Hashtable<String, Object> params = new Hashtable<>();

				this.trucks = session.findEntities("select t from TruckInfo as t",
						TruckInfo.class, params);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		VerticalLayout mainLayout = new VerticalLayout();
		{

			SearchBox searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			searchBox.setCaption("Adicionar Pedido");
			searchBox.setPlaceholder("digite o numero do pedido");
			searchBox.setWidth("640px");
			searchBox.setSuggestionGenerator(new SuggestionGenerator<OrderInfo>() {

				@Override
				public List<OrderInfo> apply(String query, Integer limit) {
					List<OrderInfo> suggestions = new ArrayList<>();

					if (query.length() >= 3) {
						try {
							Hashtable<String, Object> params = new Hashtable<>();
							params.put("order", "%" + query + "%");
							List<OrderInfo> orders = session.findEntities(
									"select o from OrderInfo as o where o.orderId.order_id like :order",
									OrderInfo.class, params);
							orders.forEach(order -> suggestions.add(order));
						} catch (Exception e) {

						}
					}
					return suggestions;
				}
			}, new SuggestionValueConverter<OrderInfo>() {

				@Override
				public String apply(OrderInfo suggestion) {
					return suggestion.getOrderId().getOrder_id();
				}
			}, new SuggestionCaptionConverter<OrderInfo>() {

				@Override
				public String apply(OrderInfo order, String query) {
					return order.getOrderId().getOrder_id() + " - " + order.getAddress().getAddress().asAddressString();
				}
			});

			searchBox.focus();
			// mainLayout.addComponent(searchBox);
		}

		this.grid = new Grid<>();
		{

			grid.addColumn(truck -> {
				return truck.getPlaca();
			}).setCaption("Placa").setWidth(200d);

			grid.addColumn(truck -> {
				return truck.getTruckDescription();
			}).setWidth(200d).setCaption("Descri��o");

			grid.addColumn(truck -> {
				Hashtable<ORDER_TYPE, Integer> dimen1 = truck.getDimensions();
				if (dimen1 != null) {
					return dimen1.get(ORDER_TYPE.DELIVERY) + "";
				}
				return null;
			}).setWidth(100d).setCaption("Entrega");

			grid.addColumn(truck -> {
				Hashtable<ORDER_TYPE, Integer> dimen1 = truck.getDimensions();
				if (dimen1 != null) {
					return dimen1.get(ORDER_TYPE.PICK) + "";
				}
				return null;
			}).setWidth(100d).setCaption("Retirada");

			grid.setItems(this.trucks);
			grid.setSizeFull();

			mainLayout.addComponent(grid);
			mainLayout.setExpandRatio(grid, 0.2f);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Selecionar");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						Set<TruckInfo> sets = grid.getSelectedItems();
						if (!sets.isEmpty()) {
							if (listener != null) {
								listener.confirm(sets.iterator().next());
							}
						}
						close();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});

			// buttons.addComponent(btnShow);
			buttons.addComponent(btnConfirm);
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}
		

		mainLayout.setSizeFull();
		setContent(mainLayout);

		setWidth("800px");
		setHeight("640px");
		setClosable(false);

	}

	private void filter() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setListener(SelectTruck listener) {
		this.listener = listener;
	}

	public static interface SelectTruck {
		public void confirm(TruckInfo truck);
	}
}
