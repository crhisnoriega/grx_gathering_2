package grx.tracking.gui.windows;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_TYPE;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;

public class ResolveOccurrenceWindow extends Window {

	private static final long serialVersionUID = 1L;
	private String option_selected;

	public ResolveOccurrenceWindow(OccurrenceInfo ocurr, GRXTrackingSession session) {
		super("Resolver Ocorr�ncia");

		VerticalLayout mainLayout = new VerticalLayout();
		OCCURRENCE_TYPE type = ocurr.getType();

		{
			TextField label = new TextField("Ocorr�ncia");
			label.setValue(type.getName());
			label.setWidth("250px");

			mainLayout.addComponent(label);
		}
		{
			TextField usuario = new TextField("Usuario");
			usuario.setValue(ocurr.getUserId().getUser_id());
			usuario.setWidth("250px");

			mainLayout.addComponent(usuario);
		}
		{

			if (OCCURRENCE_TYPE.TRUCK_BREAK.equals(type)) {
				{
					TextField truckfield = new TextField("Placa do Caminhao");
					truckfield.setWidth("250px");
					try {
						TruckInfo truck = session.findEntity(TruckInfo.class, ocurr.getTruckId());
						truckfield.setValue(truck.getPlaca());
					} catch (Exception e) {
						e.printStackTrace();
						truckfield.setValue("Caminh�o n�o encontrado");
					}
					mainLayout.addComponent(truckfield);

				}
				{
					String[] options = new String[] { "Selecionar", "Cancelar Rota" };
					ComboBox<String> combo = this.createComboOptions(options);
					combo.addSelectionListener(event -> {
						option_selected = event.getValue();
					});
					mainLayout.addComponent(combo);
				}
			}

			if (OCCURRENCE_TYPE.ORDER_PROBLEM.equals(type)) {
				{
					TextField orderfield = new TextField("Numero do Pedido");
					try {
						OrderInfo order = session.findEntity(OrderInfo.class, ocurr.getOrderId());
						orderfield.setValue(order.getOrderId().getOrder_id() + " - "
								+ order.getAddress().getAddress().asAddressString());
					} catch (Exception e) {
						e.printStackTrace();
					}
					mainLayout.addComponent(orderfield);

				}
				{
					String[] options = new String[] { "Selecionar", "Cancelar Rota", "Cancelar Pedido", };
					ComboBox<String> combo = this.createComboOptions(options);
					combo.addSelectionListener(event -> {
						option_selected = event.getValue();
					});
					mainLayout.addComponent(combo);
				}
			}
		}
		{
			TextArea description = new TextArea("Descri��o");
			description.setValue(ocurr.getDescription());
			description.setSizeFull();
			mainLayout.addComponent(description);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfim = new Button("Confirmar");
			btnConfim.addClickListener(event -> {

				if ("Cancelar Rota".equals(this.option_selected)) {
					String route_id = ocurr.getRoute_id();
					UserId userId = ocurr.getUserId();
					if (listener != null) {
						listener.cancelRoute(route_id, userId);
					}
				}

				if ("Cancelar Pedido".equals(this.option_selected)) {
					OrderId orderId = ocurr.getOrderId();
					String route_id = ocurr.getRoute_id();
					UserId userId = ocurr.getUserId();
					if (listener != null) {
						listener.cancelOrder(orderId, route_id, userId);
					}
				}
			});
			buttons.addComponent(btnConfim);

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(event -> close());
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		setContent(mainLayout);

		setWidth("600px");
		setHeight("600px");
		setClosable(false);
		setResizable(false);
	}

	private ComboBox<String> createComboOptions(String[] options) {

		ComboBox<String> optCombo = new ComboBox<>("O que deseja fazer?");
		optCombo.setItems(options);
		optCombo.setEmptySelectionAllowed(false);
		optCombo.setSelectedItem(options[0]);
		optCombo.setWidth("250px");
		optCombo.addSelectionListener((event -> {
		}));

		return optCombo;

	}

	private OptionListener listener;

	public void setListener(OptionListener listener) {
		this.listener = listener;
	}

	public static interface OptionListener {
		public void cancelRoute(String route_id, UserId userId);

		public void cancelOrder(OrderId orderId, String route_id, UserId userId);
	}
}
