 package grx.tracking.gui.views;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;
import org.vaadin.addons.searchbox.SearchBox;
import org.vaadin.addons.searchbox.event.SearchEvent;
import org.vaadin.addons.searchbox.event.SearchListener;

import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.UserTruckMap;
import grx.tracking.gui.samples.ResetButtonForTextField;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.windows.TrucksWindow;
import grx.tracking.gui.windows.TrucksWindow.SelectTruck;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.TruckInfo.TRUCK_TYPE;

public class TruckRegisterView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	private Binder<TruckInfo> binder;
	private TruckInfo currentTruck;
	private GRXTrackingSession session;
	private List<UserId> usersIds;
	private int positionToInsert = 0;

	private SearchBox searchBox;
	private Grid<UserId> gridUsers;
	private boolean newTruck = true;

	private TruckId old_truck_id;
	protected List<UserTruckMap> currentMaps;

	public TruckRegisterView(UI ui) {
		this.binder = new Binder<>();
		this.session = CurrentUser.get();
		this.usersIds = new ArrayList<>();
		this.currentMaps = new ArrayList<>();
		this.currentTruck = this.createEmptyTruck();
		{
			HorizontalLayout topLayout = new HorizontalLayout();

			// Label lblSearch = new Label("Procurar Caminhão");
			// lblSearch.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);

			this.searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			this.searchBox.setStyleName("filter-textfield");
			this.searchBox.setPlaceholder("digite a placa");
			this.searchBox.setWidth("400px");
			// this.searchBox.addValueChangeListener(event -> filter());

			this.initSearchBox(this.searchBox);

			this.searchBox.focus();
			// topLayout.addComponent(lblSearch);
			// topLayout.setComponentAlignment(lblSearch,
			// Alignment.MIDDLE_RIGHT);

			topLayout.addComponent(this.searchBox);
			
			Button mostrarTodos = new Button("Mostrar Todos");
			mostrarTodos.addClickListener(new ClickListener() {
				
				@Override
				public void buttonClick(ClickEvent event) {
					TrucksWindow w = new TrucksWindow(session);
					w.setModal(true);
					w.setListener(new SelectTruck() {
						
						@Override
						public void confirm(TruckInfo truck) {

							showSelect(truck);
						
							
						}

						
					});
					ui.addWindow(w);
				}
			});
			
			topLayout.addComponent(mostrarTodos);
			
			topLayout.setComponentAlignment(mostrarTodos, Alignment.MIDDLE_RIGHT);
			topLayout.setComponentAlignment(searchBox, Alignment.MIDDLE_RIGHT);

			topLayout.setWidth("100%");
			topLayout.setExpandRatio(searchBox, 1);
			// topLayout.setExpandRatio(lblSearch, 1);
			topLayout.setStyleName("top-bar");

			addComponent(topLayout);
		}
		{
			TextField placa = new TextField("Placa do Veiculo");
			placa.setWidth("300px");
			this.binder.forField(placa).asRequired("Placa invalida").withValidator(placa1 -> {
				if (newTruck) {
					try {
						Hashtable<String, Object> params = new Hashtable<>();
						params.put("placa", placa1);
						List<TruckInfo> ll = session.findEntities("select t from TruckInfo as t where t.placa = :placa",
								TruckInfo.class, params);
						return ll == null || ll.isEmpty();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return true;
			}, "Caminh�o j� existe").bind(TruckInfo::getPlaca, TruckInfo::setPlaca);
			addComponent(placa);
		}
		{
			TextField rntc = new TextField("RNTC do Veiculo");
			rntc.setWidth("300px");
			this.binder.forField(rntc).bind(TruckInfo::getRntc, TruckInfo::setRntc);
			addComponent(rntc);
		}
		{

			ComboBox<TRUCK_TYPE> comboType = new ComboBox<>("Tipo de Veiculo");
			comboType.setWidth("300px");
			comboType.setItems(TRUCK_TYPE.values());
			comboType.setItemCaptionGenerator(TRUCK_TYPE::getName);
			comboType.setEmptySelectionAllowed(false);

			this.binder.forField(comboType).bind(truck -> {
				return truck.getCarType();
			}, (truck, type) -> {
				if (!TRUCK_TYPE.SELECIONE.equals(type)) {
					truck.setCarType(type);
				}
			});
			addComponent(comboType);
		}
		{
			TextArea descricao = new TextArea("Descri��o");
			descricao.setWidth("600px");
			this.binder.forField(descricao).bind(TruckInfo::getTruckDescription, TruckInfo::setTruckDescription);
			addComponent(descricao);
		}
		{
			HorizontalLayout capacityLayout = new HorizontalLayout();

			{
				TextField deliver = new TextField("Capacidade Entrega");
				this.binder.forField(deliver).bind(truck -> {
					Integer value = truck.getDimensions().get(ORDER_TYPE.DELIVERY);
					return value == null ? "0" : value + "";
				}, (truck, value) -> {
					truck.getDimensions().put(ORDER_TYPE.DELIVERY, Integer.parseInt(value));
				});
				capacityLayout.addComponent(deliver);
			}
			{
				TextField pick = new TextField("Capacidade Retirada");
				this.binder.forField(pick).bind(truck -> {
					Integer value = truck.getDimensions().get(ORDER_TYPE.PICK);
					return value == null ? "0" : value + "";
				}, (truck, value) -> {
					truck.getDimensions().put(ORDER_TYPE.PICK, Integer.parseInt(value));
				});
				capacityLayout.addComponent(pick);
			}
			capacityLayout.setWidth("600px");
			addComponent(capacityLayout);
		}
		{
			final SectorInfo empty = new SectorInfo();
			empty.setName("Selecione uma Base");
			ComboBox<SectorInfo> comboSector = new ComboBox<>("Base");
			try {
				List<SectorInfo> sectors = session.findEntities("select s from SectorInfo as s", SectorInfo.class,
						new Hashtable<>());
				sectors.add(0, empty);
				comboSector.setItems(sectors);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			comboSector.setItemCaptionGenerator(SectorInfo::getName);
			comboSector.setEmptySelectionAllowed(false);
			this.binder.forField(comboSector)
					.withValidator(sector -> sector.getSectorId() != null, "Selecione uma Base").bind(truck -> {
						if (truck.getTruckId().getSector_id() != null) {
							try {
								SectorInfo sector = session.findEntity(SectorInfo.class,
										truck.getTruckId().getSector_id());
								return sector == null ? empty : sector;
							} catch (Exception e) {
								// e.printStackTrace();
							}
						}
						return empty;
					}, (truck, sector) -> {
						TruckRegisterView.this.old_truck_id = new TruckId();
						TruckRegisterView.this.old_truck_id.setTruck_id(truck.getTruckId().getTruck_id());
						TruckRegisterView.this.old_truck_id.setSector_id(truck.getTruckId().getSector_id());

						session.detachEntity(truck);
						truck.getTruckId().setSector_id(sector.getSectorId());
					});
			comboSector.setWidth("300px");
			addComponent(comboSector);
		}
		{
			TextField filter = new TextField("Usuarios do caminh�o");
			filter.setStyleName("filter-textfield");
			filter.setPlaceholder("digite um usuario");
			filter.setWidth("400px");

			ResetButtonForTextField.extend(filter);

			this.gridUsers = new Grid<>();
			gridUsers.addColumn(userId -> userId.getUser_id()).setCaption("Usuario").setWidth(300d);
			gridUsers.addComponentColumn(userId -> {
				HorizontalLayout buttons = new HorizontalLayout();

				Button btnAdd = new Button("Adicionar");
				btnAdd.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnAdd.addClickListener(new ClickListener() {

					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						positionToInsert++;
						filter.setValue("");
					}
				});

				Button btnRemove = new Button("Remover");
				btnRemove.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				btnRemove.addClickListener(new ClickListener() {

					private static final long serialVersionUID = 1L;

					@Override
					public void buttonClick(ClickEvent event) {
						positionToInsert--;
						usersIds.remove(userId);
						gridUsers.getDataProvider().refreshAll();
					}
				});

				buttons.addComponent(btnAdd);
				buttons.addComponent(btnRemove);

				return buttons;
			});
			gridUsers.setWidth("600px");
			gridUsers.setHeight("200px");
			gridUsers.setItems(usersIds);

			filter.addValueChangeListener(event -> {

				try {
					if (filter.getValue() == null || filter.getValue().isEmpty()) {
						return;
					}

					System.out.println("finding user: " + filter.getValue());
					Hashtable<String, Object> params = new Hashtable<>();
					params.put("user", "%" + filter.getValue() + "%");
					List<UserInfo> users = session.findEntities(
							"select u from UserInfo as u where u.userId.user_id like :user", UserInfo.class, params);

					System.out.println("found: " + users.size() + " usersIds: " + usersIds);
					if (!users.isEmpty()) {
						for (int index = positionToInsert; index < usersIds.size(); index++) {
							usersIds.remove(index);
						}
						System.out.println("positionToInsert: " + positionToInsert);
						usersIds.add(positionToInsert, users.get(0).getUserId());
						gridUsers.getDataProvider().refreshAll();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});

			addComponent(filter);
			addComponent(gridUsers);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnNew = new Button("Novo Caminh�o");
			btnNew.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					binder.readBean(createEmptyTruck());
					usersIds.clear();
					gridUsers.getDataProvider().refreshAll();
					positionToInsert = 0;
					newTruck = true;
					currentTruck = createEmptyTruck();
				}
			});
			btnNew.setWidth("150px");

			Button btnSave = new Button("Salvar");
			btnSave.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnSave.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					BinderValidationStatus<TruckInfo> results = binder.validate();

					try {
						binder.writeBean(currentTruck);

						if (currentTruck.getTruckId().getTruck_id() == null) {
							currentTruck.getTruckId().setTruck_id("truck_" + UUID.randomUUID().toString());
						}
						session.saveEntity(currentTruck.getTruckId(), currentTruck);

						for (UserTruckMap map : TruckRegisterView.this.currentMaps) {
							session.deleteEntity(map);
						}

						for (UserId userId : usersIds) {
							UserTruckMap map = new UserTruckMap();
							map.setId("truck_user_map_" + UUID.randomUUID().toString());
							map.setUserId(userId);
							map.setTruckId(currentTruck.getTruckId());
							session.saveEntity(map.getId(), map);

						}

						binder.readBean(createEmptyTruck());
						usersIds.clear();
						currentMaps.clear();
						gridUsers.getDataProvider().refreshAll();

						{
							if (TruckRegisterView.this.old_truck_id != null
									&& !TruckRegisterView.this.old_truck_id.equals(currentTruck.getTruckId())) {
								Hashtable<String, Object> params1 = new Hashtable<>();
								params1.put("id", TruckRegisterView.this.old_truck_id);
								List<TruckInfo> trucks = session.findEntities(
										"select t from TruckInfo as t where t.truckId =:id", TruckInfo.class, params1);

								if (trucks != null && !trucks.isEmpty()) {
									trucks.forEach(truck -> session.deleteEntity(truck));
								}
								TruckRegisterView.this.old_truck_id = null;
							}
						}

						searchBox.getSearchField().setValue("");
						positionToInsert = 0;
						currentTruck = createEmptyTruck();
						newTruck = true;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
			btnSave.setWidth("150px");

			buttons.addComponent(btnNew);
			buttons.addComponent(btnSave);

			buttons.setSizeUndefined();

			// setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		this.binder.readBean(this.currentTruck);
	}

	private void filter_not_use() {
		try {
			String valueToFind = this.searchBox.getSearchField().getValue();

			if (valueToFind.isEmpty()) {
				this.binder.readBean(this.createEmptyTruck());
				return;
			}

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("placa", "%" + valueToFind + "%");
			List<TruckInfo> trucks = session.findEntities("select u from TruckInfo as u where u.placa like :placa",
					TruckInfo.class, params);

			newTruck = false;

			if (!trucks.isEmpty()) {
				{
					this.currentTruck = trucks.get(0);
					if (this.currentTruck.getDimensions() == null) {
						this.currentTruck.setDimensions(new Hashtable<>());
					}
					this.binder.readBean(this.currentTruck);
				}
				{
					Hashtable<String, Object> params2 = new Hashtable<>();
					params2.put("truckid", this.currentTruck.getTruckId());
					List<UserTruckMap> maps = session.findEntities(
							"select m from UserTruckMap as m where m.truckId = :truckid", UserTruckMap.class, params2);
					usersIds = new ArrayList<>();
					for (UserTruckMap m : maps) {
						usersIds.add(m.getUserId());
					}
					gridUsers.setItems(usersIds);
					gridUsers.getDataProvider().refreshAll();
					positionToInsert = usersIds.size();
				}
			} else {
				this.binder.readBean(this.createEmptyTruck());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void initSearchBox(SearchBox searchBox2) {
		searchBox2.setSuggestionGenerator(new SuggestionGenerator<TruckInfo>() {

			@Override
			public List<TruckInfo> apply(String query, Integer limit) {
				List<TruckInfo> suggestions = new ArrayList<>();

				binder.readBean(createEmptyTruck());

				if (query.length() >= 0) {
					try {
						Hashtable<String, Object> params = new Hashtable<>();
						params.put("placa", "%" + query + "%");
						List<TruckInfo> trucks = session.findEntities(
								"select u from TruckInfo as u where u.placa like :placa", TruckInfo.class, params);

						trucks.forEach(truck -> suggestions.add(truck));
					} catch (Exception e) {

					}
				}
				return suggestions;
			}
		}, new SuggestionValueConverter<TruckInfo>() {

			@Override
			public String apply(TruckInfo suggestion) {
				return suggestion.getPlaca();
			}
		}, new SuggestionCaptionConverter<TruckInfo>() {

			@Override
			public String apply(TruckInfo truck, String query) {
				return truck.getPlaca();
			}
		});

		searchBox2.addSearchListener(new SearchListener<TruckInfo>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void search(SearchEvent<TruckInfo> event) {
				TruckInfo truck = event.getSelectedItem().get();
				showSelect(truck);
			}
		});

	}

	private TruckInfo createEmptyTruck() {
		TruckInfo truck = new TruckInfo();
		truck.setTruckId(new TruckId());
		truck.setDimensions(new Hashtable<>());
		return truck;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		View.super.enter(event);
	}
	
	private void showSelect(TruckInfo truck) {
		try {
			
			{
				currentTruck = truck;
				if (currentTruck.getDimensions() == null) {
					currentTruck.setDimensions(new Hashtable<>());
				}
				binder.readBean(currentTruck);
			}
			{
				Hashtable<String, Object> params2 = new Hashtable<>();
				params2.put("truckid", currentTruck.getTruckId());
				TruckRegisterView.this.currentMaps = session.findEntities(
						"select m from UserTruckMap as m where m.truckId = :truckid", UserTruckMap.class,
						params2);
				usersIds = new ArrayList<>();
				for (UserTruckMap m : TruckRegisterView.this.currentMaps) {
					usersIds.add(m.getUserId());
				}
				gridUsers.setItems(usersIds);
				gridUsers.getDataProvider().refreshAll();
				positionToInsert = usersIds.size();
			}

			newTruck = false;
		} catch (Exception e) {
			e.printStackTrace();
			binder.readBean(createEmptyTruck());
		}

		UI.getCurrent().access(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				searchBox.getSearchField().setValue("");
			}
		});
	}
}
