package grx.tracking.gui.jsmaps;

import java.util.List;
import java.util.UUID;

import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.AbstractJavaScriptComponent;
import com.vaadin.ui.JavaScriptFunction;

import elemental.json.JsonArray;
import grx.tracking.core.persistence.GeoLocation;

@JavaScript({ "http://maps.googleapis.com/maps/api/js?key=AIzaSyBG-8a072nSwUAZaA_f4cJibyJtonJG2ds&v=3.exp",
		"http://maps.googleapis.com/maps/api/js?key=AIzaSyBG-8a072nSwUAZaA_f4cJibyJtonJG2ds&libraries=geometry",
		"MyMaps.js" })
public class MyMaps extends AbstractJavaScriptComponent {

	private static final long serialVersionUID = 1L;

	private boolean mapLoaded;

	private MapListener listener;

	public MyMaps() {
		this.mapLoaded = false;

		addFunction("mapLoaded", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JsonArray arguments) {
				mapLoaded = true;
				if (listener != null) {
					listener.mapLoaded();
				}
			}
		});

		addFunction("markerClicked", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JsonArray arguments) {
				System.out.println(arguments.getString(0));
				System.out.println(arguments.getObject(1).getNumber("lat"));

				if (listener != null) {
					listener.functionCalled("markerClick", arguments.getString(0), arguments.getObject(1));
				}

			}
		});

		addFunction("driverClicked", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JsonArray arguments) {
				System.out.println(arguments.getString(0));

				if (listener != null) {
					listener.functionCalled("driverClicked", arguments.getString(0), arguments.getObject(1));
				}

			}
		});

		addFunction("markerDragEnd", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JsonArray arguments) {
				System.out.println(arguments.getString(0));
				System.out.println(arguments.getObject(1).getNumber("lat"));
				System.out.println(arguments.getObject(1).getNumber("lng"));

				if (listener != null) {
					listener.functionCalled("markerDragEnd", arguments.getString(0), arguments.getObject(1));
				}

			}
		});

		addFunction("routePathClicked", new JavaScriptFunction() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(JsonArray arguments) {
				System.out.println(arguments.getString(0));

				if (listener != null) {
					listener.functionCalled("routePathClicked", arguments.getString(0), null);
				}
			}
		});
	}

	public void setListener(MapListener listener) {
		this.listener = listener;
	}

	public void addOrderMarker(String id, String label, GeoLocation location, String description, String icon) {
		callFunction("addOrderMarker",
				new Object[] { id, label,
						"{\"lat\" : \"" + location.getLatitude() + "\", \"lng\" : \"" + location.getLongitude() + "\"}",
						description, icon });
	}

	public void addOrderMarkerWindow(String id, String label, GeoLocation location, String description, String icon) {
		callFunction("addOrderMarkerWindow",
				new Object[] { id, label,
						"{\"lat\" : \"" + location.getLatitude() + "\", \"lng\" : \"" + location.getLongitude() + "\"}",
						description, icon });
	}

	public void addBaseMarker(String id, String label, GeoLocation location, String description) {
		callFunction("addBaseMarker",
				new Object[] { id, label,
						"{\"lat\" : \"" + location.getLatitude() + "\", \"lng\" : \"" + location.getLongitude() + "\"}",
						description });
	}

	public void addDropMarker(String id, String label, GeoLocation location, String description) {
		callFunction("addDropMarker",
				new Object[] { id, label,
						"{\"lat\" : \"" + location.getLatitude() + "\", \"lng\" : \"" + location.getLongitude() + "\"}",
						description });
	}

	public void addTruckMarker(String id, String label, GeoLocation location) {
		callFunction("addTruckMarker",
				new Object[] { id, label,
						"{\"lat\" : \"" + location.getLatitude() + "\", \"lng\" : \"" + location.getLongitude() + "\"}",
						location.getOrientation() });
	}

	public void removeTruckMarker(String id) {
		callFunction("removeTruckMarker", new Object[] { id });
	}

	public void moveTruckMarker(String id, String label, GeoLocation location) {
		System.out.println("moving truck: " + id);
		callFunction("moveTruckMarker",
				new Object[] { id, label,
						"{\"lat\" : \"" + location.getLatitude() + "\", \"lng\" : \"" + location.getLongitude() + "\"}",
						location.getOrientation() });
	}

	public void clearMap() {
		callFunction("clearMap", new Object[] { "message" });
	}

	public void removeRoute(String id) {
		callFunction("removeRoute", new Object[] { id });
	}

	public void removeById(String id) {
		callFunction("removeById", new Object[] { id });
	}

	public void calculateAndDisplayRoute() {
		callFunction("calculateAndDisplayRoute", new Object[] { "OK" });
	}

	public void drawEncodedPolyline(String id, String polyline) {
		callFunction("drawEncodedPolyline", new Object[] { id, polyline });
	}

	public void drawRoute(String id, String[] polylines) {
		callFunction("drawRoute", new Object[] { id, polylines });

	}

	public void focusObject(String id, String message) {
		callFunction("focusObject", new Object[] { id, message });

	}

	public void drawRoutePath(String id, List<GeoLocation> locations, String distance) {

		String json = "";

		int size = locations.size();
		for (int i = 0; i < size - 1; i++) {
			GeoLocation location = locations.get(i);
			String latlng = "{\"lat\": " + location.getLatitude() + ", \"lng\": " + location.getLongitude() + "}";
			json = json + latlng + ",";
		}

		GeoLocation location = locations.get(size - 1);
		String latlng = "{\"lat\": " + location.getLatitude() + ", \"lng\": " + location.getLongitude() + "}";
		json = "[" + json + latlng + "]";

		callFunction("drawRoutePath", new Object[] { id, json, distance });

	}

}
