package grx.tracking.gui.views;

import com.github.appreciated.app.layout.AppLayout;
import com.github.appreciated.app.layout.behaviour.AppLayoutComponent;
import com.github.appreciated.app.layout.behaviour.Behaviour;
import com.github.appreciated.app.layout.builder.design.AppLayoutDesign;
import com.github.appreciated.app.layout.builder.elements.builders.SubmenuBuilder;
import com.github.appreciated.app.layout.builder.entities.DefaultBadgeHolder;
import com.github.appreciated.app.layout.builder.entities.DefaultNotificationHolder;
import com.github.appreciated.app.layout.component.MenuHeader;
import com.github.appreciated.app.layout.component.button.AppBarNotificationButton;
import com.github.appreciated.app.layout.interceptor.DefaultViewNameInterceptor;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.HorizontalLayout;

import static com.github.appreciated.app.layout.builder.Section.HEADER;
import grx.tracking.gui.samples.ErrorView;

public class MenuMainView extends HorizontalLayout implements View {

	public MenuMainView() {
		DefaultNotificationHolder notifications = new DefaultNotificationHolder();
		DefaultBadgeHolder badge = new DefaultBadgeHolder();

		AppLayoutComponent layout = AppLayout.getDefaultBuilder(Behaviour.LEFT_RESPONSIVE)
				.withTitle("App Layout Basic Example").addToAppBar(new AppBarNotificationButton(notifications, true))
				.withViewNameInterceptor(new DefaultViewNameInterceptor()).withDefaultNavigationView(ErrorView.class)
				.withDesign(AppLayoutDesign.MATERIAL)
				.add(new MenuHeader("Version 0.9.21", new ThemeResource("logo.png")), HEADER)
				.add("Home", VaadinIcons.HOME, badge, MapperView.class)
				.add(SubmenuBuilder.get("My Submenu", VaadinIcons.PLUS)
						.add("Charts", "test", VaadinIcons.SPLINE_CHART, ErrorView.class).build())
				.build();

		this.addComponent(layout);
	}

	@Override
	public void enter(ViewChangeEvent event) {
		View.super.enter(event);
	}
}
