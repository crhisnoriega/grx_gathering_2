package grx.tracking.gui.windows;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;
import org.vaadin.addons.searchbox.SearchBox;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.components.grid.GridDragSource;
import com.vaadin.ui.components.grid.GridDropTarget;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.maps.MapObject.MAP_OBJECT_TYPE;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.LastKnowUserLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.util.MyConvertArray;
import grx.tracking.gui.jsmaps.MyMaps;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyDialogs;
import grx.tracking.gui.views.MapperView;

public class OrdersWindow extends Window {

	private static final long serialVersionUID = 1L;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private Hashtable<RoutePointInfo, OrderInfo> orders;

	private ArrayList<RoutePointInfo> points;
	private RouteInfo routeInfo;
	private GRXTrackingSession session;

	private OrderConfirmListener listener;
	private RoutePointInfo dragged;
	private Grid<RoutePointInfo> grid;

	private MyMaps map;

	public OrdersWindow(RouteInfo routeInfo, GRXTrackingSession session) {
		super("Pedidos da Rota");

		this.routeInfo = routeInfo;
		this.session = session;
		this.orders = new Hashtable<>();
		this.points = new ArrayList<RoutePointInfo>(Arrays.asList(this.routeInfo.getPoints()));

		{
			for (RoutePointInfo point : this.points) {
				try {
					OrderInfo order1 = this.session.findEntity(OrderInfo.class, point.getOrderId());
					if (order1 != null) {
						orders.put(point, order1);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("this.orders: " + this.orders);

		VerticalLayout mainLayout = new VerticalLayout();
		{

			SearchBox searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			searchBox.setCaption("Adicionar Pedido");
			searchBox.setPlaceholder("digite o numero do pedido");
			searchBox.setWidth("640px");
			searchBox.setSuggestionGenerator(new SuggestionGenerator<OrderInfo>() {

				@Override
				public List<OrderInfo> apply(String query, Integer limit) {
					List<OrderInfo> suggestions = new ArrayList<>();

					if (query.length() >= 3) {
						try {
							Hashtable<String, Object> params = new Hashtable<>();
							params.put("order", "%" + query + "%");
							List<OrderInfo> orders = session.findEntities(
									"select o from OrderInfo as o where o.orderId.order_id like :order",
									OrderInfo.class, params);
							orders.forEach(order -> suggestions.add(order));
						} catch (Exception e) {

						}
					}
					return suggestions;
				}
			}, new SuggestionValueConverter<OrderInfo>() {

				@Override
				public String apply(OrderInfo suggestion) {
					return suggestion.getOrderId().getOrder_id();
				}
			}, new SuggestionCaptionConverter<OrderInfo>() {

				@Override
				public String apply(OrderInfo order, String query) {
					return order.getOrderId().getOrder_id() + " - " + order.getAddress().getAddress().asAddressString();
				}
			});
			searchBox.addSearchListener(e -> {
				Object obj = e.getSelectedItem().get();
				if (obj instanceof OrderInfo) {
					OrderInfo order = (OrderInfo) obj;
					RoutePointInfo point = new RoutePointInfo();
					point.setOrderId(order.getOrderId());

					points.add(point);

					orders.put(point, order);

					grid.getDataProvider().refreshAll();
				}
			});

			searchBox.focus();
			mainLayout.addComponent(searchBox);
		}

		this.grid = new Grid<>();
		{

			grid.addColumn(point -> {
				return point.getOrderId().getOrder_id();
			}).setCaption("No. de Ordem");

			grid.addColumn(point -> {
				OrderInfo order = orders.get(point);
				if (order != null) {
					return sdf.format(order.getRequestDate());
				} else {
					return "";
				}

			}).setWidth(120d).setCaption("Data");

			grid.addColumn(point -> {
				OrderInfo order = orders.get(point);
				if (order == null) {
					return "";
				}

				String tipo = "";
				switch (order.getOrderType()) {
				case DELIVERY:
					tipo = "Entrega";
					break;

				case EXCHANGE:
					tipo = "Troca";
					break;

				case PICK:
					tipo = "Retira";
					break;

				default:
					break;
				}
				return tipo;
			}).setWidth(100d).setCaption("Tipo");

			grid.addColumn(point -> {
				OrderInfo order = orders.get(point);
				if (order == null) {
					return "";
				}

				if (order.getAddress() != null && order.getAddress().getAddress() != null) {
					return order.getAddress().getAddress().asAddressString();
				} else {
					return "";
				}
			}).setCaption("Endere�o").setWidth(200d);

			grid.addComponentColumn(point -> {
				HorizontalLayout buttons = new HorizontalLayout();
				Button btnRemove = new Button("Remover da Rota");
				btnRemove.addClickListener(new ClickListener() {

					@Override
					public void buttonClick(ClickEvent event) {
						try {
							OrderInfo order = orders.get(point);

							OrderStatus currentStatus = new OrderStatus();
							currentStatus.setStatus(ORDER_STATUS.NEW);
							currentStatus.setDate(Calendar.getInstance().getTime());
							order.setCurrentStatus(currentStatus);

							session.saveEntity(order.getOrderId(), order);

							points.remove(point);
							grid.getDataProvider().refreshAll();
							
							try {
								map.clearMap();						
								OrdersWindow.this.routeInfo.setPoints(MyConvertArray.convertArrayList(points, RoutePointInfo.class));
								drawRoute(OrdersWindow.this.routeInfo);
							}catch (Exception ex) {
								ex.printStackTrace();
							}
						} catch (Exception e) {
							MyDialogs.showError("Remover Rota", "Erro: " + e.getLocalizedMessage());
						}

					}
				});
				btnRemove.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				buttons.addComponent(btnRemove);
				return buttons;
			}).setCaption("Opera��es");

			grid.setItems(this.points);
			grid.setSizeFull();

			{

				GridDragSource<RoutePointInfo> source = new GridDragSource<>(grid);

				source.addGridDragStartListener(e -> {
					dragged = e.getDraggedItems().iterator().next();
				});

				GridDropTarget<RoutePointInfo> target = new GridDropTarget<>(grid, DropMode.ON_TOP_OR_BETWEEN);
				target.addGridDropListener(e -> {
					int index = points.indexOf(e.getDropTargetRow().get());

					System.out.println("index: " + index);

					points.remove(dragged);
					points.add(index, dragged);
					grid.getDataProvider().refreshAll();
					
					try {
						map.clearMap();						
						OrdersWindow.this.routeInfo.setPoints(MyConvertArray.convertArrayList(points, RoutePointInfo.class));
						drawRoute(OrdersWindow.this.routeInfo);
					}catch (Exception ex) {
						ex.printStackTrace();
					}
				});
			}

			mainLayout.addComponent(grid);
			mainLayout.setExpandRatio(grid, 0.2f);
		}
		{
			this.map = new MyMaps();
			this.map.setId("mymap");
			// map.setSizeFull();

			this.map.setHeight("400px");
			this.map.setWidth("100%");
			// maps.setSizeFull();

			mainLayout.addComponent(map);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Confirmar Rota");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {

						OrdersWindow.this.routeInfo
								.setPoints(MyConvertArray.convertArrayList(points, RoutePointInfo.class));

						if (listener != null) {
							listener.confirm(OrdersWindow.this.routeInfo);
						}
						close();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});

			// buttons.addComponent(btnShow);
			buttons.addComponent(btnConfirm);
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}
		{
			Label msg = new Label("Arrastre a linha para mudar a ordem dos pedidos");
			mainLayout.addComponent(msg);
			mainLayout.setComponentAlignment(msg, Alignment.MIDDLE_RIGHT);
		}

		try {
			drawRoute(routeInfo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mainLayout.setSizeFull();
		setContent(mainLayout);

		setWidth("80%");
		setHeight("100%");
		setClosable(false);

	}

	private void drawRoute(RouteInfo route) throws Exception {
		
		map.clearMap();

		RoutePointInfo[] points = route.getPoints();

		for (int index = 0; index < points.length; index++) {
			RoutePointInfo current = points[index];
			GoogleRouteInfo googleRoute = current.getGoogleRoute();
			System.out.println("gr: " + googleRoute);
			System.out.println("s: " + googleRoute.getStartOrder());
			System.out.println("e: " + googleRoute.getEndOrder());
		}

		OrderInfo currentOrder = null;
		OrderInfo lastOrder = null;

		// draw truck
		//UserInfo user = session.findEntity(UserInfo.class, route.getUserId());
		// LastKnowUserLocation lastlocation = session.findEntity(LastKnowUserLocation.class, user.getUserId());

		/*if (lastlocation == null) {
			MyDialogs.showError("Visualizar Rota",
					"Nao foi possivel encontrar posiçao do usuario da rota: " + route.getUserId());
			System.out.println("dont found last location for user: " + route.getUserId());
			return;
		}*/

		/*if (lastlocation != null) {
			this.drawObjectByType(MapObject.createMapObject(user, lastlocation.getGeoLocation()));
		}*/

		// this.showOrdersInfoWindow(route);

		// draw orders
		for (RoutePointInfo point : route.getPoints()) {
			OrderInfo order = session.findEntity(OrderInfo.class, point.getOrderId());
			if (order != null) {
				MapObject order_object = MapObject.createMapObject(order);
				
				System.out.println("order_object: " + order_object +" type: " + order_object.getType());

				// this.last_route_ids.add(order_object.getId());

				this.drawObjectByType(order_object);

				if ((order.getCurrentStatus().getStatus().equals(ORDER_STATUS.ROUTING)
						|| order.getCurrentStatus().getStatus().equals(ORDER_STATUS.RESERVED))
						&& currentOrder == null) {
					currentOrder = order;
				}

				if ((order.getCurrentStatus().getStatus().equals(ORDER_STATUS.ROUTING)
						|| order.getCurrentStatus().getStatus().equals(ORDER_STATUS.RESERVED))
						&& point.equals(points[points.length - 1])) {
					lastOrder = order;
				}
			}
			
			// return;
		}

		// draw route per se

		this.drawObjectByType(MapObject.createMapObject(route));

		{

			/*System.out.println("currentOrder: " + currentOrder);
			// mostrar rota do veiculo ate o primeiro pedido
			if (currentOrder != null) {
				drawArbitraryRoute(lastlocation.getGeoLocation(), currentOrder.getAddress().getLocation(),
						route.getUserId());
			}*/

			SectorInfo endSector = session.findEntity(SectorInfo.class, route.getEndSectorId());
			this.drawObjectByType(MapObject.createMapObject(endSector));

			System.out.println("lastOrder: " + lastOrder);
			// mostrar rota ate a base
			if (lastOrder != null) {
				drawArbitraryRoute(lastOrder.getAddress().getLocation(), endSector.getLocation(), route.getUserId());
			}
		}
	}

	public void drawArbitraryRoute(GeoLocation start, GeoLocation end, UserId userId) {
		try {
			GoogleRouteInfo route1 = EJBLocator.refGRXTrackingService().findGoogleRoute(start, end);
			RouteInfo route12 = new RouteInfo();
			route12.setId("route_" + UUID.randomUUID().toString().split("-")[0]);
			route12.setUserId(userId);

			RoutePointInfo point12 = new RoutePointInfo();
			point12.setGoogleRoute(route1);
			route12.setPoints(new RoutePointInfo[] { point12 });

			drawObjectByType(MapObject.createMapObject(route12));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawObjectByType(MapObject mapObject) {
		if (mapObject.getType().equals(MAP_OBJECT_TYPE.DRIVER)) {
			executeInUIThread(
					() -> map.addTruckMarker(mapObject.getId(), mapObject.getLabel(), mapObject.getLocation()));
		}

		if (mapObject.getType().equals(MAP_OBJECT_TYPE.BASE)) {

			String content = "<div class=\"custom-header\"> Base:" + mapObject.getLabel() + "</div>"
					+ "<div class=\"custom-body\">" + mapObject.getDescription() + "</div>";

			executeInUIThread(
					() -> map.addBaseMarker(mapObject.getId(), "nao_usado", mapObject.getLocation(), content));
		}

		if (mapObject.getType().equals(MAP_OBJECT_TYPE.ORDER)) {

			String content = "<div class=\"custom-header\"> Pedido: " + mapObject.getOrderId().getOrder_id() + "</div>"
					+ "<div class=\"custom-header\"> Status: " + mapObject.getCtrStatus().getName() + "</div>"
					+ (mapObject.getPeriodType() != null
							? "<div class=\"custom-header\"> Periodo: " + mapObject.getPeriodType().getName() + "</div>"
							: "")
					+ "<div class=\"custom-body\">" + mapObject.getDescription() + "</div>";
			
			System.out.println("location: " + mapObject.getLocation());

			executeInUIThread(() -> {
				if (mapObject.getLocation() != null) {

					String icon = "";
					if (ORDER_TYPE.DELIVERY.equals(mapObject.getOrderType())) {
						icon = "marker_green_icon.png";
					}
					if (ORDER_TYPE.PICK.equals(mapObject.getOrderType())) {
						icon = "marker_red_icon.png";
					}
					if (ORDER_TYPE.EXCHANGE.equals(mapObject.getOrderType())) {
						icon = "marker_green_icon.png";
					}
					map.addOrderMarkerWindow(mapObject.getId(), "nao_usado", mapObject.getLocation(), content, icon);
				}
			});
		}

		if (mapObject.getType().equals(MAP_OBJECT_TYPE.ROUTE)) {

			List<GoogleRouteInfo> routes = mapObject.getRoute();
			for (int index = 0; index < routes.size(); index++) {
				GoogleRouteInfo route = routes.get(index);
				List<GeoLocation> locations = route.getLocationRepresentation();

				String distance_format = route.getDistance();
				try {
					distance_format = Long.parseLong(distance_format) / 1000L + " km";
				} catch (Exception e) {

				}

				final String dd = distance_format;

				String idroute = mapObject.getId() + "_" + index;

				executeInUIThread(() -> map.drawRoutePath(idroute, locations, "0 km".equals(dd) ? "" : dd));

			}

		}
	}

	private void executeInUIThread(Runnable r) {
		for (int i = 0; i < 3; i++) {
			try {
				UI.getCurrent().access(r);
				break;
			} catch (UIDetachedException e) {
				// System.err.println("UIDetachedException ui: " + ui);
				break;
			} catch (Exception e) {
				System.err.println("executeInUIThread exception type:" + e.getClass() + " localizedMessage: "
						+ e.getLocalizedMessage() + " message: " + e.getMessage());
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
				}
			}
		}
	}

	private void filter() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setListener(OrderConfirmListener listener) {
		this.listener = listener;
	}

	public static interface OrderConfirmListener {
		public void confirm(RouteInfo route);
	}
}
