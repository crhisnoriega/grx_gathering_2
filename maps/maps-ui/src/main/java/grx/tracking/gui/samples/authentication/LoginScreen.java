package grx.tracking.gui.samples.authentication;

import java.io.Serializable;

import com.vaadin.event.ShortcutAction;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.gui.jsmaps.MyMaps;
import grx.tracking.gui.windows.MapSelectWindow;

public class LoginScreen extends CssLayout {

	private static final long serialVersionUID = 1L;

	private TextField username;
	private PasswordField password;
	private Button login;
	private Button forgotPassword;
	private LoginListener loginListener;
	private AccessControl accessControl;
	private UI ui;

	public LoginScreen(UI ui, AccessControl accessControl, LoginListener loginListener) {
		this.ui = ui;
		this.loginListener = loginListener;
		this.accessControl = accessControl;
		buildUI();
		this.password.focus();
	}

	private void buildUI() {
		addStyleName("login-screen");

		// login form, centered in the available part of the screen
		Component loginForm = buildLoginForm();

		// layout to center login form when there is sufficient screen space
		// - see the theme for how this is made responsive for various screen
		// sizes
		VerticalLayout centeringLayout = new VerticalLayout();
		centeringLayout.setMargin(false);
		centeringLayout.setSpacing(false);
		centeringLayout.setStyleName("centering-layout");
		centeringLayout.addComponent(loginForm);
		centeringLayout.setComponentAlignment(loginForm, Alignment.MIDDLE_CENTER);

		Image image = new Image("", new ThemeResource("img/grx_logo_transparente.png"));
		image.setWidth("200px");
		// image.setHeight("200px");

		centeringLayout.addComponent(image);
		centeringLayout.setComponentAlignment(image, Alignment.TOP_CENTER);

		// information text about logging in
		// CssLayout loginInformation = buildLoginInformation();

		addComponent(centeringLayout);
		// addComponent(loginInformation);

	}

	private Component buildLoginForm() {
		FormLayout loginForm = new FormLayout();

		loginForm.addStyleName("login-form");
		loginForm.setSizeUndefined();
		loginForm.setMargin(false);

		loginForm.addComponent(username = new TextField("Usuario", "admin"));
		username.setWidth(15, Unit.EM);
		loginForm.addComponent(password = new PasswordField("Senha"));
		password.setWidth(15, Unit.EM);
		password.setDescription("Write anything");
		CssLayout buttons = new CssLayout();
		buttons.setStyleName("buttons");
		loginForm.addComponent(buttons);

		buttons.addComponent(login = new Button("Entrar"));
		login.setDisableOnClick(true);
		login.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				try {
					login();
				} finally {
					login.setEnabled(true);
				}
			}
		});
		login.setWidth("150px");
		login.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		login.addStyleName(ValoTheme.BUTTON_FRIENDLY);

		forgotPassword = new Button("Forgot password?");
		// buttons.addComponent(forgotPassword);
		forgotPassword.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent event) {
				showNotification(new Notification("Hint: Try anything"));
			}
		});

		forgotPassword.addStyleName(ValoTheme.BUTTON_LINK);
		return loginForm;
	}

	private CssLayout buildLoginInformation() {
		CssLayout loginInformation = new CssLayout();

		Image image = new Image("", new ThemeResource("img/grx_image.jpg"));
		image.setWidth("100%");
		loginInformation.addComponent(image);

		loginInformation.setStyleName("login-information");

		Label loginInfoText = new Label("<h1>GRX Tracking</h1>" + "Sistema de rastreamento e designação de tarefas",
				ContentMode.HTML);
		loginInfoText.setSizeFull();
		loginInformation.addComponent(loginInfoText);

		return loginInformation;
	}

	private void login() {
		if (accessControl.signIn(username.getValue(), password.getValue())) {
			loginListener.loginSuccessful();
		} else {
			// showNotification(new Notification("Login failed", "Please check
			// your username and password and try again.",
			// Notification.Type.HUMANIZED_MESSAGE));
			username.focus();
		}
	}

	private void showNotification(Notification notification) {
		// keep the notification visible a little while after moving the
		// mouse, or until clicked
		notification.setDelayMsec(2000);
		notification.show(Page.getCurrent());
	}

	public interface LoginListener extends Serializable {
		void loginSuccessful();
	}
}
