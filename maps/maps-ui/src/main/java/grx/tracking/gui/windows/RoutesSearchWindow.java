package grx.tracking.gui.windows;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.gui.samples.ResetButtonForTextField;
import grx.tracking.gui.samples.authentication.CurrentUser;

public class RoutesSearchWindow extends Window {

	private static final long serialVersionUID = 1L;

	private TextField filter;
	private List<RouteInfo> routes;
	private RouteSearchListener listener;
	private GRXTrackingSession session;

	private DateField date;

	private Grid<RouteInfo> routeGrid;

	public RoutesSearchWindow() {
		super("Busca de Rotas");
		this.session = CurrentUser.get();
		this.routes = new ArrayList<>();

		VerticalLayout mainLayout = new VerticalLayout();

		{
			HorizontalLayout filterLayout = new HorizontalLayout();

			this.filter = new TextField();
			this.filter.setStyleName("filter-textfield");
			this.filter.setPlaceholder("digite usuario ou No. de ordem");
			this.filter.setWidth("400px");
			ResetButtonForTextField.extend(filter);

			filterLayout.addComponent(this.filter);

			this.date = new DateField();
			this.date.setDateFormat("dd/MM/yyyy");
			this.date.setValue(LocalDate.now());

			filterLayout.addComponent(this.date);

			mainLayout.addComponent(filterLayout);
		}

		this.routeGrid = new Grid<>();
		{

			// this.routeGrid.addColumn(route -> route.getId()).setCaption("Id da Rota");
			this.routeGrid.addColumn(route -> {
				return route.getUserId().getUser_id();
			}).setCaption("Usuario");

			this.routeGrid.addColumn(route -> {
				return route.getPoints().length;
			}).setCaption("Qtde. de Ordens").setWidth(100d);

			/*
			 * this.routeGrid.addColumn(route -> { String orders = ""; RoutePointInfo[]
			 * points = route.getPoints(); for (RoutePointInfo point : points) { orders +=
			 * point.getOrderId().getOrder_id() + " - "; } return orders;
			 * }).setCaption("No. de Ordem");
			 */

			this.routeGrid.addColumn(route -> route.getCurrentStatus().getTimeDate()).setCaption("Data");

			this.routeGrid.addColumn(route -> {
				String status = "";
				if (route.getCurrentStatus() != null && route.getCurrentStatus().getStatus() != null) {
					status = route.getCurrentStatus().getStatus().getName();
				}
				return status;
			}).setCaption("Status");

			this.routeGrid.setSizeFull();
			this.routeGrid.setItems(this.routes);

			mainLayout.addComponent(this.routeGrid);
			mainLayout.setExpandRatio(this.routeGrid, 0.2f);

		}

		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Visualizar Rota");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						listener.result(routeGrid.getSelectedItems().iterator().next());
					} catch (Exception e) {

					}
					close();
				}
			});

			Button btnCancel = new Button("Fechar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});

			// buttons.addComponent(btnShow);
			buttons.addComponent(btnConfirm);
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}
		{
			this.filter.addValueChangeListener(event -> filter());
			this.date.addValueChangeListener(event -> filter());
		}

		mainLayout.setSizeFull();
		setContent(mainLayout);

		setWidth("800px");
		setHeight("400px");
		setClosable(false);

		this.filter.focus();

		filter();

		center();
	}

	private void filter() {

		try {
			String valueToFind = this.filter.getValue();
			Date date1 = Date.from(this.date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("date", date1);

			StringTokenizer tk = new StringTokenizer(valueToFind);
			String like_str = "";
			while (tk.hasMoreTokens()) {
				String token = tk.nextToken();
				like_str = token + "%" + like_str;
			}

			params.put("ll", "%" + like_str + "%");
			params.put("ll1", "%" + like_str + "%");

			List<RouteInfo> routes = session.findEntities(
					"select r from RouteInfo as r where r.currentStatus.timeDate >= :date or r.userId.user_id like :ll or r.k_str like :ll1 order by r.currentStatus.timeDate desc",
					RouteInfo.class, params);

			this.routeGrid.setItems(routes);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setListener(RouteSearchListener listener) {
		this.listener = listener;
	}

	public static interface RouteSearchListener {
		public void result(RouteInfo route);
	}

}
