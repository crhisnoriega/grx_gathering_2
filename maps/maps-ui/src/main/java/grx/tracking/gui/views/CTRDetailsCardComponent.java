package grx.tracking.gui.views;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.persistence.CTRCreatorInfo;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.gui.windows.SentCTRWindow;

public class CTRDetailsCardComponent extends GridLayout {

	private static final long serialVersionUID = 1L;

	private CTRInfo ctr;

	private CTRCreatorInfo creator;

	public CTRDetailsCardComponent(CTRInfo ctr, CTRCreatorInfo creator) {
		super();
		this.ctr = ctr;
		this.creator = creator;
	}

	public void show() {
		GridLayout content = new GridLayout();
		content.addStyleName("my-card");
		content.setWidth("100%");

		{
			Label title = new Label(VaadinIcons.RECORDS.getHtml() + " CTR " + this.ctr.getId().split("-")[0],
					ContentMode.HTML);
			title.addStyleName(ValoTheme.LABEL_LARGE);
			title.addStyleName(ValoTheme.LABEL_BOLD);
			title.setHeight("40px");

			HorizontalLayout titleBar = new HorizontalLayout();
			titleBar.addComponent(new CheckBox());
			titleBar.addComponent(title);

			content.addComponent(titleBar);

		}

		{
			Label creator = new Label("Solicitante: " + this.creator.getName());
			content.addComponent(creator);
		}
		{
			Label address = new Label("Endereço: " + this.ctr.getEntrega().asAddressString());
			content.addComponent(address);
		}
		{
			Label creator = new Label("Date: " + this.ctr.getStatus().getDate());
			content.addComponent(creator);
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();
			buttons.setMargin(true);

			Button btn = new Button("Enviar CTR para Obra");
			btn.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			buttons.addComponent(btn);
			btn.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					SentCTRWindow window = new SentCTRWindow();
					window.show();
					window.setModal(true);
					UI.getCurrent().addWindow(window);
				}
			});

			Button btn0 = new Button("Enviar CTR para Gerador");
			btn0.addStyleName(ValoTheme.BUTTON_PRIMARY);
			buttons.addComponent(btn0);

			Button btn1 = new Button("Cancelar CTR");
			btn1.addStyleName(ValoTheme.BUTTON_DANGER);
			buttons.addComponent(btn1);

			content.addComponent(buttons);
			content.setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);
		}

		content.setWidth("100%");

		this.setWidth("100%");
		this.addComponent(content);
	}

}
