package grx.tracking.gui.listener;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Hashtable;

import com.github.appreciated.app.layout.builder.entities.DefaultNotification;
import com.github.appreciated.app.layout.builder.entities.DefaultNotificationHolder;

import grx.tracking.core.listener.GRXUIEventListener;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.gui.views.MapperView;
import grx.tracking.gui.views.OccurrencesManagementView;
import grx.tracking.gui.views.RoutesManagementView;
import grx.tracking.gui.windows.MyNotification;

public class GRXUIListener implements GRXUIEventListener {

	private MapperView mapView;
	private OccurrencesManagementView occurrView;
	private RoutesManagementView routeView;

	private DefaultNotificationHolder notifHolder;

	public GRXUIListener(MapperView mapView, OccurrencesManagementView occurrView, RoutesManagementView routeView,
			DefaultNotificationHolder notifHolder) {
		super();
		this.mapView = mapView;
		this.occurrView = occurrView;
		this.routeView = routeView;
		this.notifHolder = notifHolder;
	}

	@Override
	public void showOcurrence(String title, String message, String ocurrence_id, Hashtable<String, Object> props)
			throws Exception {
		try {
			MyNotification notification = new MyNotification(title, message);
			notification.setOcurrence_id(ocurrence_id);

			Date date = (Date) props.get("date");
			LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());

			notification.setTime1(ldt);

			this.notifHolder.addNotification(notification);
			this.occurrView.updateOccurrTable();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void drawMapObjectInMap(MapObject mapObject) throws Exception {
		try {
			System.out.println("drawMapObjectInMap: " + this);
			this.mapView.drawMapObjectInMap(mapObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateMapObjectInfo(MapObject mapObject) throws Exception {
		try {
			System.out.println("updateMapObjectInfo: " + this);
			this.mapView.updateMapObjectInfo(mapObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void removeMapObjectInMap(MapObject mapObject) throws Exception {
		try {
			System.out.println("removeMapObjectInMap: " + this);
			this.mapView.removeMapObject(mapObject);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void showNotificationInWindow(String title, String message, Hashtable<String, Object> props)
			throws Exception {
		try {
			System.out.println("showNotificationInWindow: " + this);
			this.mapView.showNotif(title, message, props);
			this.routeView.updateRouteTable(props.containsKey("route_id") ? props.get("route_id").toString() : "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doFilterMap(boolean update) throws Exception {
		try {
			System.out.println("doFilterMap: " + this);
			this.mapView.doFilterMap(update);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean isUIActive() {
		try {
			return this.mapView.isUIOK();
		} catch (Exception e) {

		}
		return false;
	}

}
