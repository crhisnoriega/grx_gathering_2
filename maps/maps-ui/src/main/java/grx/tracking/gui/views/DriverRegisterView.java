package grx.tracking.gui.views;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.vaadin.addons.autocomplete.converter.SuggestionCaptionConverter;
import org.vaadin.addons.autocomplete.converter.SuggestionValueConverter;
import org.vaadin.addons.autocomplete.generator.SuggestionGenerator;
import org.vaadin.addons.searchbox.SearchBox;
import org.vaadin.addons.searchbox.event.SearchEvent;
import org.vaadin.addons.searchbox.event.SearchListener;

import com.vaadin.data.Binder;
import com.vaadin.data.BinderValidationStatus;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import de.steinwedel.messagebox.MessageBox;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.PermissionDescr;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.PermissionDescr.PERMISSION_TYPE;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.util.MyConvertArray;
import grx.tracking.gui.samples.ResetButtonForTextField;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.windows.UsersWindow;
import grx.tracking.gui.windows.UsersWindow.SelectUser;

public class DriverRegisterView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	private UserInfo curretUser;

	private SearchBox searchBox;

	private GRXTrackingSession session;

	private Binder<UserInfo> binder;

	private TextField username;

	private boolean newUser = true;

	public DriverRegisterView(UI ui) {

		this.session = CurrentUser.get();
		this.curretUser = this.createEmptyUser();
		this.binder = new Binder<>();

		{
			HorizontalLayout topLayout = new HorizontalLayout();

			// Label lblSearch = new Label("Procurar Usuario");
			// lblSearch.addStyleName(ValoTheme.TEXTFIELD_ALIGN_RIGHT);

			this.searchBox = new SearchBox(VaadinIcons.SEARCH, SearchBox.ButtonPosition.LEFT);
			this.searchBox.setStyleName("filter-textfield");
			this.searchBox.setPlaceholder("digite nome do usuario");
			this.searchBox.setWidth("400px");
			// this.searchBox.addValueChangeListener(event -> filter());

			this.initSearchBox(this.searchBox);

			this.searchBox.focus();

			// topLayout.addComponent(lblSearch);
			// topLayout.setComponentAlignment(lblSearch,
			// Alignment.MIDDLE_RIGHT);

			Button mostrarTodos = new Button("Mostrar Todos");
			mostrarTodos.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					UsersWindow window = new UsersWindow(session);
					window.setModal(true);
					window.setListener(new SelectUser() {

						@Override
						public void confirm(UserInfo user) {
							showSelect(user);
						}
					});
					ui.addWindow(window);

				}
			});

			topLayout.addComponent(this.searchBox);
			topLayout.setComponentAlignment(searchBox, Alignment.MIDDLE_RIGHT);

			topLayout.addComponent(mostrarTodos);
			topLayout.setComponentAlignment(mostrarTodos, Alignment.MIDDLE_RIGHT);

			topLayout.setWidth("100%");
			topLayout.setExpandRatio(searchBox, 1);
			// topLayout.setExpandRatio(lblSearch, 1);
			topLayout.setStyleName("top-bar");

			addComponent(topLayout);
		}

		{
			this.username = new TextField("Login");
			this.username.setWidth("300px");
			this.binder.forField(username).asRequired("Nome do usuario invalido").withValidator(user -> {
				if (newUser) {
					try {
						Hashtable<String, Object> params = new Hashtable<>();
						params.put("username", user);
						List<UserInfo> ll = session.findEntities(
								"select u from UserInfo as u where u.userId.user_id = :username", UserInfo.class,
								params);
						return ll == null || ll.isEmpty();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				return true;
			} , "Usuario j� existe").bind(user -> user.getUserId().getUser_id(),
					(user, value) -> user.getUserId().setUser_id(value));

			addComponent(username);

		}
		{
			TextField name = new TextField("Nome do Usuario");
			name.setWidth("500px");
			this.binder.bind(name, UserInfo::getName, UserInfo::setName);
			addComponent(name);
		}
		{
			TextField email = new TextField("Email");
			email.setWidth("300px");
			this.binder.forField(email).withValidator(new EmailValidator("Email invalido")).bind(UserInfo::getEmail,
					UserInfo::setEmail);
			addComponent(email);
		}
		{
			PasswordField password = new PasswordField("Senha do Usuario");
			password.setWidth("300px");
			this.binder.bind(password, UserInfo::getPassword, UserInfo::setPassword);
			addComponent(password);
		}
		{
			ComboBox<USER_ROLE> comboRole = new ComboBox<>("Tipo de Usuario");
			comboRole.setItems(new USER_ROLE[] { USER_ROLE.DRIVER, USER_ROLE.CONTROLLER });
			comboRole.setItemCaptionGenerator(USER_ROLE::getName);
			comboRole.setEmptySelectionAllowed(false);
			this.binder.forField(comboRole).withValidator(value -> !value.getName().isEmpty(), "Nao pode ser vazio")
					.bind(user -> user.getRoles().length == 0 ? USER_ROLE.DRIVER : user.getRoles()[0],
							(user, value) -> {
								user.setRoles(new USER_ROLE[] { value });
							});
			addComponent(comboRole);
		}
		{
			final SectorInfo empty = new SectorInfo();
			empty.setName("Selecione uma Base");
			ComboBox<SectorInfo> comboSector = new ComboBox<>("Base");
			try {
				List<SectorInfo> sectors = session.findEntities("select s from SectorInfo as s", SectorInfo.class,
						new Hashtable<>());
				sectors.add(0, empty);
				comboSector.setItems(sectors);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			comboSector.setItemCaptionGenerator(SectorInfo::getName);
			comboSector.setEmptySelectionAllowed(false);
			this.binder.forField(comboSector)
					.withValidator(sector -> sector.getSectorId() != null, "Selecione uma Base").bind(user -> {
						if (user.getUserId().getSector_id() != null) {
							try {
								SectorInfo sector = session.findEntity(SectorInfo.class,
										user.getUserId().getSector_id());
								return sector == null ? empty : sector;
							} catch (Exception e) {
								// e.printStackTrace();
							}
						}
						return empty;
					} , (user, sector) -> {
						user.getUserId().setSector_id(sector.getSectorId());
					});
			comboSector.setWidth("300px");
			addComponent(comboSector);
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnNew = new Button("Novo Motorista");
			btnNew.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					curretUser = createEmptyUser();
					binder.readBean(curretUser);
					newUser = true;
				}
			});
			btnNew.setWidth("150px");

			Button btnSave = new Button("Salvar");
			btnSave.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			btnSave.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {

					try {

						if (MyConvertArray.cointais(curretUser.getRoles(), USER_ROLE.CONTROLLER)) {
							PermissionDescr perm = new PermissionDescr();
							perm.setType(PERMISSION_TYPE.ALLOW_SECTION);
							perm.setSector_id(curretUser.getUserId().getSector_id());
							curretUser.getPermissions().add(perm);
							System.out.println("adicionando permissao");
						}

						binder.writeBean(curretUser);

						session.saveEntity(curretUser.getUserId(), curretUser);

						binder.readBean(createEmptyUser());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			btnSave.setWidth("150px");

			buttons.addComponent(btnNew);
			buttons.addComponent(btnSave);

			buttons.setSizeUndefined();

			// setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		this.binder.readBean(this.curretUser);

	}

	private void initSearchBox(SearchBox searchBox2) {
		searchBox2.setSuggestionGenerator(new SuggestionGenerator<UserInfo>() {

			@Override
			public List<UserInfo> apply(String query, Integer limit) {
				List<UserInfo> suggestions = new ArrayList<>();

				binder.readBean(createEmptyUser());

				if (query.length() >= 0) {
					try {
						Hashtable<String, Object> params = new Hashtable<>();
						params.put("user", "%" + query + "%");
						List<UserInfo> users = session.findEntities(
								"select u from UserInfo as u where u.userId.user_id like :user", UserInfo.class,
								params);

						users.forEach(user -> suggestions.add(user));
					} catch (Exception e) {

					}
				}
				return suggestions;
			}
		}, new SuggestionValueConverter<UserInfo>() {

			@Override
			public String apply(UserInfo suggestion) {
				return suggestion.getUserId().getUser_id();
			}
		}, new SuggestionCaptionConverter<UserInfo>() {

			@Override
			public String apply(UserInfo user, String query) {
				return user.getUserId().getUser_id() + " - " + user.getName();
			}
		});

		searchBox2.addSearchListener(new SearchListener<UserInfo>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void search(SearchEvent<UserInfo> event) {
				UserInfo user = event.getSelectedItem().get();
				showSelect(user);
			}

		});

	}

	/*
	 * private void filter() { try { String valueToFind =
	 * this.searchBox.getValue();
	 * 
	 * if (valueToFind.isEmpty()) {
	 * this.binder.readBean(this.createEmptyUser()); return; }
	 * 
	 * Hashtable<String, Object> params = new Hashtable<>(); params.put("user",
	 * "%" + valueToFind + "%"); List<UserInfo> users = session.findEntities(
	 * "select u from UserInfo as u where u.userId.user_id like :user",
	 * UserInfo.class, params);
	 * 
	 * newUser = false; if (!users.isEmpty()) { for (UserInfo user : users) { if
	 * (user.getUserId().getUser_id().equals(valueToFind)) { this.curretUser =
	 * user; break; } else { this.curretUser = users.get(0); } }
	 * 
	 * this.binder.readBean(this.curretUser); } else {
	 * this.binder.readBean(this.createEmptyUser()); } } catch (Exception e) {
	 * e.printStackTrace(); }
	 * 
	 * }
	 */

	private UserInfo createEmptyUser() {
		UserInfo user = new UserInfo();
		user.setUserId(new UserId());
		user.setRoles(new USER_ROLE[] {});
		return user;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		View.super.enter(event);
	}

	private void showSelect(UserInfo user) {
		try {
			curretUser = user;
			binder.readBean(curretUser);
			newUser = false;
		} catch (Exception e) {
			e.printStackTrace();
			binder.readBean(createEmptyUser());
		}

		UI.getCurrent().access(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				searchBox.getSearchField().setValue("");
			}
		});
	}
}
