window.grx_tracking_gui_jsmaps_MyMaps = function() {

	var car = {
		svg : 'M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759 c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713 v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336 h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805z',
	};

	var arrow = {
		svg : 'M924.3,254C880.5,179,821,119.5,746,75.7C670.9,31.9,588.9,10,500,10c-88.9,0-170.9,21.9-246,65.7C179,119.5,119.5,179,75.7,254C31.9,329.1,10,411.1,10,500c0,88.9,21.9,170.9,65.7,246C119.5,821,179,880.5,254,924.3S411.1,990,500,990c88.9,0,170.9-21.9,246-65.7C821,880.5,880.5,821,924.3,746c43.8-75.1,65.7-157.1,65.7-246C990,411.1,968.1,329.1,924.3,254z M817.7,529.3l-231,231l-58.1,58.1c-7.7,7.7-17.2,11.5-28.7,11.5c-11.5,0-21-3.8-28.7-11.5l-58.1-58.1l-231-231c-7.7-7.7-11.5-17.2-11.5-28.7c0-11.5,3.8-21.1,11.5-28.7l58.1-58.1c8.1-8.1,17.7-12.1,28.7-12.1c11.1,0,20.6,4,28.7,12.1l120.6,120.6V214.2c0-11.1,4-20.6,12.1-28.7c8.1-8.1,17.7-12.1,28.7-12.1h81.7c11.1,0,20.6,4,28.7,12.1c8.1,8.1,12.1,17.7,12.1,28.7v320.3l120.6-120.6c7.7-7.7,17.2-11.5,28.7-11.5c11.5,0,21.1,3.8,28.7,11.5l58.1,58.1c7.7,7.7,11.5,17.2,11.5,28.7C829.2,512.1,825.4,521.7,817.7,529.3z',
	};

	var truck = {
		svg : 'M373.2,401.1c-30.2,0-54.8-24.6-54.8-54.8c0-30.2,24.6-54.8,54.8-54.8c30.2,0,54.8,24.6,54.8,54.8    C427.9,376.5,403.4,401.1,373.2,401.1z M373.2,318.9c-15.1,0-27.4,12.3-27.4,27.4c0,15.1,12.3,27.4,27.4,27.4    c15.1,0,27.4-12.3,27.4-27.4C400.6,331.2,388.3,318.9,373.2,318.9z',
	};

	var container = {
		svg : 'M489.739,166.957h-66.514l-62.571-84.145c7.601-8.785,12.216-20.223,12.216-32.725C372.87,22.468,350.405,0,322.785,0 s-50.089,22.468-50.089,50.087c0,27.619,22.468,50.087,50.087,50.087c2.932,0,5.8-0.267,8.595-0.754l50.219,67.536H130.403 l50.219-67.536c2.795,0.486,5.663,0.754,8.595,0.754c27.619,0,50.087-22.468,50.087-50.087C239.304,22.468,216.84,0,189.22,0 S139.13,22.468,139.13,50.087c0,12.502,4.615,23.939,12.216,32.725l-62.571,84.145H22.261c-9.22,0-16.696,7.475-16.696,16.696 v33.391c0,9.22,7.475,16.696,16.696,16.696h467.478c9.22,0,16.696-7.475,16.696-16.696v-33.391 C506.435,174.432,498.96,166.957,489.739,166.957z M41.079,267.13l28.79,211.196C72.5,497.522,89.098,512,108.478,512h295.043c19.38,0,35.978-14.478,38.609-33.695 			l28.79-211.174H41.079z M172.522,428.522c0,9.217-7.479,16.696-16.696,16.696c-9.217,0-16.696-7.479-16.696-16.696V317.217 			c0-9.217,7.479-16.696,16.696-16.696c9.217,0,16.696,7.479,16.696,16.696V428.522z M272.696,428.522 			c0,9.217-7.479,16.696-16.696,16.696s-16.696-7.479-16.696-16.696V317.217c0-9.217,7.479-16.696,16.696-16.696 			s16.696,7.479,16.696,16.696V428.522z M372.87,428.522c0,9.217-7.479,16.696-16.696,16.696c-9.217,0-16.696-7.479-16.696-16.696 			V317.217c0-9.217,7.479-16.696,16.696-16.696c9.217,0,16.696,7.479,16.696,16.696V428.522z',
	};

	var colors = [ "#df1fdf", "#df1fdf", "#df1fdf" ];

	var cssId = 'myCss';
	if (!document.getElementById(cssId)) {
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		link.id = cssId;
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.href = 'http://grxtrackingserver.ddns.net/tracking/VAADIN/snazzy-info-window.min.css';
		link.media = 'all';
		head.appendChild(link);
	}

	var jsId = 'myJs1';
	if (!document.getElementById(jsId)) {
		var head = document.getElementsByTagName('head')[0];
		var scriptjs = document.createElement('script');
		scriptjs.id = jsId;
		scriptjs.type = 'text/javascript';
		scriptjs.src = 'http://grxtrackingserver.ddns.net/tracking/VAADIN/snazzy-info-window.js';
		head.appendChild(scriptjs);
	}

	var jsId = 'myJs2';
	if (!document.getElementById(jsId)) {
		var head = document.getElementsByTagName('head')[0];
		var scriptjs = document.createElement('script');
		scriptjs.id = jsId;
		scriptjs.type = 'text/javascript';
		scriptjs.src = 'http://localhost:8080/tracking/VAADIN/snazzy-info-window.js';
		head.appendChild(scriptjs);
	}

	window.map_object = {};
	window.info_windows = {};
	window.routes = {};
	window.last_info_window = null;
	window.last_route = null;

	// /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////

	window.map = new google.maps.Map(document.getElementById('mymap'), {
		zoom : 11,
		center : {
			lat : -23.5489,
			lng : -46.6388
		},
		mapTypeControl : false,
		styles : [ {
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#1d2c4d"
			} ]
		}, {
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#8ec3b9"
			} ]
		}, {
			"elementType" : "labels.text.stroke",
			"stylers" : [ {
				"color" : "#1a3646"
			} ]
		}, {
			"featureType" : "administrative",
			"elementType" : "geometry",
			"stylers" : [ {
				"visibility" : "off"
			} ]
		}, {
			"featureType" : "administrative.country",
			"elementType" : "geometry.stroke",
			"stylers" : [ {
				"color" : "#4b6878"
			} ]
		}, {
			"featureType" : "administrative.land_parcel",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#64779e"
			} ]
		}, {
			"featureType" : "administrative.province",
			"elementType" : "geometry.stroke",
			"stylers" : [ {
				"color" : "#4b6878"
			} ]
		}, {
			"featureType" : "landscape.man_made",
			"elementType" : "geometry.stroke",
			"stylers" : [ {
				"color" : "#334e87"
			} ]
		}, {
			"featureType" : "landscape.natural",
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#023e58"
			} ]
		}, {
			"featureType" : "poi",
			"stylers" : [ {
				"visibility" : "off"
			} ]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#283d6a"
			} ]
		}, {
			"featureType" : "poi",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#6f9ba5"
			} ]
		}, {
			"featureType" : "poi",
			"elementType" : "labels.text.stroke",
			"stylers" : [ {
				"color" : "#1d2c4d"
			} ]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [ {
				"color" : "#023e58"
			} ]
		}, {
			"featureType" : "poi.park",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#3C7680"
			} ]
		}, {
			"featureType" : "road",
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#304a7d"
			} ]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [ {
				"visibility" : "off"
			} ]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#98a5be"
			} ]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.stroke",
			"stylers" : [ {
				"color" : "#1d2c4d"
			} ]
		}, {
			"featureType" : "road.highway",
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#2c6675"
			} ]
		}, {
			"featureType" : "road.highway",
			"elementType" : "geometry.stroke",
			"stylers" : [ {
				"color" : "#255763"
			} ]
		}, {
			"featureType" : "road.highway",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#b0d5ce"
			} ]
		}, {
			"featureType" : "road.highway",
			"elementType" : "labels.text.stroke",
			"stylers" : [ {
				"color" : "#023e58"
			} ]
		}, {
			"featureType" : "transit",
			"stylers" : [ {
				"visibility" : "off"
			} ]
		}, {
			"featureType" : "transit",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#98a5be"
			} ]
		}, {
			"featureType" : "transit",
			"elementType" : "labels.text.stroke",
			"stylers" : [ {
				"color" : "#1d2c4d"
			} ]
		}, {
			"featureType" : "transit.line",
			"elementType" : "geometry.fill",
			"stylers" : [ {
				"color" : "#283d6a"
			}, {
				"visibility" : "on"
			}, {
				"weight" : 4
			} ]
		}, {
			"featureType" : "transit.station",
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#3a4762"
			} ]
		}, {
			"featureType" : "water",
			"elementType" : "geometry",
			"stylers" : [ {
				"color" : "#0e1626"
			} ]
		}, {
			"featureType" : "water",
			"elementType" : "labels.text.fill",
			"stylers" : [ {
				"color" : "#4e6d70"
			} ]
		} ]
	});

	var self = this;

	google.maps.event.addListenerOnce(window.map, 'tilesloaded', function() {
		self.mapLoaded();
	});

	google.maps.event.addListener(window.map, 'click', function(event) {
		try {
			if (window.last_route != null) {
				removeRoute(window.last_route);
			}
		} catch (e) {

		}

		try {
			if (window.last_info_window != null) {
				window.last_info_window.close();
			}
		} catch (e) {

		}
	});

	// /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////

	this.clearMap = function(message) {
		for ( var key in window.map_object) {
			var marker = window.map_object[key];
			marker.setMap(null);
		}

		for ( var key in window.info_windows) {
			var window1 = window.info_windows[key];
			window1.close();
		}

		if (window.last_info_window != null) {
			window.last_info_window.close();
		}

		if (window.last_route != null) {
			removeRoute(window.last_route);
		}
	}

	this.focusObject = function(id, message) {
		var marker = window.map_object[id];
		if (marker != null) {
			map.setZoom(15);
			map.setCenter(marker.getPosition());
		}

		if (window.last_info_window != null) {
			window.last_info_window.close();
		}

		var infowindow = window.info_windows[id];
		if (infowindow != null) {
			window.last_info_window = infowindow;
			infowindow.open();
		}
	};

	this.removeById = function(id) {
		var object = window.map_object[id];
		if (object != null) {
			object.setMap(null);
		}

		var window1 = window.info_windows[id];
		if (window1 != null) {
			window1.close();
		}
	};

	var counter = 1;
	var lng_radius = 0.0008;
	var lat_to_lng = 111.23 / 71.7;
	var lat_radius = lng_radius / lat_to_lng;

	this.addOrderMarkerWindow = function(id, label, location_str, info, icon) {
		var location = JSON.parse(location_str);

		var image_marker = {
			url : 'http://grxtrackingserver.ddns.net/tracking/VAADIN/img/'
					+ icon,
			scaledSize : new google.maps.Size(40, 40), // scaled size
			origin : new google.maps.Point(0, 0), // origin
			anchor : new google.maps.Point(20, 40)

		};

		var position_loc = new google.maps.LatLng(parseFloat(location.lat),
				parseFloat(location.lng));
		
		var angle = 0.5 + counter * 2 * Math.PI / 20;
		// alert('angle: ' + angle);

		var lat1 = parseFloat(location.lat)
				+ (Math.sin(angle) * lat_radius);

		var lng1 = parseFloat(location.lng)
				+ (Math.cos(angle) * lng_radius);
		position_loc = new google.maps.LatLng(lat1, lng1);
		counter = counter + 1;

		
		var marker = new google.maps.Marker({
			position : position_loc,
			map : window.map,
			optimized : false,
			clickable : true,
			icon : image_marker
		});
		
		window.map_object[id] = marker;

		marker.addListener('click', function() {
			self.markerClicked(id, marker.getPosition());
		});

		var infowindow = new SnazzyInfoWindow({
			content : info,
			fontColor : '#fff',
			fontSize : '14px',
			marker : marker,
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			showCloseButton : true,
			closeOnMapClick : true,
			padding : '15px',
			backgroundColor : 'rgba(0, 0, 0, 0.7)',
			border : false,
			borderRadius : '10px',
			shadow : false,
		});

		marker.addListener('click', function() {
			if (window.last_info_window != null) {
				window.last_info_window.close();
			}
			infowindow.open();
			window.last_info_window = infowindow;
		});

		
		window.info_windows[id] = infowindow;
	};

	this.addOrderMarker = function(id, label, location_str, info, icon) {
		var location = JSON.parse(location_str);

		var image_marker = {
			url : 'http://grxtrackingserver.ddns.net/tracking/VAADIN/img/'
					+ icon,
			scaledSize : new google.maps.Size(40, 40), // scaled size
			origin : new google.maps.Point(0, 0), // origin
			anchor : new google.maps.Point(20, 40)

		};

		var position_loc = new google.maps.LatLng(parseFloat(location.lat),
				parseFloat(location.lng));

		// alert('chang0:' + position_loc);
		if (window.map.getBounds().contains(position_loc)) {

			// alert('chang1:' + position_loc);
			// alert('change1: ' + position_loc);
			var angle = 0.5 + counter * 2 * Math.PI / 20;
			// alert('angle: ' + angle);

			var lat1 = parseFloat(location.lat)
					+ (Math.sin(angle) * lat_radius);

			var lng1 = parseFloat(location.lng)
					+ (Math.cos(angle) * lng_radius);
			position_loc = new google.maps.LatLng(lat1, lng1);
			counter = counter + 1;

			// alert('change2: ' + position_loc);
		}

		// alert('chang3:' + position_loc);

		var marker = new google.maps.Marker({
			position : position_loc,
			map : window.map,
			optimized : false,
			clickable : true,
			icon : image_marker
		});
		
		window.map_object[id] = marker;

		marker.addListener('click', function() {
			self.markerClicked(id, marker.getPosition());
		});

		var infowindow = new SnazzyInfoWindow({
			content : info,
			fontColor : '#fff',
			fontSize : '14px',
			marker : marker,
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			showCloseButton : true,
			closeOnMapClick : true,
			padding : '15px',
			backgroundColor : 'rgba(0, 0, 0, 0.7)',
			border : false,
			borderRadius : '10px',
			shadow : false,
		});

		marker.addListener('click', function() {
			if (window.last_info_window != null) {
				window.last_info_window.close();
			}
			infowindow.open();
			window.last_info_window = infowindow;
		});

		
		window.info_windows[id] = infowindow;
	};

	this.addBaseMarker = function(id, label, location_str, info) {
		var location = JSON.parse(location_str);

		var image_marker = {
			url : 'http://grxtrackingserver.ddns.net/tracking/VAADIN/img/marker_base.png',
			scaledSize : new google.maps.Size(40, 40), // scaled size
			origin : new google.maps.Point(0, 0)
		// , origin
		// anchor : new google.maps.Point(20, 40)

		};

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			map : window.map,
			optimized : false,
			clickable : true
		// icon : image_marker
		});

		marker.addListener('click', function() {
			self.markerClicked(id, marker.getPosition());
		});

		var infowindow = new SnazzyInfoWindow({
			content : info,
			fontColor : '#fff',
			fontSize : '14px',
			marker : marker,
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			showCloseButton : true,
			closeOnMapClick : true,
			padding : '15px',
			backgroundColor : 'rgba(0, 0, 0, 0.7)',
			border : false,
			borderRadius : '10px',
			shadow : false,
		});

		marker.addListener('click', function() {
			if (window.last_info_window != null) {
				window.last_info_window.close();
			}
			infowindow.open();
			window.last_info_window = infowindow;
		});

		window.map_object[id] = marker;
		window.info_windows[id] = infowindow;
	};

	this.drawEncodedPolyline = function(id, line) {
		var decodedPath = google.maps.geometry.encoding.decodePath(line);
		var decodedLevels = decodeLevels("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");

		var path = new google.maps.Polyline({
			path : decodedPath,
			levels : decodedLevels,
			strokeColor : colors[Math.floor(Math.random() * 3)],
			strokeOpacity : 1.0,
			strokeWeight : 4,
			map : window.map
		});

		map_object[id] = path;

		google.maps.event.addListener(routePath, 'click', function() {
			self.routepathClicked(id, marker.getPosition());
		});
	};

	this.drawRoute = function(id, lines) {
		for (i = 0; i < lines.length; i++) {
			this.drawEncodedPolyline(id, lines[i]);
		}
	};

	this.removeRoute = function(id) {
		var route = map_object[id];
		if (route != null) {
			route.setMap(null);
		}
	};

	this.drawRoutePath = function(id, locations_array, distance) {
		/*
		 * if (window.last_route != null) { removeRoute(window.last_route); }
		 */

		var decodedPath = JSON.parse(locations_array);

		var routePath = new google.maps.Polyline({
			path : decodedPath,
			geodesic : true,
			strokeColor : colors[Math.floor(Math.random() * colors.length)],
			strokeOpacity : 1.0,
			strokeWeight : 4,
			map : window.map
		});

		map_object[id] = routePath;
		window.last_route = id;

		google.maps.event.addListener(routePath, 'click', function() {
			self.routePathClicked(id);
		});

		if (distance.length != 0) {
			var marker = new google.maps.Marker({
				position : decodedPath[Math.floor(decodedPath.length / 2)],
				map : window.map,
				optimized : false,
				clickable : true,
				visible : false
			});

			var infowindow = new google.maps.InfoWindow({
				content : distance
			});

			infowindow.open(window.map, marker);

			window.info_windows[id] = infowindow;
		}
	};

	this.removeTruckMarker = function(id) {
		var object = window.map_object[id];
		if (object != null) {
			object.setMap(null);
		}
		var window1 = window.info_windows[id];
		if (window1 != null) {
			window1.close();
		}

	};

	this.addTruckMarker = function(id, label, location_str, rotate_angle) {
		try {
			var old_marker = window.map_object[id];
			if (old_marker != null) {
				old_marker.setMap(null);
			}

			var old_window = window.info_windows[id];
			if (old_window != null) {
				old_window.close();
			}
		} catch (e) {
		}

		var location = JSON.parse(location_str);

		var image_driver = {
			url : 'http://grxtrackingserver.ddns.net/tracking/VAADIN/img/seta_azul.png',
			scaledSize : new google.maps.Size(40, 40), // scaled size
			origin : new google.maps.Point(0, 0), // origin
			anchor : new google.maps.Point(20, 20)
		};

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			map : window.map,
			optimized : false,
			clickable : true,
			icon : image_driver
		});

		marker.addListener('click', function() {
			self.driverClicked(id, marker.getPosition());
		});

		var infowindow = new google.maps.InfoWindow({
			content : label
		});

		marker.addListener('click', function() {
			infowindow.open(window.map, marker);
		});

		window.map_object[id] = marker;
		window.info_windows[id] = infowindow;
	};

	this.moveTruckMarker = function(id, label, location_str, rotate_angle) {
		var location = JSON.parse(location_str);
		var marker = window.map_object[id];

		// se nao tem marker, criar
		if (marker == null) {
			self.addTruckMarker(id, label, location_str, rotate_angle);
			marker = window.map_object[id];
		}

		marker.setPosition(new google.maps.LatLng(parseFloat(location.lat),
				parseFloat(location.lng)));

	};

	// /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////

	var directionsService = new google.maps.DirectionsService;
	var directionsDisplay = new google.maps.DirectionsRenderer({
		suppressInfoWindows : true,
		suppressMarkers : true,
		suppressBicyclingLayer : true,
		polylineOptions : {
			fillColor : '#FF0000',
			strokeOpacity : 1.0,
			strokeWeight : 10
		},
		travelMode : google.maps.TravelMode.DRIVING
	});

	directionsDisplay.setMap(window.map);

	this.calculateAndDisplayRoute = function(json) {
		directionsService.route({
			origin : 'tatuape, sp, brazil',
			destination : 'penha, sp, brazil',
			travelMode : 'DRIVING'
		}, function(response, status) {
			if (status === 'OK') {
				alert(JSON.stringify(response));
				console.log(JSON.stringify(response));
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
		});
	}

	this.drawGoogleRoute = function(json) {
		alert(json);
		var response = JSON.parse(json);

		var bounds = new google.maps.LatLngBounds(
				response.routes[0].bounds.southwest,
				response.routes[0].bounds.northeast);
		response.routes[0].bounds = bounds;

		response.routes[0].overview_path = google.maps.geometry.encoding
				.decodePath(response.routes[0].overview_polyline.points);

		alert(response.routes[0].overview_path);

		response.routes[0].legs = response.routes[0].legs.map(function(leg) {
			leg.start_location = new google.maps.LatLng(leg.start_location.lat,
					leg.start_location.lng);
			leg.end_location = new google.maps.LatLng(leg.end_location.lat,
					leg.end_location.lng);
			leg.steps = leg.steps.map(function(step) {
				step.path = google.maps.geometry.encoding
						.decodePath(step.polyline.points);
				step.start_location = new google.maps.LatLng(
						step.start_location.lat, step.start_location.lng);
				step.end_location = new google.maps.LatLng(
						step.end_location.lat, step.end_location.lng);
				return step;
			});
			return leg;
		});

		directionsDisplay.setDirections(response);
	}

	function decodeLevels(encodedLevelsString) {
		var decodedLevels = [];

		for (var i = 0; i < encodedLevelsString.length; ++i) {
			var level = encodedLevelsString.charCodeAt(i) - 63;
			decodedLevels.push(level);
		}
		return decodedLevels;
	}

	// /////////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////
	// /////////////////////////////////////////////////////////////////

	this.addDropMarker = function(id, label, location_str, info) {
		var location = JSON.parse(location_str);

		var marker = new google.maps.Marker({
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			map : window.map,
			optimized : false,
			clickable : true,
			draggable : true,
			animation : google.maps.Animation.DROP,
		});

		window.map.setZoom(18);
		window.map.setCenter(marker.getPosition());

		marker.addListener('dragend', function(event) {
			var event_position = new google.maps.LatLng(event.latLng.lat(),
					event.latLng.lng());
			self.markerDragEnd(id, event_position);
		});

		var infowindow = new SnazzyInfoWindow({
			content : info,
			fontColor : '#fff',
			fontSize : '14px',
			marker : marker,
			position : new google.maps.LatLng(parseFloat(location.lat),
					parseFloat(location.lng)),
			showCloseButton : true,
			closeOnMapClick : false,
			padding : '15px',
			backgroundColor : 'rgba(0, 0, 0, 0.7)',
			border : false,
			borderRadius : '10px',
			shadow : false,
		});

		marker.addListener('click', function() {
			infowindow.open();
		});

		window.map_object[id] = marker;
		window.info_windows[id] = infowindow;
	};
};