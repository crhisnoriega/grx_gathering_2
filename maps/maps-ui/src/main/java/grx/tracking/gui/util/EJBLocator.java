package grx.tracking.gui.util;

import javax.naming.InitialContext;

import grx.tracking.core.ejb.GRXEventService;
import grx.tracking.core.ejb.GRXTrackingService;

public class EJBLocator {

	public static GRXTrackingService refGRXTrackingService() throws Exception {
		InitialContext ctxt = new InitialContext();
		return (GRXTrackingService) ctxt.lookup(
				"java:global/grxtracking/grxtrackingejb/GRXTrackingServiceEJB!grx.tracking.core.ejb.GRXTrackingService");
	}

	public static GRXEventService refGRXEventService() throws Exception {
		InitialContext ctxt = new InitialContext();
		return (GRXEventService) ctxt.lookup(
				"java:global/grxtracking/grxtrackingejb/GRXEventServiceEJB!grx.tracking.core.ejb.GRXEventService");
	}

}
