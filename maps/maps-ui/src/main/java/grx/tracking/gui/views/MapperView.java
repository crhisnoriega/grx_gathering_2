package grx.tracking.gui.views;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.UIDetachedException;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import elemental.json.JsonObject;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.maps.MapModel;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.maps.MapObject.MAP_OBJECT_TYPE;
import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.CTRStatus.CTR_STATUS;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.LastKnowUserLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.RouteStatus.ROUTE_STATUS;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.gui.jsmaps.MapListener;
import grx.tracking.gui.jsmaps.MyMaps;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyDialogs;
import grx.tracking.gui.windows.RouteOnMapInfoLayout;
import grx.tracking.gui.windows.RouteOnMapInfoLayout.RouteOnMapListener;
import grx.tracking.gui.windows.RoutesSearchWindow;
import grx.tracking.gui.windows.RoutesSearchWindow.RouteSearchListener;

public class MapperView extends AbsoluteLayout implements View {

	private static final long serialVersionUID = 1L;
	private UI ui;
	private MyMaps myMap;

	private MapModel mapModel;
	private List<MapObject> objectsToShow;
	private CTR_STATUS ctrStatus;
	private ORDER_TYPE orderType;
	private GRXTrackingSession session;
	private DateField date;
	private RouteOnMapInfoLayout info;

	private List<String> last_route_ids;

	public MapperView(UI ui, MapModel mapModel) {
		setSizeFull();
		addStyleName("crud-view");

		this.ui = ui;
		// this.mapModel = mapModel;
		// this.objectsToShow = this.mapModel.getObjects();

		this.last_route_ids = new ArrayList<>();

		this.session = CurrentUser.get();

		this.myMap = new MyMaps();

		this.myMap.setSizeFull();
		this.myMap.setId("mymap");
		this.myMap.setListener(new MyMapListener());

		this.addComponent(this.myMap);

		{
			ComboBox<CTR_STATUS> statusCombo = new ComboBox<>();
			statusCombo.setItems(CTR_STATUS.values());
			statusCombo.setItemCaptionGenerator(CTR_STATUS::getName);
			statusCombo.setEmptySelectionAllowed(false);
			statusCombo.setSelectedItem(CTR_STATUS.SELECT);
			statusCombo.setWidth("200px");
			statusCombo.addSelectionListener((event -> {
				ctrStatus = event.getValue();
				filterMapStatus(false);
			}));
			ctrStatus = CTR_STATUS.SELECT;

			this.addComponent(statusCombo, "left: 10px; top: 10px;");
		}
		
		{
			this.date = new DateField();
			this.date.setWidth("150px");
			this.date.setDateFormat("dd/MM/yyyy");
			this.date.setValue(LocalDate.now());
			this.date.addValueChangeListener(new ValueChangeListener<LocalDate>() {

				@Override
				public void valueChange(ValueChangeEvent<LocalDate> event) {
					filterMapDate(false);
				}
			});

			this.addComponent(this.date, "left: 215px; top: 10px;");
		}
		
		{
			Button btnRefresh = new Button(VaadinIcons.REFRESH);
			btnRefresh.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					filterMapDate(false);
				}
			});

			this.addComponent(btnRefresh, "right: 55px; top: 10px;");
		}
		{

			this.info = new RouteOnMapInfoLayout(ui);
			this.info.setListener(new RouteOnMapListener() {

				@Override
				public void focusObject(String id) {
					myMap.focusObject(id, "");
				}

				@Override
				public void filterMapAndClose() {
					info.setVisible(false);
					filterMapDate(false);
					selectedRouteUserId = null;
				}
			});
			this.info.setVisible(false);

			this.addComponent(this.info, "left: 0px; top: 50px;");
		}
		this.setSizeFull();

		System.out.println("ending constructor");

	}

	public void refreshMapModel(UserId userId, Date date) throws Exception {
		Hashtable<String, Object> params = new Hashtable<>();
		params.put("userId", userId);
		params.put("date", date);
		this.mapModel = EJBLocator.refGRXEventService().getMapModel(params);
		this.objectsToShow = this.mapModel.getObjects();
		System.out.println("refresh map model: " + this.mapModel.getObjects());
	}

	public void showOrdersInfoWindow(RouteInfo route) throws Exception {
		RoutePointInfo[] points = route.getPoints();
		List<OrderInfo> items = new ArrayList<>();
		for (RoutePointInfo point : points) {
			OrderInfo order = session.findEntity(OrderInfo.class, point.getOrderId());
			if (order != null) {
				items.add(order);
			}
		}
		this.info.populateWindow(route, items);
		this.info.setVisible(true);
	}

	public void filterMapDate(boolean showNotif) {
		Date dateRequested = Date.from(this.date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

		try {
			this.refreshMapModel(this.session.getUser().getUserId(), dateRequested);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.filterMapStatus(showNotif);
	}

	public void filterMapStatus(boolean showNotif) {

		List<MapObject> objects = mapModel.getObjects();
		objectsToShow = objects.stream().filter(object -> {
			if (object.getType().equals(MAP_OBJECT_TYPE.ORDER)) {
				return true;
			}
			return true;
		}).collect(Collectors.toList());

		// objectsToShow.forEach(object -> {
		// System.out.println("status:" + object.getOrderStatus());
		// });

		this.myMap.clearMap();
		drawModelMap();

	}

	@Override
	public void enter(ViewChangeEvent event) {
		System.out.println("MapperView parameters: " + event.getParameters());

		/*
		 * try {
		 * 
		 * UserId userId = CurrentUser.get().getUser().getUserId(); this.mapModel =
		 * EJBLocator.refGRXEventService().getMapModel(userId); this.objectsToShow =
		 * this.mapModel.getObjects();
		 * 
		 * drawModelMap(); } catch (Exception e1) { e1.printStackTrace(); }
		 */
	}

	public boolean isUIOK() {
		try {
			ui.access(() -> {
			});
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private void executeInUIThread(Runnable r) {
		for (int i = 0; i < 3; i++) {
			try {
				ui.access(r);
				break;
			} catch (UIDetachedException e) {
				System.err.println("UIDetachedException ui: " + ui);
				break;
			} catch (Exception e) {
				System.err.println("executeInUIThread exception type:" + e.getClass() + " localizedMessage: "
						+ e.getLocalizedMessage() + " message: " + e.getMessage());
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
				}
			}
		}
	}

	private void removeObjectByType(MapObject mapObject) {
		if (mapObject.getType().equals(MAP_OBJECT_TYPE.DRIVER)) {
			executeInUIThread(() -> myMap.removeTruckMarker(mapObject.getId()));
		}
	}

	public void doFilterMap(boolean update) {
		this.filterMapStatus(update);
	}

	public void updateMapObjectInfo(MapObject mapObject) {
		if (mapObject != null) {
			System.out.println("%%%%% mapObject: " + mapObject.getId() + " ss: " + mapObject.getCtrStatus());
		}

		List<MapObject> modelObjects = this.mapModel.getObjects();
		if (modelObjects.contains(mapObject)) {
			System.out.println("removing: " + mapObject.getId());
			modelObjects.remove(mapObject);
		}
		modelObjects.add(mapObject);

		if (this.info.isVisible() && MapObject.MAP_OBJECT_TYPE.ORDER.equals(mapObject.getType())) {
			new Thread(() -> {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				ui.access(() -> {
					try {
						OrderInfo order = session.findEntity(OrderInfo.class, mapObject.getOrderId());
						if (order != null) {
							info.updateOrderInfo(order);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				});

			}).start();
		}

		/*
		 * if (mapObject.getType().equals(MAP_OBJECT_TYPE.ORDER)) {
		 * 
		 * String content = "<div class=\"custom-header\"> Pedido: " +
		 * mapObject.getOrderId().getOrder_id() + "</div>" +
		 * "<div class=\"custom-header\"> Status: " +
		 * mapObject.getOrderStatus().getName() + "</div>" +
		 * "<div class=\"custom-body\">" + mapObject.getDescription() + "</div>";
		 * 
		 * executeInUIThread(() -> { if (mapObject.getLocation() != null) {
		 * 
		 * String icon = ""; if (ORDER_TYPE.DELIVERY.equals(mapObject.getOrderType())) {
		 * icon = "marker_green_icon.png"; } if
		 * (ORDER_TYPE.PICK.equals(mapObject.getOrderType())) { icon =
		 * "marker_red_icon.png"; } if
		 * (ORDER_TYPE.EXCHANGE.equals(mapObject.getOrderType())) { icon =
		 * "marker_green_icon.png"; } myMap.removeById(mapObject.getId());
		 * myMap.addOrderMarker(mapObject.getId(), "nao_usado", mapObject.getLocation(),
		 * content, icon); } }); }
		 */

	}

	private void drawObjectByType(MapObject mapObject) {
		if (mapObject.getType().equals(MAP_OBJECT_TYPE.DRIVER)) {
			executeInUIThread(
					() -> myMap.addTruckMarker(mapObject.getId(), mapObject.getLabel(), mapObject.getLocation()));
		}

		if (mapObject.getType().equals(MAP_OBJECT_TYPE.BASE)) {

			String content = "<div class=\"custom-header\"> Base:" + mapObject.getLabel() + "</div>"
					+ "<div class=\"custom-body\">" + mapObject.getDescription() + "</div>";

			executeInUIThread(
					() -> myMap.addBaseMarker(mapObject.getId(), "nao_usado", mapObject.getLocation(), content));
		}

		if (mapObject.getType().equals(MAP_OBJECT_TYPE.ORDER)) {
			System.out.println("drawing order: " + mapObject);

			String content = "<div class=\"custom-header\"> CTR: " + mapObject.getId().split("-")[0] + "</div>"
					+ "<div class=\"custom-header\"> Status: " + mapObject.getCtrStatus().getName() + "</div>"
					+ "<div class=\"custom-header\"> Gerador: " + mapObject.getGerator() + "</div>"
					+ "<div class=\"custom-body\">" + mapObject.getDescription() + "</div>";

			executeInUIThread(() -> {
				if (mapObject.getLocation() != null) {

					String icon = "";
					if (ORDER_TYPE.DELIVERY.equals(mapObject.getOrderType())) {
						icon = "marker_green_icon.png";
					}
					if (ORDER_TYPE.PICK.equals(mapObject.getOrderType())) {
						icon = "marker_red_icon.png";
					}
					if (ORDER_TYPE.EXCHANGE.equals(mapObject.getOrderType())) {
						icon = "marker_green_icon.png";
					}
					myMap.addOrderMarker(mapObject.getId(), "nao_usado", mapObject.getLocation(), content, icon);
				}
			});
		}

		if (mapObject.getType().equals(MAP_OBJECT_TYPE.ROUTE)) {

			List<GoogleRouteInfo> routes = mapObject.getRoute();
			for (int index = 0; index < routes.size(); index++) {
				GoogleRouteInfo route = routes.get(index);
				List<GeoLocation> locations = route.getLocationRepresentation();

				String distance_format = route.getDistance();
				try {
					distance_format = Long.parseLong(distance_format) / 1000L + " km";
				} catch (Exception e) {

				}

				final String dd = distance_format;

				String idroute = mapObject.getId() + "_" + index;

				this.last_route_ids.add(idroute);

				executeInUIThread(() -> myMap.drawRoutePath(idroute, locations, "0 km".equals(dd) ? "" : dd));
			}
		}
	}

	public void drawModelMap() {
		this.objectsToShow.forEach(mapObject -> {
			drawObjectByType(mapObject);
		});
	}

	public void showGeoLocation(String message, GeoLocation location) throws Exception {

	}

	public void drawMapObjectInMap(MapObject mapObject) {
		drawObjectByType(mapObject);
	}

	public void removeMapObject(MapObject mapObject) {
		removeObjectByType(mapObject);
	}

	public void showNotif(String title, String message, Hashtable<String, Object> props) {
		ui.access(() -> {
			Notification notification = new Notification(title, "Nova rota: <a href=\"/tracking/routes_to_aproval/"
					+ props.get("route_id") + "\">" + props.get("route_id") + "</a>" + message,
					Notification.Type.TRAY_NOTIFICATION);
			notification.setHtmlContentAllowed(true);
			notification.setDelayMsec(5000);
			notification.setIcon(VaadinIcons.NEWSPAPER);
			notification.addCloseListener(event -> {
			});

			notification.show(ui.getPage());
		});
	}

	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////

	private UserId selectedRouteUserId;

	private class MyMapListener implements MapListener {
		@Override
		public void mapLoaded() {
			// drawModelMap();
			filterMapDate(false);
			System.out.println("loading after mapLoaded... ");
		}

		@Override
		public void functionCalled(String functionName, String id, JsonObject jsonObject) {

			try {
				if ("driverClicked".equals(functionName)) {
					try {

						String[] part_id = id.split("\\|");
						UserId userId = new UserId(part_id[0], part_id[1]);

						selectedRouteUserId = userId;

						RouteInfo route = EJBLocator.refGRXTrackingService().getUserSessionEJB(userId)
								.getCurrentRoute();

						MapperView.this.myMap.clearMap();

						if (route != null) {
							MapperView.this.drawRoute(route);
						} else {
							removeLastRouteObjects();
							MyDialogs.showError("Rota do motorista", "O motorista nao possui rota designada.");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if ("markerClick".equals(functionName)) {

					if (info.isVisible()) {
						String[] part_id = id.split("\\|");
						if (part_id.length == 2) {
							OrderId orderId = new OrderId();
							orderId.setOrder_id(part_id[0]);
							orderId.setSector_id(part_id[1]);

							OrderInfo order = new OrderInfo();
							order.setOrderId(orderId);

							info.select(order);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class RouteSearchResult implements RouteSearchListener {
		private final RoutesSearchWindow window;

		private RouteSearchResult(RoutesSearchWindow window) {
			this.window = window;
		}

		@Override
		public void result(RouteInfo route) {
			ui.access(() -> {
				try {
					myMap.clearMap();

					MapperView.this.drawRoute(route);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
	}

	public void drawArbitraryRoute(GeoLocation start, GeoLocation end, UserId userId) {
		try {
			GoogleRouteInfo route1 = EJBLocator.refGRXTrackingService().findGoogleRoute(start, end);
			RouteInfo route12 = new RouteInfo();
			route12.setId("route_" + UUID.randomUUID().toString().split("-")[0]);
			route12.setUserId(userId);

			RoutePointInfo point12 = new RoutePointInfo();
			point12.setGoogleRoute(route1);
			route12.setPoints(new RoutePointInfo[] { point12 });

			drawObjectByType(MapObject.createMapObject(route12));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeLastRouteObjects() {
		if (this.last_route_ids != null && !this.last_route_ids.isEmpty()) {
			this.last_route_ids.forEach(id -> myMap.removeById(id));
		}
	}

	private void drawRoute(RouteInfo route) throws Exception {
		this.removeLastRouteObjects();

		this.last_route_ids = new ArrayList<>();

		RoutePointInfo[] points = route.getPoints();

		for (int index = 0; index < points.length; index++) {
			RoutePointInfo current = points[index];
			GoogleRouteInfo googleRoute = current.getGoogleRoute();
			System.out.println("gr: " + googleRoute);
			System.out.println("s: " + googleRoute.getStartOrder());
			System.out.println("e: " + googleRoute.getEndOrder());
		}

		OrderInfo currentOrder = null;
		OrderInfo lastOrder = null;

		// draw truck
		UserInfo user = session.findEntity(UserInfo.class, route.getUserId());
		LastKnowUserLocation lastlocation = session.findEntity(LastKnowUserLocation.class, user.getUserId());

		if (lastlocation == null) {
			MyDialogs.showError("Visualizar Rota",
					"Nao foi possivel encontrar posiçao do usuario da rota: " + route.getUserId());
			System.out.println("dont found last location for user: " + route.getUserId());
			return;
		}

		MapperView.this.drawObjectByType(MapObject.createMapObject(user, lastlocation.getGeoLocation()));

		MapperView.this.showOrdersInfoWindow(route);

		// draw orders
		for (RoutePointInfo point : route.getPoints()) {
			OrderInfo order = session.findEntity(OrderInfo.class, point.getOrderId());
			if (order != null) {
				MapObject order_object = MapObject.createMapObject(order);

				this.last_route_ids.add(order_object.getId());

				drawObjectByType(order_object);

				if ((order.getCurrentStatus().getStatus().equals(ORDER_STATUS.ROUTING)
						|| order.getCurrentStatus().getStatus().equals(ORDER_STATUS.RESERVED))
						&& currentOrder == null) {
					currentOrder = order;
				}

				if ((order.getCurrentStatus().getStatus().equals(ORDER_STATUS.ROUTING)
						|| order.getCurrentStatus().getStatus().equals(ORDER_STATUS.RESERVED))
						&& point.equals(points[points.length - 1])) {
					lastOrder = order;
				}
			}
		}

		// draw route per se
		MapperView.this.drawObjectByType(MapObject.createMapObject(route));

		{

			System.out.println("currentOrder: " + currentOrder);
			// mostrar rota do veiculo ate o primeiro pedido
			if (currentOrder != null) {
				drawArbitraryRoute(lastlocation.getGeoLocation(), currentOrder.getAddress().getLocation(),
						route.getUserId());
			}

			SectorInfo endSector = session.findEntity(SectorInfo.class, route.getEndSectorId());
			MapperView.this.drawObjectByType(MapObject.createMapObject(endSector));

			System.out.println("lastOrder: " + lastOrder);
			// mostrar rota ate a base
			if (lastOrder != null) {
				drawArbitraryRoute(lastOrder.getAddress().getLocation(), endSector.getLocation(), route.getUserId());
			}
		}
	}

}
