package grx.tracking.gui.windows;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import org.ocpsoft.prettytime.PrettyTime;

import com.github.appreciated.app.layout.builder.entities.DefaultNotification;

public class MyNotification extends DefaultNotification {

	private String ocurrence_id;
	private LocalDateTime time1;

	public MyNotification(String title, String description) {
		super(title, description);
	}

	public void setTime1(LocalDateTime time1) {
		this.time1 = time1;
	}

	@Override
	public LocalDateTime getTime() {
		return time1;
	}

	public String getOcurrence_id() {
		return ocurrence_id;
	}

	public void setOcurrence_id(String ocurrence_id) {
		this.ocurrence_id = ocurrence_id;
	}

	@Override
	public String getTimeAgo() {
		return new PrettyTime().format(Date.from(time1.atZone(ZoneId.systemDefault()).toInstant()));
	     
	}
}
