package grx.tracking.gui.samples.authentication;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyDialogs;

/**
 * Default mock implementation of {@link AccessControl}. This implementation
 * accepts any string as a password, and considers the user "admin" as the only
 * administrator.
 */
public class BasicAccessControl implements AccessControl {

	@Override
	public boolean signIn(String username, String password) {
		if (username == null || username.isEmpty()) {
			return false;
		}

		try {
			UserInfo user = EJBLocator.refGRXTrackingService().authenticate(username, password, "",
					USER_ROLE.CONTROLLER, "v1.0.6");
			GRXTrackingSession session = EJBLocator.refGRXTrackingService().getUserSessionEJB(user.getUserId());
			CurrentUser.set(session);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			MyDialogs.showError("Erro no Login", e.getLocalizedMessage());
		}

		return false;
	}

	@Override
	public boolean isUserSignedIn() {
		return CurrentUser.get() != null;
	}

	@Override
	public boolean isUserInRole(String role) {
		if ("admin".equals(role)) {
			// Only the "admin" user is in the "admin" role
			return getPrincipalName().equals("admin");
		}

		// All users are in all non-admin roles
		return true;
	}

	@Override
	public String getPrincipalName() {
		return CurrentUser.get().getUser().getName();
	}

}
