package grx.tracking.gui.views;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import org.vaadin.teemu.switchui.Switch;

import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.util.MyConfig;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.windows.OrdersWindow;
import grx.tracking.gui.windows.OrdersWindow.OrderConfirmListener;

public class RoutesManagementView extends VerticalLayout implements View {

	public static Logger logger = Logger.getLogger(RoutesManagementView.class.getCanonicalName());

	private static final long serialVersionUID = 1L;

	private GRXTrackingSession session;

	private Grid<RouteInfo> routeGrid;
	public List<RouteInfo> okRoutes;

	private DateField date;

	private MyConfig config;
	private Timer timer;

	private List<RouteInfo> routes;

	private UI ui;

	public RoutesManagementView(UI ui) {
		super();
		this.ui = ui;
		this.config = new MyConfig();
		this.session = CurrentUser.get();
		this.okRoutes = new ArrayList<>();
		{
			HorizontalLayout buttons = new HorizontalLayout();

			this.date = new DateField();
			this.date.setWidth("150px");
			this.date.setDateFormat("dd/MM/yyyy");
			this.date.setValue(LocalDate.now());
			this.date.addValueChangeListener(new ValueChangeListener<LocalDate>() {

				@Override
				public void valueChange(ValueChangeEvent<LocalDate> event) {
					filter();
				}
			});
			buttons.addComponent(this.date);

			Switch s = new Switch("Aprovação Automática de Rotas");
			s.addValueChangeListener(new ValueChangeListener<Boolean>() {

				@Override
				public void valueChange(ValueChangeEvent<Boolean> event) {
					String vv = s.getValue() + "";
					config.setConfirmRoutes(vv);
					try {
						config.save();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			if (this.timer != null) {
				this.timer.cancel();
				this.timer.purge();
			}

			this.timer = new Timer();
			this.timer.schedule(new TimerTask() {

				@Override
				public void run() {
					ui.access(new Runnable() {

						@Override
						public void run() {
							try {
								config.load();
							} catch (Exception e) {
								e.printStackTrace();
							}
							s.setValue(Boolean.parseBoolean(config.getConfirmRoutes()));
						}
					});

				}
			}, 0, 2000L);

			s.setValue(Boolean.parseBoolean(config.getConfirmRoutes()));

			buttons.addComponent(s);
			buttons.setComponentAlignment(s, Alignment.MIDDLE_LEFT);

			Button btnRefresh = new Button(VaadinIcons.REFRESH);
			btnRefresh.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					filter();
				}
			});
			buttons.addComponent(btnRefresh);

			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);

		}

		{
			this.routeGrid = this.createGrid();
			this.routeGrid.setSizeFull();
			this.routeGrid.addSelectionListener(new SelectionListener<RouteInfo>() {

				private static final long serialVersionUID = 1L;

				@Override
				public void selectionChange(SelectionEvent<RouteInfo> event) {

				}
			});

			addComponent(this.routeGrid);
			setExpandRatio(this.routeGrid, 0.2f);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnShow = new Button("Pedidos da Rota");
			btnShow.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					Set<RouteInfo> selected = routeGrid.getSelectedItems();
					if (!selected.isEmpty()) {

						OrdersWindow window = new OrdersWindow(selected.iterator().next(), session);
						window.setModal(true);
						window.setListener(new OrderConfirmListener() {
							@Override
							public void confirm(RouteInfo route) {
								try {
									session.recalculateRoute(route);

									RoutePointInfo[] points = route.getPoints();
									for (RoutePointInfo point : points) {
										System.out.println("point: " + point.getOrderId().getOrder_id());
										GoogleRouteInfo gr = point.getGoogleRoute();
										if (gr != null) {
											System.out.println("s: " + gr.getStartOrder().getOrderId().getOrder_id()
													+ " e: " + gr.getEndOrder().getOrderId().getOrder_id());
										}
									}

									EJBLocator.refGRXTrackingService().confirmRoute(route, route.getUserId(),
											"OK_FOR_WAITING");

									// routeGrid.getDataProvider().refreshItem(route1);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});
						window.center();
						ui.addWindow(window);
					}

					/*
					 * RouteInfo routeInfo = routeGrid.getSelectedItems().iterator().next();
					 * ui.getNavigator().navigateTo("/rotas/" + routeInfo.getId());
					 */
				}
			});

			Button btnConfirm = new Button("Confirmar Rotas");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						okRoutes.forEach(route -> {
							try {
								EJBLocator.refGRXTrackingService().confirmRoute(route, route.getUserId(),
										"OK_FOR_WAITING");
								route = session.findEntity(RouteInfo.class, route.getId());

							} catch (Exception e) {
								e.printStackTrace();
							}
						});
						okRoutes.clear();
						routeGrid.getDataProvider().refreshAll();
						filter();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			Button btnRemove = new Button("Cancelar Rotas");
			btnRemove.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						okRoutes.forEach(route -> {
							try {
								EJBLocator.refGRXTrackingService().cancelRoute(route.getId(), route.getUserId(), true,
										true);
								route = session.findEntity(RouteInfo.class, route.getId());
							} catch (Exception e) {
								e.printStackTrace();
							}
						});
						okRoutes.clear();
						routeGrid.getDataProvider().refreshAll();
						filter();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			buttons.addComponent(btnShow);
			buttons.addComponent(btnRemove);
			buttons.addComponent(btnConfirm);

			buttons.setSizeUndefined();

			setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		filter();

		setSizeFull();
	}

	private Grid<RouteInfo> createGrid() {
		Grid<RouteInfo> grid = new Grid<>();

		grid.addComponentColumn(route -> {
			CheckBox check = new CheckBox();
			check.addValueChangeListener(new ValueChangeListener<Boolean>() {

				private static final long serialVersionUID = 1L;

				@Override
				public void valueChange(ValueChangeEvent<Boolean> event) {
					if (event.getValue() == false && okRoutes.contains(route)) {
						okRoutes.remove(route);
					}

					if (event.getValue() == true && !okRoutes.contains(route)) {
						okRoutes.add(route);
					}
				}
			});
			check.setValue(okRoutes.contains(route));
			return check;
		}).setCaption("#").setWidth(60d);

		grid.addColumn(route -> route.getId().split("-")[0]).setCaption("ID da Rota");
		grid.addColumn(route -> route.getCurrentStatus().getStatus().getName()).setCaption("Status");
		grid.addColumn(route -> route.getCurrentStatus().getTimeDate()).setCaption("Data Atualização");
		grid.addColumn(route -> {
			return route.getUserId().getUser_id();
		}).setCaption("Motorista ID");

		return grid;
	}

	private void filter() {
		try {

			Date date1 = Date.from(this.date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("date", date1);
			this.routes = this.session.findEntities(
					"select r from RouteInfo as r where r.currentStatus.timeDate >= :date order by r.currentStatus.timeDate desc",
					RouteInfo.class, params);
			this.routeGrid.setItems(this.routes);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void enter(ViewChangeEvent event) {
		logger.info("enter view event.getParameters(): " + event.getParameters());
	}

	public void updateRouteTable(String route_id) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				ui.access(new Runnable() {

					@Override
					public void run() {
						filter();

						routes.forEach(route -> {
							if (route.getId().equals(route_id)) {
								routeGrid.select(route);
								return;
							}
						});

					}
				});
			}
		});
		t.start();
	}
}
