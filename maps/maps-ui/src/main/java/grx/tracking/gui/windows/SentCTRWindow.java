package grx.tracking.gui.windows;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.vaadin.data.Binder;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import grx.tracking.core.persistence.SentCTRInfo;

public class SentCTRWindow extends Window {

	private Binder<SentCTRInfo> binderSent;

	public SentCTRWindow() {
		this.binderSent = new Binder<SentCTRInfo>(SentCTRInfo.class);
	}

	public void show() {
		GridLayout mainLayout = new GridLayout();
		mainLayout.setMargin(true);
		{
			Label title = new Label(VaadinIcons.BUILDING.getHtml() + " Enviar CTR para Obra", ContentMode.HTML);
			title.addStyleName(ValoTheme.LABEL_LARGE);
			title.addStyleName(ValoTheme.LABEL_BOLD);
			title.setHeight("40px");

			mainLayout.addComponent(title);
		}

		{
			HorizontalLayout row1 = new HorizontalLayout();
			{
				DateField sentDate = new DateField("Data de Envio");
				this.binderSent.forField(sentDate).asRequired().bind(sent -> {
					return sent.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				}, (sent, value) -> {
					sent.setDate(Date.from(value.atStartOfDay(ZoneId.systemDefault()).toInstant()));
				});

				row1.addComponent(sentDate);

			}
			{
				TextField field = new TextField("Identificação da Caçamba");

				this.binderSent.forField(field).asRequired().bind("containerId");

				row1.addComponent(field);
			}

			mainLayout.addComponent(row1);
			mainLayout.setComponentAlignment(row1, Alignment.MIDDLE_CENTER);
		}
		{
			ComboBox<String> placa = new ComboBox<String>("Placa do Veiculo");
			placa.setItems(new String[] { "ABC-1234", "ACC-2312" });

			this.binderSent.forField(placa).asRequired().bind(SentCTRInfo::getTruckPlaca, SentCTRInfo::setTruckPlaca);

			mainLayout.addComponent(placa);
		}

		{
			ComboBox<String> placa = new ComboBox<String>("Local Estacionamento da Caçamba");
			placa.setItems(new String[] { "Via Pública", "Predio" });

			this.binderSent.forField(placa).asRequired().bind(SentCTRInfo::getLocaleContainer,
					SentCTRInfo::setLocaleContainer);

			mainLayout.addComponent(placa);
			
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();
			buttons.setMargin(true);

			Button btn = new Button("Enviar CTR");
			btn.addStyleName(ValoTheme.BUTTON_FRIENDLY);
			buttons.addComponent(btn);

			Button btn0 = new Button("Cancelar");
			btn0.addStyleName(ValoTheme.BUTTON_DANGER);
			buttons.addComponent(btn0);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		mainLayout.setWidth("100%");
		mainLayout.setHeightUndefined();

		setContent(mainLayout);

		setWidth("30%");
		setHeight("37%");
		setClosable(false);
		center();
	}

}
