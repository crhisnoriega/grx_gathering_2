package grx.tracking.gui.windows;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import elemental.json.JsonObject;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.gui.jsmaps.MapListener;
import grx.tracking.gui.jsmaps.MyMaps;

public class MapSelectWindow extends Window {

	private static final long serialVersionUID = 1L;
	private OrderInfo orderInfo;
	private MapSelectListener listener;
	private MyMaps map;

	public void setOrderInfo(OrderInfo orderInfo) {
		this.orderInfo = orderInfo;
		GeoLocation location = new GeoLocation();
		location.setLatitude(-23.5489);
		location.setLongitude(-46.6388);

		if (this.orderInfo.getAddress() != null && this.orderInfo.getAddress().getLocation() != null) {
			location = this.orderInfo.getAddress().getLocation();
		}

		map.addDropMarker(this.orderInfo.getOrderId().getOrder_id(),
				"No Ordem: " + this.orderInfo.getOrderId().getOrder_id(), location,
				this.orderInfo.getDescription() + "");

	}

	public MapSelectWindow(OrderInfo orderInfo) {
		super("Ordem Localização");

		setWidth("800px");
		setHeight("600px");
		setClosable(false);

		this.orderInfo = orderInfo;

		VerticalLayout verticalLayout = new VerticalLayout();
		{
			this.map = new MyMaps();
			map.setId("mymap");
			map.setSizeFull();

			GeoLocation location = new GeoLocation();
			location.setLatitude(-23.5489);
			location.setLongitude(-46.6388);

			if (this.orderInfo.getAddress() != null && this.orderInfo.getAddress().getLocation() != null) {
				location = this.orderInfo.getAddress().getLocation();
			}

			map.setListener(new MapListener() {

				@Override
				public void mapLoaded() {
				}

				@Override
				public void functionCalled(String functionName, String id, JsonObject jsonObject) {
					double lat = jsonObject.getNumber("lat");
					double lng = jsonObject.getNumber("lng");

					GeoLocation location = new GeoLocation();
					location.setLatitude(lat);
					location.setLongitude(lng);

					AddressInfo address = MapSelectWindow.this.orderInfo.getAddress();
					if (address == null) {
						address = new AddressInfo();
					}
					address.setLocation(location);
					MapSelectWindow.this.orderInfo.setAddress(address);
				}
			});

			map.addDropMarker(this.orderInfo.getOrderId().getOrder_id(),
					"No Ordem: " + this.orderInfo.getOrderId().getOrder_id(), location,
					this.orderInfo.getDescription() + "");

			verticalLayout.addComponent(map);
			verticalLayout.setExpandRatio(map, 0.2f);
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Confirmar");
			btnConfirm.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					if (listener != null) {
						listener.confirm(orderInfo, true);
					}

					close();

				}
			});
			buttons.addComponent(btnConfirm);

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});
			buttons.addComponent(btnCancel);
			buttons.setSizeUndefined();
			verticalLayout.addComponent(buttons);
			verticalLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);

		}

		verticalLayout.setSizeFull();
		setContent(verticalLayout);
		center();

	}

	public void setListener(MapSelectListener listener) {
		this.listener = listener;
	}

	public interface MapSelectListener {
		public void confirm(OrderInfo orderInfo, boolean calculateGeoLocation);
	}

}
