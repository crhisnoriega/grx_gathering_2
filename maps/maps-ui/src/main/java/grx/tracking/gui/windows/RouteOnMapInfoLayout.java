package grx.tracking.gui.windows;

import java.util.List;

import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import grx.tracking.core.maps.MapObject;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;

public class RouteOnMapInfoLayout extends VerticalLayout {

	private static final long serialVersionUID = 1L;
	private Grid<OrderInfo> orderGrid;
	private UI ui;
	private RouteOnMapListener listener;
	private List<OrderInfo> orders;
	private RouteInfo route;
	private TextField routeMoto;
	private TextField routeStatus;

	public RouteOnMapInfoLayout(UI ui) {
		this.ui = ui;

		this.setWidth("300px");
		{
			HorizontalLayout route_info = new HorizontalLayout();
			this.routeMoto = new TextField();
			this.routeStatus = new TextField();

			route_info.addComponent(this.routeMoto);
			route_info.addComponent(this.routeStatus);

			this.addComponent(route_info);
		}

		{
			this.orderGrid = new Grid<OrderInfo>();
			this.orderGrid.setWidth("300px");
			this.orderGrid.addColumn(order -> order.getOrderId().getOrder_id()).setWidth(100d).setCaption("No. Ordem");
			this.orderGrid.addColumn(order -> {
				String type = "";
				switch (order.getOrderType()) {
				case EXCHANGE:
					type = "Troca";
					break;

				case PICK:
					type = "Retirada";
					break;

				case DELIVERY:
					type = "Entrega";
					break;

				default:
					break;
				}

				return type;

			}).setWidth(100d).setCaption("Tipo");
			// this.orderGrid.addColumn(order ->
			// order.getAddress().getAddress().asAddressString()).setCaption("Endereço");
			this.orderGrid.addColumn(order -> order.getCurrentStatus().getStatus().getName()).setCaption("Status");

			this.orderGrid.addSelectionListener(new SelectionListener<OrderInfo>() {

				@Override
				public void selectionChange(SelectionEvent<OrderInfo> event) {
					if (orderGrid.getSelectedItems().iterator().hasNext()) {
						OrderInfo order = orderGrid.getSelectedItems().iterator().next();

						if (order != null) {
							MapObject object = MapObject.createMapObject(order);

							RouteOnMapInfoLayout.this.ui.access(new Runnable() {

								@Override
								public void run() {
									if (listener != null) {
										listener.focusObject(object.getId());
									}
								}
							});
						}
					}

				}
			});
			this.addComponent(this.orderGrid);
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();
			Button button_close = new Button("Fechar");
			button_close.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					if (listener != null) {
						listener.filterMapAndClose();
					}
				}
			});

			buttons.addComponent(button_close);
			buttons.setComponentAlignment(button_close, Alignment.MIDDLE_CENTER);

			this.addComponent(buttons);
		}

	}

	public void updateOrderInfo(OrderInfo order) {

		System.out.println("update order: " + order.getOrderId());

		if (orders.contains(order)) {
			orders.remove(order);
		}

		if (!order.getCurrentStatus().getStatus().equals(ORDER_STATUS.NEW)) {
			orders.add(order);
		}
		orderGrid.getDataProvider().refreshAll();

	}

	public void setListener(RouteOnMapListener listener) {
		this.listener = listener;
	}

	public void populateWindow(RouteInfo route, List<OrderInfo> orders) {
		this.route = route;
		this.routeMoto.setValue(route.getUserId().toString());
		this.routeStatus.setValue(route.getCurrentStatus().getStatus().getName());

		this.orders = orders;
		this.orderGrid.setItems(this.orders);
	}

	public void select(OrderInfo order) {
		this.orderGrid.select(order);
	}

	public static interface RouteOnMapListener {
		public void filterMapAndClose();

		public void focusObject(String id);
	}
}
