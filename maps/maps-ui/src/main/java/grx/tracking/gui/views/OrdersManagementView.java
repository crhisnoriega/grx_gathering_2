package grx.tracking.gui.views;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.jboss.logging.Logger;

import com.vaadin.data.Binder;
import com.vaadin.data.HasValue;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.HasValue.ValueChangeEvent;
import com.vaadin.data.HasValue.ValueChangeListener;
import com.vaadin.event.selection.MultiSelectionEvent;
import com.vaadin.event.selection.MultiSelectionListener;
import com.vaadin.event.selection.SelectionEvent;
import com.vaadin.event.selection.SelectionListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.SerializableSupplier;
import com.vaadin.server.Setter;
import com.vaadin.shared.ui.grid.DropMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FetchItemsCallback;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.StyleGenerator;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.components.grid.GridDragSource;
import com.vaadin.ui.components.grid.GridDropTarget;
import com.vaadin.ui.components.grid.GridSelectionModel;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.ComponentRenderer;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.fileimport.ExcelFileImport;
import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.CustomerId;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.gui.samples.authentication.CurrentUser;
import grx.tracking.gui.util.EJBLocator;
import grx.tracking.gui.util.MyDialogs;
import grx.tracking.gui.windows.MapSelectWindow;
import grx.tracking.gui.windows.MapSelectWindow.MapSelectListener;
import grx.tracking.gui.windows.MyNotification;

public class OrdersManagementView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;

	private ByteArrayOutputStream uploadedFile;
	private Grid<OrderInfo> orderGrid;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	public List<OrderInfo> ordersList;
	public List<OrderInfo> okOrders;
	private GRXTrackingSession session;
	private GoogleMapsServices services;

	private TextField orders_qtde;
	private OrderInfo dragged;

	private int qtde_validate_orders;
	private SectorInfo empty;
	private List<SectorInfo> sectors;
	private Hashtable<String, SectorInfo> sectorsTable;

	public OrdersManagementView(MapperView mapView, UI ui) {

		this.session = CurrentUser.get();
		this.okOrders = new ArrayList<>();
		this.services = new GoogleMapsServices();
		this.qtde_validate_orders = 0;
		{
			try {
				this.sectorsTable = new Hashtable<>();
				this.sectors = session.findEntities("select s from SectorInfo as s", SectorInfo.class,
						new Hashtable<>());
				this.sectors.forEach(sector -> sectorsTable.put(sector.getSectorId(), sector));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		{
			HorizontalLayout buttons = new HorizontalLayout();
			FileReceiver receiver = new FileReceiver();
			Upload upload = new Upload("Arquivo Excel de Pedidos", receiver);
			upload.setButtonCaption("Procurar Planilha");
			upload.addSucceededListener(receiver);
			upload.addStyleName(ValoTheme.BUTTON_FRIENDLY);

			this.orders_qtde = new TextField("Pedidos Pendentes");
			this.orders_qtde.setReadOnly(true);

			buttons.addComponent(this.orders_qtde);
			buttons.addComponent(upload);

			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);

		}
		{
			this.orderGrid = this.createGrid();
			this.orderGrid.addSelectionListener(new SelectionListener<OrderInfo>() {

				private static final long serialVersionUID = 1L;

				@Override
				public void selectionChange(SelectionEvent<OrderInfo> event) {
				}
			});
			this.orderGrid.setSizeFull();

			addComponent(this.orderGrid);
			setExpandRatio(this.orderGrid, 0.2f);

		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnSelectAll = new Button("Selecionar Todos");
			btnSelectAll.addClickListener(new ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					okOrders.addAll(OrdersManagementView.this.ordersList);
					orderGrid.getDataProvider().refreshAll();
				}
			});

			Button btnRemove = new Button("Remover");
			btnRemove.addClickListener(new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					if (!orderGrid.getSelectedItems().isEmpty()) {
						OrderInfo orderInfo = orderGrid.getSelectedItems().iterator().next();
						ordersList.remove(orderInfo);
						orderGrid.getDataProvider().refreshAll();
					}
				}
			});
			Button btnMap = new Button("Localizar no mapa");
			btnMap.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					if (!orderGrid.getSelectedItems().isEmpty()) {
						OrderInfo orderInfo = orderGrid.getSelectedItems().iterator().next();

						MapSelectWindow window = new MapSelectWindow(orderInfo);
						window.setModal(true);
						window.setListener(new MapSelectListener() {
							@Override
							public void confirm(OrderInfo orderInfo, boolean calculate) {
								if (calculate) {
									try {
										Address address = services
												.reverseDecoding(orderInfo.getAddress().getLocation());
										orderInfo.getAddress().setAddress(address);
									} catch (Exception e) {
										Notification.show("Error: " + e.getLocalizedMessage(), Type.ERROR_MESSAGE);
									}

									try {
										SectorInfo near_sector = EJBLocator.refGRXTrackingService()
												.findNearestBase(orderInfo.getAddress().getLocation());
										orderInfo.setNearest_Sector_Id(near_sector.getSectorId());
									} catch (Exception e) {
										Notification.show("Error: " + e.getLocalizedMessage(), Type.ERROR_MESSAGE);
									}

								}

								orderGrid.getDataProvider().refreshItem(orderInfo);
							}
						});
						UI.getCurrent().addWindow(window);

					}
				}
			});
			Button btnConfirm = new Button("Confirmar Pedidos");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					System.out.println("okOrders: " + okOrders.size());
					final List<OrderInfo> toRemoveFromOkOrders = new ArrayList<>();
					okOrders.forEach(order1 -> {
						try {
							if (order1.getAddress() == null || order1.getAddress().getLocation() == null) {
								return;
							}

							if (order1.getNearest_Sector_Id() == null
									|| order1.getNearest_Sector_Id().trim().isEmpty()) {
								return;
							}

							if (order1.getRequestDate() == null) {
								return;
							}

							OrderStatus currentStatus = new OrderStatus();
							currentStatus.setStatus(ORDER_STATUS.NEW);
							currentStatus.setDate(Calendar.getInstance().getTime());
							order1.setCurrentStatus(currentStatus);

							session.saveEntity(order1.getOrderId(), order1);
							qtde_validate_orders++;

							ordersList.remove(order1);

							toRemoveFromOkOrders.add(order1);
						} catch (Exception e) {
							e.printStackTrace();
						}
					});

					orderGrid.getDataProvider().refreshAll();

					try {
						orders_qtde.setValue(ordersList.size() + "");
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						mapView.filterMapDate(true);
					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Notification notification = new Notification("Confirmar Pedidos",
								"Foram adicionados " + qtde_validate_orders + " pedidos corretamente",
								Notification.Type.HUMANIZED_MESSAGE);
						notification.setDelayMsec(2000);
						notification.show(Page.getCurrent());
					} catch (Exception e) {
						e.printStackTrace();
					}

					qtde_validate_orders = 0;
					okOrders.removeAll(toRemoveFromOkOrders);

				}
			});

			buttons.addComponent(btnSelectAll);
			buttons.addComponent(btnRemove);
			buttons.addComponent(btnMap);
			buttons.addComponent(btnConfirm);

			buttons.setSizeUndefined();

			setSizeFull();
			addComponent(buttons);
			setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}
	}

	private Grid<OrderInfo> createGrid() {
		Grid<OrderInfo> grid = new Grid<>();

		Binder<OrderInfo> binder = grid.getEditor().getBinder();
		grid.getEditor().setEnabled(true);

		grid.addComponentColumn(order -> {
			CheckBox check = new CheckBox();
			check.addValueChangeListener(new ValueChangeListener<Boolean>() {

				private static final long serialVersionUID = 1L;

				@Override
				public void valueChange(ValueChangeEvent<Boolean> event) {
					if (event.getValue() == false && okOrders.contains(order)) {
						okOrders.remove(order);
					}

					if (event.getValue() == true && !okOrders.contains(order)) {
						okOrders.add(order);
					}
				}
			});
			check.setValue(okOrders.contains(order));
			return check;
		}).setCaption("#");

		grid.addColumn(order -> {
			return order.getOrderId().getOrder_id();
		}).setCaption("No. de Ordem");

		grid.addColumn(order -> order.getCustomerId().getCustomer_id())
				.setEditorBinding(binder.forField(new TextField()).bind(new ValueProvider<OrderInfo, String>() {

					@Override
					public String apply(OrderInfo source) {
						return source.getCustomerId().getCustomer_id();
					}
				}, new Setter<OrderInfo, String>() {

					@Override
					public void accept(OrderInfo bean, String fieldvalue) {
						bean.getCustomerId().setCustomer_id(fieldvalue);

					}
				})).setWidth(300d).setCaption("Cliente");

		grid.addColumn(order -> sdf.format(order.getRequestDate()))
				.setEditorBinding(binder.forField(new TextField()).bind(new ValueProvider<OrderInfo, String>() {

					@Override
					public String apply(OrderInfo source) {
						return sdf.format(source.getRequestDate());
					}
				}, new Setter<OrderInfo, String>() {

					@Override
					public void accept(OrderInfo bean, String fieldvalue) {
						try {
							bean.setRequestDate(sdf.parse(fieldvalue));
						} catch (ParseException e) {
							Notification.show("Erro no formato da data:" + fieldvalue, Type.ERROR_MESSAGE);
						}
					}
				})).setWidth(120d).setCaption("Data");

		grid.addColumn(order -> order.getPeriodType().getName()).setCaption("Periodo").setWidth(100d);

		grid.addColumn(order -> {
			String tipo = "";
			switch (order.getOrderType()) {
			case DELIVERY:
				tipo = "Entrega";
				break;

			case EXCHANGE:
				tipo = "Troca";
				break;

			case PICK:
				tipo = "Retira";
				break;

			default:
				break;
			}
			return tipo;
		}).setWidth(100d).setCaption("Tipo");

		grid.addColumn(order -> {
			return sectorsTable.get(order.getNearest_Sector_Id()).getName();
		}).setEditorBinding(binder.forField(createComboSector()).bind(new ValueProvider<OrderInfo, SectorInfo>() {

			@Override
			public SectorInfo apply(OrderInfo source) {
				SectorInfo sector = empty;
				if (source.getNearest_Sector_Id() != null) {
					try {
						sector = session.findEntity(SectorInfo.class, source.getNearest_Sector_Id());
					} catch (Exception e) {
					}
				}
				return sector;
			}
		}, new Setter<OrderInfo, SectorInfo>() {

			@Override
			public void accept(OrderInfo bean, SectorInfo fieldvalue) {
				bean.setNearest_Sector_Id(fieldvalue.getSectorId());
			}
		})).setCaption("Base").setWidth(250d);

		grid.addColumn(order -> {
			if (order.getAddress() != null && order.getAddress().getAddress() != null) {
				return order.getAddress().getAddress().asAddressString();
			} else {
				return "";
			}
		}).setEditorBinding(binder.forField(new TextField()).bind(new ValueProvider<OrderInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(OrderInfo order) {
				if (order.getAddress() != null && order.getAddress().getAddress() != null) {
					return order.getAddress().getAddress().asAddressString();
				} else {
					return "";
				}
			}
		}, new Setter<OrderInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(OrderInfo bean, String fieldvalue) {
				try {
					Address addresss = Address.extractAddressSimpleWithException(fieldvalue);
					if (bean.getAddress() != null && bean.getAddress().getAddress() != null) {
						bean.getAddress().getAddress().merge(addresss);
					} else if (bean.getAddress() != null && bean.getAddress().getAddress() == null) {
						bean.getAddress().setAddress(addresss);
					} else if (bean.getAddress() == null) {
						AddressInfo address1 = new AddressInfo();
						address1.setAddress(addresss);
						bean.setAddress(address1);
					}

					GeoLocation location = services.geoDecoding(addresss);
					bean.getAddress().setLocation(location);

					// SectorInfo near_section = EJBLocator.refGRXTrackingService().findNearestBase(location);
					// bean.setNearest_Sector_Id(near_section.getSectorId());

					grid.getDataProvider().refreshItem(bean);
				} catch (Exception e) {
					System.out.println("error on address: " + e.getLocalizedMessage());

					bean.setAddress(null);

					if (bean.getAddress() != null) {
						bean.getAddress().setLocation(null);
					}
				}
			}
		})).setWidth(500d).setCaption("Endere�o");

		grid.addColumn(order -> {
			if (order.getAddress() != null && order.getAddress().getLocation() != null) {

				GeoLocation location = order.getAddress().getLocation();
				String location_str = "lat: " + location.getLatitude() + " lng: " + location.getLongitude();
				return location_str;
			} else {
				return "";
			}
		}).setCaption("Localiza��o").setWidth(400);

		// lazy grid
		// grid.setDataProvider((order, offset, limit) -> new
		// ArrayList<OrderInfo>().stream(), () -> 2);

		try {
			Hashtable<String, Object> params = new Hashtable<>();
			params.put("status", OrderStatus.ORDER_STATUS.TO_VALIDATE);
			this.ordersList = session.findEntities(
					"select o from OrderInfo as o where o.currentStatus.status = :status", OrderInfo.class, params);
			grid.setItems(this.ordersList);
			orders_qtde.setValue(this.ordersList.size() + "");
			this.qtde_validate_orders = 0;

		} catch (Exception e) {
			e.printStackTrace();
			grid.setItems(new ArrayList<>());
		}
		grid.setSizeFull();

		{

			GridDragSource<OrderInfo> source = new GridDragSource<>(grid);

			source.addGridDragStartListener(e -> {
				dragged = e.getDraggedItems().iterator().next();
			});

			GridDropTarget<OrderInfo> target = new GridDropTarget<>(grid, DropMode.ON_TOP_OR_BETWEEN);
			target.addGridDropListener(e -> {
				System.out.println("drop: " + e.getDropTargetRow().get());
				int index = ordersList.indexOf(e.getDropTargetRow().get());
				ordersList.remove(dragged);

				System.out.println("index: " + index);
				if (0 <= index && index < ordersList.size()) {
					ordersList.add(index, dragged);
				}
				grid.getDataProvider().refreshAll();
			});
		}

		return grid;
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

	private class FileReceiver implements Receiver, SucceededListener {

		private static final long serialVersionUID = 1L;

		@Override
		public OutputStream receiveUpload(String filename, String mimeType) {
			uploadedFile = new ByteArrayOutputStream();
			return uploadedFile;
		}

		@Override
		public void uploadSucceeded(SucceededEvent event) {
			if (uploadedFile != null) {
				try {
					Path file = Files.createTempFile("file_uploaded", ".xlsx");
					FileOutputStream fos = new FileOutputStream(file.toFile());
					fos.write(uploadedFile.toByteArray());
					fos.close();
					Future<List<OrderInfo>> result = session.processFile(file.toFile().getAbsolutePath());
					OrdersManagementView.this.ordersList = result.get(10, TimeUnit.MINUTES);
					orderGrid.setItems(OrdersManagementView.this.ordersList);
					orders_qtde.setValue(OrdersManagementView.this.ordersList.size() + "");
				} catch (Exception e) {
					e.printStackTrace();
					MyDialogs.showError("Processando Planilha",
							"Erro processando planilha: " + e.getLocalizedMessage());
				}
			}

		}

	}

	public ComboBox<SectorInfo> createComboSector() {

		ComboBox<SectorInfo> comboSector = new ComboBox<>();
		try {
			// sectors.add(0, this.empty);
			if (!sectors.isEmpty()) {
				empty = sectors.get(0);
			}
			comboSector.setItems(sectors);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		comboSector.setItemCaptionGenerator(SectorInfo::getName);
		comboSector.setEmptySelectionAllowed(false);

		comboSector.setWidth("300px");

		return comboSector;

	}
}
