package grx.tracking.gui.windows;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.SectorInfo;

public class SectorWindow extends Window {

	private static final long serialVersionUID = 1L;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	// private Hashtable<RoutePointInfo, OrderInfo> orders;

	private List<SectorInfo> sectors;
	// private RouteInfo routeInfo;
	private GRXTrackingSession session;

	private SelectSector listener;

	private Grid<SectorInfo> grid;

	public SectorWindow(GRXTrackingSession session) {
		super("Bases Cadastradas");

		this.session = session;

		{
			try {
				Hashtable<String, Object> params = new Hashtable<>();

				this.sectors = session.findEntities("select t from SectorInfo as t", SectorInfo.class, params);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		VerticalLayout mainLayout = new VerticalLayout();
		{

		}

		this.grid = new Grid<>();
		{
			grid.addColumn(sector -> {
				return sector.getName();
			}).setCaption("Nome").setWidth(200d);

			grid.addColumn(sector -> {
				return sector.getAddress().asAddressString();
			}).setCaption("Endere�o");

			grid.setItems(this.sectors);
			grid.setSizeFull();

			mainLayout.addComponent(grid);
			mainLayout.setExpandRatio(grid, 0.2f);
		}
		{
			HorizontalLayout buttons = new HorizontalLayout();

			Button btnConfirm = new Button("Selecionar");
			btnConfirm.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						Set<SectorInfo> sets = grid.getSelectedItems();
						if (!sets.isEmpty()) {
							if (listener != null) {
								listener.confirm(sets.iterator().next());
							}
						}
						close();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			Button btnCancel = new Button("Cancelar");
			btnCancel.addClickListener(new ClickListener() {

				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					close();
				}
			});

			// buttons.addComponent(btnShow);
			buttons.addComponent(btnConfirm);
			buttons.addComponent(btnCancel);

			mainLayout.addComponent(buttons);
			mainLayout.setComponentAlignment(buttons, Alignment.MIDDLE_CENTER);
		}

		mainLayout.setSizeFull();
		setContent(mainLayout);

		setWidth("600px");
		setHeight("640px");
		setClosable(false);

	}

	private void filter() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setListener(SelectSector listener) {
		this.listener = listener;
	}

	public static interface SelectSector {
		public void confirm(SectorInfo truck);
	}
}
