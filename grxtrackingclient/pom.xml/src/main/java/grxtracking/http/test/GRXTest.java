package grxtracking.http.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventChangeStatusRoute;
import grx.tracking.core.events.EventFindTasks;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.events.GRXEvent.EVENT_TYPE;
import grx.tracking.core.events.GRXEvent.TO_TYPE;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RouteStatus;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteStatus.ROUTE_STATUS;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.EventMessage;
import grxtracking.http.client.HttpClient;
import grxtracking.http.client.JSONConverter;

public class GRXTest {

	private UserId userId;
	private HttpClient client;
	private RouteInfo route;
	private List<OrderInfo> orders;
	private TruckId truckId;

	public GRXTest() {
		this.userId = new UserId();
		this.userId.setUser_id("app2");
		this.userId.setSector_id("base1");

		this.client = new HttpClient();
	}

	public void login() throws Exception {

		Credentials user = new Credentials();
		user.setUsername("app2");
		user.setSector_id("base1");
		user.setPassword("app2");
		UserInfo result = client.loginWithUser(user);

		this.client.setToken(result.getCurrentToken());
	}

	public void location() throws Exception {

		for (int i = 0; i < 10; i++) {
			EventMessage message = new EventMessage();

			EventUpdateGeoLocation update = new EventUpdateGeoLocation();

			update.setFromUserId(this.userId);
			update.setToUserId(new UserId("", "base1"));
			update.setTo(TO_TYPE.TO_SECTION);
			update.setType(EVENT_TYPE.REAL_TIME);
			update.setMessage("ponto " + i);

			List<GeoLocation> locations = new ArrayList<GeoLocation>();
			GeoLocation e = new GeoLocation();
			e.setLatitude(-23.5489 + (i * 0.01d));
			e.setLongitude(-46.6388 + (i * 0.02d));

			locations.add(e);
			update.setLocations(locations);

			JSONConverter conv = new JSONConverter();

			message.setEventClass(EventUpdateGeoLocation.class.getCanonicalName());
			message.setJsonString(conv.toJson(update));

			this.client.sendEvent(message);

			Thread.sleep(1000);
			break;
		}

	}

	public void find() throws Exception {

		EventFindTasks start = new EventFindTasks();
		start.setFromUserId(this.userId);

		this.truckId = client.getTrucks(this.userId).get(0).getTruckId();

		System.out.println(this.truckId.getTruck_id());

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		start.setDateEvent(sdf.parse("22/09/2018 18:00:00"));
		start.setTruckId(this.truckId);
		start.setStartBase(false);
		start.setType(EVENT_TYPE.REAL_TIME);
		start.setTo(TO_TYPE.TO_USER);
		start.setConfirmRoute(false);
		start.setSector_Id("sector_48e78fa7-3782-42db-afd1-850526434b28");

		JSONConverter conv = new JSONConverter();

		EventMessage message = new EventMessage();
		message.setEventClass(EventFindTasks.class.getCanonicalName());
		message.setJsonString(conv.toJson(start));

		this.client.sendEvent(message);
	}

	public void route() throws Exception {
		for (int i = 0; i < 5; i++) {
			try {
				Thread.sleep(10000);
				this.route = this.client.getRoute();
				this.orders = this.client.getOrders();

				System.out.println(this.orders);
				return;
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("dont found");
			}
		}
	}

	public void occurrence() throws Exception {
		OrderInfo order = null;
		if (this.orders != null && !this.orders.isEmpty()) {
			order = this.orders.get(0);
		}

		JSONConverter conv = new JSONConverter();

		EventOcurrence ocurr = new EventOcurrence();
		ocurr.setFromUserId(this.userId);
		ocurr.setTo(TO_TYPE.TO_USER);
		ocurr.setType(EVENT_TYPE.REAL_TIME);

		OccurrenceInfo ocurrence = new OccurrenceInfo();
		ocurrence.setType(OCCURRENCE_TYPE.ORDER_PROBLEM);
		ocurrence.setDescription("");
		ocurrence.setUserId(this.userId);
		ocurrence.setTruckId(this.truckId);

		ocurrence.setOrderId(order.getOrderId());

		ocurrence.setDate(Calendar.getInstance().getTime());
		ocurr.setOcurrence(ocurrence);

		EventMessage message = new EventMessage();
		message.setEventClass(EventOcurrence.class.getCanonicalName());
		message.setJsonString(conv.toJson(ocurr));

		this.client.sendEvent(message);

	}

	public void cancel_route() throws Exception {
		if (this.route != null) {
			EventChangeStatusRoute change = new EventChangeStatusRoute();
			change.setFromUserId(this.userId);
			change.setTo(TO_TYPE.TO_USER);
			change.setType(EVENT_TYPE.REAL_TIME);

			RouteStatus newStatus = new RouteStatus();
			newStatus.setStatus(ROUTE_STATUS.CANCEL);
			newStatus.setTimeDate(Calendar.getInstance().getTime());
			change.setNewStatus(newStatus);

			change.setRoute_id(this.route.getId());

			JSONConverter conv = new JSONConverter();
			EventMessage message = new EventMessage();
			message.setEventClass(EventChangeStatusRoute.class.getCanonicalName());
			message.setJsonString(conv.toJson(change));
			this.client.sendEvent(message);

		}

	}

	public void checkin_order() throws Exception {

		if (this.orders != null && !this.orders.isEmpty()) {

			JSONConverter conv = new JSONConverter();

			OrderInfo order = this.orders.get(0);

			System.out.println("checkin order: " + order.getOrderId());

			EventChangeStatusOrder change = new EventChangeStatusOrder();
			change.setFromUserId(this.userId);
			change.setTo(TO_TYPE.TO_USER);
			change.setType(EVENT_TYPE.REAL_TIME);
			change.setNewStatus(ORDER_STATUS.FINISHED);

			change.setRoute_id(this.route.getId());
			change.setOrderId(order.getOrderId());

			EventMessage message = new EventMessage();
			message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
			message.setJsonString(conv.toJson(change));
			this.client.sendEvent(message);

		}
	}

	public void cancel_order() throws Exception {

		if (this.orders != null && !this.orders.isEmpty()) {

			JSONConverter conv = new JSONConverter();

			OrderInfo order = this.orders.get(0);

			System.out.println("cancelando order: " + order.getOrderId());

			EventChangeStatusOrder change = new EventChangeStatusOrder();
			change.setFromUserId(this.userId);
			change.setTo(TO_TYPE.TO_USER);
			change.setType(EVENT_TYPE.REAL_TIME);
			change.setNewStatus(ORDER_STATUS.NEW);

			change.setRoute_id(this.route.getId());
			change.setOrderId(order.getOrderId());

			EventMessage message = new EventMessage();
			message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
			message.setJsonString(conv.toJson(change));
			this.client.sendEvent(message);

		}
	}

	public void change_order_status(ORDER_STATUS ss) throws Exception {
		// if (this.orders != null && !this.orders.isEmpty()) {

		JSONConverter conv = new JSONConverter();

		// OrderInfo order = this.orders.get(0);
		OrderInfo order = new OrderInfo();
		OrderId orderId = new OrderId();
		orderId.setOrder_id("56011.0");
		orderId.setSector_id("base1");
		order.setOrderId(orderId);

		System.out.println("sending: " + order.getOrderId());

		EventChangeStatusOrder change = new EventChangeStatusOrder();
		change.setFromUserId(this.userId);
		change.setTo(TO_TYPE.TO_USER);
		change.setType(EVENT_TYPE.REAL_TIME);
		change.setNewStatus(ss);

		change.setRoute_id("");
		change.setOrderId(order.getOrderId());

		EventMessage message = new EventMessage();
		message.setEventClass(EventChangeStatusOrder.class.getCanonicalName());
		message.setJsonString(conv.toJson(change));
		this.client.sendEvent(message);
		// }
	}

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////

	public static void main(String[] args) {

		GRXTest test = new GRXTest();

		try {
			// login
			test.login();

			// location
			new Thread(() -> {
				while (true) {
					try {
						test.location();

					} catch (Exception e) {
						e.printStackTrace();
					}

					try {
						Thread.sleep(10000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		} catch (Exception e) {
			e.printStackTrace();
		}

		do {

			System.out.print("Option:");
			Scanner n = new Scanner(System.in);
			int option = n.nextInt();

			try {

				switch (option) {
				case 0:
					test.find();
					test.route();
					break;

				case 1:
					test.route();
					test.occurrence();
					break;

				case 2:
					test.route();
					test.cancel_order();
					break;

				case 3:
					test.route();
					test.cancel_route();
					break;

				case 4:
					// test.route();
					test.change_order_status(ORDER_STATUS.FINISHED);
					break;

				case 5:
					test.route();
					test.checkin_order();
					break;

				default:
					break;
				}

				// search
				System.out.println("finished");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} while (true);
	}

}
