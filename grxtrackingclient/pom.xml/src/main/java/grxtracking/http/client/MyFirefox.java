package grxtracking.http.client;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyFirefox {

	public static void getQR(FirefoxDriver firefox) throws Exception {
		firefox.navigate().to("https://web.whatsapp.com/teste");

		Thread.sleep(2000);

		File scrFile = ((TakesScreenshot) firefox).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("c:\\qr.png"));
	}

	public static void sendMessage(FirefoxDriver firefox, String numero, String message) throws Exception {
		firefox.navigate().to("https://web.whatsapp.com/send?phone=" + numero + "&text=" + message);

		WebDriverWait wait = new WebDriverWait(firefox, 40);

		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("/html/body/div[1]/div/div/div[4]/div/footer/div[1]/div[3]/button")));

		System.out.println("element: " + element);
		Thread.sleep(2000);

		Actions actions = new Actions(firefox);

		actions.moveToElement(element).click().perform();
	}

	public static void main(String[] args) {
		try {

			System.setProperty("webdriver.gecko.driver", "G:\\TTIHumanOCR\\dist\\geckodriver.exe");
			System.setProperty("webdriver.firefox.marionette", "false");

			File pathBinary = new File("D:\\MyF\\firefox.exe");
			FirefoxBinary firefoxBinary = new FirefoxBinary(pathBinary);

			DesiredCapabilities desired = DesiredCapabilities.firefox();
			desired.setCapability("marionette", false);

			ProfilesIni profileIni = new ProfilesIni();
			FirefoxProfile profile = profileIni.getProfile("whats");

			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(profile);
			
			desired.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(firefoxBinary));

			FirefoxDriver firefox = new FirefoxDriver(options);

			// sendMessage(firefox, "5511983133364", "helo my friend");
			// Thread.sleep(2000);
			// sendMessage(firefox, "5511952839799", ":)");

			getQR(firefox);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
