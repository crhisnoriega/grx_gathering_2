package grxtracking.http.client.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

// import grx.tracking.core.events.EventCancelOrder;
import grx.tracking.core.events.EventFindTasks;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.events.GRXEvent.EVENT_TYPE;
import grx.tracking.core.events.GRXEvent.TO_TYPE;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_TYPE;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.EventMessage;
import grxtracking.http.client.HttpClient;
import grxtracking.http.client.JSONConverter;

public class TestHttp {

	public static void main2(String[] args) {
		try {

			String TOKEN_TEST = "ed2TGr4Au3U:APA91bEDaJwr_OalRLZrZSqAUp3sSUqwP-4YYhMPT1YJjErV5-GrGdox7hggoznbunKx3pJ3uCgC1naVEkHu1-19LANebFtkQedZBOFxrxATkVDWzOfcq4tq17lrB5Pqsk_3HmgwxiGf";

			HttpClient client = new HttpClient();
			RouteInfo route = new RouteInfo();
			RoutePointInfo[] points = new RoutePointInfo[1];
			points[0] = new RoutePointInfo();
			points[0].setSequence(2);
			route.setPoints(points);

			JSONConverter conv = new JSONConverter();

			EventMessage message = new EventMessage();
			message.setJsonString(conv.toJson(route));
			message.setEventClass(RouteInfo.class.getCanonicalName());
			client.sendFirebaseMessage(TOKEN_TEST, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			HttpClient client = new HttpClient();
			
			// UserId app2 = new UserId("app2", "sector_22803a3f-6697-432e-8daf-96aa1ad66ce1");
			UserId app2 = new UserId("app3", "sector_48e78fa7-3782-42db-afd1-850526434b28");

			{
				try {
					Credentials user = new Credentials();
					user.setUsername(app2.getUser_id());
					user.setSector_id(app2.getSector_id());
					user.setPassword("app3");
					user.setApp_version("v1.0.5");
					UserInfo result = client.loginWithUser(user);
					System.out.println("last login: " + result.getLastLogin());
					client.setToken(result.getCurrentToken());
				} catch (Exception e) {
					System.out.println(e.getMessage());
					System.exit(2);
				}

				

				System.out.println(client.getBases());

				List<TruckInfo> trucks = client.getTrucks(app2);
				trucks.forEach(t -> {
					System.out.println(t.getTruckId().getTruck_id());
					System.out.println(t.getTruckId().getSector_id());
					System.out.println(t.getPlaca());
					System.out.println(t.getTruckDescription());
					System.out.println(t.getDimensions());
				});

				// client.getRoute();
			}
			{
				JSONConverter conv = new JSONConverter();

				EventOcurrence ocurr = new EventOcurrence();
				UserId fromUserId = app2;
				ocurr.setFromUserId(fromUserId);
				ocurr.setTo(TO_TYPE.TO_USER);
				ocurr.setType(EVENT_TYPE.REAL_TIME);

				OrderId orderId = new OrderId();
				orderId.setOrder_id("47066.0");
				orderId.setSector_id("base1");

				OccurrenceInfo ocurrence = new OccurrenceInfo();
				ocurrence.setType(OCCURRENCE_TYPE.TRUCK_BREAK);
				ocurrence.setDescription("");
				ocurrence.setUserId(fromUserId);
				// ocurrence.setTruckId(client.getTrucks("app2", "base1").get(0).getTruckId());

				ocurrence.setDate(Calendar.getInstance().getTime());
				ocurr.setOcurrence(ocurrence);

				EventMessage message = new EventMessage();
				message.setEventClass(EventOcurrence.class.getCanonicalName());
				message.setJsonString(conv.toJson(ocurr));

				// client.sendEvent(message);
			}
			{/*
				 * JSONConverter conv = new JSONConverter();
				 * 
				 * EventCancelOrder cancel = new EventCancelOrder(); UserId fromUserId = new
				 * UserId("app2", "base1"); cancel.setFromUserId(fromUserId);
				 * cancel.setTo(TO_TYPE.TO_USER); cancel.setType(EVENT_TYPE.REAL_TIME); OrderId
				 * orderId = new OrderId(); orderId.setOrder_id("47066.0");
				 * orderId.setSector_id("base1"); cancel.setOrderId(orderId);
				 * 
				 * EventMessage message = new EventMessage();
				 * message.setEventClass(EventCancelOrder.class.getCanonicalName());
				 * message.setJsonString(conv.toJson(cancel));
				 */

				// client.sendEvent(message);
			}

			/*
			 * { TokenInfo token = new TokenInfo(); token.setTokenInfo("fasdfasdf");
			 * client.registerTokenFirebase(token); }
			 */

			{
				/*
				 * List<GeoLocation> locations = new ArrayList<GeoLocation>(); GeoLocation geo =
				 * new GeoLocation(); locations.add(geo); GeoLocationUpdate update = new
				 * GeoLocationUpdate(); update.setUserId(new UserId("user", "sector"));
				 * update.setLocations(locations); client.updateGeoLocation(update);
				 */
			}

			{
				for (int i = 0; i < 10; i++) {
					EventMessage message = new EventMessage();

					EventUpdateGeoLocation update = new EventUpdateGeoLocation();

					UserId fromUserId = app2;
					update.setFromUserId(fromUserId);

					update.setToUserId(new UserId("", "base1"));
					update.setTo(TO_TYPE.TO_SECTION);
					update.setType(EVENT_TYPE.REAL_TIME);
					update.setMessage("ponto " + i);

					List<GeoLocation> locations = new ArrayList<GeoLocation>();
					GeoLocation e = new GeoLocation();
					e.setLatitude(-23.5489 + (i * 0.01d));
					e.setLongitude(-46.6388 + (i * 0.02d));

					locations.add(e);
					update.setLocations(locations);

					JSONConverter conv = new JSONConverter();

					message.setEventClass(EventUpdateGeoLocation.class.getCanonicalName());
					message.setJsonString(conv.toJson(update));

					client.sendEvent(message);

					Thread.sleep(1000);
					break;
				}
			}

			{
				EventFindTasks start = new EventFindTasks();
				start.setFromUserId(app2);
				TruckId truckId = client.getTrucks(app2).get(0).getTruckId();

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				start.setDateEvent(sdf.parse("07/11/2018 08:00:00"));
				start.setTruckId(truckId);
				start.setStartBase(false);
				start.setType(EVENT_TYPE.REAL_TIME);
				start.setTo(TO_TYPE.TO_USER);
				start.setConfirmRoute(false);

				start.setSector_Id("sector_48e78fa7-3782-42db-afd1-850526434b28");

				// start.setDeliverCapacity(5);
				// start.setPickupCapacity(3);

				JSONConverter conv = new JSONConverter();

				EventMessage message = new EventMessage();
				message.setEventClass(EventFindTasks.class.getCanonicalName());
				message.setJsonString(conv.toJson(start));

				client.sendEvent(message);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
