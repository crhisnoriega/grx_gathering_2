package grxtracking.http.client;

import java.io.FileWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringEscapeUtils;

import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.FormBody.Builder;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

public class VoceAzulHttpClient {

	private static Logger logger = Logger.getLogger(VoceAzulHttpClient.class.getCanonicalName());

	// private static String URL = "http://grxtracking.ddns.net/trackingrest";
	// private static String URL =
	// "http://grxtrackingserver.ddns.net/grxtrackingrest/v1";
	// private static String URL = "https://api.voceazul.com.br/v1";
	private static String URL = "https://api.voceazul.com.br/utils";

	private JSONConverter conv;

	private String token;

	public VoceAzulHttpClient() {
		this.conv = new JSONConverter();
	}

	public void setToken(String token) {
		this.token = token;
	}

	private Response post(String endpoint, String json) throws Exception {
		OkHttpClient client = new OkHttpClient();

		RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

		Request request = new Request.Builder().url(URL + endpoint).post(post)
				.addHeader("Authorization", "Bearer " + this.token).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	private Response postRaw(String endpoint, String json) throws Exception {
		OkHttpClient client = new OkHttpClient();

		RequestBody post = RequestBody.create(MediaType.parse("form-data"), json);

		Request request = new Request.Builder().url(endpoint).post(post).build();

		Response response = client.newCall(request).execute();

		return response;
	}

	public Response postURL(String endpoint, Hashtable<String, Object> params) throws Exception {
		OkHttpClient client = new OkHttpClient();

		Builder builder = new FormBody.Builder();
		for (String param : params.keySet()) {
			builder.add(param, params.get(param) + "");
		}

		RequestBody formBody = builder.build();

		Request request = new Request.Builder().url(URL + endpoint).post(formBody).build();

		System.out.println(this.bodyToString(request));

		Response response = client.newCall(request).execute();

		return response;
	}

	private String bodyToString(final Request request) {

		try {
			final Request copy = request.newBuilder().build();
			final Buffer buffer = new Buffer();
			copy.body().writeTo(buffer);
			return buffer.readUtf8();
		} catch (final Exception e) {
			return "did not work";
		}
	}

	private <T> Response post(String endpoint, Object obj) throws Exception {
		String json = this.conv.toJson(obj);
		return this.post(endpoint, json);
	}

	private Response get(String endpoint, String json) throws Exception {
		OkHttpClient client = new OkHttpClient();
		// RequestBody get =
		// RequestBody.create(MediaType.parse("application/json;
		// charset=utf-8"), json);

		Request request = new Request.Builder().url(URL + endpoint).addHeader("Authorization", "Bearer " + this.token)
				.header("Content-Length", "0").build();

		Response response = client.newCall(request).execute();

		// System.out.println(response.body().string());

		return response;
	}

	private Response get(String url) throws Exception {
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(url).build();

		Response response = client.newCall(request).execute();

		// System.out.println(response.body().string());

		return response;
	}

	public Hashtable<String, Object> queryCEP(String cep) throws Exception {
		Response response = this.get("https://viacep.com.br/ws/" + cep + "/json");

		if (response.code() == 404) {
			throw new Exception("CEP nao encontrado");
		} else {
			return this.conv.toObject(response.body().string());
		}
	}

	private Response get(String endpoint, Object obj) throws Exception {
		String json = this.conv.toJson(obj);
		return this.get(endpoint, json);
	}

	private void asyncPost(String endpoint, String json, Callback callback) throws Exception {
		OkHttpClient client = new OkHttpClient.Builder().readTimeout(5, TimeUnit.MINUTES)
				.connectTimeout(5, TimeUnit.MINUTES).build();
		RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

		Request request = new Request.Builder().url(URL + endpoint).post(post)
				.addHeader("Authorization", "Bearer " + this.token).build();

		client.newCall(request).enqueue(callback);

	}

	public void asyncPost(String endpoint, Object obj, Callback callback) throws Exception {
		String json = this.conv.toJson(obj);
		this.asyncPost(endpoint, json, callback);
	}

	private Response get(String endpoint, Hashtable<String, String> params) throws Exception {
		OkHttpClient client = new OkHttpClient();
		HttpUrl.Builder httpBuider = HttpUrl.parse(endpoint).newBuilder();

		for (String key : params.keySet()) {
			httpBuider.addQueryParameter(key, params.get(key));
		}

		HttpUrl url = httpBuider.build();

		Request request = new Request.Builder().url(url).addHeader("Authorization", "Bearer " + this.token)
				.header("Content-Length", "0").build();

		Response response = client.newCall(request).execute();

		return response;
	}

	public static void main33(String[] args) {
		try {
			VoceAzulHttpClient client = new VoceAzulHttpClient();

			Hashtable<String, String> info = new Hashtable<>();
			info.put("name", "Crhistian");
			info.put("id", "23135250822");
			info.put("password", "Crhistian");

			JSONConverter conv = new JSONConverter();

			// Response response =
			// client.postRaw("http://community.enigmou.com:3000/api/v1/users",
			// conv.toJson(info));

			info.put("query", "{users(id:\"5b86f3578eac7e31de8362af\"){name,id}}");

			Response response1 = client.get("http://community.enigmou.com:3000/graphql", info);
			System.out.println(response1.body().string());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");

			String CPF = "838.787.696-87";
			// String IP = "200.171.83.92";
			String hhmmss = sdf.format(Calendar.getInstance().getTime());

			BigDecimal bd_cpf = new BigDecimal(CPF.replace(".", "").replace("-", ""));
			// BigDecimal bd_ip = new BigDecimal(IP.replace(".", "").replace("-", ""));
			BigDecimal hh_mm = new BigDecimal(hhmmss.replace(":", "").replace("-", ""));

			BigDecimal total = new BigDecimal("0");
			total = total.add(bd_cpf);
			// total = total.add(bd_ip);
			total = total.add(hh_mm);

			System.out.println("cpf:" + CPF);
			// System.out.println("ip:" + IP);
			System.out.println("hhmmss: " + hhmmss);
			System.out.println("total:" + total);
			// String password = "115217172902";
			String hash = BCrypt.hashpw(total.toString(), BCrypt.gensalt());

			System.out.println("hash:" + hash);

			// System.out.println(BCrypt.checkpw(total.toString(), hash));

			int counter = 0;
			for (char c : hhmmss.toCharArray()) {
				hash = hash.substring(0, counter + 1) + c + hash.substring(counter + 1, hash.length());
				counter++;
				counter++;
			}

			System.out.println("token:" + hash);

			VoceAzulHttpClient client = new VoceAzulHttpClient();

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("cpf", CPF);
			params.put("token", hash);
			Response response = client.postURL("/consultas/cpf", params);

			String json = response.body().string();
			System.out.println("response: " + decode(json));

			FileWriter fw = new FileWriter("C:\\error.html");
			fw.write(json);
			fw.close();

			/*
			 * 
			 * System.out.println(client.queryCEP("03064000"));
			 * 
			 * // params.put("email", "suporte@voceazul.com.br");
			 * 
			 * response = client.postURL("/guest/users/email/exists", params); String
			 * response_str = response.body().string();
			 * 
			 * System.out.println(response_str);
			 * 
			 * JSONConverter conv = new JSONConverter();
			 * System.out.println(conv.toObject(response_str, Hashtable.class));
			 */

			// Hashtable<String, Object> map = conv.toObject(json);
			// System.out.println(((Map) map.get("data")).get("exists"));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static final String decode(final String in) {
		String working = in;
		int index;
		index = working.indexOf("\\u");
		while (index > -1) {
			int length = working.length();
			if (index > (length - 6))
				break;
			int numStart = index + 2;
			int numFinish = numStart + 4;
			String substring = working.substring(numStart, numFinish);
			int number = Integer.parseInt(substring, 16);
			String stringStart = working.substring(0, index);
			String stringEnd = working.substring(numFinish);
			working = stringStart + ((char) number) + stringEnd;
			index = working.indexOf("\\u");
		}
		return working;
	}

}
