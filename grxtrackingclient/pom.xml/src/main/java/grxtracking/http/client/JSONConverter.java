package grxtracking.http.client;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import grx.tracking.core.persistence.PermissionDescr;

public class JSONConverter {

	private Gson gson;

	public JSONConverter() {
		GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}
		});

		builder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {

			@Override
			public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
				return src == null ? null : new JsonPrimitive(src.getTime());
			}
		});

		builder.registerTypeAdapter(Serializable.class, new JsonDeserializer<List<PermissionDescr>>() {
			public List<PermissionDescr> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {
				return (List) context.deserialize(json, List.class);
			}
		});

		this.gson = builder.create();
	}

	public String toJson(Object obj) throws Exception {
		return this.gson.toJson(obj);
	}

	public <T> T toObject(String json, Class<T> classs) throws Exception {
		return this.gson.fromJson(json, classs);
	}

	public Hashtable<String, Object> toObject(String json) throws Exception {
		Type type = new TypeToken<Hashtable<String, Object>>() {
		}.getType();
		return this.gson.fromJson(json, type);
	}

}
