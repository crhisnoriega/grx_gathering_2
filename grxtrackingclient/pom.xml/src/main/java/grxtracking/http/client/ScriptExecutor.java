package grxtracking.http.client;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;

public class ScriptExecutor {
	private String command;
	private ByteArrayOutputStream out;
	private ByteArrayOutputStream error;
	private boolean finished;
	private String scriptsDir = System.getProperty("user.home") + File.separator + "scripts";

	public void runScript(String command) throws Exception {
		System.out.println("runScript:" + command);
		this.command = command;
		CommandLine cmdLine = CommandLine.parse(this.command);

		this.out = new ByteArrayOutputStream();
		this.error = new ByteArrayOutputStream();

		DefaultExecutor exec = new DefaultExecutor();
		exec.setStreamHandler(new PumpStreamHandler(this.out, this.error));
		exec.execute(cmdLine);

		System.out.println("exec.execute: " + cmdLine);
	}

	public boolean isFinished() {
		return finished;
	}

	public ByteArrayOutputStream getOut() {
		return out;
	}

	public void exec() {
		File[] scripts = new File(this.scriptsDir).listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getAbsolutePath().endsWith(".sh");
			}
		});

		this.finished = false;

		for (File script : scripts) {
			try {
				this.runScript(script.getAbsolutePath());
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}

		this.finished = true;

		System.out.println("acabou exec");

	}

	public ByteArrayOutputStream getError() {
		return error;
	}

	public interface ExecuterListener {
		public void showOut(String str);
	}

	public static void main(String[] args) {
		ScriptExecutor exec = new ScriptExecutor();
		new Thread(() -> {
			try {
				exec.runScript("/Users/cnoriega/myscript");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();

		while (!exec.isFinished()) {

			if (exec.getOut() != null) {
				System.out.println("out:" + exec.getOut().toString());

			}
			try {

				Thread.sleep(500);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
