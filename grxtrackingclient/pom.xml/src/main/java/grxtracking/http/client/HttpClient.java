package grxtracking.http.client;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import grx.tracking.core.events.EventFindTasks;
import grx.tracking.core.http.JSONConverter;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.EventMessage;
import grx.tracking.core.transport.TokenInfo;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpClient {

	private static Logger logger = Logger.getLogger(HttpClient.class.getCanonicalName());

	// private static String URL = "http://grxtracking.ddns.net/trackingrest";
	private static String URL = "http://grxtrackingserver.ddns.net/grxtrackingrest/v1";
	// private static String URL = "http://localhost:8080/grxtrackingrest/v1";

	private JSONConverter conv;

	private String token;

	public HttpClient() {
		this.conv = new JSONConverter();
	}

	public void setToken(String token) {
		this.token = token;
	}

	private Response post(String endpoint, String json) throws Exception {
		OkHttpClient client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.MINUTES)
				.readTimeout(5, TimeUnit.MINUTES).build();
		RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

		Request request = new Request.Builder().url(URL + endpoint).post(post)
				.addHeader("Authorization", "Bearer " + this.token).build();

		Response response = client.newCall(request).execute();

		if (response.code() == 403) {
			throw new Exception("erro enviando no servidor");
		}

		return response;
	}

	private <T> Response post(String endpoint, Object obj) throws Exception {
		String json = this.conv.toJson(obj);
		return this.post(endpoint, json);
	}

	private Response get(String endpoint, String json) throws Exception {
		OkHttpClient client = new OkHttpClient();
		// RequestBody get =
		// RequestBody.create(MediaType.parse("application/json;
		// charset=utf-8"), json);

		Request request = new Request.Builder().url(URL + endpoint).addHeader("Authorization", "Bearer " + this.token)
				.header("Content-Length", "0").build();

		Response response = client.newCall(request).execute();

		// System.out.println(response.body().string());

		return response;
	}

	private Response get(String endpoint, Object obj) throws Exception {
		String json = this.conv.toJson(obj);
		return this.get(endpoint, json);
	}

	public UserInfo loginWithUser(Credentials user) throws Exception {
		Response response = this.post("/authentication/user", user);
		if (response.isSuccessful()) {
			String json_response = response.body().string();

			logger.info("json response: " + json_response);

			return this.conv.toObject(json_response, UserInfo.class);
		} else {
			throw new Exception(response.message() + ": " + response.body().string());
		}
	}

	public void registerTokenFirebase(TokenInfo token) throws Exception {
		Response response = this.post("/authentication/registertoken", token);
		if (response.isSuccessful()) {
			String json_response = response.body().string();

			logger.info("json response: " + json_response);

		} else {
			throw new Exception(response.message() + ": " + response.body().string());
		}
	}

	private void asyncPost(String endpoint, String json, Callback callback) throws Exception {
		OkHttpClient client = new OkHttpClient.Builder().readTimeout(5, TimeUnit.MINUTES)
				.connectTimeout(5, TimeUnit.MINUTES).build();
		RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);

		Request request = new Request.Builder().url(URL + endpoint).post(post)
				.addHeader("Authorization", "Bearer " + this.token).build();

		client.newCall(request).enqueue(callback);

	}

	public void asyncPost(String endpoint, Object obj, Callback callback) throws Exception {
		String json = this.conv.toJson(obj);
		this.asyncPost(endpoint, json, callback);
	}

	public void sendEvent(EventMessage message) throws Exception {
		this.post("/grxevents/update", message);
	}

	public RouteInfo getRoute() throws Exception {
		Response response = this.get("/authentication/getroute", token);
		String jsonString = response.body().string();

		logger.info("jsonString: " + jsonString);

		RouteInfo route = this.conv.toObject(jsonString, RouteInfo.class);

		return route;

	}

	public List<OrderInfo> getOrders() throws Exception {
		Response response = this.get("/authentication/getrouteorders", token);
		String jsonString = response.body().string();

		System.out.println("jsonString: " + jsonString);

		return Arrays.asList(this.conv.toObject(jsonString, OrderInfo[].class));
	}

	private Response get(String endpoint, Hashtable<String, String> params) throws Exception {
		OkHttpClient client = new OkHttpClient();
		HttpUrl.Builder httpBuider = HttpUrl.parse(URL + endpoint).newBuilder();

		for (String key : params.keySet()) {
			httpBuider.addQueryParameter(key, params.get(key));
		}

		HttpUrl url = httpBuider.build();

		Request request = new Request.Builder().url(url).addHeader("Authorization", "Bearer " + this.token)
				.header("Content-Length", "0").build();

		Response response = client.newCall(request).execute();

		return response;
	}

	public List<TruckInfo> getTrucks(UserId userId) throws Exception {

		Hashtable<String, String> params = new Hashtable<>();
		params.put("user_id", userId.getUser_id());
		params.put("sector_id", userId.getSector_id());

		Response response = this.get("/dataprovider/trucksinfo", params);

		String jsonString = response.body().string();

		System.out.println("jsonString: " + jsonString);

		TruckInfo[] trucks = this.conv.toObject(jsonString, TruckInfo[].class);
		return Arrays.asList(trucks);
	}

	public List<SectorInfo> getBases() throws Exception {

		Hashtable<String, String> params = new Hashtable<>();

		Response response = this.get("/dataprovider/basesinfo", params);

		String jsonString = response.body().string();

		System.out.println("jsonString: " + jsonString);

		SectorInfo[] sectors = this.conv.toObject(jsonString, SectorInfo[].class);
		return Arrays.asList(sectors);
	}

	private String SERVER_KEY = "AAAACyDoCs0:APA91bFgJwnsCTVRmXdCkPNjET_a6lKu-ZnPNWd8aFhDFNF744krtgJ-1CuSt499DXKc7GURNBx8Aatc1whH321G0ola12cV4fsmOvZOZ9pC-d45bf6GJffJ40nmFgkdeNbclQL1gVDk";

	public void sendFirebaseMessage(String token, EventMessage message) throws Exception {

		JSONConverter conv = new JSONConverter();
		String jsonMessage = conv.toJson(message);

		String jsonGoogle = "{\"to\":\"" + token + "\",\"data\":" + jsonMessage + "}";

		OkHttpClient client = new OkHttpClient();
		RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonGoogle);

		Request request = new Request.Builder().url("https://fcm.googleapis.com/fcm/send").post(post)
				.addHeader("Authorization", "key=" + SERVER_KEY).build();

		Response response = client.newCall(request).execute();
		System.out.println(response.body().string());
	}

	/*
	 * public void updateGeoLocation(GeoLocationUpdate update) throws Exception { //
	 * this.post("/geolocation/update", locations);
	 * this.asyncPost("/geolocation/update", update, new Callback() {
	 * 
	 * public void onResponse(Call call, Response response) throws IOException {
	 * System.out.println(response.body().string()); }
	 * 
	 * public void onFailure(Call call, IOException io) {
	 * System.out.println(io.getMessage());
	 * 
	 * } }); System.out.println("called"); //
	 * System.out.println(response.body().string()); }
	 */
}
