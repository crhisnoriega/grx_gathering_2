package grx.tracking.rest;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserTruckMap;
import grx.tracking.rest.security.Secured;
import grx.tracking.rest.util.EJBLocator;

@ManagedBean
@Path("/dataprovider")
public class DataProviderEndpoint {

	public static Logger logger = Logger.getLogger(DataProviderEndpoint.class.getCanonicalName());

	@EJB
	private GRXTrackingService service;

	private EJBLocator locator;

	public DataProviderEndpoint() {

		this.locator = new EJBLocator();
	}

	@Secured
	@GET
	@Path("/trucksinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTruckInfo(@Context SecurityContext securityContext, @QueryParam("user_id") String user_id,
			@QueryParam("sector_id") String sector_id) throws Exception {
		try {
			logger.info("user_id:" + user_id + " sector_id: " + sector_id);

			UserId userId = new UserId(user_id, sector_id);
			String sql = "select m from UserTruckMap as m where m.userId = :userId";

			Hashtable<String, Object> params = new Hashtable<>();
			params.put("userId", userId);

			List<TruckInfo> trucks = new ArrayList<>();
			List<UserTruckMap> maps = this.service.query(UserTruckMap.class, sql, params);
			for (UserTruckMap map : maps) {
				TruckInfo truck = this.service.findEntity(map.getTruckId(), TruckInfo.class);
				if (truck != null) {
					trucks.add(truck);
				}
			}

			return Response.status(200).entity(trucks).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	@Secured
	@GET
	@Path("/basesinfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBasesInfo(@Context SecurityContext securityContext) throws Exception {
		try {
			String sql = "select b from SectorInfo as b";
			List<SectorInfo> sector = this.service.query(SectorInfo.class, sql, new Hashtable<>());
			return Response.status(200).entity(sector).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

}
