package grx.tracking.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.transport.Credentials;
import grx.tracking.core.transport.TokenInfo;
import grx.tracking.rest.security.Secured;
import grx.tracking.rest.util.EJBLocator;

@ManagedBean
@Path("/authentication")
public class AuthenticationEndpoint {

	public static Logger logger = Logger.getLogger(AuthenticationEndpoint.class.getCanonicalName());

	@EJB
	private GRXTrackingService service;

	private EJBLocator locator;

	public AuthenticationEndpoint() {
		this.locator = new EJBLocator();
	}

	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticateUser(Credentials credentials, @Context SecurityContext securityContext)
			throws Exception {
		try {
			UserInfo user = this.service.authenticate(credentials.getUsername(), credentials.getPassword(),
					credentials.getSector_id(), USER_ROLE.DRIVER, credentials.getApp_version());
			return Response.ok(user).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}

	}

	@GET
	@Path("/driver")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response authenticateDriver(Credentials credentials, @Context SecurityContext securityContext)
			throws Exception {

		UserInfo user = this.service.authenticate(credentials.getUsername(), credentials.getPassword(),
				credentials.getSector_id(), USER_ROLE.DRIVER, credentials.getApp_version());

		return Response.ok(user).build();
	}

	@Secured
	@GET
	@Path("/getroute")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRouteInfo(@Context SecurityContext securityContext) throws Exception {
		try {
			GRXTrackingSession session = this.locator.refUserSession(this.service,
					securityContext.getUserPrincipal().getName());
			return Response.ok().entity(session.getCurrentRoute()).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	@Secured
	@GET
	@Path("/getrouteorders")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRouteOrders(@Context SecurityContext securityContext) throws Exception {
		try {
			GRXTrackingSession session = this.locator.refUserSession(this.service,
					securityContext.getUserPrincipal().getName());

			List<OrderInfo> orders = session.getRouteOrders();
			orders.forEach(order -> order.getAddress()
					.setAddress(Address.extractAddressSimple(order.getAddress().getAddress().asAddressString())));

			return Response.ok().entity(orders).build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	@Secured
	@POST
	@Path("/registertoken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerFirebaseToken(TokenInfo token, @Context SecurityContext securityContext) throws Exception {
		try {
			GRXTrackingSession session = this.locator.refUserSession(this.service,
					securityContext.getUserPrincipal().getName());
			session.registerFirebaseToken(token);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.status(500).entity(e.getMessage()).build();
		}
	}

	@GET
	@Path("/list/{classs}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=iso-8859-1")
	public Response list(@PathParam("classs") String classs) throws Exception {
		logger.info("service: " + this.service);
		List ll = this.service.list(Class.forName(classs));
		return Response.status(200).entity(ll).encoding("iso-8859-1").build();
	}

}
