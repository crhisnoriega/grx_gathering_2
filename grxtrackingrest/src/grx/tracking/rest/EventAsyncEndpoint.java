package grx.tracking.rest;

import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSPasswordCredential;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import grx.tracking.core.ejb.GRXEventService;
import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.events.GRXEvent.EVENT_TYPE;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.transport.EventMessage;
import grx.tracking.rest.security.Secured;
import grx.tracking.rest.util.JSONConverter;

@ManagedBean
@Path("/grxevents")
public class EventAsyncEndpoint {
	private static Logger logger = Logger.getLogger(EventAsyncEndpoint.class.getCanonicalName());

	// usar fora do container
	@Resource(lookup = "java:/grxtracking/GRXConnectionFactory")
	private ConnectionFactory connectionFactory;

	@Resource(lookup = "java:/grxtracking/GRXQueueGeoLocation")
	private Destination destination;

	@EJB
	private GRXEventService eventService;

	// usar dentro do container
	@Inject
	@JMSConnectionFactory("java:/grxtracking/GRXConnectionFactory")
	@JMSPasswordCredential(userName = "grxclient", password = "crhisn2572")
	private JMSContext context;

	@Secured
	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	public void queueEvent(EventMessage event, @Context SecurityContext securityContext,
			@Suspended AsyncResponse response) throws Exception {

		// logger.info("json: " + event.getJsonString());

		/*
		 * logger.info("context: " + context); new Thread(() -> {
		 * context.createProducer().send(destination, locationUpdate); }).start();
		 */

		CompletableFuture.runAsync(() -> {
			try {

				JSONConverter conv = new JSONConverter();
				Object obj = conv.toObject(event.getJsonString(), Class.forName(event.getEventClass()));

				// logger.info("JSON obj: " + obj);

				if (obj instanceof GRXEvent) {
					GRXEvent grxEvent = (GRXEvent) obj;

					if (EVENT_TYPE.REAL_TIME.equals(grxEvent.getType())) {
						eventService.processEvent(grxEvent);
						response.resume(Response.ok("OK").build());
					}

					if (EVENT_TYPE.QUEUE.equals(grxEvent.getType())) {
						QueueConnection connection = (QueueConnection) connectionFactory.createConnection("grxclient",
								"crhisn2572");
						QueueSession session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
						MessageProducer producer = session.createProducer(destination);

						ObjectMessage message = session.createObjectMessage(grxEvent);
						producer.setDeliveryMode(DeliveryMode.PERSISTENT);
						producer.send(message);

						producer.close();
						session.close();
						connection.close();

						response.resume(Response.ok("OK").build());
					}
				}

			} catch (Exception e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
				response.resume(Response.status(500).entity(e.getLocalizedMessage()).build());
			}
		});

	}

	@EJB
	private GRXTrackingService service;

	@GET
	@Path("/test")
	public void test() {
		try {
			logger.info("runnig test");
			Future<List<UserId>> future = service.getSessionsRemote(null, USER_ROLE.CONTROLLER);
			List<UserId> users = future.get();
			users.forEach(user -> {
				try {
					Hashtable<String, Object> props = new Hashtable<>();
					props.put("route_id", "Route 123");
					service.getUserSessionEJB(user).showNotificationInWindow("hola", "hola", props);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
