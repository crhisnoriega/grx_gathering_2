package grx.tracking.rest.util;

import java.lang.reflect.Type;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class JSONConverter {

	private Gson gson;

	public JSONConverter() {
		GsonBuilder builder = new GsonBuilder();

		builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
					throws JsonParseException {
				return new Date(json.getAsJsonPrimitive().getAsLong());
			}
		});

		this.gson = builder.create();
	}

	public String toJson(Object obj) throws Exception {
		return this.gson.toJson(obj);
	}

	public <T> T toObject(String json, Class<T> classs) throws Exception {
		return this.gson.fromJson(json, classs);
	}

}
