package grx.tracking.rest.util;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.UserId;

public class EJBLocator {

	public GRXTrackingSession refUserSession(GRXTrackingService service, String userIdAsString) throws Exception {
		String[] parts = userIdAsString.split("\\|");

		String user_id = parts[0];
		String sector_id = parts[1];

		UserId userId = new UserId(user_id, sector_id);
		return service.getUserSessionEJB(userId);
	}
}
