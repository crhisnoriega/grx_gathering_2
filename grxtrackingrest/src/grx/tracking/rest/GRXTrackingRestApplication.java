package grx.tracking.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/v1")
public class GRXTrackingRestApplication extends Application {

}
