package grx.tracking.rest.security;

import java.io.IOException;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import grx.tracking.core.ejb.GRXTrackingService;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

	public static Logger logger = Logger.getLogger(AuthenticationFilter.class.getCanonicalName());

	@EJB
	private GRXTrackingService service;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

		if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
			throw new NotAuthorizedException("Authorization header must be provided");
		}

		try {
			String token = authorizationHeader.substring("Bearer".length()).trim();

			String userIdString = this.service.validateToken(token);

			requestContext.setSecurityContext(new SecurityContext() {

				@Override
				public boolean isUserInRole(String arg0) {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public boolean isSecure() {
					// TODO Auto-generated method stub
					return true;
				}

				@Override
				public Principal getUserPrincipal() {
					// TODO Auto-generated method stub
					return new Principal() {

						@Override
						public String getName() {
							return userIdString;
						}
					};
				}

				@Override
				public String getAuthenticationScheme() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		} catch (Exception e) {
			logger.warning("%%%%%% autenthication error: " + e.getLocalizedMessage() + " calling method: "
					+ requestContext.getMethod() + " %%%%%%");
			requestContext
					.abortWith(Response.status(403).entity("autenthication error: " + e.getLocalizedMessage()).build());
		}

	}

}