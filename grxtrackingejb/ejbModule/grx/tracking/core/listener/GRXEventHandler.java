package grx.tracking.core.listener;

import grx.tracking.core.events.GRXEvent;

public interface GRXEventHandler {

	public void processEvent(GRXEvent event) throws Exception;
}
