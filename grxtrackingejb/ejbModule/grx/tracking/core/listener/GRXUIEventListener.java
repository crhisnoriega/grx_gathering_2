package grx.tracking.core.listener;

import java.rmi.Remote;
import java.util.Hashtable;

import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.UserId;

public interface GRXUIEventListener extends Remote {

	public void drawMapObjectInMap(MapObject mapObject) throws Exception;

	public void removeMapObjectInMap(MapObject mapObject) throws Exception;

	public void showNotificationInWindow(String title, String message, Hashtable<String, Object> props)
			throws Exception;

	public void showOcurrence(String title, String message, String ocurrence_id, Hashtable<String, Object> props)
			throws Exception;

	public boolean isUIActive() throws Exception;

	public void updateMapObjectInfo(MapObject mapObject) throws Exception;

	public void doFilterMap(boolean update) throws Exception;
}
