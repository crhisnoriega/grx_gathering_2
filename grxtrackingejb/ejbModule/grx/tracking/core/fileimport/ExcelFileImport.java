package grx.tracking.core.fileimport;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.CustomerId;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.util.MySphericalUtil;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;

public class ExcelFileImport {

	private static Logger logger = Logger.getLogger(ExcelFileImport.class.getCanonicalName());

	private Workbook workbook;
	private Sheet firstSheet;
	private String sector_id;
	private GoogleMapsServices googleServices;
	private EntityManager em;
	private List<SectorInfo> bases;

	public ExcelFileImport(EntityManager em, String sector_id, String filePath) throws Exception {
		this.em = em;
		this.sector_id = sector_id;
		{
			TypedQuery<SectorInfo> query = this.em.createQuery("select b from SectorInfo as b", SectorInfo.class);
			this.bases = query.getResultList();
		}

		if (filePath.endsWith("xlsx")) {
			this.workbook = new XSSFWorkbook(new File(filePath));
		} else {
			this.workbook = new HSSFWorkbook(new FileInputStream(new File(filePath)));
		}

		this.firstSheet = this.workbook.getSheetAt(0);
		this.googleServices = new GoogleMapsServices();
	}

	private String getValue(Cell cell, DecimalFormat formater, int type) {
		if (cell == null) {
			return null;
		}

		String value = "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			value = cell.getStringCellValue();

			if (Cell.CELL_TYPE_STRING != type) {
				value = null;
			}
			break;

		case Cell.CELL_TYPE_NUMERIC:
			if (formater != null) {
				value = formater.format(cell.getNumericCellValue());
			} else {
				value = cell.getNumericCellValue() + "";
			}

			if (Cell.CELL_TYPE_NUMERIC != type) {
				value = null;
			}
			break;

		default:
			break;
		}

		return value;
	}

	private SectorInfo findNearestBase(GeoLocation location) {

		SectorInfo near = null;
		double minor_distance = Double.MAX_VALUE;
		for (SectorInfo base : this.bases) {
			double distance = MySphericalUtil.computeDistanceBetween(location, base.getLocation());
			if (distance < minor_distance) {
				near = base;
				minor_distance = distance;
			}
		}

		return near;
	}

	public List<OrderInfo> process() {
		List<OrderInfo> orders = new ArrayList<>();
		Iterator<Row> iterator = this.firstSheet.iterator();
		while (iterator.hasNext()) {
			Row nextRow = iterator.next();
			String cliente = this.getValue(nextRow.getCell(1), null, Cell.CELL_TYPE_STRING);

			String endereco = this.getValue(nextRow.getCell(4), null, Cell.CELL_TYPE_STRING);
			String qtde = this.getValue(nextRow.getCell(5), null, Cell.CELL_TYPE_NUMERIC);
			String date = this.getValue(nextRow.getCell(6), null, Cell.CELL_TYPE_STRING);
			String numero_coleta = this.getValue(nextRow.getCell(7), null, Cell.CELL_TYPE_NUMERIC);
			String tipo = this.getValue(nextRow.getCell(8), null, Cell.CELL_TYPE_STRING);
			String situacao = this.getValue(nextRow.getCell(9), null, Cell.CELL_TYPE_STRING);
			String qtde_equip = this.getValue(nextRow.getCell(10), null, Cell.CELL_TYPE_NUMERIC);
			String tipo_equip = this.getValue(nextRow.getCell(11), null, Cell.CELL_TYPE_STRING);

			if (numero_coleta == null || numero_coleta.isEmpty()) {
				continue;
			}

			OrderInfo order = new OrderInfo();

			OrderId orderId = new OrderId();
			orderId.setOrder_id(numero_coleta);
			orderId.setSector_id(this.sector_id);

			order.setOrderId(orderId);
			try {
				order.setQtdRequested(Integer.parseInt(qtde));
			} catch (Exception e) {
			}

			try {
				Address addr = Address.extractAddress(endereco);

				AddressInfo address = new AddressInfo();
				address.setAddress(addr);
				order.setAddress(address);

				GeoLocation geolocation = this.googleServices.geoDecoding(addr);
				address.setLocation(geolocation);

				// find near base
				order.setNearest_Sector_Id(this.findNearestBase(geolocation).getSectorId());

				System.out.println(order.getNearest_Sector_Id());
				
			} catch (Exception e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
			}

			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				order.setRequestDate(sdf.parse(date));
			} catch (Exception e) {
				// logger.log(Level.INFO, e.getLocalizedMessage(), e);
			}
			{
				if (tipo.toLowerCase().equals("retira")) {
					order.setOrderType(ORDER_TYPE.PICK);
				}

				if (tipo.toLowerCase().equals("entrega")) {
					order.setOrderType(ORDER_TYPE.DELIVERY);
				}

				if (tipo.toLowerCase().equals("troca")) {
					order.setOrderType(ORDER_TYPE.EXCHANGE);
				}
			}

			{
				OrderStatus status = new OrderStatus();
				status.setStatus(ORDER_STATUS.TO_VALIDATE);
				order.setCurrentStatus(status);
			}
			{
				CustomerId customerId = new CustomerId();
				customerId.setCustomer_id(cliente);
				customerId.setSector_id(this.sector_id);
				order.setCustomerId(customerId);
			}

			orders.add(order);
			if (this.em.find(OrderInfo.class, order.getOrderId()) == null) {
				this.em.persist(order);
			} else {
				this.em.merge(order);
			}
			this.em.flush();

		}

		logger.info("################# acabou #########################");
		return orders;
	}

	public static void main(String[] args) {
		try {
			ExcelFileImport importer = new ExcelFileImport(null, "", "C:\\LIBSERV.xlsx");
			importer.process();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
