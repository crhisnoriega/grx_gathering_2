package grx.tracking.core.events;

import grx.tracking.core.persistence.RouteStatus;

public class EventChangeStatusRoute extends GRXEvent {

	private static final long serialVersionUID = 1L;
	private RouteStatus newStatus;
	private String route_id;

	public RouteStatus getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(RouteStatus newStatus) {
		this.newStatus = newStatus;
	}

	public String getRoute_id() {
		return route_id;
	}

	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}

}
