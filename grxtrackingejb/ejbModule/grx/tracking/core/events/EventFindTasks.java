package grx.tracking.core.events;

import java.util.List;

import grx.tracking.core.persistence.EquipmentAllocateInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;

public class EventFindTasks extends GRXEvent {

	private static final long serialVersionUID = 1L;

	private TruckId truckId;
	private boolean startBase;
	private String sector_id;
	private List<EquipmentAllocateInfo> allocated;
	private int deliverCapacity;
	private int pickupCapacity;
	private boolean confirmRoute;

	public String getSector_Id() {
		return sector_id;
	}

	public void setSector_Id(String sector_id) {
		this.sector_id = sector_id;
	}

	public TruckId getTruckId() {
		return truckId;
	}

	public void setTruckId(TruckId truckId) {
		this.truckId = truckId;
	}

	public boolean isStartBase() {
		return startBase;
	}

	public void setStartBase(boolean startBase) {
		this.startBase = startBase;
	}

	public List<EquipmentAllocateInfo> getAllocated() {
		return allocated;
	}

	public void setAllocated(List<EquipmentAllocateInfo> allocated) {
		this.allocated = allocated;
	}

	public int getDeliverCapacity() {
		return deliverCapacity;
	}

	public void setDeliverCapacity(int deliverCapacity) {
		this.deliverCapacity = deliverCapacity;
	}

	public int getPickupCapacity() {
		return pickupCapacity;
	}

	public void setPickupCapacity(int pickupCapacity) {
		this.pickupCapacity = pickupCapacity;
	}

	public boolean isConfirmRoute() {
		return confirmRoute;
	}

	public void setConfirmRoute(boolean confirmRoute) {
		this.confirmRoute = confirmRoute;
	}

}
