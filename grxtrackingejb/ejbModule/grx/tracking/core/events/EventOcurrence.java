package grx.tracking.core.events;

import grx.tracking.core.persistence.OccurrenceInfo;

public class EventOcurrence extends GRXEvent {

	private static final long serialVersionUID = 1L;

	private OccurrenceInfo ocurrence;

	public OccurrenceInfo getOcurrence() {
		return ocurrence;
	}

	public void setOcurrence(OccurrenceInfo ocurrence) {
		this.ocurrence = ocurrence;
	}

}
