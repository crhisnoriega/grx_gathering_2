package grx.tracking.core.events;

import java.io.Serializable;
import java.util.Date;

import grx.tracking.core.persistence.UserId;

public abstract class GRXEvent implements Serializable {

	public static enum EVENT_TYPE {
		QUEUE, REAL_TIME
	};

	public static enum TO_TYPE {
		TO_SECTION, TO_USER, BROADCAST
	}

	private static final long serialVersionUID = 1L;

	protected String id;
	protected UserId fromUserId;
	protected UserId toUserId;
	protected Date dateEvent;
	protected EVENT_TYPE type;
	protected TO_TYPE to;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserId getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(UserId fromUserId) {
		this.fromUserId = fromUserId;
	}

	public UserId getToUserId() {
		return toUserId;
	}

	public void setToUserId(UserId toUserId) {
		this.toUserId = toUserId;
	}

	public Date getDateEvent() {
		return dateEvent;
	}

	public void setDateEvent(Date dateEvent) {
		this.dateEvent = dateEvent;
	}

	public EVENT_TYPE getType() {
		return type;
	}

	public void setType(EVENT_TYPE type) {
		this.type = type;
	}

	public TO_TYPE getTo() {
		return to;
	}

	public void setTo(TO_TYPE to) {
		this.to = to;
	}

}
