package grx.tracking.core.events;

import java.util.Date;

import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;

public class EventChangeStatusOrder extends GRXEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private OrderId orderId;

	private ORDER_STATUS newStatus;
	
	private String route_id;

	private Date date;

	public OrderId getOrderId() {
		return orderId;
	}

	public void setOrderId(OrderId orderId) {
		this.orderId = orderId;
	}

	public ORDER_STATUS getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(ORDER_STATUS newStatus) {
		this.newStatus = newStatus;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getRoute_id() {
		return route_id;
	}

	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}

	
}
