package grx.tracking.core.events;

import grx.tracking.core.persistence.GeoLocation;

import java.util.List;

public class EventUpdateGeoLocation extends GRXEvent {

	private static final long serialVersionUID = 1L;
	public List<GeoLocation> locations;
	public String message;

	public List<GeoLocation> getLocations() {
		return locations;
	}

	public void setLocations(List<GeoLocation> locations) {
		this.locations = locations;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
