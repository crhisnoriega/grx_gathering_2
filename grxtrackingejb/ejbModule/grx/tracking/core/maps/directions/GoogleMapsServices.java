package grx.tracking.core.maps.directions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import com.google.maps.DirectionsApi;
import com.google.maps.ElevationApi;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.ElevationResult;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;

import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.GeoLocation;

public class GoogleMapsServices {

	private static Logger logger = Logger.getLogger(GoogleMapsServices.class.getCanonicalName());

	// public static String API_KEY = "AIzaSyBG-8a072nSwUAZaA_f4cJibyJtonJG2ds";
	public static String API_KEY = "AIzaSyBGsIGkqfhqSg2VC0ysYvgoD85gVck3KnE";

	private GeoApiContext context;

	public GoogleMapsServices() {
		this.context = new GeoApiContext.Builder().retryTimeout(2, TimeUnit.SECONDS).apiKey(API_KEY).build();

	}

	public GeoLocation geoDecoding(Address addr) throws Exception {
		String address_str = addr.getXLgr() + " " + addr.getEndereco() + ", " + addr.getNro() + ", " + addr.getXBairro()
				+ ", " + addr.getCidade() + ", " + addr.getXMun();
		address_str = address_str.replace("null", "");
		return this.geoDecoding(address_str);
	}

	public GeoLocation geoDecoding(String address) throws Exception {

		GeoLocation geoLocation = new GeoLocation();

		GeocodingResult[] results = GeocodingApi.geocode(this.context, address).await();
		for (GeocodingResult result : results) {
			geoLocation.setLatitude(result.geometry.location.lat);
			geoLocation.setLongitude(result.geometry.location.lng);

			break;
		}

		return geoLocation;
	}

	public Address reverseDecoding(GeoLocation location) throws Exception {
		Address address = new Address();

		LatLng location2 = new LatLng(location.getLatitude(), location.getLongitude());
		GeocodingResult[] results = GeocodingApi.reverseGeocode(this.context, location2).await();
		for (GeocodingResult result : results) {

			AddressComponent[] address_comp = result.addressComponents;
			for (AddressComponent component : address_comp) {
				System.out.println(component.longName + " " + component.types[0]);
				List<AddressComponentType> types = Arrays.asList(new AddressComponentType[] { component.types[0] });
				if (types.contains(AddressComponentType.STREET_NUMBER)) {
					address.setNro(component.longName.replace("-", " ").toUpperCase());
				}
				if (types.contains(AddressComponentType.ROUTE)) {
					address.setEndereco(component.longName.toUpperCase());
				}
				if (types.contains(AddressComponentType.POLITICAL)) {
					address.setXBairro(component.longName.toUpperCase());
				}

				if (types.contains(AddressComponentType.LOCALITY)) {
					address.setXMun(component.longName.toUpperCase());

				}

				if (address.getXMun() == "" && types.contains(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {
					address.setXMun(component.longName.toUpperCase());
				}

				if (address.getXMun() == "" && types.contains(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_2)) {
					address.setXMun(component.longName.toUpperCase());
				}

				if (types.contains(AddressComponentType.POSTAL_CODE)) {
					address.setCEP(component.longName.toUpperCase());
				}
			}

			break;
		}

		logger.info("address:" + address.getXBairro());
		return address;
	}

	public GoogleRouteInfo extractRouteInfo(String start, String end) throws Exception {
		return extractRouteInfo(geoDecoding(start), geoDecoding(end));
	}

	public static List<LatLng> decode(final String encodedPath) {
		int len = encodedPath.length();

		// For speed we preallocate to an upper bound on the final length, then
		// truncate the array before returning.
		final List<LatLng> path = new ArrayList<LatLng>();
		int index = 0;
		int lat = 0;
		int lng = 0;

		while (index < len) {
			int result = 1;
			int shift = 0;
			int b;
			do {
				b = encodedPath.charAt(index++) - 63 - 1;
				result += b << shift;
				shift += 5;
			} while (b >= 0x1f);
			lat += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

			result = 1;
			shift = 0;
			do {
				b = encodedPath.charAt(index++) - 63 - 1;
				result += b << shift;
				shift += 5;
			} while (b >= 0x1f);
			lng += (result & 1) != 0 ? ~(result >> 1) : (result >> 1);

			path.add(new LatLng(lat * 1e-5, lng * 1e-5));
		}

		return path;
	}

	public GoogleRouteInfo extractRouteInfo(GeoLocation start, GeoLocation end) throws Exception {

		logger.info("### using key: " + API_KEY);
		GoogleRouteInfo googleRoute = new GoogleRouteInfo();

		googleRoute.setStart(start);
		googleRoute.setEnd(end);

		List<String> drawRepresentation = new ArrayList<>();
		googleRoute.setDrawRepresentation(drawRepresentation);

		List<GeoLocation> locations = new ArrayList<>();
		googleRoute.setLocationRepresentation(locations);

		{
			LatLng origin = new LatLng(start.getLatitude(), start.getLongitude());
			LatLng destination = new LatLng(end.getLatitude(), end.getLongitude());

			DirectionsResult calculatedRoutes = DirectionsApi.newRequest(context).mode(TravelMode.DRIVING)
					.origin(origin).destination(destination).await();

			DirectionsRoute[] routes = calculatedRoutes.routes;
			for (DirectionsRoute route : routes) {
				for (DirectionsLeg leg : route.legs) {
					for (DirectionsStep step : leg.steps) {
						drawRepresentation.add(step.polyline.getEncodedPath());

						// System.out.println(decode(step.polyline.getEncodedPath()));

						List<LatLng> points = decode(step.polyline.getEncodedPath());

						for (LatLng result : points) {
							GeoLocation loc = new GeoLocation();
							loc.setLatitude(result.lat);
							loc.setLongitude(result.lng);

							locations.add(loc);
						}
						/*
						 * { // System.out.println("encodedPath:" + step.polyline.getEncodedPath());
						 * EncodedPolyline encodedPolyline = new
						 * EncodedPolyline(step.polyline.getEncodedPath()); ElevationResult[] results =
						 * ElevationApi.getByPoints(context, encodedPolyline).await();
						 * 
						 * List<GeoLocation> tmp = new ArrayList<>(); for (ElevationResult result :
						 * results) { GeoLocation loc = new GeoLocation();
						 * loc.setLatitude(result.location.lat); loc.setLongitude(result.location.lng);
						 * loc.setElevation(result.elevation);
						 * 
						 * locations.add(loc); }
						 * 
						 * System.out.println(tmp); }
						 */

					}
					googleRoute.setDistance(leg.distance.inMeters + "");
					googleRoute.setTime(leg.duration.inSeconds + "");

					// just first leg
					break;
				}

				// just first route
				break;
			}
		}

		return googleRoute;
	}

	public void calculateRoute(String origin, String destination) throws Exception {

		DirectionsResult calculatedRoutes = DirectionsApi.newRequest(context).mode(TravelMode.DRIVING).origin(origin)
				.destination(destination).await();
		DirectionsRoute[] rr = calculatedRoutes.routes;
		for (DirectionsRoute r : rr) {
			for (DirectionsLeg leg : r.legs) {
				System.out.println("leg: " + leg.distance.inMeters);

				for (DirectionsStep step : leg.steps) {

					System.out.println(step.polyline.getEncodedPath());

					EncodedPolyline encodedPolyline = new EncodedPolyline(step.polyline.getEncodedPath());
					ElevationResult[] result = ElevationApi.getByPoints(context, encodedPolyline).await();

					for (ElevationResult rrr : result) {

					}
				}

				System.out.println(" l --------------------------------------");

			}

			System.out.println(" r --------------------------------------");
		}
	}

	public static void main(String[] args) {

		try {

			GoogleMapsServices c = new GoogleMapsServices();
			GeoLocation location = new GeoLocation();
			location.setLatitude(-23.58847669263849);
			location.setLongitude(-46.54373703848421);
			Address addresss = c.reverseDecoding(location);

			System.out.println(addresss.asAddressString());
			// c.calculateRoute("tatuape, sao paulo, brazil", "penha, sao paulo, brazil");*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
