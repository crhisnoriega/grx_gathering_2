package grx.tracking.core.maps.directions;

public class Route {

	private Leg[] legs;

	public Leg[] getLegs() {
		return legs;
	}

	public void setLegs(Leg[] legs) {
		this.legs = legs;
	}

}
