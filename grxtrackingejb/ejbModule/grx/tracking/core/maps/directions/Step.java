package grx.tracking.core.maps.directions;

import com.google.gson.JsonObject;

public class Step {
	private JsonObject distance;

	private JsonObject polyline;

	public JsonObject getDistance() {
		return distance;
	}

	public void setDistance(JsonObject distance) {
		this.distance = distance;
	}

	public JsonObject getPolyline() {
		return polyline;
	}

	public void setPolyline(JsonObject polyline) {
		this.polyline = polyline;
	}

}
