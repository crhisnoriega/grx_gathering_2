package grx.tracking.core.maps.directions;

import java.io.Serializable;
import java.util.List;

import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderInfo;

public class GoogleRouteInfo implements Serializable {

	private GeoLocation start;
	private GeoLocation end;

	private OrderInfo startOrder;
	private OrderInfo endOrder;

	private String time;
	private String distance;
	private List<String> drawRepresentation;
	private List<GeoLocation> locationRepresentation;

	public GeoLocation getStart() {
		return start;
	}

	public void setStart(GeoLocation start) {
		this.start = start;
	}

	public GeoLocation getEnd() {
		return end;
	}

	public void setEnd(GeoLocation end) {
		this.end = end;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public List<String> getDrawRepresentation() {
		return drawRepresentation;
	}

	public void setDrawRepresentation(List<String> drawRepresentation) {
		this.drawRepresentation = drawRepresentation;
	}

	public List<GeoLocation> getLocationRepresentation() {
		return locationRepresentation;
	}

	public void setLocationRepresentation(List<GeoLocation> locationRepresentation) {
		this.locationRepresentation = locationRepresentation;
	}

	public OrderInfo getStartOrder() {
		return startOrder;
	}

	public void setStartOrder(OrderInfo startOrder) {
		this.startOrder = startOrder;
	}

	public OrderInfo getEndOrder() {
		return endOrder;
	}

	public void setEndOrder(OrderInfo endOrder) {
		this.endOrder = endOrder;
	}

}
