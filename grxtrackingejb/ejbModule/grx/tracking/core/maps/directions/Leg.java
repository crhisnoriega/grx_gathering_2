package grx.tracking.core.maps.directions;

public class Leg {

	private Step[] steps;

	public Step[] getSteps() {
		return steps;
	}

	public void setSteps(Step[] steps) {
		this.steps = steps;
	}

}
