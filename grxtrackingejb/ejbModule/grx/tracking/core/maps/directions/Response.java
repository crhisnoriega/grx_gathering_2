package grx.tracking.core.maps.directions;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Response {

	private Route[] routes;

	private JsonArray geocoded_waypoints;

	private String status;

	public Route[] getRoutes() {
		return routes;
	}

	public void setRoutes(Route[] routes) {
		this.routes = routes;
	}

	public JsonArray getGeocoded_waypoints() {
		return geocoded_waypoints;
	}

	public void setGeocoded_waypoints(JsonArray geocoded_waypoints) {
		this.geocoded_waypoints = geocoded_waypoints;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
