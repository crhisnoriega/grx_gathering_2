package grx.tracking.core.maps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import grx.tracking.core.maps.MapObject.MAP_OBJECT_TYPE;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.CTRStatus.CTR_STATUS;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderInfo.PERIOD_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;

public class MapObject implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum MAP_OBJECT_TYPE {
		DRIVER, ORDER, ROUTE, BASE
	}

	private String id;
	private String label;
	private MAP_OBJECT_TYPE type;

	private GeoLocation location;
	private List<GoogleRouteInfo> route;

	private UserId userId;
	private OrderId orderId;
	private ORDER_TYPE orderType;
	private CTR_STATUS ctrStatus;
	private PERIOD_TYPE periodType;
	private String route_id;

	private String description;
	private String distance;

	private Date date;

	private String gerator;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public void setLocation(GeoLocation location) {
		this.location = location;
	}

	public List<GoogleRouteInfo> getRoute() {
		return route;
	}

	public void setRoute(List<GoogleRouteInfo> route) {
		this.route = route;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public OrderId getOrderId() {
		return orderId;
	}

	public void setOrderId(OrderId orderId) {
		this.orderId = orderId;
	}

	public String getRoute_id() {
		return route_id;
	}

	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}

	public MAP_OBJECT_TYPE getType() {
		return type;
	}

	public void setType(MAP_OBJECT_TYPE type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ORDER_TYPE getOrderType() {
		return orderType;
	}

	public void setOrderType(ORDER_TYPE orderType) {
		this.orderType = orderType;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public PERIOD_TYPE getPeriodType() {
		return periodType;
	}

	public void setPeriodType(PERIOD_TYPE periodType) {
		this.periodType = periodType;
	}

	public static MapObject createMapObject(SectorInfo base) {

		MapObject mapObj = new MapObject();
		mapObj.setId(base.getSectorId());

		if (base.getLocation() != null) {
			mapObj.setLocation(base.getLocation());

			if (base.getAddress() != null) {
				mapObj.setDescription(base.getAddress().asAddressString());
			}

			mapObj.setLabel(base.getName());
			mapObj.setType(MAP_OBJECT_TYPE.BASE);

		}

		return mapObj;

	}

	public static MapObject createMapObject(UserInfo userInfo, GeoLocation currentLocation) {
		MapObject driverObject = new MapObject();
		driverObject.setId(userInfo.getUserId().toString());
		driverObject.setType(MAP_OBJECT_TYPE.DRIVER);
		driverObject.setLocation(currentLocation);
		driverObject.setLabel(userInfo.getName());
		return driverObject;
	}

	public static MapObject createMapObject(RouteInfo route) {
		MapObject routeObject = new MapObject();
		routeObject.setId("route_" + route.getId());
		routeObject.setType(MAP_OBJECT_TYPE.ROUTE);
		routeObject.setLabel("Rota do usuario " + route.getUserId().getUser_id());

		List<GoogleRouteInfo> points = new ArrayList<>();

		RoutePointInfo[] routePoints = route.getPoints();
		for (RoutePointInfo routePoint : routePoints) {
			points.add(routePoint.getGoogleRoute());
		}

		routeObject.setRoute(points);

		return routeObject;
	}

	public static MapObject createMapObject(OrderInfo order) {

		MapObject mapObj = new MapObject();
		mapObj.setId(order.getOrderId().toString());

		if (order.getAddress() != null) {
			mapObj.setLocation(order.getAddress().getLocation());
			String address_str = order.getAddress().getAddress().getEndereco() + " "
					+ order.getAddress().getAddress().getNro() + ", " + order.getAddress().getAddress().getXBairro()
					+ ", " + order.getAddress().getAddress().getCidade() + ".";

			address_str = address_str.replace("null", "");
			mapObj.setDescription(address_str);
		}

		mapObj.setLabel(order.getOrderId().toString());
		mapObj.setOrderId(order.getOrderId());
		mapObj.setType(MAP_OBJECT_TYPE.ORDER);
		mapObj.setPeriodType(order.getPeriodType());

		mapObj.setOrderType(order.getOrderType());

		return mapObj;

	}

	public CTR_STATUS getCtrStatus() {
		return ctrStatus;
	}

	public void setCtrStatus(CTR_STATUS ctrStatus) {
		this.ctrStatus = ctrStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapObject other = (MapObject) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getGerator() {
		return gerator;
	}

	public void setGerator(String gerator) {
		this.gerator = gerator;
	}

}
