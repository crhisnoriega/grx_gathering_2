package grx.tracking.core.maps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MapModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<MapObject> objects;

	public List<MapObject> getObjects() {
		if (this.objects == null) {
			this.objects = new ArrayList<>();
		}
		return objects;
	}

	public void setObjects(List<MapObject> objects) {
		this.objects = objects;
	}

}
