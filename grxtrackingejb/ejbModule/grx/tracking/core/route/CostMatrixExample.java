package grx.tracking.core.route;

import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import com.graphhopper.jsprit.core.problem.cost.VehicleRoutingTransportCosts;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.VehicleRoute;
import com.graphhopper.jsprit.core.problem.solution.route.activity.TourActivities;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleType;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleTypeImpl;
import com.graphhopper.jsprit.core.reporting.SolutionPrinter;
import com.graphhopper.jsprit.core.util.Solutions;
import com.graphhopper.jsprit.core.util.VehicleRoutingTransportCostsMatrix;
//import com.graphhopper.jsprit.util.Examples;

import java.util.Collection;

/**
 * Illustrates how you can use jsprit with an already compiled distance and time
 * matrix.
 *
 * @author schroeder
 */
public class CostMatrixExample {

	public static void main(String[] args) {
		/*
		 * some preparation - create output folder
		 */
		// Examples.createOutputFolder();

		VehicleType type = VehicleTypeImpl.Builder.newInstance("type").addCapacityDimension(0, 10).build();
		VehicleImpl vehicle = VehicleImpl.Builder.newInstance("vehicle").setStartLocation(Location.newInstance("0"))
				.setType(type).build();

		Service s1 = Service.Builder.newInstance("1").setPriority(10).addSizeDimension(0, 1)
				.setLocation(Location.newInstance("1")).build();
		Service s2 = Service.Builder.newInstance("2").setPriority(1).addSizeDimension(0, 1)
				.setLocation(Location.newInstance("2")).build();
		Service s3 = Service.Builder.newInstance("3").setPriority(1).addSizeDimension(0, 1)
				.setLocation(Location.newInstance("3")).build();

		/*
		 * Assume the following symmetric distance-matrix from,to,distance 0,1,10.0
		 * 0,2,20.0 0,3,5.0 1,2,4.0 1,3,1.0 2,3,2.0
		 *
		 * and this time-matrix 0,1,5.0 0,2,10.0 0,3,2.5 1,2,2.0 1,3,0.5 2,3,1.0
		 */
		// define a matrix-builder building a symmetric matrix
		VehicleRoutingTransportCostsMatrix.Builder costMatrixBuilder = VehicleRoutingTransportCostsMatrix.Builder
				.newInstance(true);
		costMatrixBuilder.addTransportDistance("0", "1", 20.0);
		costMatrixBuilder.addTransportDistance("0", "2", 20.0);
		costMatrixBuilder.addTransportDistance("0", "3", 60.0);
		costMatrixBuilder.addTransportDistance("3", "0", 1.0);
		costMatrixBuilder.addTransportDistance("1", "2", 4.0);
		costMatrixBuilder.addTransportDistance("1", "3", 10.0);
		costMatrixBuilder.addTransportDistance("2", "3", 2.0);

		/*
		 * costMatrixBuilder.addTransportTime("0", "1", 100.0);
		 * costMatrixBuilder.addTransportTime("0", "2", 20.0);
		 * costMatrixBuilder.addTransportTime("0", "3", 60.0);
		 * costMatrixBuilder.addTransportTime("1", "2", 4.0);
		 * costMatrixBuilder.addTransportTime("1", "3", 1.0);
		 * costMatrixBuilder.addTransportTime("2", "3", 2.0);
		 */

		VehicleRoutingTransportCosts costMatrix = costMatrixBuilder.build();

		VehicleRoutingProblem vrp = VehicleRoutingProblem.Builder.newInstance().setFleetSize(FleetSize.INFINITE)
				.setRoutingCost(costMatrix).addVehicle(vehicle).addJob(s1).addJob(s2).addJob(s3).build();

		VehicleRoutingAlgorithm vra = Jsprit.createAlgorithm(vrp);

		Collection<VehicleRoutingProblemSolution> solutions = vra.searchSolutions();

		VehicleRoutingProblemSolution best = Solutions.bestOf(solutions);
		Collection<VehicleRoute> routes = best.getRoutes();

		System.out.println(routes.size());
		routes.forEach(route -> {
			System.out.println(route.getStart());

			System.out.println(route.getEnd());

			TourActivities activity = route.getTourActivities();
			activity.getJobs().forEach(job -> System.out.println(job));
			activity.getActivities().forEach(act -> System.out.println(act));

			System.out.println("--------------------------------------");
			route.getActivities().forEach(activity2 -> {
				System.out.println(activity2.getLocation().getId());
				// System.out.println(activity2.getIndex());
			});

			System.out.println("--------------------------------------");

		});

		SolutionPrinter.print(Solutions.bestOf(solutions));

		// new Plotter(vrp, Solutions.bestOf(solutions)).plot("output/yo.png",
		// "po");

	}

}