package grx.tracking.core.route;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.algorithm.state.StateManager;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.Builder;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import com.graphhopper.jsprit.core.problem.constraint.ConstraintManager;
import com.graphhopper.jsprit.core.problem.constraint.ConstraintManager.Priority;
import com.graphhopper.jsprit.core.problem.constraint.HardActivityConstraint;
import com.graphhopper.jsprit.core.problem.constraint.ServiceDeliveriesFirstConstraint;
import com.graphhopper.jsprit.core.problem.constraint.ShipmentPickupsFirstConstraint;
import com.graphhopper.jsprit.core.problem.cost.VehicleRoutingTransportCosts;
import com.graphhopper.jsprit.core.problem.job.Delivery;
import com.graphhopper.jsprit.core.problem.job.Pickup;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.job.Shipment;
import com.graphhopper.jsprit.core.problem.misc.JobInsertionContext;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.activity.DeliverService;
import com.graphhopper.jsprit.core.problem.solution.route.activity.DeliverShipment;
import com.graphhopper.jsprit.core.problem.solution.route.activity.PickupService;
import com.graphhopper.jsprit.core.problem.solution.route.activity.PickupShipment;
import com.graphhopper.jsprit.core.problem.solution.route.activity.TourActivity;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleType;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleTypeImpl;
import com.graphhopper.jsprit.core.reporting.SolutionPrinter;
import com.graphhopper.jsprit.core.util.Solutions;
import com.graphhopper.jsprit.core.util.VehicleRoutingTransportCostsMatrix;

import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderInfo.PERIOD_TYPE;
import grx.tracking.core.persistence.OrderInfo.PRIOR_TYPE;
import grx.tracking.core.persistence.OrderRestriction;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.util.MyConvertArray;

public class GRXTaskerWithShipment {

	private static Logger logger = Logger.getLogger(GRXTaskerWithShipment.class.getCanonicalName());

	private EntityManagerFactory fact;
	// private RouteInfo routePlanned;
	private GRXRouteDistanceManagerEJB manager;

	public GRXTaskerWithShipment(EntityManagerFactory fact, GRXRouteDistanceManagerEJB manager) {
		super();
		this.fact = fact;
		this.manager = manager;
	}

	private boolean checkConstraint(List<OrderRestriction> orderRestrictions, TruckInfo truck) {
		return true;
	}

	private GoogleRouteInfo findRouteFromMatrix(Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrixRoute,
			OrderId src, OrderId dest) {

		GoogleRouteInfo route = null;

		Hashtable<OrderId, GoogleRouteInfo> row = matrixRoute.get(src);

		if (row != null) {
			route = row.get(dest);
		}

		if (route == null) {
			Hashtable<OrderId, GoogleRouteInfo> row2 = matrixRoute.get(dest);
			route = row2.get(src);
		}

		if (route == null) {

		}

		return route;

	}

	private int convertPriority(PRIOR_TYPE prior) {
		if (prior == null) {
			return 5;
		}

		switch (prior) {
		case LOW:
			return 0;

		case NORMAL:
			return 5;

		case HIGH:
			return 10;

		default:
			break;
		}

		return 0;
	}

	private List<GoogleRouteInfo> convertMatrixToRoutes(
			Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrix) {
		List<GoogleRouteInfo> routes = new ArrayList<>();

		for (OrderId startId : matrix.keySet()) {
			Hashtable<OrderId, GoogleRouteInfo> table_src = matrix.get(startId);
			for (OrderId endId : table_src.keySet()) {
				GoogleRouteInfo routesss = table_src.get(endId);
				routes.add(routesss);
			}
		}
		return routes;
	}

	private OrderInfo getUserInfo(TourActivity activity) {
		if (activity instanceof DeliverService) {
			DeliverService deliver = (DeliverService) activity;
			if (deliver.getJob().getUserData() != null) {
				return (OrderInfo) deliver.getJob().getUserData();
			}
		}
		if (activity instanceof PickupService) {
			PickupService pickup = (PickupService) activity;
			if (pickup.getJob().getUserData() != null) {
				return (OrderInfo) pickup.getJob().getUserData();
			}
		}

		return null;
	}

	public RouteInfo scheduleTasks(UserId userId, GeoLocation userLocation, TruckId truckId, String sector_id,
			int deliverCapacity, int pickupCapacity, PERIOD_TYPE period, boolean startBase) throws Exception {

		EntityManager em = this.fact.createEntityManager();

		final RouteInfo routePlanned = new RouteInfo();
		TruckInfo truckInfo = em.find(TruckInfo.class, truckId);
		SectorInfo sectorInfo = em.find(SectorInfo.class, sector_id);

		OrderId fakeBaseId = new OrderId();
		fakeBaseId.setOrder_id("section");
		fakeBaseId.setSector_id(userId.getSector_id());
		OrderInfo fakeBaseOrder = new OrderInfo();
		fakeBaseOrder.setOrderType(ORDER_TYPE.DELIVERY);
		fakeBaseOrder.setOrderId(fakeBaseId);
		AddressInfo address = new AddressInfo();
		address.setLocation(sectorInfo.getLocation());
		fakeBaseOrder.setAddress(address);

		OrderId fakeVehicleId = new OrderId();
		fakeVehicleId.setOrder_id("user");
		fakeVehicleId.setSector_id(userId.getSector_id());
		OrderInfo fakeVehicleOrder = new OrderInfo();
		fakeVehicleOrder.setOrderId(fakeVehicleId);
		AddressInfo addressv = new AddressInfo();
		addressv.setLocation(userLocation);
		fakeVehicleOrder.setAddress(addressv);

		Hashtable<ORDER_TYPE, Integer> dimessions = truckInfo.getDimensions();
		if (dimessions.containsKey(ORDER_TYPE.DELIVERY)) {
			deliverCapacity = 2;// dimessions.get(ORDER_TYPE.DELIVERY);
		}

		if (dimessions.containsKey(ORDER_TYPE.PICK)) {
			pickupCapacity = 5;// dimessions.get(ORDER_TYPE.PICK);
		}

		System.out.println("deliverCapacity: " + deliverCapacity + " pickupCapacity: " + pickupCapacity);
		// request orders;
		// sync method call
		List<OrderInfo> orders = this.manager.requestOrders(truckInfo, sector_id, userLocation, deliverCapacity,
				pickupCapacity, period, startBase, Calendar.getInstance().getTime());
		orders.add(fakeBaseOrder);
		// logger.info("orders found: " + orders);

		// calculate distance
		// async method call
		Future<Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>>> matrixFuture = this.manager
				.calculateMatrixCostForOrdersToDestination(fakeVehicleOrder, orders);
		Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrix = matrixFuture.get(3, TimeUnit.MINUTES);

		logger.info("matrix: " + matrix.size());

		List<GoogleRouteInfo> routes = this.convertMatrixToRoutes(matrix);

		logger.info("capacity deliverCapacity: " + deliverCapacity + " pickupCapacity: " + pickupCapacity);

		// setting vehicle
		VehicleType type = VehicleTypeImpl.Builder.newInstance(truckInfo.getCarType() + "")
				.addCapacityDimension(ORDER_TYPE.DELIVERY.ordinal(), deliverCapacity)
				.addCapacityDimension(ORDER_TYPE.PICK.ordinal(), pickupCapacity).setCostPerDistance(1).build();

		VehicleImpl vehicle = VehicleImpl.Builder.newInstance("truckid")
				.setStartLocation(Location.newInstance(fakeBaseId.toString())).setType(type).build();

		VehicleRoutingTransportCostsMatrix.Builder costMatrixBuilder = VehicleRoutingTransportCostsMatrix.Builder
				.newInstance(true);

		// setting matrix
		for (GoogleRouteInfo route : routes) {
			costMatrixBuilder.addTransportDistance(route.getStartOrder().getOrderId().toString(),
					route.getEndOrder().getOrderId().toString(), Double.parseDouble(route.getDistance()));
			costMatrixBuilder.addTransportTime(route.getStartOrder().getOrderId().toString(),
					route.getEndOrder().getOrderId().toString(), Double.parseDouble(route.getTime()));
		}

		// creating problem
		VehicleRoutingTransportCosts costMatrix = costMatrixBuilder.build();

		Builder builderProblem = VehicleRoutingProblem.Builder.newInstance().setFleetSize(FleetSize.FINITE)
				.setRoutingCost(costMatrix).addVehicle(vehicle);

		orders.remove(fakeBaseOrder);
		for (OrderInfo order3 : orders) {
			Shipment ship = null;

			if (order3.getOrderType().equals(ORDER_TYPE.PICK)) {
				ship = Shipment.Builder.newInstance(order3.getOrderId().toString())
						.setName(order3.getOrderId().getOrder_id()).setPriority(convertPriority(order3.getPriorType()))
						.addSizeDimension(ORDER_TYPE.PICK.ordinal(), 1)
						.addSizeDimension(ORDER_TYPE.DELIVERY.ordinal(), 0).setUserData(order3)
						.setPickupLocation(Location.newInstance(order3.getOrderId().toString()))
						.setDeliveryLocation(Location.newInstance(fakeBaseId.toString())).build();

			}

			if (order3.getOrderType().equals(ORDER_TYPE.DELIVERY)
					|| order3.getOrderType().equals(ORDER_TYPE.EXCHANGE)) {
				ship = Shipment.Builder.newInstance(order3.getOrderId().toString())
						.setName(order3.getOrderId().getOrder_id()).setPriority(convertPriority(order3.getPriorType()))
						.addSizeDimension(ORDER_TYPE.PICK.ordinal(), 0)
						.addSizeDimension(ORDER_TYPE.DELIVERY.ordinal(), 1).setUserData(order3)
						.setPickupLocation(Location.newInstance(fakeBaseId.toString()))
						.setDeliveryLocation(Location.newInstance(order3.getOrderId().toString())).build();

			}

			builderProblem.addJob(ship);
		}

		// add constraints
		VehicleRoutingProblem vrp = builderProblem.build();

		StateManager stateManager = new StateManager(vrp);
		ConstraintManager constraintManager = new ConstraintManager(vrp, stateManager);

		// constraint entregas primeiro
		// constraintManager.addConstraint(new
		// ServiceDeliveriesFirstConstraint(),
		// Priority.HIGH);

		constraintManager.addConstraint(new BananasFirst(), Priority.HIGH);

		VehicleRoutingAlgorithm vra = Jsprit.Builder.newInstance(vrp).setProperty(Jsprit.Parameter.THREADS, "10")
				.setStateAndConstraintManager(stateManager, constraintManager).buildAlgorithm();

		// calculate solutions
		Collection<VehicleRoutingProblemSolution> solutions = vra.searchSolutions();
		VehicleRoutingProblemSolution bestSolution = Solutions.bestOf(solutions);

		SolutionPrinter.print(vrp, Solutions.bestOf(solutions), SolutionPrinter.Print.VERBOSE);

		orders.forEach(order -> {
			System.out.println(order.getOrderId().getOrder_id() + " " + order.getOrderType());
		});

		List<OrderInfo> selectedOrders = new ArrayList<>();

		bestSolution.getRoutes().forEach(route -> {

			List<RoutePointInfo> routePoints = new ArrayList<>();

			OrderId lastOrderId = fakeBaseId;
			String key_string = "";

			for (TourActivity activity : route.getActivities()) {

				System.out.println("######### " + activity.getClass().getSimpleName() + ": " + activity.getLocation());

				if (activity instanceof PickupShipment) {
					PickupShipment ss = (PickupShipment) activity;
					System.out.println(ss.getJob().getId());

				}

				if (activity instanceof DeliverShipment) {
					DeliverShipment dd = (DeliverShipment) activity;
					System.out.println(dd.getJob().getId());
				}

				OrderInfo order = this.getUserInfo(activity);

				if (order == null) {
					logger.info("activity:" + activity.getName());
					continue;
				}

				logger.info("###### activity: " + order.getOrderId() + " type: " + order.getOrderType() + " ######");

				OrderId nextOrderId = order.getOrderId();

				key_string += nextOrderId.getOrder_id() + " ";

				// ordens selecionadas
				selectedOrders.add(order);

				RoutePointInfo routePoint = new RoutePointInfo();
				routePoint.setOrderId(nextOrderId);
				// point.setEstimateTime(activity.getArrTime() + "");

				{
					logger.info("lastOrderId: " + lastOrderId + " nextOrderId: " + nextOrderId);

					GoogleRouteInfo googleRoute = this.findRouteFromMatrix(matrix, lastOrderId, nextOrderId);

					logger.info("googleRoute: " + googleRoute);

					lastOrderId = nextOrderId;
					routePoint.setGoogleRoute(googleRoute);
				}

				routePoints.add(routePoint);
				routePlanned.addQtde(order.getOrderType());
			}

			routePlanned.setK_Str(key_string);
			routePlanned.setPoints(MyConvertArray.convertArrayList(routePoints, RoutePointInfo.class));

		});

		// marcar ordens
		orders.removeAll(selectedOrders);
		// this.manager.markOrdersWith(orders, selectedOrders, null);

		logger.info("route found:" + routePlanned);
		logger.info(
				"route points:" + (routePlanned.getPoints().length == 0 ? "vazio" : routePlanned.getPoints().length));
		for (RoutePointInfo point : routePlanned.getPoints()) {
			GoogleRouteInfo googleRoute = point.getGoogleRoute();
			if (googleRoute != null) {
				logger.info("point from:" + googleRoute.getStartOrder().getOrderId().toString() + " to: "
						+ googleRoute.getEndOrder().getOrderId().toString());
			}
		}

		em.clear();
		em.close();

		return routePlanned;
	}

	/*
	 * public RouteInfo getRoutePlanned() { return routePlanned; }
	 */

	public static void main(String[] args) {
		Persistence.createEntityManagerFactory("grxtracking.ds.local.test");
	}

	public class ShipmenDeliveriesFirstConstraint implements HardActivityConstraint {

		@Override
		public ConstraintsStatus fulfilled(JobInsertionContext iFacts, TourActivity prevAct, TourActivity newAct,
				TourActivity nextAct, double prevActDepTime) {
			if (nextAct instanceof DeliverShipment && newAct instanceof PickupShipment) {
				return ConstraintsStatus.NOT_FULFILLED_BREAK;
			}
			if (prevAct instanceof PickupShipment && newAct instanceof DeliverShipment) {
				return ConstraintsStatus.NOT_FULFILLED;
			}
			return ConstraintsStatus.FULFILLED;
		}

	}

	static class BananasFirst implements HardActivityConstraint {

		@Override
		public ConstraintsStatus fulfilled(JobInsertionContext jobInsertionContext, TourActivity prevActivity,
				TourActivity newActivity, TourActivity nextActivity, double departureTimeAtPrevActivity) {
			if (isBananaPickup(newActivity) && isApplePickup(prevActivity))
				return ConstraintsStatus.NOT_FULFILLED_BREAK;
			if (isBananaPickup(nextActivity) && isApplePickup(newActivity))
				return ConstraintsStatus.NOT_FULFILLED;
			return ConstraintsStatus.FULFILLED;
		}

		private boolean isApplePickup(TourActivity act) {
			return act.getSize().get(ORDER_TYPE.PICK.ordinal()) > 0;
		}

		private boolean isBananaPickup(TourActivity act) {
			return act.getSize().get(ORDER_TYPE.DELIVERY.ordinal()) > 0;
		}
	}

}
