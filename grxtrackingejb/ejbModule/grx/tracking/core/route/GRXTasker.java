package grx.tracking.core.route;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.algorithm.state.StateManager;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.Builder;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import com.graphhopper.jsprit.core.problem.constraint.ConstraintManager;
import com.graphhopper.jsprit.core.problem.constraint.ConstraintManager.Priority;
import com.graphhopper.jsprit.core.problem.constraint.ServiceDeliveriesFirstConstraint;
import com.graphhopper.jsprit.core.problem.cost.VehicleRoutingTransportCosts;
import com.graphhopper.jsprit.core.problem.job.Delivery;
import com.graphhopper.jsprit.core.problem.job.Pickup;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.job.Shipment;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.activity.DeliverService;
import com.graphhopper.jsprit.core.problem.solution.route.activity.PickupService;
import com.graphhopper.jsprit.core.problem.solution.route.activity.PickupShipment;
import com.graphhopper.jsprit.core.problem.solution.route.activity.TourActivity;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleType;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleTypeImpl;
import com.graphhopper.jsprit.core.util.Solutions;
import com.graphhopper.jsprit.core.util.VehicleRoutingTransportCostsMatrix;

import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderInfo.PERIOD_TYPE;
import grx.tracking.core.persistence.OrderInfo.PRIOR_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.OrderRestriction;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.RouteStatus.ROUTE_STATUS;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.util.MyConvertArray;

public class GRXTasker {

	private static Logger logger = Logger.getLogger(GRXTasker.class.getCanonicalName());

	private EntityManagerFactory fact;
	// private RouteInfo routePlanned;
	private GRXRouteDistanceManagerEJB manager;
	private SimpleDateFormat time_sdf = new SimpleDateFormat("HH:mm:ss");
	private Date affternoon;

	public GRXTasker(EntityManagerFactory fact, GRXRouteDistanceManagerEJB manager) {
		super();
		this.fact = fact;
		this.manager = manager;
		try {
			this.affternoon = this.time_sdf.parse("17:59:59");
		} catch (ParseException e) {
		}
	}

	private boolean checkConstraint(List<OrderRestriction> orderRestrictions, TruckInfo truck) {
		return true;
	}

	private GoogleRouteInfo findRouteFromMatrix(Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrixRoute,
			OrderId src, OrderId dest) {

		GoogleRouteInfo route = null;

		Hashtable<OrderId, GoogleRouteInfo> row = matrixRoute.get(src);

		if (row != null) {
			route = row.get(dest);
		}

		if (route == null) {
			Hashtable<OrderId, GoogleRouteInfo> row2 = matrixRoute.get(dest);
			route = row2.get(src);
		}

		if (route == null) {

		}

		return route;

	}

	private int convertPriority(PRIOR_TYPE prior) {
		if (prior == null) {
			return 5;
		}

		switch (prior) {
		case LOW:
			return 0;

		case NORMAL:
			return 5;

		case HIGH:
			return 10;

		default:
			break;
		}

		return 0;
	}

	private List<GoogleRouteInfo> convertMatrixToRoutes(
			Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrix) {
		List<GoogleRouteInfo> routes = new ArrayList<>();

		for (OrderId startId : matrix.keySet()) {
			Hashtable<OrderId, GoogleRouteInfo> table_src = matrix.get(startId);
			for (OrderId endId : table_src.keySet()) {
				GoogleRouteInfo routesss = table_src.get(endId);
				routes.add(routesss);
			}
		}
		return routes;
	}

	private OrderInfo getUserInfo(TourActivity activity) {
		if (activity instanceof DeliverService) {
			DeliverService deliver = (DeliverService) activity;
			if (deliver.getJob().getUserData() != null) {
				return (OrderInfo) deliver.getJob().getUserData();
			}
		}
		if (activity instanceof PickupService) {
			PickupService pickup = (PickupService) activity;
			if (pickup.getJob().getUserData() != null) {
				return (OrderInfo) pickup.getJob().getUserData();
			}
		}

		return null;
	}

	public RouteInfo scheduleTasks(UserId userId, GeoLocation userLocation, TruckId truckId, String sector_id,
			int deliverCapacity, int pickupCapacity, Date requestDate, boolean startBase, boolean confirmRoute)
					throws Exception {

		EntityManager em = this.fact.createEntityManager();

		final RouteInfo routePlanned = new RouteInfo();
		TruckInfo truckInfo = em.find(TruckInfo.class, truckId);
		SectorInfo sectorInfo = em.find(SectorInfo.class, sector_id);

		OrderId fakeBaseId = new OrderId();
		OrderInfo fakeBaseOrder = new OrderInfo();

		{
			fakeBaseId.setOrder_id("base_start_point");
			fakeBaseId.setSector_id(userId.getSector_id());

			fakeBaseOrder.setOrderId(fakeBaseId);
			AddressInfo address = new AddressInfo();

			// main change - consider truck start on base
			address.setLocation(sectorInfo.getLocation());
			fakeBaseOrder.setAddress(address);
		}

		Hashtable<ORDER_TYPE, Integer> dimessions = truckInfo.getDimensions();
		if (dimessions.containsKey(ORDER_TYPE.DELIVERY)) {
			deliverCapacity = dimessions.get(ORDER_TYPE.DELIVERY);
		}

		if (dimessions.containsKey(ORDER_TYPE.PICK)) {
			pickupCapacity = dimessions.get(ORDER_TYPE.PICK);
		}

		PERIOD_TYPE period = null;

		{
			Date hour = this.time_sdf.parse(this.time_sdf.format(requestDate));
			period = hour.after(this.affternoon) ? PERIOD_TYPE.NIGHT : PERIOD_TYPE.DAY;
			logger.info("Time: " + hour + " Period Type: " + period.getName());
		}

		// request orders;
		// sync method call
		List<OrderInfo> orders = this.manager.requestOrders(truckInfo, sector_id, userLocation, deliverCapacity,
				pickupCapacity, period, startBase, requestDate);
				// logger.info("orders found: " + orders);

		// calculate distance
		// async method call
		Future<Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>>> matrixFuture = this.manager
				.calculateMatrixCostForOrdersFromLocation(fakeBaseOrder, orders);
		Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrix = matrixFuture.get(3, TimeUnit.MINUTES);

		logger.info("matrix: " + matrix.size());

		List<GoogleRouteInfo> routes = this.convertMatrixToRoutes(matrix);

		logger.info("capacity deliverCapacity: " + deliverCapacity + " pickupCapacity: " + pickupCapacity);

		// setting vehicle
		VehicleType type = VehicleTypeImpl.Builder.newInstance(truckInfo.getCarType() + "")
				.addCapacityDimension(ORDER_TYPE.DELIVERY.ordinal(), deliverCapacity)
				.addCapacityDimension(ORDER_TYPE.PICK.ordinal(), pickupCapacity).setCostPerDistance(1).build();

		VehicleImpl vehicle = VehicleImpl.Builder.newInstance(truckId.toString())
				.setStartLocation(Location.newInstance(fakeBaseId.toString())).setType(type).build();

		VehicleRoutingTransportCostsMatrix.Builder costMatrixBuilder = VehicleRoutingTransportCostsMatrix.Builder
				.newInstance(true);

		// setting matrix
		for (GoogleRouteInfo route : routes) {
			costMatrixBuilder.addTransportDistance(route.getStartOrder().getOrderId().toString(),
					route.getEndOrder().getOrderId().toString(), Double.parseDouble(route.getDistance()));
			costMatrixBuilder.addTransportTime(route.getStartOrder().getOrderId().toString(),
					route.getEndOrder().getOrderId().toString(), Double.parseDouble(route.getTime()));
		}

		// creating problem
		VehicleRoutingTransportCosts costMatrix = costMatrixBuilder.build();

		Builder builderProblem = VehicleRoutingProblem.Builder.newInstance().setFleetSize(FleetSize.FINITE)
				.setRoutingCost(costMatrix).addVehicle(vehicle);

		orders.forEach(order -> {

			Service service = null;

			if (order.getOrderType().equals(ORDER_TYPE.EXCHANGE)) {
				service = Pickup.Builder.newInstance(order.getOrderId().toString())
						.setPriority(convertPriority(order.getPriorType()))
						.addSizeDimension(ORDER_TYPE.PICK.ordinal(), 1)
						.addSizeDimension(ORDER_TYPE.DELIVERY.ordinal(), 1).setUserData(order)
						.setLocation(Location.newInstance(order.getOrderId().toString())).build();

				builderProblem.addJob(service);
			}

			if (order.getOrderType().equals(ORDER_TYPE.PICK)) {
				service = Pickup.Builder.newInstance(order.getOrderId().toString())
						.setPriority(convertPriority(order.getPriorType()))
						.addSizeDimension(ORDER_TYPE.PICK.ordinal(), 1).setUserData(order)
						.setLocation(Location.newInstance(order.getOrderId().toString())).build();

				builderProblem.addJob(service);
			}

			if (order.getOrderType().equals(ORDER_TYPE.DELIVERY)) {
				service = Delivery.Builder.newInstance(order.getOrderId().toString())
						.setPriority(convertPriority(order.getPriorType()))
						.addSizeDimension(ORDER_TYPE.DELIVERY.ordinal(), 1).setUserData(order)
						.setLocation(Location.newInstance(order.getOrderId().toString())).build();

				builderProblem.addJob(service);
			}

		});

		// add constraints
		VehicleRoutingProblem vrp = builderProblem.build();

		StateManager stateManager = new StateManager(vrp);
		ConstraintManager constraintManager = new ConstraintManager(vrp, stateManager);

		// constraint entregas primeiro
		constraintManager.addConstraint(new ServiceDeliveriesFirstConstraint(), Priority.HIGH);

		VehicleRoutingAlgorithm vra = Jsprit.Builder.newInstance(vrp).setProperty(Jsprit.Parameter.THREADS, "10")
				.setStateAndConstraintManager(stateManager, constraintManager).buildAlgorithm();

		// calculate solutions
		Collection<VehicleRoutingProblemSolution> solutions = vra.searchSolutions();
		VehicleRoutingProblemSolution bestSolution = Solutions.bestOf(solutions);

		List<OrderInfo> selectedOrders = new ArrayList<>();

		bestSolution.getRoutes().forEach(route -> {

			List<RoutePointInfo> routePoints = new ArrayList<>();

			OrderId lastOrderId = fakeBaseId;
			String key_string = "";

			for (TourActivity activity : route.getActivities()) {
				OrderInfo order = this.getUserInfo(activity);

				logger.info("###### activity: " + order.getOrderId() + " type: " + order.getOrderType() + " ######");

				OrderId nextOrderId = order.getOrderId();

				key_string += nextOrderId.getOrder_id() + " ";

				// ordens selecionadas
				selectedOrders.add(order);

				RoutePointInfo routePoint = new RoutePointInfo();
				routePoint.setOrderId(nextOrderId);
				// point.setEstimateTime(activity.getArrTime() + "");

				{
					logger.info("lastOrderId: " + lastOrderId + " nextOrderId: " + nextOrderId);

					GoogleRouteInfo googleRoute = this.findRouteFromMatrix(matrix, lastOrderId, nextOrderId);

					logger.info("googleRoute: " + googleRoute);

					lastOrderId = nextOrderId;
					routePoint.setGoogleRoute(googleRoute);
				}

				routePoints.add(routePoint);
				routePlanned.addQtde(order.getOrderType());
			}

			if (startBase) {
				routePlanned.setStartSectorId(sector_id);
			} else {
				routePlanned.setStartSectorId("vehicle_start");
			}
			routePlanned.setEndSectorId(sector_id);
			routePlanned.setK_Str(key_string);
			routePlanned.setPoints(MyConvertArray.convertArrayList(routePoints, RoutePointInfo.class));

		});

		// marcar ordens
		orders.removeAll(selectedOrders);

		this.manager.markOrdersWith(orders, selectedOrders,
				(confirmRoute ? ORDER_STATUS.RESERVED : ORDER_STATUS.ROUTING));

		logger.info("route found:" + routePlanned);
		logger.info(
				"route points:" + (routePlanned.getPoints().length == 0 ? "vazio" : routePlanned.getPoints().length));
		for (RoutePointInfo point : routePlanned.getPoints()) {
			GoogleRouteInfo googleRoute = point.getGoogleRoute();
			if (googleRoute != null) {
				logger.info("point from:" + googleRoute.getStartOrder().getOrderId().toString() + " to: "
						+ googleRoute.getEndOrder().getOrderId().toString());
			}
		}

		em.clear();
		em.close();

		return routePlanned;
	}

	/*
	 * public RouteInfo getRoutePlanned() { return routePlanned; }
	 */

	public static void main(String[] args) {
		Persistence.createEntityManagerFactory("grxtracking.ds.local");
	}

}
