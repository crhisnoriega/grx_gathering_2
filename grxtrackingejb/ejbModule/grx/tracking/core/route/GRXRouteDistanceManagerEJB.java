package grx.tracking.core.route;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.TransactionScoped;

import org.apache.commons.lang3.SerializationUtils;
import org.infinispan.Cache;

import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.CalculatedRouteInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderInfo.PERIOD_TYPE;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.TruckInfo;

@Singleton
@Startup
@Local
public class GRXRouteDistanceManagerEJB {
	public static Logger logger = Logger.getLogger(GRXRouteDistanceManagerEJB.class.getCanonicalName());

	@Resource(lookup = "java:jboss/infinispan/cache/grxtrackingcache/orders")
	private Cache<OrderId, OrderInfo> ordersCache;

	@PersistenceContext(unitName = "grxtracking.ds")
	private EntityManager em;

	@PersistenceUnit(unitName = "grxtracking.ds.local")
	private EntityManagerFactory fact_local;

	@PostConstruct
	public void postContruct() {
		logger.info("########### " + this.ordersCache + " ##########");
	}

	public GoogleRouteInfo findGoogleRoute(GeoLocation start, GeoLocation end) throws Exception {
		return this.findGoogleRoute(new GoogleMapsServices(), start, end);
	}

	private GoogleRouteInfo findGoogleRoute(GoogleMapsServices services, GeoLocation start, GeoLocation end)
			throws Exception {
		// this.em.clear();

		// logger.info("start lat: " + start.getLatitude() + " start lng: " +
		// start.getLatitude());
		// logger.info("end lat: " + end.getLatitude() + " end lng: " +
		// end.getLatitude());

		TypedQuery<CalculatedRouteInfo> query = this.em.createQuery(
				"select r from CalculatedRouteInfo as r where r.start = :start and r.end = :end",
				CalculatedRouteInfo.class);
		query.setParameter("start", start);
		query.setParameter("end", end);

		List<CalculatedRouteInfo> results = query.getResultList();

		// logger.info("results size: " + results.size());

		if (!results.isEmpty()) {
			byte[] bytes = results.get(0).getRoute_Bytes();
			GoogleRouteInfo route = SerializationUtils.deserialize(bytes);
			return route;
		} else {
			GoogleRouteInfo newRoute = services.extractRouteInfo(start, end);
			CalculatedRouteInfo calculated = new CalculatedRouteInfo();
			calculated.setId("route_" + UUID.randomUUID().toString());
			calculated.setStart(start);
			calculated.setEnd(end);
			calculated.setRoute_Bytes(SerializationUtils.serialize(newRoute));

			this.em.persist(calculated);
			this.em.flush();

			return newRoute;
		}

	}

	private void createMatrixRoute(Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrixRoute, OrderId src,
			OrderId dest, GoogleRouteInfo route) {
		Hashtable<OrderId, GoogleRouteInfo> row = matrixRoute.get(src);
		if (row == null) {
			row = new Hashtable<>();
			matrixRoute.put(src, row);
		}

		row.put(dest, route);
	}

	@Asynchronous
	public Future<Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>>> calculateMatrixCostForOrdersFromLocation(
			OrderInfo startOrder, List<OrderInfo> orders) throws Exception {

		Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrixRoute = new Hashtable<>();

		GoogleMapsServices services = new GoogleMapsServices();

		for (int index1 = 0; index1 < orders.size(); index1++) {
			OrderInfo order1 = orders.get(index1);
			{
				/*
				 * GoogleRouteInfo route = this.googleServices.extractRouteInfo(startPoint,
				 * order1.getAddress().getLocation());
				 */

				GoogleRouteInfo route = null;
				try {
					route = this.findGoogleRoute(services, startOrder.getAddress().getLocation(),
							order1.getAddress().getLocation());
				} catch (Exception e) {
					logger.warning("error: " + e.getLocalizedMessage());
				}

				if (route != null) {

					route.setStartOrder(startOrder);
					route.setEndOrder(order1);

					this.createMatrixRoute(matrixRoute, startOrder.getOrderId(), order1.getOrderId(), route);
				}
			}
			{
				for (int index2 = index1 + 1; index2 < orders.size(); index2++) {
					OrderInfo order2 = orders.get(index2);

					// GoogleRouteInfo route =
					// this.googleServices.extractRouteInfo(order1, order2);
					GoogleRouteInfo route2 = null;
					try {
						route2 = this.findGoogleRoute(services, order1.getAddress().getLocation(),
								order2.getAddress().getLocation());
					} catch (Exception e) {
						logger.warning("error: " + e.getLocalizedMessage());
					}

					if (route2 != null) {
						route2.setStartOrder(order1);
						route2.setEndOrder(order2);

						this.createMatrixRoute(matrixRoute, order1.getOrderId(), order2.getOrderId(), route2);
					}
				}
			}
		}

		return CompletableFuture.completedFuture(matrixRoute);
		// return new AsyncResult<Hashtable<OrderId, Hashtable<OrderId,
		// GoogleRouteInfo>>>(matrixRoute);
	}

	@Asynchronous
	public Future<Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>>> calculateMatrixCostForOrdersToDestination(
			OrderInfo toDestination, List<OrderInfo> orders) throws Exception {

		Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrixRoute = new Hashtable<>();

		GoogleMapsServices services = new GoogleMapsServices();

		for (int index1 = 0; index1 < orders.size(); index1++) {
			OrderInfo order1 = orders.get(index1);
			{
				/*
				 * GoogleRouteInfo route = this.googleServices.extractRouteInfo(startPoint,
				 * order1.getAddress().getLocation());
				 */

				GoogleRouteInfo route = null;
				try {
					route = this.findGoogleRoute(services, order1.getAddress().getLocation(),
							toDestination.getAddress().getLocation());
				} catch (Exception e) {
					logger.warning("error: " + e.getLocalizedMessage());
				}

				if (route != null) {

					route.setStartOrder(toDestination);
					route.setEndOrder(order1);

					this.createMatrixRoute(matrixRoute, order1.getOrderId(), toDestination.getOrderId(), route);
				}
			}
			{
				for (int index2 = index1 + 1; index2 < orders.size(); index2++) {
					OrderInfo order2 = orders.get(index2);

					// GoogleRouteInfo route =
					// this.googleServices.extractRouteInfo(order1, order2);
					GoogleRouteInfo route2 = null;
					try {
						route2 = this.findGoogleRoute(services, order1.getAddress().getLocation(),
								order2.getAddress().getLocation());
					} catch (Exception e) {
						logger.warning("error: " + e.getLocalizedMessage());
					}

					if (route2 != null) {
						route2.setStartOrder(order1);
						route2.setEndOrder(order2);

						this.createMatrixRoute(matrixRoute, order1.getOrderId(), order2.getOrderId(), route2);
					}
				}
			}
		}

		// return CompletableFuture.completedFuture(matrixRoute);
		return new AsyncResult<Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>>>(matrixRoute);
	}

	private int countOrdersForDistanceAndStatus(GeoLocation center, float kmDistance, ORDER_STATUS status,
			String sector_id, PERIOD_TYPE period, boolean startBase, Date requestDate) {

		String order_type_sql = startBase ? " " : " AND orderType = " + ORDER_TYPE.PICK.ordinal();

		String sql = "SELECT count(*), (6371 * acos( cos( radians(" + center.getLatitude()
				+ ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" + center.getLongitude()
				+ ") ) + sin( radians(" + center.getLatitude()
				+ ") ) * sin(radians(latitude)) ) ) AS distance FROM OrderInfo INNER JOIN AddressInfo ON OrderInfo.address_id = AddressInfo.id "
				+ " WHERE nearest_sector_id = '" + sector_id + "' AND status = " + status.ordinal()
				+ " AND periodType = " + period.ordinal() + order_type_sql + " AND requestDate >= '"
				+ mysql_sdf_start.format(requestDate) + "' AND requestDate <= '" + mysql_sdf_end.format(requestDate)
				+ "' " + " GROUP BY distance " + " HAVING distance < " + kmDistance;

		logger.info("count sql:" + sql);

		Query query = this.em.createNativeQuery(sql);

		List result = query.getResultList();

		int total = 0;
		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] obj1 = (Object[]) object;
				int value = ((BigInteger) obj1[0]).intValue();
				total += value;
			}
		}

		return total;

	}

	private List<OrderInfo> findNearNewOrders(TruckInfo truck2, GeoLocation center, float kmDistance,
			ORDER_STATUS status, int max, String sector_id, PERIOD_TYPE period, boolean startBase, Date requestDate) {
		String order_type_sql = startBase ? " " : " AND orderType = " + ORDER_TYPE.PICK.ordinal();

		String sql = "SELECT *, (6371 * acos( cos( radians(" + center.getLatitude()
				+ ") ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(" + center.getLongitude()
				+ ") ) + sin( radians(" + center.getLatitude()
				+ ") ) * sin(radians(latitude)) ) ) AS distance FROM OrderInfo INNER JOIN AddressInfo ON OrderInfo.address_id = AddressInfo.id "
				+ " WHERE nearest_sector_id = '" + sector_id + "' AND status = " + status.ordinal()
				+ " AND periodType = " + period.ordinal() + order_type_sql + " AND requestDate >= '"
				+ mysql_sdf_start.format(requestDate) + "' AND requestDate <= '" + mysql_sdf_end.format(requestDate)
				+ "' " + " HAVING distance < " + kmDistance + " ORDER BY distance ASC LIMIT 0," + max + ";";

		logger.info("sql for orders: " + sql);

		Query orders = this.em.createNativeQuery(sql, OrderInfo.class);

		List<OrderInfo> results = (List<OrderInfo>) orders.getResultList();

		return results;
	}

	// @Asynchronous
	@AccessTimeout(value = 5, unit = TimeUnit.MINUTES)
	public void markOrdersWith(List<OrderInfo> toRelease, List<OrderInfo> takedOrders, ORDER_STATUS status) {
		logger.info("toRelease: " + toRelease.size() + " takedOrders: " + takedOrders.size());
		toRelease.forEach(order -> {
			OrderStatus currentStatus = new OrderStatus();
			currentStatus.setStatus(ORDER_STATUS.NEW);
			currentStatus.setDate(Calendar.getInstance().getTime());
			order.setCurrentStatus(currentStatus);

			// mudar prioridade?

			em.merge(order);
			em.flush();
		});

		takedOrders.forEach(order -> {
			OrderStatus currentStatus = new OrderStatus();
			currentStatus.setStatus(status);
			currentStatus.setDate(Calendar.getInstance().getTime());
			order.setCurrentStatus(currentStatus);

			em.merge(order);
			em.flush();
		});
	}

	private SimpleDateFormat mysql_sdf_start = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat mysql_sdf_end = new SimpleDateFormat("yyyy-MM-dd 23:59:59");

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	@AccessTimeout(value = 5, unit = TimeUnit.MINUTES)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<OrderInfo> requestOrders(TruckInfo truckInfo, String sector_id, GeoLocation userLocation,
			int deliverCapacity, int pickupCapacity, PERIOD_TYPE period, boolean startBase, Date requestDate) {

		this.em.clear();

		int truck_total_capacity = 3 * (deliverCapacity + pickupCapacity);
		float kmDistance = 5f;
		List<OrderInfo> orders = new ArrayList<>();

		for (int index = 1; index <= 5; index++) {
			int orderQtde = this.countOrdersForDistanceAndStatus(userLocation, kmDistance, ORDER_STATUS.NEW, sector_id,
					period, startBase, requestDate);
			orders = this.findNearNewOrders(truckInfo, userLocation, kmDistance, ORDER_STATUS.NEW, truck_total_capacity,
					sector_id, period, startBase, requestDate);

			logger.info("#### orderQtde: " + orderQtde + " orders.size(): " + orders.size() + " truck_max_capacity: "
					+ truck_total_capacity + " kmDistance: " + kmDistance + " ####");

			if ((orders.size() < truck_total_capacity)) {
				// aumentar capacidade max e repetir busca
				if (truck_total_capacity < orderQtde) {
					truck_total_capacity = (index + 3) * (deliverCapacity + pickupCapacity);
				} else {
					kmDistance = (index + 2) * kmDistance;
				}
			} else {
				break;
			}
		}

		// marcar orders
		logger.info("reserverd orders: " + orders.size());

		orders.forEach(order -> {
			OrderStatus currentStatus = new OrderStatus();
			currentStatus.setStatus(ORDER_STATUS.RESERVED);
			currentStatus.setDate(Calendar.getInstance().getTime());
			order.setCurrentStatus(currentStatus);

			this.em.merge(order);
			this.em.flush();
		});

		return orders;
	}
}
