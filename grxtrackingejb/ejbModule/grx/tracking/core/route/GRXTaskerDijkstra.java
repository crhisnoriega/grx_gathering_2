package grx.tracking.core.route;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.Order;

import com.graphhopper.jsprit.core.algorithm.VehicleRoutingAlgorithm;
import com.graphhopper.jsprit.core.algorithm.box.Jsprit;
import com.graphhopper.jsprit.core.algorithm.state.StateManager;
import com.graphhopper.jsprit.core.problem.Location;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.Builder;
import com.graphhopper.jsprit.core.problem.VehicleRoutingProblem.FleetSize;
import com.graphhopper.jsprit.core.problem.constraint.ConstraintManager;
import com.graphhopper.jsprit.core.problem.constraint.ConstraintManager.Priority;
import com.graphhopper.jsprit.core.problem.constraint.ServiceDeliveriesFirstConstraint;
import com.graphhopper.jsprit.core.problem.cost.VehicleRoutingTransportCosts;
import com.graphhopper.jsprit.core.problem.job.Delivery;
import com.graphhopper.jsprit.core.problem.job.Pickup;
import com.graphhopper.jsprit.core.problem.job.Service;
import com.graphhopper.jsprit.core.problem.job.Shipment;
import com.graphhopper.jsprit.core.problem.solution.VehicleRoutingProblemSolution;
import com.graphhopper.jsprit.core.problem.solution.route.activity.DeliverService;
import com.graphhopper.jsprit.core.problem.solution.route.activity.PickupService;
import com.graphhopper.jsprit.core.problem.solution.route.activity.PickupShipment;
import com.graphhopper.jsprit.core.problem.solution.route.activity.TourActivity;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleImpl;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleType;
import com.graphhopper.jsprit.core.problem.vehicle.VehicleTypeImpl;
import com.graphhopper.jsprit.core.util.Solutions;
import com.graphhopper.jsprit.core.util.VehicleRoutingTransportCostsMatrix;

import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderInfo.PERIOD_TYPE;
import grx.tracking.core.persistence.OrderInfo.PRIOR_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.OrderRestriction;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.RouteStatus.ROUTE_STATUS;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.seach.Edge;
import grx.tracking.core.util.MyConvertArray;

public class GRXTaskerDijkstra {

	private static Logger logger = Logger.getLogger(GRXTaskerDijkstra.class.getCanonicalName());

	private EntityManagerFactory fact;
	// private RouteInfo routePlanned;
	private GRXRouteDistanceManagerEJB manager;
	private SimpleDateFormat time_sdf = new SimpleDateFormat("HH:mm:ss");
	private Date affternoon;

	public GRXTaskerDijkstra(EntityManagerFactory fact, GRXRouteDistanceManagerEJB manager) {
		super();
		this.fact = fact;
		this.manager = manager;
		try {
			this.affternoon = this.time_sdf.parse("17:59:59");
		} catch (ParseException e) {
		}
	}

	private boolean checkConstraint(List<OrderRestriction> orderRestrictions, TruckInfo truck) {
		return true;
	}

	private GoogleRouteInfo findRouteFromMatrix(Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrixRoute,
			OrderId src, OrderId dest) {

		GoogleRouteInfo route = null;

		Hashtable<OrderId, GoogleRouteInfo> row = matrixRoute.get(src);

		if (row != null) {
			route = row.get(dest);
		}

		if (route == null) {
			Hashtable<OrderId, GoogleRouteInfo> row2 = matrixRoute.get(dest);
			route = row2.get(src);
		}

		if (route == null) {

		}

		return route;

	}

	private int convertPriority(PRIOR_TYPE prior) {
		if (prior == null) {
			return 5;
		}

		switch (prior) {
		case LOW:
			return 0;

		case NORMAL:
			return 5;

		case HIGH:
			return 10;

		default:
			break;
		}

		return 0;
	}

	private List<GoogleRouteInfo> convertMatrixToRoutes(
			Hashtable<OrderId, Hashtable<OrderId, GoogleRouteInfo>> matrix) {
		List<GoogleRouteInfo> routes = new ArrayList<>();

		for (OrderId startId : matrix.keySet()) {
			Hashtable<OrderId, GoogleRouteInfo> table_src = matrix.get(startId);
			for (OrderId endId : table_src.keySet()) {
				GoogleRouteInfo routesss = table_src.get(endId);
				routes.add(routesss);
			}
		}
		return routes;
	}

	private OrderInfo getUserInfo(TourActivity activity) {
		if (activity instanceof DeliverService) {
			DeliverService deliver = (DeliverService) activity;
			if (deliver.getJob().getUserData() != null) {
				return (OrderInfo) deliver.getJob().getUserData();
			}
		}
		if (activity instanceof PickupService) {
			PickupService pickup = (PickupService) activity;
			if (pickup.getJob().getUserData() != null) {
				return (OrderInfo) pickup.getJob().getUserData();
			}
		}

		return null;
	}

	public RouteInfo scheduleTasks(UserId userId, GeoLocation userLocation, TruckId truckId, String sector_id,
			int deliverCapacity, int pickupCapacity, Date requestDate, boolean startBase, boolean confirmRoute)
			throws Exception {

		EntityManager em = this.fact.createEntityManager();

		final RouteInfo routePlanned = new RouteInfo();
		TruckInfo truckInfo = em.find(TruckInfo.class, truckId);
		SectorInfo sectorInfo = em.find(SectorInfo.class, sector_id);

		OrderId fakeStartId = new OrderId();
		OrderInfo fakeStartPoint = new OrderInfo();

		{
			fakeStartId.setOrder_id("start_point");
			fakeStartId.setSector_id(userId.getSector_id());

			fakeStartPoint.setOrderId(fakeStartId);
			AddressInfo address = new AddressInfo();

			if (startBase) {
				address.setLocation(sectorInfo.getLocation());
			} else {
				address.setLocation(userLocation);
			}
			fakeStartPoint.setAddress(address);
		}

		Hashtable<ORDER_TYPE, Integer> dimessions = truckInfo.getDimensions();
		if (dimessions.containsKey(ORDER_TYPE.DELIVERY)) {
			deliverCapacity = dimessions.get(ORDER_TYPE.DELIVERY);
		}

		if (dimessions.containsKey(ORDER_TYPE.PICK)) {
			pickupCapacity = dimessions.get(ORDER_TYPE.PICK);
		}

		logger.info("deliverCapacity: " + deliverCapacity + " pickupCapacity: " + pickupCapacity);

		PERIOD_TYPE period = null;

		{
			Date hour = this.time_sdf.parse(this.time_sdf.format(requestDate));
			period = hour.after(this.affternoon) ? PERIOD_TYPE.NIGHT : PERIOD_TYPE.DAY;
			logger.info("Time: " + hour + " Period Type: " + period.getName());
		}

		// request orders;
		// sync method call
		List<OrderInfo> orders_orig = this.manager.requestOrders(truckInfo, sector_id, userLocation, deliverCapacity,
				pickupCapacity, period, startBase, requestDate);

		List<OrderInfo> orders = new ArrayList<>(orders_orig);
		// logger.info("orders found: " + orders);

		String key_string = "";

		List<OrderInfo> path_orders = new ArrayList<>();
		List<RoutePointInfo> routePoints = new ArrayList<>();

		OrderInfo current_order = fakeStartPoint;
		while (!orders.isEmpty()) {
			GoogleRouteInfo min_route = null;
			OrderInfo min_order = null;
			double min_distance = Double.MAX_VALUE;

			List<OrderInfo> filter = orders.stream().filter(order -> ORDER_TYPE.DELIVERY.equals(order.getOrderType()))
					.collect(Collectors.toList());

			if (filter.isEmpty() || deliverCapacity <= 0) {

				filter = orders.stream().filter(order -> ORDER_TYPE.EXCHANGE.equals(order.getOrderType()))
						.collect(Collectors.toList());

				if (filter.isEmpty() || deliverCapacity <= 0 || pickupCapacity <= 0) {

					filter = orders.stream().filter(order -> ORDER_TYPE.PICK.equals(order.getOrderType()))
							.collect(Collectors.toList());
					if (filter.isEmpty() || pickupCapacity <= 0) {
						break;
					}
				}
			}

			for (OrderInfo nextOrder : filter) {

				GoogleRouteInfo route = this.manager.findGoogleRoute(current_order.getAddress().getLocation(),
						nextOrder.getAddress().getLocation());
				route.setStartOrder(current_order);
				route.setEndOrder(nextOrder);

				double distance = Double.parseDouble(route.getDistance());

				logger.info("testing: " + nextOrder.getOrderId() + " distance: " + distance);

				if (distance < min_distance) {
					min_distance = distance;
					min_order = nextOrder;
					min_route = route;
				}
			}
			logger.info("min_order: " + min_order.getOrderId());

			if (ORDER_TYPE.DELIVERY.equals(min_order.getOrderType())) {
				deliverCapacity--;
			}
			if (ORDER_TYPE.EXCHANGE.equals(min_order.getOrderType())) {
				deliverCapacity--;
				pickupCapacity--;
			}
			if (ORDER_TYPE.PICK.equals(min_order.getOrderType())) {
				pickupCapacity--;
			}

			{
				RoutePointInfo routePoint = new RoutePointInfo();
				routePoint.setOrderId(min_order.getOrderId());
				routePoint.setGoogleRoute(min_route);
				routePoints.add(routePoint);
			}

			key_string += min_order.getOrderId().getOrder_id() + " ";
			path_orders.add(min_order);

			current_order = min_order;
			orders.remove(min_order);
		}

		if (startBase) {
			routePlanned.setStartSectorId(sector_id);
		} else {
			routePlanned.setStartSectorId("vehicle_start");
		}
		routePlanned.setEndSectorId(sector_id);
		routePlanned.setK_Str(key_string);
		routePlanned.setPoints(MyConvertArray.convertArrayList(routePoints, RoutePointInfo.class));

		path_orders.forEach(order -> {
			logger.info("######## " + order.getOrderId().getOrder_id() + " " + order.getOrderType());
		});

		// marcar ordens
		orders_orig.removeAll(path_orders);

		this.manager.markOrdersWith(orders_orig, path_orders,
				(confirmRoute ? ORDER_STATUS.RESERVED : ORDER_STATUS.ROUTING));

		logger.info("route found:" + routePlanned);
		logger.info(
				"route points:" + (routePlanned.getPoints().length == 0 ? "vazio" : routePlanned.getPoints().length));
		for (RoutePointInfo point : routePlanned.getPoints()) {
			GoogleRouteInfo googleRoute = point.getGoogleRoute();
			if (googleRoute != null) {
				logger.info("point from:" + googleRoute.getStartOrder().getOrderId().toString() + " to: "
						+ googleRoute.getEndOrder().getOrderId().toString());
			}
		}

		em.clear();
		em.close();

		return routePlanned;
	}

	/*
	 * public RouteInfo getRoutePlanned() { return routePlanned; }
	 */

	public static void main(String[] args) {
		Persistence.createEntityManagerFactory("grxtracking.ds.local");
	}

}
