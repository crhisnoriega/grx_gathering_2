package grx.tracking.core.transport;

import java.io.Serializable;

import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.UserId;

public class SendOrderMessage implements Serializable {

	private UserId userId;

	private OrderInfo order;

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public OrderInfo getOrder() {
		return order;
	}

	public void setOrder(OrderInfo order) {
		this.order = order;
	}

}
