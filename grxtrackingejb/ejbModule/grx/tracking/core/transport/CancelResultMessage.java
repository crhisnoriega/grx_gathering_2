package grx.tracking.core.transport;

import java.io.Serializable;

import grx.tracking.core.persistence.OrderId;

public class CancelResultMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private OrderId orderId;
	private String route_id;
	private String status;
	private String message;

	private String wasCancel;

	public String getRoute_id() {
		return route_id;
	}

	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OrderId getOrderId() {
		return orderId;
	}

	public void setOrderId(OrderId orderId) {
		this.orderId = orderId;
	}

	public String getWasCancel() {
		return wasCancel;
	}

	public void setWasCancel(String wasCancel) {
		this.wasCancel = wasCancel;
	}

}
