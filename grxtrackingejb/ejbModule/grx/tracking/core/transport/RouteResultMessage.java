package grx.tracking.core.transport;

import java.io.Serializable;
import java.util.Hashtable;

import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;

public class RouteResultMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private RouteInfo route;
	private String message;
	private String status;

	public RouteInfo getRoute() {
		return route;
	}

	public void setRoute(RouteInfo route) {
		this.route = route;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
