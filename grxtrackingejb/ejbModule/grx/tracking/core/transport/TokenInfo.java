package grx.tracking.core.transport;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

public class TokenInfo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String userName;
	private String tokenInfo;
	private Date dateGenerated;

	private Hashtable<String, Object> extras;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTokenInfo() {
		return tokenInfo;
	}

	public void setTokenInfo(String tokenInfo) {
		this.tokenInfo = tokenInfo;
	}

	public Date getDateGenerated() {
		return dateGenerated;
	}

	public void setDateGenerated(Date dateGenerated) {
		this.dateGenerated = dateGenerated;
	}

	public Hashtable<String, Object> getExtras() {
		return extras;
	}

	public void setExtras(Hashtable<String, Object> extras) {
		this.extras = extras;
	}
}
