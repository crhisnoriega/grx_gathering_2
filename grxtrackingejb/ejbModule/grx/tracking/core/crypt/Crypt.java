package grx.tracking.core.crypt;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Crypt {

	private static final String DATA_FORMAT = "B590D095C11C6228F295520F3A49B4FF";
	public static final String AES = "AES";

	public static String ecpt(String value) throws GeneralSecurityException, IOException {

		SecretKeySpec sks = getkk();
		Cipher cipher = Cipher.getInstance(Crypt.AES);
		cipher.init(Cipher.ENCRYPT_MODE, sks, cipher.getParameters());
		byte[] encrypted = cipher.doFinal(value.getBytes());
		return byteArrayToHexString(encrypted);
	}

	/**
	 * decrypt a value
	 * 
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	public static String decpt(String message) throws GeneralSecurityException, IOException {
		SecretKeySpec sks = getkk();
		Cipher cipher = Cipher.getInstance(Crypt.AES);
		cipher.init(Cipher.DECRYPT_MODE, sks);
		byte[] decrypted = cipher.doFinal(hexStringToByteArray(message));
		return new String(decrypted);
	}

	private static SecretKeySpec getkk() throws NoSuchAlgorithmException, IOException {
		byte[] key = readKeyFile();
		SecretKeySpec sks = new SecretKeySpec(key, Crypt.AES);
		return sks;
	}

	private static byte[] readKeyFile() throws FileNotFoundException {
		// Scanner scanner = new Scanner(keyFile).useDelimiter("\\Z");
		String keyValue = DATA_FORMAT;
		// String keyValue = scanner.next();
		// System.out.println("key:" + keyValue);
		// scanner.close();
		return hexStringToByteArray(keyValue);
	}

	private static String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}

	private static byte[] hexStringToByteArray(String s) {
		byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++) {
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		return b;
	}

	public static void main(String[] args) {
		try {
			String a = Crypt.ecpt("@##@2017-04-01");
			System.out.println(a);
			System.out.println(Crypt.decpt(a));
		} catch (GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}