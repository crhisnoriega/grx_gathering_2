package grx.tracking.core.ejb;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;

import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventChangeStatusRoute;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.listener.GRXUIEventListener;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.EquipmentAllocateInfo;
import grx.tracking.core.persistence.LastKnowUserLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.TokenInfo;

public interface GRXTrackingSession {

	public <T> void saveEntity(Object id, T obj) throws Exception;

	public <T> void detachEntity(T entity);

	public <T> void deleteEntity(T entity);

	public <T> T refreshEntity(T entity);

	public <T> List<T> findEntities(String sql, Class<T> c, Hashtable<String, Object> params) throws Exception;

	public <T> T findEntity(Class<T> c, Object id) throws Exception;

	public void processLocation(EventUpdateGeoLocation update) throws Exception;

	public void recalculateRoute(RouteInfo route) throws Exception;

	public void startScheduleTasks(boolean startBase, TruckId truckId, String sector_id, int deliverCapacity,
			int pickupCapacity, boolean confirmRoute, List<EquipmentAllocateInfo> allocate, Date requestDate)
					throws Exception;

	public void registerListener(GRXUIEventListener listener) throws Exception;

	public void unRegisterListener(GRXUIEventListener listener) throws Exception;

	public void killSession(String status) throws Exception;

	public void registerFirebaseToken(TokenInfo token) throws Exception;

	public void setUser(UserInfo user);

	public UserInfo getUser();

	public void updateRouteWithNewStatus(EventChangeStatusRoute newStatus) throws Exception;

	public void removeMapObjectInMap(MapObject mapObject) throws Exception;

	public void showNotificationInWindow(String title, String message, Hashtable<String, Object> props)
			throws Exception;

	public void showOcurrence(String title, String message, Hashtable<String, Object> props) throws Exception;

	public List<MapObject> getMapRepresention() throws Exception;

	public void drawMapObjectInMap(MapObject mapObject) throws Exception;

	public RouteInfo getCurrentRoute();

	public LastKnowUserLocation getLastLocation();

	// public void confirmRoute(RouteInfo routeInfo) throws Exception;

	public void addOrderToRoute(UserId userId, OrderInfo order) throws Exception;

	public List<OrderInfo> getRouteOrders();

	public Future<List<OrderInfo>> processFile(String filepath) throws Exception;

	public boolean isAlive() throws Exception;

	public boolean reloadSession(Hashtable<String, Object> props) throws Exception;

	// public void cancelRoute(EventCancelRoute cancelRoute) throws Exception;

	public void reloadRoute(String route_id) throws Exception;

	// public void cancelOrder(EventCancelOrder cancelOrder) throws Exception;

	public MapObject getRouteAsMapObject();

	public void checkListeners();

	public void processOcorrenceEvent(EventOcurrence event) throws Exception;

	public void updateOrderWithNewStatus(EventChangeStatusOrder status) throws Exception;

	public void updateMapObjectInfo(MapObject object) throws Exception;

	public void updateMapObjectInfo(List<MapObject> objects) throws Exception;

	public void saverCTR(CTRInfo ctr, String status, Hashtable<String, String> params);

	public void showCTROnMap(CTRInfo ctr);

}
