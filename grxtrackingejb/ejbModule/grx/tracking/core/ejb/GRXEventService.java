package grx.tracking.core.ejb;

import java.util.Hashtable;

import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.maps.MapModel;
import grx.tracking.core.persistence.UserId;

public interface GRXEventService {

	public void processEvent(GRXEvent event) throws Exception;

	public MapModel getMapModel(Hashtable<String, Object> params) throws Exception;
}
