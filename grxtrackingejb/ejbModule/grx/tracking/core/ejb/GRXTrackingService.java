package grx.tracking.core.ejb;

import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;

import javax.ejb.AsyncResult;

import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.transport.CancelResultMessage;
import grx.tracking.core.transport.RouteResultMessage;
import grx.tracking.core.transport.SendOrderMessage;

public interface GRXTrackingService {

	public String validateToken(String token) throws Exception;

	public UserInfo authenticate(String username, String password, String section_id, USER_ROLE role,
			String app_version) throws Exception;

	public GRXTrackingSession getUserSessionEJB(UserId userId) throws Exception;

	public <T> List<T> list(Class<T> classs) throws Exception;

	

	public <T> List<T> query(Class<T> classs, String sql, Hashtable<String, Object> params) throws Exception;

	public <T> T findEntity(Object id, Class<T> c) throws Exception;

	public void confirmRoute(RouteInfo routeInfo, UserId userId, String statusStr) throws Exception;

	public void cancelRoute(String route_id, UserId userId, boolean sendNotif, boolean reloadSession) throws Exception;

	public void cancelOrder(OrderId orderId, String route_id, UserId userId, boolean sendNotif) throws Exception;

	public void sendMessageToUser(Object content, UserId userId) throws Exception;

	public GoogleRouteInfo findGoogleRoute(GeoLocation start, GeoLocation end) throws Exception;

	public Future<List<UserInfo>> getSessionUsersRemote(UserId currentId, USER_ROLE role);
	public Future<List<UserId>> getSessionsRemote(UserId currenId, USER_ROLE role);
	

	public SectorInfo findNearestBase(GeoLocation location);
	
	public Address findAddressForCEP(String cep);
	
	
	

}
