package grx.tracking.core.ejb.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.AccessTimeout;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;

import grx.tracking.core.ejb.GRXEventService;
import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventChangeStatusRoute;
import grx.tracking.core.events.EventFindTasks;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.listener.GRXEventHandler;
import grx.tracking.core.maps.MapModel;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.maps.MapObject.MAP_OBJECT_TYPE;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.CTRCreatorInfo;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderInfo.PERIOD_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;

@Singleton
@AccessTimeout(unit = TimeUnit.MINUTES, value = 5) // concurrency
@Local(GRXEventService.class)
public class GRXEventServiceEJB implements GRXEventService {

	private static Logger logger = Logger.getLogger(GRXEventService.class.getCanonicalName());

	@PersistenceContext(unitName = "grxtracking.ds")
	private EntityManager em;

	@PersistenceUnit(unitName = "grxtracking.ds")
	private EntityManagerFactory fact;

	@EJB
	private GRXTrackingService service;

	private boolean updateMapModel;

	private Hashtable<Class<? extends GRXEvent>, GRXEventHandler> handlers;

	private MapModel mapModel;

	public GRXEventServiceEJB() {
		this.updateMapModel = false;
		this.handlers = new Hashtable<>();
		this.initHandlers();
	}

	private void initHandlers() {
		this.handlers.put(EventUpdateGeoLocation.class, new ShowGeoLocationUIHandler());
		this.handlers.put(EventFindTasks.class, new StartTaskerHandler());
		this.handlers.put(EventChangeStatusRoute.class, new ChangeRoutePointStatus());
		// this.handlers.put(EventCancelRoute.class, new CancelRouteHandler());
		// this.handlers.put(EventCancelOrder.class, new CancelOrderHandler());
		this.handlers.put(EventOcurrence.class, new OcurrenceHandler());
		this.handlers.put(EventChangeStatusOrder.class, new OrderChangeStatusHandler());
	}

	@Override
	public void processEvent(GRXEvent event) throws Exception {
		GRXEventHandler handler = this.handlers.get(event.getClass());
		if (handler != null) {
			handler.processEvent(event);
		}
	}

	private List<OrderInfo> findAvailableOrder(UserId userId, Date date) throws Exception {
		EntityManager em = this.fact.createEntityManager();
		em.clear();

		TypedQuery<OrderInfo> query = em.createQuery(
				// "select o from OrderInfo as o where o.currentStatus.status =
				// :status and o.orderId.sector_id = :sector",
				"select o from OrderInfo as o where o.requestDate >= :date", OrderInfo.class);
		// query.setParameter("status", ORDER_STATUS.NEW);
		// query.setParameter("sector", userId.getSector_id());
		query.setParameter("date", date);

		return query.getResultList();
	}

	private CTRCreatorInfo findCreator(String geratorId) {
		CTRCreatorInfo gerator = this.em.find(CTRCreatorInfo.class, geratorId);
		return gerator;

	}

	private List<SectorInfo> findBases() throws Exception {
		EntityManager em = this.fact.createEntityManager();

		TypedQuery<SectorInfo> query = em.createQuery("select b from SectorInfo as b", SectorInfo.class);

		return query.getResultList();
	}

	private List<RouteInfo> findRoutesInfo(UserId userId) throws Exception {
		EntityManager em = this.fact.createEntityManager();

		TypedQuery<RouteInfo> query = em.createQuery("select o from RouteInfo as o where o.userId.sector_id = :sector",
				RouteInfo.class);

		query.setParameter("sector", userId.getSector_id());

		return query.getResultList();
	}

	@Override
	synchronized public MapModel getMapModel(Hashtable<String, Object> params) throws Exception {
		/*
		 * if (!this.updateMapModel && this.mapModel != null) { this.updateMapModel =
		 * false; return this.mapModel; }
		 */

		this.mapModel = new MapModel();

		UserId userId = null;
		Date date = null;
		// orders
		List<CTRInfo> ctrs = this.findCTRs(userId, date);
		ctrs.forEach(order -> {
			MapObject mapObj = new MapObject();
			mapObj.setId(order.getId());

			mapObj.setLocation(order.getLocation());

			mapObj.setDescription(order.getEntrega().asAddressString());

			mapObj.setDate(Calendar.getInstance().getTime());
			mapObj.setLabel(order.getId());

			mapObj.setType(MAP_OBJECT_TYPE.ORDER);
			mapObj.setPeriodType(PERIOD_TYPE.DAY);
			mapObj.setCtrStatus(order.getStatus().getStatus());
			mapObj.setOrderType(ORDER_TYPE.DELIVERY);

			{
				CTRCreatorInfo gerator = this.findCreator(order.getGeradorId());
				if (gerator != null) {
					mapObj.setGerator(gerator.getName());
				} else {
					mapObj.setGerator("");
				}
			}

			this.mapModel.getObjects().add(mapObj);

		});

		logger.info("map object model:" + this.mapModel.getObjects());

		return this.mapModel;
	}

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////

	private List<CTRInfo> findCTRs(UserId userId, Date date) {
		TypedQuery<CTRInfo> query = this.em.createQuery("select c from CTRInfo as c", CTRInfo.class);
		return query.getResultList();
	}

	public final class ShowGeoLocationUIHandler implements GRXEventHandler {
		@Override
		public void processEvent(GRXEvent event) throws Exception {
			// eventos de localiza�ao sao enviados para o session do usuario

			if (event instanceof EventUpdateGeoLocation) {
				CompletableFuture.runAsync(() -> {
					try {
						EventUpdateGeoLocation update = (EventUpdateGeoLocation) event;
						service.getUserSessionEJB(event.getFromUserId()).processLocation(update);
					} catch (Throwable e) {
						logger.log(Level.INFO, "error: " + e.getLocalizedMessage());
					}
				});
			}

		}
	}

	private final class StartTaskerHandler implements GRXEventHandler {
		@Override
		public void processEvent(GRXEvent event) throws Exception {
			if (event instanceof EventFindTasks) {
				CompletableFuture.runAsync(() -> {
					try {
						EventFindTasks requestEvent = (EventFindTasks) event;
						service.getUserSessionEJB(requestEvent.getFromUserId()).startScheduleTasks(
								requestEvent.isStartBase(), requestEvent.getTruckId(), requestEvent.getSector_Id(),
								requestEvent.getDeliverCapacity(), requestEvent.getPickupCapacity(),
								requestEvent.isConfirmRoute(), requestEvent.getAllocated(),
								requestEvent.getDateEvent());
						updateMapModel = true;
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
				});
			}
		}
	}

	private final class OcurrenceHandler implements GRXEventHandler {

		@Override
		public void processEvent(GRXEvent event) throws Exception {
			if (event instanceof EventOcurrence) {

				CompletableFuture.runAsync(() -> {
					try {
						EventOcurrence ocurrence = (EventOcurrence) event;
						service.getUserSessionEJB(ocurrence.getFromUserId()).processOcorrenceEvent(ocurrence);
						updateMapModel = true;
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
				});
			}

		}

	}

	private final class ChangeRoutePointStatus implements GRXEventHandler {
		@Override
		public void processEvent(GRXEvent event) throws Exception {
			if (event instanceof EventChangeStatusRoute) {
				try {
					EventChangeStatusRoute newStatus = (EventChangeStatusRoute) event;
					service.getUserSessionEJB(newStatus.getFromUserId()).updateRouteWithNewStatus(newStatus);
					updateMapModel = true;
				} catch (Exception e) {
					logger.log(Level.INFO, e.getLocalizedMessage(), e);
				}

			}

		}
	}

	/*
	 * private final class CancelRouteHandler implements GRXEventHandler {
	 * 
	 * @Override public void processEvent(GRXEvent event) throws Exception { if
	 * (event instanceof EventCancelRoute) { EventCancelRoute cancelRoute =
	 * (EventCancelRoute) event;
	 * service.refUserSession(cancelRoute.getFromUserId()).cancelRoute(
	 * cancelRoute); updateMapModel = true; } } }
	 * 
	 * private final class CancelOrderHandler implements GRXEventHandler {
	 * 
	 * @Override public void processEvent(GRXEvent event) throws Exception { if
	 * (event instanceof EventCancelOrder) { EventCancelOrder cancelRoute =
	 * (EventCancelOrder) event;
	 * service.refUserSession(cancelRoute.getFromUserId()).cancelOrder(
	 * cancelRoute); updateMapModel = true; } } }
	 */

	private final class OrderChangeStatusHandler implements GRXEventHandler {

		@Override
		public void processEvent(GRXEvent event) throws Exception {
			if (event instanceof EventChangeStatusOrder) {

				CompletableFuture.runAsync(() -> {
					try {
						EventChangeStatusOrder status = (EventChangeStatusOrder) event;
						GRXTrackingSession session = service.getUserSessionEJB(status.getFromUserId());

						logger.info("session:" + session);

						session.updateOrderWithNewStatus(status);
						updateMapModel = true;
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
				});
			}

		}

	}

}
