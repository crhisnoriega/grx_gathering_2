package grx.tracking.core.ejb.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import org.apache.commons.math3.util.MathArrays.OrderDirection;
import org.jboss.ejb3.annotation.TransactionTimeout;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventChangeStatusRoute;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.fileimport.ExcelFileImportNew;
import grx.tracking.core.listener.GRXUIEventListener;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.maps.MapObject.MAP_OBJECT_TYPE;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.EquipmentAllocateInfo;
import grx.tracking.core.persistence.FirebaseTokenInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.LastKnowUserLocation;
import grx.tracking.core.persistence.OccurrenceInfo;
import grx.tracking.core.persistence.OccurrenceInfo.OCCURRENCE_STATUS;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.PRIOR_TYPE;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.RouteStatus;
import grx.tracking.core.persistence.RouteStatus.ROUTE_STATUS;

import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.persistence.UserTruckMap;
import grx.tracking.core.route.GRXRouteDistanceManagerEJB;
import grx.tracking.core.route.GRXTasker;
import grx.tracking.core.route.GRXTaskerDijkstra;
import grx.tracking.core.transport.RouteResultMessage;
import grx.tracking.core.transport.SendOrderMessage;
import grx.tracking.core.transport.TokenInfo;
import grx.tracking.core.util.MyConfig;
import grx.tracking.core.util.MyConvertArray;
import grx.tracking.core.util.SecurityController;

@Stateful
@Local(GRXTrackingSession.class)
@AccessTimeout(unit = TimeUnit.MINUTES, value = 5) // concurrency
@StatefulTimeout(unit = TimeUnit.MINUTES, value = 60) // eviction
public class GRXTrackingSessionEJB implements GRXTrackingSession {

	private static Logger logger = Logger.getLogger(GRXTrackingSessionEJB.class.getCanonicalName());

	@PersistenceContext(unitName = "grxtracking.ds", type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	@PersistenceUnit(unitName = "grxtracking.ds")
	private EntityManagerFactory fact;

	@PersistenceUnit(unitName = "grxtracking.ds.local")
	private EntityManagerFactory fact_local;

	@EJB
	private GRXTrackingService service;

	@EJB
	private GRXRouteDistanceManagerEJB distanceManager;

	@Resource
	private ManagedExecutorService executionService;

	private UserInfo user;

	private GeoLocation currentLocation;

	private RouteInfo currentRoute;

	private List<GRXUIEventListener> listeners;
	private Future<RouteInfo> currentFutureRoute;

	private Timer timer;
	private MyConfig config;

	public GRXTrackingSessionEJB() {
		this.listeners = new ArrayList<>();
		this.config = new MyConfig();

	}

	@Override
	public <T> void deleteEntity(T entity) {
		if (this.em.contains(entity)) {
			this.em.remove(entity);
		} else {
			if (entity instanceof UserTruckMap) {
				UserTruckMap map = (UserTruckMap) entity;
				UserTruckMap ref = this.em.getReference(UserTruckMap.class, map.getId());
				this.em.remove(ref);
			}
		}

		this.em.flush();
	}

	@Override
	public <T> void detachEntity(T entity) {
		this.em.detach(entity);
	}

	@Override
	public <T> T refreshEntity(T entity) {
		this.em.clear();
		this.em.refresh(entity);
		return entity;
	}

	@Override
	public <T> T findEntity(Class<T> c, Object id) throws Exception {
		this.em.clear();
		T entity = this.em.find(c, id);
		if (entity != null) {
			this.em.detach(entity);
		}
		return entity;
	}

	@Override
	public <T> List<T> findEntities(String sql, Class<T> c, Hashtable<String, Object> params) throws Exception {
		this.em.clear();
		TypedQuery<T> query = this.em.createQuery(sql, c);
		for (String param : params.keySet()) {
			query.setParameter(param, params.get(param));
		}
		return query.getResultList();
	}

	private boolean isInRoute(GoogleRouteInfo route, OrderId start, OrderId end) {
		boolean hasStart = route.getStartOrder().getOrderId().equals(start)
				|| route.getEndOrder().getOrderId().equals(start);
		boolean hasEnd = route.getStartOrder().getOrderId().equals(end) || route.getEndOrder().getOrderId().equals(end);

		return hasStart && hasEnd;

	}

	@Override
	public void processOcorrenceEvent(EventOcurrence event) throws Exception {
		OccurrenceInfo ocurrence = event.getOcurrence();

		ocurrence.setId("ocurr_" + UUID.randomUUID().toString());
		ocurrence.setStatus(OCCURRENCE_STATUS.RECEIVED);

		this.em.persist(ocurrence);
		this.em.flush();

		this.executionService.submit(() -> {
			try {
				Future<List<UserId>> future = this.service.getSessionsRemote(this.user.getUserId(),
						USER_ROLE.CONTROLLER);
				List<UserId> controllers = future.get();

				logger.info("controllers: " + controllers);

				controllers.forEach(userId -> {
					try {
						String title = ocurrence.getType().getName();
						String message = ocurrence.getDescription();
						Hashtable<String, Object> props = new Hashtable<>();
						props.put("date", ocurrence.getDate());
						props.put("occurr_id", ocurrence.getId());
						service.getUserSessionEJB(userId).showOcurrence(title, message, props);
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
				});
			} catch (Exception e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
			}
		});

	}

	@Override
	public void recalculateRoute(RouteInfo route) throws Exception {

		try {
			RoutePointInfo[] points = route.getPoints();

			for (int index = 1; index < points.length; index++) {
				RoutePointInfo current = points[index];

				GoogleRouteInfo googleRoute = current.getGoogleRoute();

				// logger.info("gr: " + googleRoute);
				// logger.info("s: " +
				// googleRoute.getStartOrder().getOrderId().getOrder_id());
				// logger.info("e: " +
				// googleRoute.getEndOrder().getOrderId().getOrder_id());

				RoutePointInfo prev = points[index - 1];
				if (googleRoute == null || !this.isInRoute(googleRoute, prev.getOrderId(), current.getOrderId())) {

					logger.info("calculando nova rota prev: " + prev.getOrderId().getOrder_id() + " current: "
							+ current.getOrderId().getOrder_id());

					OrderInfo start = this.em.find(OrderInfo.class, prev.getOrderId());
					OrderInfo end = this.em.find(OrderInfo.class, current.getOrderId());

					// calcular rota
					GoogleRouteInfo newGoogleRoute = distanceManager.findGoogleRoute(start.getAddress().getLocation(),
							end.getAddress().getLocation());
					newGoogleRoute.setStartOrder(start);
					newGoogleRoute.setEndOrder(end);

					current.setGoogleRoute(newGoogleRoute);
				}

			}

			this.em.merge(route);
			this.em.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addOrderToRoute(UserId userId, OrderInfo order) throws Exception {

		if (this.currentRoute != null) {
			List<RoutePointInfo> orders = new ArrayList<>();

			{
				RoutePointInfo[] points = this.currentRoute.getPoints();
				for (RoutePointInfo point : points) {
					orders.add(point);
				}

				RoutePointInfo pp = new RoutePointInfo();
				pp.setOrderId(order.getOrderId());
				pp.setPrior(PRIOR_TYPE.NORMAL);

				orders.add(pp);
			}
			{
				this.currentRoute.setPoints(MyConvertArray.convertArrayList(orders, RoutePointInfo.class));

				this.recalculateRoute(this.currentRoute);

				this.em.merge(this.currentRoute);
				this.em.flush();
			}
			{
				SendOrderMessage sendMessage = new SendOrderMessage();
				sendMessage.setUserId(userId);
				sendMessage.setOrder(order);
				this.service.sendMessageToUser(sendMessage, userId);
			}
		}

	}

	/*
	 * // @Override public void confirmRoute(RouteInfo routeInfo) throws
	 * Exception { if (this.currentRoute != null) { logger.info(
	 * "this.currentRoute: " + this.currentRoute); }
	 * 
	 * this.currentRoute = routeInfo;
	 * 
	 * if (this.currentRoute == null) { throw new Exception(
	 * "rota nao encontrada"); }
	 * 
	 * RouteStatus status = new RouteStatus();
	 * status.setStatus(ROUTE_STATUS.RUNNING);
	 * status.setTimeDate(Calendar.getInstance().getTime());
	 * this.currentRoute.setCurrentStatus(status);
	 * 
	 * this.em.merge(this.currentRoute); this.em.flush();
	 * 
	 * { this.updateMapObjectInMap(this.createRouteMapObject()); } {
	 * RouteResultMessage resultMessage = new RouteResultMessage();
	 * 
	 * RouteInfo fakeRoute = new RouteInfo();
	 * fakeRoute.setId(this.currentRoute.getId());
	 * resultMessage.setRoute(fakeRoute); resultMessage.setStatus("OK");
	 * 
	 * this.service.sendFirebaseMessage(resultMessage, this.user.getUserId()); }
	 * 
	 * }
	 */

	@Override
	@Asynchronous
	public void startScheduleTasks(boolean startBase, TruckId truckId, String sector_id, int deliverCapacity,
			int pickupCapacity, boolean confirmRoute_NAO_USADO, List<EquipmentAllocateInfo> allocate, Date requestDate)
					throws Exception {

		this.em.clear();

		RouteResultMessage resultMessage = new RouteResultMessage();

		if (this.currentRoute != null) {
			logger.info("################# user has route: " + this.currentRoute + " #################");
			resultMessage.setStatus("USER_WITH_ROUTE");
			resultMessage.setMessage("Usuario ja tem rota: " + this.currentRoute.getId());

			this.service.sendMessageToUser(resultMessage, this.user.getUserId());
			return;
		}

		try {
			logger.info("### starting find routes for user: " + this.user.getUserId() + "###");

			this.config.load();

			final boolean confirmRoute = !Boolean.parseBoolean(this.config.getConfirmRoutes());

			logger.info("confirmRoute: " + confirmRoute);

			this.currentFutureRoute = this.executionService.submit(() -> {
				GRXTaskerDijkstra tasker = new GRXTaskerDijkstra(this.fact, this.distanceManager);

				RouteInfo route = tasker.scheduleTasks(this.user.getUserId(), this.currentLocation, truckId, sector_id,
						deliverCapacity, pickupCapacity, requestDate, startBase, confirmRoute);

				return route;
			});

			try {
				this.currentRoute = this.currentFutureRoute.get(3, TimeUnit.MINUTES);
				if (this.currentRoute == null || this.currentRoute.getPoints() == null
						|| this.currentRoute.getPoints().length == 0) {
					this.currentRoute = null;
				}
			} catch (TimeoutException e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
				resultMessage.setStatus("TIMEOUT");
				resultMessage.setMessage(e.getLocalizedMessage());
			} catch (ExecutionException e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
				resultMessage.setStatus("ERROR_1");
				resultMessage.setMessage(e.getLocalizedMessage());
			} catch (Exception e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
				resultMessage.setStatus("ERROR_3");
				resultMessage.setMessage(e.getLocalizedMessage());
			}

			if (this.currentRoute != null) {
				{
					this.currentRoute.setId("route_" + UUID.randomUUID().toString());
					this.currentRoute.setUserId(this.user.getUserId());

					RouteStatus currentStatus = new RouteStatus();
					currentStatus.setStatus(confirmRoute ? ROUTE_STATUS.TO_APROVAL : ROUTE_STATUS.RUNNING);
					currentStatus.setTimeDate(Calendar.getInstance().getTime());
					this.currentRoute.setCurrentStatus(currentStatus);

					// persist route
					this.em.persist(this.currentRoute);
					this.em.flush();

				}
				{
					RouteInfo fakeRoute = new RouteInfo();
					fakeRoute.setId(this.currentRoute.getId());
					resultMessage.setRoute(fakeRoute);
					resultMessage.setStatus(confirmRoute ? "WAITING_CONFIRM" : "OK");
				}

				// notificar os controladores
				{
					List<MapObject> mapObjects = new ArrayList<>();
					RoutePointInfo[] points = this.currentRoute.getPoints();
					for (RoutePointInfo point : points) {
						OrderInfo order = this.em.find(OrderInfo.class, point.getOrderId());
						if (order != null) {
							logger.info("order: " + order.getOrderId() + " status: "
									+ order.getCurrentStatus().getStatus());
							mapObjects.add(MapObject.createMapObject(order));
						}
					}

					this.executionService.submit(() -> {

						try {
							Future<List<UserInfo>> future = this.service.getSessionUsersRemote(this.user.getUserId(),
									USER_ROLE.CONTROLLER);
							List<UserInfo> controllers = future.get();

							logger.info("controllers: " + controllers);

							controllers.forEach(user -> {
								try {
									if (confirmRoute) {
										Hashtable<String, Object> props = new Hashtable<>();
										props.put("route_id", currentRoute.getId());
										logger.info("######### showNotificationInWindow: " + props + " #########");
										service.getUserSessionEJB(user.getUserId()).showNotificationInWindow(
												"Nova rota encontrada", "Aprovar rota com ID:" + currentRoute.getId(),
												props);
									}

									service.getUserSessionEJB(user.getUserId()).updateMapObjectInfo(mapObjects);
								} catch (Exception e) {
									logger.log(Level.INFO, e.getLocalizedMessage(), e);
								}
							});

						} catch (Exception e) {
							logger.log(Level.INFO, e.getLocalizedMessage(), e);
						}
					});
				}

				logger.info("### end find routes OK ###");

			} else {

				RouteInfo fakeRoute = new RouteInfo();
				fakeRoute.setId("id_route");
				resultMessage.setRoute(fakeRoute);
				resultMessage.setStatus("SEM ROTAS");

				logger.info("### end routes dont found OK ###");
			}

		} catch (Exception e) {
			logger.log(Level.INFO, e.getLocalizedMessage(), e);
			logger.info("### end find routes ERROR ###");
			resultMessage.setStatus("ERROR_2");
			resultMessage.setMessage(e.getLocalizedMessage());
		}

		this.service.sendMessageToUser(resultMessage, this.user.getUserId());

		System.gc();

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public LastKnowUserLocation getLastLocation() {
		LastKnowUserLocation lastknow = this.em.find(LastKnowUserLocation.class, this.user.getUserId());
		if (lastknow != null) {
			try {
				this.em.refresh(lastknow);
				this.currentLocation = lastknow.getGeoLocation();
			} catch (javax.persistence.EntityNotFoundException e) {
				logger.warning("controled error: " + e.getLocalizedMessage());
				this.currentLocation = null;
			}

		} else {
			this.currentLocation = null;
		}
		return lastknow;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public RouteInfo getCurrentRoute() {
		logger.info("relaoding current route");
		if (this.currentRoute != null && this.currentRoute.getId() != null) {
			try {
				return this.findEntity(RouteInfo.class, this.currentRoute.getId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public List<OrderInfo> getRouteOrders() {
		if (this.currentRoute != null) {

			List<OrderInfo> orders = new ArrayList<>();
			RoutePointInfo[] points = this.currentRoute.getPoints();
			for (RoutePointInfo point : points) {
				orders.add(this.em.find(OrderInfo.class, point.getOrderId()));
			}
			return orders;
		}

		return null;
	}

	private GeoLocation calculateLocation(EventUpdateGeoLocation update) {
		List<GeoLocation> geoCoordinates = update.getLocations();

		if (geoCoordinates.size() == 1) {
			return geoCoordinates.get(0);
		}

		double x = 0;
		double y = 0;
		double z = 0;

		for (GeoLocation geoCoordinate : geoCoordinates) {

			double latitude = geoCoordinate.getLatitude() * Math.PI / 180;
			double longitude = geoCoordinate.getLongitude() * Math.PI / 180;

			x += Math.cos(latitude) * Math.cos(longitude);
			y += Math.cos(latitude) * Math.sin(longitude);
			z += Math.sin(latitude);
		}
		int total = geoCoordinates.size();

		x = x / total;
		y = y / total;
		z = z / total;

		double centralLongitude = Math.atan2(y, x);
		double centralSquareRoot = Math.sqrt(x * x + y * y);
		double centralLatitude = Math.atan2(z, centralSquareRoot);

		GeoLocation central = new GeoLocation();
		central.setLatitude(centralLatitude * 180 / Math.PI);
		central.setLongitude(centralLongitude * 180 / Math.PI);
		return central;
	}

	@Override
	synchronized public void processLocation(EventUpdateGeoLocation update) throws Exception {
		// atualizar localizacao atual
		this.currentLocation = calculateLocation(update);

		{
			try {
				LastKnowUserLocation lastknow = this.em.find(LastKnowUserLocation.class, this.user.getUserId());
				if (lastknow == null) {
					lastknow = new LastKnowUserLocation();
					lastknow.setUserId(this.user.getUserId());
					lastknow.setGeoLocation(this.currentLocation);
					lastknow.setUpdateDate(Calendar.getInstance().getTime());
					this.em.persist(lastknow);
					this.em.flush();
				} else {
					lastknow.setUserId(this.user.getUserId());
					lastknow.setGeoLocation(this.currentLocation);
					lastknow.setUpdateDate(Calendar.getInstance().getTime());
					this.em.merge(lastknow);
					this.em.flush();
				}
			} catch (javax.persistence.OptimisticLockException e) {
				logger.warning("controlled exception: " + e.getLocalizedMessage());
			}
		}

		this.executionService.submit(() -> {
			try {
				// notificar as session de controladores
				Future<List<UserId>> future = this.service.getSessionsRemote(this.user.getUserId(),
						USER_ROLE.CONTROLLER);
				List<UserId> controllers = future.get();

				for (UserId userId : controllers) {

					// UserInfo user = this.em.find(UserInfo.class, userId);

					// if (SecurityController.checkPermission(update,
					// user.getPermissions())) {
					try {
						MapObject mapObject = new MapObject();
						mapObject.setId(GRXTrackingSessionEJB.this.user.getUserId().toString());
						mapObject.setType(MAP_OBJECT_TYPE.DRIVER);
						mapObject.setLocation(this.currentLocation);
						mapObject.setLabel(this.user.getName());
						service.getUserSessionEJB(userId).drawMapObjectInMap(mapObject);
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
					// }

				}
			} catch (Exception e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
			}
		});
	}

	@Override
	public void registerFirebaseToken(TokenInfo token) throws Exception {
		logger.info("registrando token: " + token);
		FirebaseTokenInfo tokenInfo = this.em.find(FirebaseTokenInfo.class, this.user.getUserId());

		if (tokenInfo == null) {
			tokenInfo = new FirebaseTokenInfo();
			tokenInfo.setUserId(this.user.getUserId());
			tokenInfo.setToken(token);
			tokenInfo.setRegisterDate(Calendar.getInstance().getTime());
			this.em.persist(tokenInfo);
		} else {
			tokenInfo.setToken(token);
			tokenInfo.setRegisterDate(Calendar.getInstance().getTime());
			this.em.merge(tokenInfo);
		}
	}

	private MapObject createRouteMapObject() {
		if (this.currentRoute == null) {
			return null;
		}

		MapObject routeObject = new MapObject();
		routeObject.setId("route_" + this.user.getUserId().toString());
		routeObject.setType(MAP_OBJECT_TYPE.ROUTE);
		routeObject.setLabel("Rota do usuario " + this.user.getName());

		List<GoogleRouteInfo> route = new ArrayList<>();

		RoutePointInfo[] routePoints = this.currentRoute.getPoints();
		for (RoutePointInfo routePoint : routePoints) {
			route.add(routePoint.getGoogleRoute());
		}

		routeObject.setRoute(route);

		return routeObject;
	}

	@Override
	public MapObject getRouteAsMapObject() {
		return this.createRouteMapObject();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<MapObject> getMapRepresention() throws Exception {
		List<MapObject> objects = new ArrayList<>();

		{
			this.getLastLocation();

			if (this.currentLocation != null) {
				MapObject driverObject = new MapObject();
				driverObject.setId(this.user.getUserId().toString());
				driverObject.setType(MAP_OBJECT_TYPE.DRIVER);
				driverObject.setLocation(this.currentLocation);
				driverObject.setLabel(this.user.getName());

				objects.add(driverObject);
			}
		}
		/*
		 * { if (this.currentRoute != null) {
		 * objects.add(this.createRouteMapObject()); } }
		 */
		return objects;
	}

	@Override
	public <T> void saveEntity(Object id, T obj) throws Exception {
		if (this.em.find(obj.getClass(), id) == null) {
			this.em.persist(obj);
		} else {
			this.em.merge(obj);
		}
		this.em.flush();
	}

	@Override
	public void saverCTR(CTRInfo ctr, String status, Hashtable<String, String> params) {
		// TODO Auto-generated method stub

		if (this.em.find(CTRInfo.class, ctr.getId()) == null) {
			this.em.persist(ctr);
		} else {
			this.em.merge(ctr);
		}
		this.em.flush();

		OrderInfo order = new OrderInfo();

		AddressInfo adrinfo = new AddressInfo();
		adrinfo.setAddress(ctr.getEntrega());
		adrinfo.setLocation(ctr.getLocation());

		order.setAddress(adrinfo);
		OrderId orderId = new OrderId();
		orderId.setOrder_id(ctr.getId().split("-")[0]);
		orderId.setSector_id(ctr.getGeradorId());
		order.setOrderId(orderId);

		order.setStatus(ctr.getStatus());

		OrderStatus currentStatus = new OrderStatus();
		currentStatus.setStatus(ORDER_STATUS.GENERATE);
		currentStatus.setDate(Calendar.getInstance().getTime());
		order.setCurrentStatus(currentStatus);

		this.em.persist(orderId);
		this.em.flush();

		try {
			this.notifyNewObjectUpdate(MapObject.createMapObject(order));
		} catch (Exception e) {
			logger.log(Level.INFO, e.getLocalizedMessage(), e);
		}

	}

	@Override
	@Asynchronous
	@TransactionTimeout(unit = TimeUnit.MINUTES, value = 10)
	public Future<List<OrderInfo>> processFile(String filepath) throws Exception {
		ExcelFileImportNew importer = new ExcelFileImportNew(this.em, this.user.getUserId().getSector_id(), filepath);
		List<OrderInfo> result = importer.process();
		AsyncResult<List<OrderInfo>> async = new AsyncResult<List<OrderInfo>>(result);
		logger.info("async: " + async);
		return async;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void setUser(UserInfo user) {
		this.user = user;
		// this.user_map_id = this.user.getUserId().getUser_id() + "|" +
		// this.user.getUserId().getSector_id();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public UserInfo getUser() {
		return this.user;
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// new api
	///////////////////////////////////////////////////////////////////////////// :)///////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	/*
	 * @Override public void cancelOrder(EventCancelOrder cancelOrder) throws
	 * Exception { logger.info("cancel order: " + cancelOrder.getOrderId());
	 * 
	 * OrderInfo order = this.em.find(OrderInfo.class,
	 * cancelOrder.getOrderId());
	 * 
	 * if (order != null) { this.service.cancelOrder(order.getOrderId(),
	 * cancelOrder.getRoute_id(), this.user.getUserId(), false); } }
	 * 
	 * @Override public void cancelRoute(EventCancelRoute cancelRoute) throws
	 * Exception { logger.info("cancel route: " + cancelRoute.getRoute_id());
	 * 
	 * if (this.currentRoute != null) {
	 * this.service.cancelRoute(this.currentRoute.getId(),
	 * this.user.getUserId(), false); this.currentRoute = null; } }
	 */

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void reloadRoute(String route_id) throws Exception {
		this.em.clear();
		RouteInfo route = this.em.find(RouteInfo.class, route_id);
		if (route != null && route.getUserId().equals(this.user.getUserId())) {
			this.em.detach(route);
			this.currentRoute = route;
			logger.info("reloadRoute this.currentRoute: " + this.currentRoute);
		}

	}

	private void notifyNewObjectUpdate(MapObject mapObject) throws Exception {

		this.executionService.submit(() -> {
			try {
				Future<List<UserInfo>> future = this.service.getSessionUsersRemote(this.user.getUserId(),
						USER_ROLE.CONTROLLER);
				List<UserInfo> controllers = future.get();

				logger.info("controllers: " + controllers);

				controllers.forEach(user -> {
					try {
						this.service.getUserSessionEJB(user.getUserId()).updateMapObjectInfo(mapObject);
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
				});
			} catch (Exception e) {
				logger.log(Level.INFO, e.getLocalizedMessage(), e);
			}
		});
	}

	@Override
	public void updateRouteWithNewStatus(EventChangeStatusRoute status) throws Exception {
		logger.info("route id: " + status.getRoute_id() + " status: " + status.getNewStatus().getStatus());
		final RouteInfo route = this.em.find(RouteInfo.class, status.getRoute_id());

		if (route != null) {
			if (ROUTE_STATUS.CANCEL.equals(status.getNewStatus().getStatus())) {
				this.service.cancelRoute(status.getRoute_id(), this.user.getUserId(), false, false);
				this.currentRoute = null;
			} else if (ROUTE_STATUS.SELECT.equals(status.getNewStatus().getStatus())) {
				this.reloadRoute(status.getRoute_id());
			} else {
				RouteStatus currentStatus = status.getNewStatus();
				currentStatus.setTimeDate(Calendar.getInstance().getTime());
				route.setCurrentStatus(currentStatus);

				this.em.merge(route);
				this.em.flush();

				this.notifyNewObjectUpdate(MapObject.createMapObject(route));
			}
		}
	}

	@Override
	public void updateOrderWithNewStatus(EventChangeStatusOrder status) throws Exception {
		logger.info("order: " + status.getOrderId() + " status: " + status.getNewStatus());
		OrderInfo order = this.em.find(OrderInfo.class, status.getOrderId());

		if (order != null) {
			if (ORDER_STATUS.NEW.equals(status.getNewStatus())) {
				this.service.cancelOrder(status.getOrderId(), status.getRoute_id(), this.user.getUserId(), false);
			} else {
				OrderStatus currentStatus = new OrderStatus();
				currentStatus.setStatus(status.getNewStatus());
				currentStatus.setDate(Calendar.getInstance().getTime());
				order.setCurrentStatus(currentStatus);

				this.em.merge(order);
				this.em.flush();

				this.notifyNewObjectUpdate(MapObject.createMapObject(order));
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	@PostConstruct
	public void postConstruct() {
		try {
			if (this.timer != null) {
				this.timer.purge();
				this.timer.cancel();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		this.timer = new java.util.Timer(false);
		this.timer.schedule(new TimerTask() {
			@Override
			public void run() {
				checkListeners();
			}
		}, 0, 20000);
	}

	@Override
	// schedules dont works in stateful ejbs?!
	// @Schedule(hour = "*", minute = "*", second = "*/20", persistent = false)
	// @Schedule(second = "1", minute = "*", hour = "*", dayOfMonth = "*", month
	// =
	// "*", year = "*", persistent = false)
	public void checkListeners() {
		List<GRXUIEventListener> toRemove = new ArrayList<>();
		synchronized (this.listeners) {
			for (GRXUIEventListener listener : this.listeners) {
				try {
					if (!listener.isUIActive()) {
						toRemove.add(listener);
					}
				} catch (Exception e) {
					logger.warning("error: " + e);
					toRemove.add(listener);
				}
			}

			if (!toRemove.isEmpty()) {
				logger.info("removing listeners: " + toRemove);
				this.listeners.removeAll(toRemove);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void removeMapObjectInMap(MapObject mapObject) throws Exception {
		synchronized (this.listeners) {

			this.listeners.forEach(listener -> {

				for (int i = 0; i < 3; i++) {
					try {
						logger.info("removeMapObjectInMap listener: " + listener);
						listener.removeMapObjectInMap(mapObject);
						break;
					} catch (java.rmi.NoSuchObjectException e) {
						// tentar novamente
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
						continue;
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
						break;
					}
				}
			});
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void drawMapObjectInMap(MapObject mapObject) throws Exception {
		synchronized (this.listeners) {

			this.listeners.forEach(listener -> {

				for (int i = 0; i < 3; i++) {
					try {
						logger.info("updateMapObjectInMap listener: " + listener);
						listener.drawMapObjectInMap(mapObject);
						break;
					} catch (java.rmi.NoSuchObjectException e) {
						// tentar novamente
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
						continue;
					} catch (Exception e) {
						logger.log(Level.WARNING,
								"%%%%%%%%%%%%% erro notificando ui: " + e.getLocalizedMessage() + " %%%%%%%%%%%%%");
						break;
					}
				}
			});
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateMapObjectInfo(List<MapObject> objects) throws Exception {

		synchronized (this.listeners) {

			this.listeners.forEach(listener -> {
				for (int i = 0; i < 3; i++) {
					try {
						logger.info("updateMapObjectInfo listener: " + listener);
						for (MapObject object : objects) {
							listener.updateMapObjectInfo(object);
						}

						listener.doFilterMap(true);
						break;
					} catch (java.rmi.NoSuchObjectException e) {
						// tentar novamente
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
						continue;
					} catch (Exception e) {
						logger.log(Level.WARNING,
								"%%%%%%%%%%%%% erro notificando ui: " + e.getLocalizedMessage() + " %%%%%%%%%%%%%");
						break;
					}
				}
			});

		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateMapObjectInfo(MapObject object) throws Exception {

		synchronized (this.listeners) {

			this.listeners.forEach(listener -> {
				for (int i = 0; i < 3; i++) {
					try {
						logger.info("updateMapObjectInfo listener: " + listener);
						listener.updateMapObjectInfo(object);
						listener.doFilterMap(true);
						break;
					} catch (java.rmi.NoSuchObjectException e) {
						// tentar novamente
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
						continue;
					} catch (Exception e) {
						logger.log(Level.WARNING,
								"%%%%%%%%%%%%% erro notificando ui: " + e.getLocalizedMessage() + " %%%%%%%%%%%%%");
						break;
					}
				}
			});
		}

	}

	@Override
	public void showCTROnMap(CTRInfo ctr) {

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void showNotificationInWindow(String title, String message, Hashtable<String, Object> props)
			throws Exception {
		synchronized (this.listeners) {

			this.listeners.forEach(listener -> {
				for (int i = 0; i < 3; i++) {
					try {
						logger.info("showNotificationInWindow listener: " + listener);
						listener.showNotificationInWindow(title, message, props);
						break;
					} catch (java.rmi.NoSuchObjectException e) {
						// tentar novamente
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
						continue;
					} catch (Exception e) {
						logger.log(Level.WARNING,
								"%%%%%%%%%%%%% erro notificando ui: " + e.getLocalizedMessage() + " %%%%%%%%%%%%%");
						break;
					}
				}
			});
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void showOcurrence(String title, String message, Hashtable<String, Object> props) throws Exception {
		synchronized (this.listeners) {

			this.listeners.forEach(listener -> {
				for (int i = 0; i < 3; i++) {
					try {
						logger.info("showOcurrence listener: " + listener);
						listener.showOcurrence(title, message, props.get("occurr_id").toString(), props);
						break;
					} catch (java.rmi.NoSuchObjectException e) {
						// tentar novamente
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
						}
						continue;
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
						break;
					}
				}
			});
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void registerListener(GRXUIEventListener listener) throws Exception {
		logger.info("registrando listener:" + listener + " in: " + this);
		synchronized (this.listeners) {
			if (!this.listeners.contains(listener)) {
				this.listeners.add(listener);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void unRegisterListener(GRXUIEventListener listener) throws Exception {
		logger.info("des-registrando listener:" + listener + " in: " + this);
		synchronized (this.listeners) {
			if (this.listeners.contains(listener)) {
				this.listeners.remove(listener);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isAlive() throws Exception {
		return true;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean reloadSession(Hashtable<String, Object> props) throws Exception {
		this.currentRoute = null;
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////

	@PreDestroy
	public void preDestroy() {

	}

	@Override
	@Remove
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void killSession(String status) throws Exception {
		System.gc();
	}
}
