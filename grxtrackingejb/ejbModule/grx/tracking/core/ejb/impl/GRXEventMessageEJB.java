package grx.tracking.core.ejb.impl;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import grx.tracking.core.ejb.GRXEventService;
import grx.tracking.core.events.GRXEvent;

@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/grxtracking/GRXQueueGeoLocation") })
public class GRXEventMessageEJB implements MessageListener {

	private static Logger logger = Logger.getLogger(GRXEventMessageEJB.class.getCanonicalName());

	@PersistenceContext(unitName = "grxtracking.ds")
	private EntityManager em;

	@EJB
	private GRXEventService eventService;

	@Override
	public void onMessage(Message message) {
		try {
			logger.info("process message: " + message);

			if (message instanceof ObjectMessage) {
				ObjectMessage om = (ObjectMessage) message;
		
				if (om.getObject() instanceof GRXEvent) {
					GRXEvent grxEvent = (GRXEvent) om.getObject();
					eventService.processEvent(grxEvent);
				}

			}
		} catch (Exception e) {
			logger.log(Level.INFO, e.getLocalizedMessage(), e);
			throw new RuntimeException(e);
		}

	}

}
