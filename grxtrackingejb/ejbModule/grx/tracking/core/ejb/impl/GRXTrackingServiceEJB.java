package grx.tracking.core.ejb.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.NoSuchEJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import grx.tracking.core.crypt.Crypt;
import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.http.HttpClient;
import grx.tracking.core.http.JSONConverter;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.maps.MapObject.MAP_OBJECT_TYPE;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.FirebaseTokenInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.LastKnowUserLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.RouteStatus;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.RouteStatus.ROUTE_STATUS;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;
import grx.tracking.core.route.GRXRouteDistanceManagerEJB;
import grx.tracking.core.transport.CancelResultMessage;
import grx.tracking.core.transport.EventMessage;
import grx.tracking.core.transport.RouteResultMessage;
import grx.tracking.core.util.GRXSessionMock;
import grx.tracking.core.util.MyConvertArray;
import grx.tracking.core.util.MySphericalUtil;
import grx.tracking.core.util.PopulateMockData;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Startup
@Singleton
@AccessTimeout(unit = TimeUnit.MINUTES, value = 5) // concurrency
@Local(GRXTrackingService.class)
public class GRXTrackingServiceEJB implements GRXTrackingService {

	private static long TIME_CHECK_LASK_KNOW_LOCATION = 5 * 60 * 1000L; // 5min

	private static Logger logger = Logger.getLogger(GRXTrackingServiceEJB.class.getCanonicalName());

	@PersistenceContext(unitName = "grxtracking.ds")
	private EntityManager em;

	@PersistenceUnit(unitName = "grxtracking.ds.local")
	private EntityManagerFactory fact;

	private Hashtable<UserId, GRXTrackingSession> sessions;

	@Resource
	private ManagedExecutorService executionService;

	@EJB
	private GRXRouteDistanceManagerEJB router;

	private Timer timer;

	private List<SectorInfo> bases;

	public GRXTrackingServiceEJB() {
		this.sessions = new Hashtable<>();
		this.timer = new Timer();
	}

	@PostConstruct
	public void postCostruct() {
		PopulateMockData mock = new PopulateMockData(this.em);
		mock.populateUsers();
		// mock.populateOrder();
		// mock.pupulateTrucks();
		// mock.populateSector();
		{
			TypedQuery<SectorInfo> query = this.em.createQuery("select b from SectorInfo as b", SectorInfo.class);
			this.bases = query.getResultList();
		}

		/*this.timer.schedule(new TimerTask() {

			@Override
			public void run() {
				EntityManager em1 = fact.createEntityManager();
				TypedQuery<LastKnowUserLocation> query = em1.createQuery("select l from LastKnowUserLocation as l",
						LastKnowUserLocation.class);
				List<LastKnowUserLocation> results = query.getResultList();
				if (results != null) {
					Date currentTime = Calendar.getInstance().getTime();
					results.forEach(lastknow -> {
						if ((currentTime.getTime()
								- lastknow.getUpdateDate().getTime()) > TIME_CHECK_LASK_KNOW_LOCATION) {

							logger.info("removendo location do usuario: " + lastknow.getUserId());

							try {
								EntityTransaction t = em1.getTransaction();

								t.begin();
								em1.remove(lastknow);
								em1.flush();
								t.commit();

								List<UserId> contollers = getSessionsLocal(lastknow.getUserId(), USER_ROLE.CONTROLLER);
								contollers.forEach(userId -> {
									try {
										MapObject mapObject = new MapObject();
										mapObject.setId(userId.toString());
										mapObject.setType(MAP_OBJECT_TYPE.DRIVER);
										mapObject.setUserId(userId);
										getUserSessionEJB(userId).removeMapObjectInMap(mapObject);
									} catch (Exception e) {
										logger.log(Level.INFO, e.getLocalizedMessage(), e);
									}
								});

								GRXTrackingSession session = getUserSessionEJB(lastknow.getUserId());
								session.getLastLocation();

							} catch (Exception e) {
								logger.log(Level.INFO, e.getLocalizedMessage(), e);
							}
						}
					});
				}

				em1.close();

			}
		}, 0, TIME_CHECK_LASK_KNOW_LOCATION);*/

		logger.info("############# ##################### #############");
		logger.info("############# GRXGathering v0.1.00 #############");
		logger.info("############# ##################### #############");

	}

	@Override
	public GoogleRouteInfo findGoogleRoute(GeoLocation start, GeoLocation end) throws Exception {
		return router.findGoogleRoute(start, end);
	}

	@Override
	public String validateToken(String token) throws Exception {
		String tokenInfo = null;
		try {
			tokenInfo = Crypt.decpt(token);
		} catch (Exception e) {
			throw new Exception("nao foi possivel desencriptar o token: " + token);
		}

		logger.info("decpt:" + tokenInfo);

		String[] parts = tokenInfo.split("\\|");

		String user_id = parts[0];
		String sector_id = parts[1];
		String tokenDate = parts[2];

		UserId userId = new UserId();
		userId.setSector_id(sector_id);
		userId.setUser_id(user_id);

		// validar usuario
		UserInfo user = this.em.find(UserInfo.class, userId);
		if (user == null) {
			throw new Exception("usuario nao valido para token");
		}

		if (!token.equals(user.getCurrentToken())) {
			throw new Exception("token enviado difere do registrado");
		}

		// validar se tem sessao
		if (!this.sessions.containsKey(userId)) {
			throw new Exception("usuario sem sessao");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmSSSS");
		Date date = sdf.parse(tokenDate);

		String userIdString = userId.getUser_id() + "|" + userId.getSector_id();
		return userIdString;
	}

	@Override
	public GRXTrackingSession getUserSessionEJB(UserId userId) throws Exception {
		GRXTrackingSession session = this.sessions.get(userId);
		if (session == null) {
			return new GRXSessionMock();
		} else {
			return session;
		}
	}

	private GRXTrackingSession createUserSession() throws Exception {
		InitialContext ctxt = new InitialContext();
		return (GRXTrackingSession) ctxt
				.lookup("java:app/grxtrackingejb/GRXTrackingSessionEJB!grx.tracking.core.ejb.GRXTrackingSession");
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public UserInfo authenticate(String username, String password, String section_id, USER_ROLE role,
			String app_version) throws Exception {

		// UserId userId = new UserId(username, section_id);
		// UserInfo user = this.em.find(UserInfo.class, userId);

		if (!"v1.0.6".equals(app_version)) {
			throw new Exception("vers�o do aplicativo invalida");
		}

		UserInfo user = null;
		try {
			user = this.em.createQuery("select u from UserInfo as u where u.userId.user_id ='" + username + "'",
					UserInfo.class).getSingleResult();
		} catch (Exception e) {
			throw new Exception("erro consultando o banco");
		}

		if (user == null) {
			throw new Exception("usuario nao encontrado");
		}

		if (!user.getPassword().equals(password)) {
			throw new Exception("senha invalida");
		}

		if (!MyConvertArray.cointais(user.getRoles(), role)) {
			throw new Exception("perfil errado do usuario");
		}

		Date lastLogin = user.getLastLogin();

		{
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmSSSS");

			String tokenInfo = user.getUserId().getUser_id() + "|" + user.getUserId().getSector_id() + "|"
					+ sdf.format(Calendar.getInstance().getTime());
			String token = Crypt.ecpt(tokenInfo);
			user.setCurrentToken(token);
			user.setTokenDate(Calendar.getInstance().getTime());
			user.setLastLogin(Calendar.getInstance().getTime());

			this.em.merge(user);
			this.em.flush();
			this.em.detach(user);
		}

		{

			GRXTrackingSession newSession = this.createUserSession();
			newSession.setUser(user);

			GRXTrackingSession oldSession = this.sessions.get(user.getUserId());
			if (oldSession != null) {
				try {
					// check is alive
					oldSession.isAlive();

					// kill old session
					oldSession.killSession("relogin");
				} catch (Exception e) {
					logger.warning("error calling old session: " + e.getLocalizedMessage());
				}
			}

			this.sessions.put(user.getUserId(), newSession);
		}

		user.setLastLogin(lastLogin);

		return user;
	}

	@Override
	public <T> List<T> query(Class<T> classs, String sql, Hashtable<String, Object> params) throws Exception {
		TypedQuery<T> query = this.em.createQuery(sql, classs);
		for (String key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}
		return query.getResultList();
	}

	private List<UserId> getSessionsLocal(UserId currentId, USER_ROLE role) {
		List<UserId> selected = new ArrayList<>();
		List<UserId> toRemove = new ArrayList<>();

		for (UserId userId : this.sessions.keySet()) {
			if (currentId == null || !userId.equals(currentId)) {
				// UserInfo user = null;
				try {
					GRXTrackingSession session = this.sessions.get(userId);
					session.isAlive();
					// user = session.getUser();
				} catch (NoSuchEJBException e) {
					logger.info("NoSuchEJBException e: " + e.getLocalizedMessage() + " userid: " + userId);
					toRemove.add(userId);
					continue;
				} catch (Exception e) {
					logger.info("erro call session ejb: " + e.getLocalizedMessage() + " userid: " + userId);
					toRemove.add(userId);
					continue;
				}

				selected.add(userId);

				// if (MyConvertArray.cointais(user.getRoles(), role)) {
				// selected.add(userId);
				// }
			}
		}

		for (UserId userId : toRemove) {
			this.sessions.remove(userId);
		}

		return selected;
	}

	@Override
	@Asynchronous
	public Future<List<UserId>> getSessionsRemote(UserId currentId, USER_ROLE role) {
		List<UserId> selected = new ArrayList<>();
		List<UserId> toRemove = new ArrayList<>();

		for (UserId userId : this.sessions.keySet()) {
			if (currentId == null || !userId.equals(currentId)) {
				UserInfo user = null;
				try {
					GRXTrackingSession session = this.sessions.get(userId);
					user = session.getUser();
				} catch (NoSuchEJBException e) {
					logger.info("NoSuchEJBException e: " + e.getLocalizedMessage() + " userid: " + userId);
					toRemove.add(userId);
					continue;
				} catch (Exception e) {
					logger.info("erro call session ejb: " + e.getLocalizedMessage() + " userid: " + userId);
					toRemove.add(userId);
					continue;
				}

				if (MyConvertArray.cointais(user.getRoles(), role)) {
					selected.add(userId);
				}
			}
		}

		for (UserId userId : toRemove) {
			this.sessions.remove(userId);
		}

		return new AsyncResult<List<UserId>>(selected);
	}

	@Override
	@Asynchronous
	public Future<List<UserInfo>> getSessionUsersRemote(UserId currentId, USER_ROLE role) {
		List<UserInfo> selected = new ArrayList<>();
		List<UserId> toRemove = new ArrayList<>();

		for (UserId userId : this.sessions.keySet()) {
			if (currentId == null || !userId.equals(currentId)) {
				UserInfo user = null;
				try {
					GRXTrackingSession session = this.sessions.get(userId);
					user = session.getUser();
				} catch (NoSuchEJBException e) {
					logger.info("NoSuchEJBException e: " + e.getLocalizedMessage() + " userid: " + userId);
					toRemove.add(userId);
					continue;
				} catch (Exception e) {
					logger.info("erro call session ejb: " + e.getLocalizedMessage() + " userid: " + userId);
					toRemove.add(userId);
					continue;
				}

				if (MyConvertArray.cointais(user.getRoles(), role)) {
					selected.add(this.em.find(UserInfo.class, userId));
				}
			}
		}

		for (UserId userId : toRemove) {
			this.sessions.remove(userId);
		}

		return new AsyncResult<List<UserInfo>>(selected);
	}

	@Override
	public void confirmRoute(RouteInfo routeInfo, UserId userId, String statusStr) throws Exception {

		logger.info("confirm route: " + routeInfo.getId() + " user: " + userId);

		RouteStatus status = new RouteStatus();
		status.setStatus(ROUTE_STATUS.RUNNING);
		status.setTimeDate(Calendar.getInstance().getTime());
		routeInfo.setCurrentStatus(status);

		this.em.merge(routeInfo);
		this.em.flush();

		List<MapObject> mapObjects = new ArrayList<>();

		{
			this.executionService.submit(() -> {
				try {
					this.getUserSessionEJB(userId).reloadRoute(routeInfo.getId());
				} catch (Exception e) {
					logger.log(Level.INFO, e.getLocalizedMessage(), e);
				}
			});

		}
		{

			RoutePointInfo[] points = routeInfo.getPoints();
			for (RoutePointInfo point : points) {
				OrderInfo order = this.em.find(OrderInfo.class, point.getOrderId());

				if (order != null) {
					OrderStatus currentStatus = new OrderStatus();
					currentStatus.setStatus(ORDER_STATUS.ROUTING);
					currentStatus.setDate(Calendar.getInstance().getTime());
					order.setCurrentStatus(currentStatus);

					this.em.merge(order);
					this.em.flush();

					mapObjects.add(MapObject.createMapObject(order));
				}
			}

		}
		{
			RouteResultMessage resultMessage = new RouteResultMessage();

			RouteInfo fakeRoute = new RouteInfo();
			fakeRoute.setId(routeInfo.getId());
			resultMessage.setRoute(fakeRoute);
			resultMessage.setStatus(statusStr);

			this.sendMessageToUser(resultMessage, userId);
		}
		{
			this.executionService.submit(() -> {
				try {
					// notificar TODOS os controladores
					List<UserId> controllers = this.getSessionsLocal(userId, USER_ROLE.CONTROLLER);

					logger.info("notify to: " + controllers);

					for (UserId userId2 : controllers) {
						this.getUserSessionEJB(userId2).updateMapObjectInfo(mapObjects);
					}

				} catch (Exception e) {
					logger.log(Level.INFO, e.getLocalizedMessage(), e);
				}
			});
		}

	}

	@Override
	public void cancelOrder(OrderId orderId, String route_id, UserId userId, boolean sendNotif) throws Exception {
		logger.info("cancel order: " + orderId + " route id: " + route_id);
		CancelResultMessage cancelResult = new CancelResultMessage();
		cancelResult.setWasCancel("order");

		OrderInfo order = this.em.find(OrderInfo.class, orderId);

		try {

			if (order == null) {
				throw new Exception("pedido nao encontrado");
			}

			if (order != null) {
				OrderStatus currentStatus = new OrderStatus();
				currentStatus.setDate(Calendar.getInstance().getTime());
				currentStatus.setStatus(ORDER_STATUS.NEW);
				order.setCurrentStatus(currentStatus);

				this.em.merge(order);
				this.em.flush();

				if (route_id != null) {
					RouteInfo route = this.em.find(RouteInfo.class, route_id);
					RoutePointInfo[] points = route.getPoints();

					if (points != null) {
						List<RoutePointInfo> new_points = new ArrayList<>();

						for (RoutePointInfo point : points) {
							if (!point.getOrderId().equals(order.getOrderId())) {
								new_points.add(point);
							}
						}

						RoutePointInfo[] newPoints = MyConvertArray.convertArrayList(new_points, RoutePointInfo.class);

						logger.info("###### newPoints: " + newPoints + " size: " + newPoints.length + " #####");

						route.setPoints(newPoints);

						try {
							String k_str = route.getK_Str();
							k_str = k_str.replace(order.getOrderId().getOrder_id().toString(), "");
							route.setK_Str(k_str);
						} catch (Exception e) {
							logger.warning("controlled exception 1: " + e.getLocalizedMessage());
						}

						try {
							RouteStatus oldStatus = route.getCurrentStatus();

							RouteStatus currentStatus1 = new RouteStatus();
							currentStatus1.setStatus(oldStatus.getStatus());
							currentStatus1.setTimeDate(Calendar.getInstance().getTime());
							route.setCurrentStatus(currentStatus1);
						} catch (Exception e) {
							logger.warning("controlled exception 2: " + e.getLocalizedMessage());
						}

						this.em.merge(route);
						this.em.flush();
					}
				}
			}
			cancelResult.setOrderId(orderId);
			cancelResult.setStatus("OK");

		} catch (Exception e) {
			logger.log(Level.INFO, e.getLocalizedMessage(), e);
			cancelResult.setStatus("ERROR");
			cancelResult.setMessage(e.getLocalizedMessage());
		}

		if (sendNotif) {
			this.sendMessageToUser(cancelResult, userId);
		}
		{
			this.executionService.submit(() -> {
				try {
					// notificar TODOS controladores

					List<UserId> controllers = this.getSessionsLocal(userId, USER_ROLE.CONTROLLER);

					logger.info("notify to: " + controllers);

					MapObject mapObject = MapObject.createMapObject(order);
					for (UserId userId2 : controllers) {
						this.getUserSessionEJB(userId2).updateMapObjectInfo(mapObject);
					}
				} catch (Exception e) {
					logger.log(Level.INFO, e.getLocalizedMessage(), e);
				}
			});
		}
	}

	@Override
	public void cancelRoute(String route_id, UserId userId, boolean sendNotif, boolean reloadSession) throws Exception {
		CancelResultMessage cancelResult = new CancelResultMessage();
		cancelResult.setWasCancel("route");

		RouteInfo route = this.em.find(RouteInfo.class, route_id);
		List<MapObject> mapObjects = new ArrayList<>();

		try {
			if (route == null) {
				throw new Exception("rota nao encontrada");
			}

			if (route != null) {
				{
					RoutePointInfo[] points = route.getPoints();
					for (RoutePointInfo point : points) {
						OrderInfo order = this.em.find(OrderInfo.class, point.getOrderId());
						if (order != null && (ORDER_STATUS.RESERVED.equals(order.getCurrentStatus().getStatus())
								|| ORDER_STATUS.ROUTING.equals(order.getCurrentStatus().getStatus()))) {

							OrderStatus status = new OrderStatus();
							status.setDate(Calendar.getInstance().getTime());
							status.setStatus(ORDER_STATUS.NEW);
							order.setCurrentStatus(status);

							this.em.merge(order);
							this.em.flush();

							mapObjects.add(MapObject.createMapObject(order));
						}
					}
				}

				{
					RouteStatus currentStatus = new RouteStatus();
					currentStatus.setStatus(ROUTE_STATUS.CANCEL);
					currentStatus.setTimeDate(Calendar.getInstance().getTime());
					route.setCurrentStatus(currentStatus);

					this.em.merge(route);
					this.em.flush();
				}
			}

			cancelResult.setRoute_id(route.getId());
			cancelResult.setStatus("OK");

		} catch (Exception e) {
			logger.log(Level.INFO, e.getLocalizedMessage(), e);
			cancelResult.setStatus("ERROR");
			cancelResult.setMessage(e.getLocalizedMessage());
		}

		if (sendNotif) {
			this.sendMessageToUser(cancelResult, userId);
		}

		{
			if (reloadSession) {
				this.executionService.submit(() -> {
					try {
						this.getUserSessionEJB(userId).reloadSession(null);
					} catch (Exception e) {
						logger.log(Level.INFO, e.getLocalizedMessage(), e);
					}
				});
			}
		}

		{
			this.executionService.submit(() -> {
				try {
					// notificar as session de controladores
					List<UserId> controllers = this.getSessionsLocal(userId, USER_ROLE.CONTROLLER);

					for (UserId userId2 : controllers) {
						this.getUserSessionEJB(userId2).updateMapObjectInfo(mapObjects);
					}

				} catch (Exception e) {
					logger.log(Level.INFO, e.getLocalizedMessage(), e);
				}
			});
		}

	}

	@Override
	public Address findAddressForCEP(String cep) {
		try {
			cep = cep.replace(".", "").replaceAll("-", "");
			OkHttpClient client = new OkHttpClient();

			Request request = new Request.Builder().url("http://viacep.com.br/ws/" + cep + "/json/ ").get().build();

			Response response = client.newCall(request).execute();

			String result = response.body().string();
			logger.info("result CEP: " + result);

			Map<String, Object> retMap = new Gson().fromJson(result, new TypeToken<HashMap<String, Object>>() {
			}.getType());

			Address adr = new Address();

			adr.setCEP(cep);
			adr.setEndereco(retMap.get("logradouro").toString());
			adr.setXLgr(retMap.get("logradouro").toString());
			adr.setXCpl(retMap.get("complemento").toString());
			adr.setXBairro(retMap.get("bairro").toString());
			adr.setUF(retMap.get("uf").toString());
			adr.setCidade(retMap.get("localidade").toString());

			return adr;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public void sendMessageToUser(Object content, UserId userId) throws Exception {

		logger.info("sending push: " + content + " to: " + userId);

		FirebaseTokenInfo tokenInfo = this.em.find(FirebaseTokenInfo.class, userId);

		if (tokenInfo != null && tokenInfo.getToken() != null) {
			HttpClient client = new HttpClient();
			JSONConverter conv = new JSONConverter();

			String jsonString = conv.toJson(content);

			logger.info("push jsonString: " + jsonString);

			EventMessage message = new EventMessage();
			message.setJsonString(jsonString);
			message.setEventClass(content.getClass().getCanonicalName());
			for (int index = 0; index < 5; index++) {
				if (client.sendFirebaseMessage(tokenInfo.getToken().getTokenInfo(), message)) {
					break;
				}
				Thread.sleep(2000);
			}
		}

	}

	@Override
	public SectorInfo findNearestBase(GeoLocation location) {

		SectorInfo near = null;
		double minor_distance = Double.MAX_VALUE;
		for (SectorInfo base : this.bases) {
			double distance = MySphericalUtil.computeDistanceBetween(location, base.getLocation());
			if (distance < minor_distance) {
				near = base;
				minor_distance = distance;
			}
		}

		return near;
	}

	@Override
	public <T> List<T> list(Class<T> classs) throws Exception {
		return this.em.createQuery("select o from " + classs.getName() + " as o", classs).getResultList();
	}

	@Override
	public <T> T findEntity(Object id, Class<T> c) throws Exception {
		return this.em.find(c, id);
	}

}
