package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import grx.tracking.core.transport.TokenInfo;

@Entity
public class FirebaseTokenInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserId userId;

	@Lob
	private TokenInfo token;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date registerDate;

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public TokenInfo getToken() {
		return token;
	}

	public void setToken(TokenInfo token) {
		this.token = token;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	
}
