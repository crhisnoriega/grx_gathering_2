package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CTRInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum CTR_STATUS {
		SELECT("Selecione Status"), NEW("Gerado"), ENVIADO("Enviado a Obra"), RECEBIDO("Recebido em Obra"), RETIRADO(
				"Retirado");

		private String name;

		private CTR_STATUS(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@Id
	private String id;

	private String geradorId;

	@Embedded
	private Address entrega;

	@Embedded
	private ContainerCTRInfo container;

	@Embedded
	private CTRStatus status;

	@Embedded
	private GeoLocation location;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGeradorId() {
		return geradorId;
	}

	public void setGeradorId(String geradorId) {
		this.geradorId = geradorId;
	}

	public Address getEntrega() {
		return entrega;
	}

	public void setEntrega(Address entrega) {
		this.entrega = entrega;
	}

	public ContainerCTRInfo getContainer() {
		return container;
	}

	public void setContainer(ContainerCTRInfo container) {
		this.container = container;
	}

	public CTRStatus getStatus() {
		return status;
	}

	public void setStatus(CTRStatus status) {
		this.status = status;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public void setLocation(GeoLocation location) {
		this.location = location;
	}

}
