package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import grx.tracking.core.persistence.TruckInfo.TRUCK_TYPE;

@Entity
@Cacheable
public class OrderInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum PERIOD_TYPE {
		SELECT("Selecione Periodo"), DAY("Diurno"), NIGHT("Noturno"), OTHER("Outro");

		private String name;

		private PERIOD_TYPE(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	};

	public static enum ORDER_TYPE {
		SELECT("Selecione Tipo"), DELIVERY("Entrega"), PICK("Retirada"), EXCHANGE("Troca");

		private String name;

		private ORDER_TYPE(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	};

	public static enum PRIOR_TYPE {
		LOW, NORMAL, HIGH, FORCE
	};

	@EmbeddedId
	private OrderId orderId;

	private Date requestDate;

	private Date dueDate;

	private ORDER_TYPE orderType;
	private PRIOR_TYPE priorType;
	private PERIOD_TYPE periodType;

	@Lob
	private TRUCK_TYPE[] trucks;

	@Embedded
	private OrderStatus currentStatus;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "date", column = @Column(name = "date_ctr")),
			@AttributeOverride(name = "status", column = @Column(name = "status_ctr")) })
	private CTRStatus status;

	@OneToOne(cascade = CascadeType.ALL)
	private AddressInfo address;

	private String nearest_sector_id;

	private String description;

	private String driverMessage;

	private int qtdRequested;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_order")),
			@AttributeOverride(name = "customer_id", column = @Column(name = "customer_id_order")) })
	private CustomerId customerId;

	@Lob
	private List<OrderRestriction> restrictions;

	public OrderId getOrderId() {
		return orderId;
	}

	public void setOrderId(OrderId orderId) {
		this.orderId = orderId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public ORDER_TYPE getOrderType() {
		return orderType;
	}

	public void setOrderType(ORDER_TYPE orderType) {
		this.orderType = orderType;
	}

	public PRIOR_TYPE getPriorType() {
		return priorType;
	}

	public void setPriorType(PRIOR_TYPE priorType) {
		this.priorType = priorType;
	}

	public OrderStatus getCurrentStatus() {
		return currentStatus;
	}

	public PERIOD_TYPE getPeriodType() {
		return periodType;
	}

	public void setPeriodType(PERIOD_TYPE periodType) {
		this.periodType = periodType;
	}

	public void setCurrentStatus(OrderStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public AddressInfo getAddress() {
		return address;
	}

	public void setAddress(AddressInfo address) {
		this.address = address;
	}

	public List<OrderRestriction> getRestrictions() {
		return restrictions;
	}

	public void setRestrictions(List<OrderRestriction> restrictions) {
		this.restrictions = restrictions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDriverMessage() {
		return driverMessage;
	}

	public void setDriverMessage(String driverMessage) {
		this.driverMessage = driverMessage;
	}

	public int getQtdRequested() {
		return qtdRequested;
	}

	public void setQtdRequested(int qtdRequested) {
		this.qtdRequested = qtdRequested;
	}

	public CustomerId getCustomerId() {
		return customerId;
	}

	public void setCustomerId(CustomerId customerId) {
		this.customerId = customerId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		return result;
	}

	public String getNearest_Sector_Id() {
		return nearest_sector_id;
	}

	public void setNearest_Sector_Id(String nearest_sector_id) {
		this.nearest_sector_id = nearest_sector_id;
	}

	public TRUCK_TYPE[] getTrucks() {
		return trucks;
	}

	public void setTrucks(TRUCK_TYPE[] trucks) {
		this.trucks = trucks;
	}

	public CTRStatus getStatus() {
		return status;
	}

	public void setStatus(CTRStatus status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderInfo other = (OrderInfo) obj;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		return true;
	}

}
