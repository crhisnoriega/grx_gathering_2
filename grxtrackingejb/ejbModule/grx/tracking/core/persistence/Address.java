package grx.tracking.core.persistence;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.Embeddable;

@Embeddable
public class Address implements Serializable {

	private static final long serialVersionUID = 1L;

	protected String xLgr = "";
	protected String endereco = "";
	protected String nro = "";
	protected String xCpl = "";
	protected String xBairro = "";
	protected String cMun = "";
	protected String xMun = "";
	protected String uf = "";
	protected String cep = "";
	protected String cPais = "";
	protected String xPais = "";
	protected String fone = "";
	protected String cidade = "";

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * Gets the value of the xLgr property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getXLgr() {
		return xLgr;
	}

	/**
	 * Sets the value of the xLgr property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setXLgr(String value) {
		this.xLgr = value;
	}

	/**
	 * Gets the value of the nro property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getNro() {
		return nro;
	}

	/**
	 * Sets the value of the nro property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setNro(String value) {
		this.nro = value;
	}

	/**
	 * Gets the value of the xCpl property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getXCpl() {
		return xCpl;
	}

	/**
	 * Sets the value of the xCpl property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setXCpl(String value) {
		this.xCpl = value;
	}

	/**
	 * Gets the value of the xBairro property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getXBairro() {
		return xBairro;
	}

	/**
	 * Sets the value of the xBairro property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setXBairro(String value) {
		this.xBairro = value;
	}

	/**
	 * Gets the value of the cMun property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCMun() {
		return cMun;
	}

	/**
	 * Sets the value of the cMun property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCMun(String value) {
		this.cMun = value;
	}

	/**
	 * Gets the value of the xMun property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getXMun() {
		return xMun;
	}

	/**
	 * Sets the value of the xMun property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setXMun(String value) {
		this.xMun = value;
	}

	/**
	 * Gets the value of the uf property.
	 * 
	 * @return possible object is {@link Uf }
	 * 
	 */
	public String getUF() {
		return uf;
	}

	/**
	 * Sets the value of the uf property.
	 * 
	 * @param value
	 *            allowed object is {@link Uf }
	 * 
	 */
	public void setUF(String value) {
		this.uf = value;
	}

	/**
	 * Gets the value of the cep property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCEP() {
		return cep;
	}

	/**
	 * Sets the value of the cep property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCEP(String value) {
		this.cep = value;
	}

	/**
	 * Gets the value of the cPais property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCPais() {
		return cPais;
	}

	/**
	 * Sets the value of the cPais property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCPais(String value) {
		this.cPais = value;
	}

	/**
	 * Gets the value of the xPais property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getXPais() {
		return xPais;
	}

	/**
	 * Sets the value of the xPais property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setXPais(String value) {
		this.xPais = value;
	}

	/**
	 * Gets the value of the fone property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getFone() {
		return fone;
	}

	/**
	 * Sets the value of the fone property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setFone(String value) {
		this.fone = value;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String asAddressString() {
		String address_str = this.getEndereco() + ", " + this.getNro() + " - " + this.getXBairro() + " - "
				+ this.getXMun() + ".";
		return address_str.replace("null", "");

	}
	
	

	public void merge(Address addr) {
		this.endereco = addr.getEndereco();
		this.nro = addr.getNro();
		this.xBairro = addr.getXBairro();
		this.xMun = addr.getXMun();
	}

	public static Address extractAddressSimpleWithException(String address_str) throws Exception {
		Address address = new Address();
		String[] address_split = address_str.split("-");

		String lgr = address_split[0];
		String lgr_numero = null;
		if (lgr.contains(",")) {
			String[] lgr_split = lgr.split(",");
			lgr = lgr_split[0];
			lgr_numero = lgr_split[1];
		}

		String bairro = address_split[1];
		String municipio = address_split[2];

		address.setEndereco(lgr);
		address.setNro(lgr_numero);
		address.setXBairro(bairro);
		address.setXMun(municipio);

		return address;
	}

	public static Address extractAddressSimple(String address_str) {
		Address address = new Address();
		try {
			String[] address_split = address_str.split("-");

			String lgr = address_split[0];
			String lgr_numero = null;
			if (lgr.contains(",")) {
				String[] lgr_split = lgr.split(",");
				lgr = lgr_split[0];
				lgr_numero = lgr_split[1];
			}

			String bairro = address_split[1];
			String municipio = address_split[2];

			address.setEndereco(lgr);
			address.setNro(lgr_numero);
			address.setXBairro(bairro);
			address.setXMun(municipio);

		} catch (Exception e) {
			System.err.println("error: " + e.getLocalizedMessage());
			address.setEndereco(address_str);

		}
		return address;
	}

	public static Address extractAddress(String address_str) {
		Address address = new Address();
		try {
			String[] address_split = address_str.split("-");

			String cep = address_split[0];
			String lgr = address_split[1];

			String[] lgr_split = lgr.split(",");

			lgr = lgr_split[0];
			String lgr_numero = lgr_split[1];
			String bairro = address_split[2];

			String municipio = null;
			if (address_split.length > 3) {
				municipio = address_split[3];
			}

			String estado = null;
			if (address_split.length > 4) {
				estado = address_split[4];
			}

			address.setCEP(cep);
			// address.setXLgr(lgr);
			address.setEndereco(lgr);
			address.setNro(lgr_numero);
			address.setXBairro(bairro);
			address.setXMun(municipio);
			address.setUF(estado);

		} catch (Exception e) {
			System.err.println("error: " + e.getLocalizedMessage());
			try {
				int first_pos = address_str.indexOf("-");
				address_str = address_str.substring(0, first_pos) + address_str.substring(first_pos + 1);
				System.out.println("new address: " + address_str);
				return extractAddress(address_str);
			} catch (Exception e1) {
				System.err.println("error second: " + e1.getLocalizedMessage());
				address.setEndereco(address_str);
			}

		}
		return address;
	}

	public static void main(String[] args) {
		Address ad = Address.extractAddress("09080-001 - Av. Dom Pedro II, 2112 - Campestre, Santo Andr� - SP");
		System.out.println(ad.asAddressString());
		ad = Address.extractAddress("03605020 - R. HENRIQUE DE SOUSA QUEIROS, 436 - PENHA DE FRAN�A - SAO PAULO - SP");
		System.out.println(ad.asAddressString());
	}

}
