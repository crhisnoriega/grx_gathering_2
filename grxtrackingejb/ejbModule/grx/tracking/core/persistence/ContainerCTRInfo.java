package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;

@Embeddable
public class ContainerCTRInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private ContainerType type;

	private String TRASH_1;
	private String TRASH_2;
	private String TRASH_3;
	private String TRASH_4;
	private String TRASH_5;
	private String TRASH_6;
	private String TRASH_7;
	private String TRASH_8;
	private String TRASH_9;
	private String TRASH_10;
	private String TRASH_11;
	private String TRASH_12;
	
	

	public ContainerType getType() {
		return type;
	}

	public void setType(ContainerType type) {
		this.type = type;
	}

	public String getTRASH_1() {
		return TRASH_1;
	}

	public void setTRASH_1(String tRASH_1) {
		TRASH_1 = tRASH_1;
	}

	public String getTRASH_2() {
		return TRASH_2;
	}

	public void setTRASH_2(String tRASH_2) {
		TRASH_2 = tRASH_2;
	}

	public String getTRASH_3() {
		return TRASH_3;
	}

	public void setTRASH_3(String tRASH_3) {
		TRASH_3 = tRASH_3;
	}

	public String getTRASH_4() {
		return TRASH_4;
	}

	public void setTRASH_4(String tRASH_4) {
		TRASH_4 = tRASH_4;
	}

	public String getTRASH_5() {
		return TRASH_5;
	}

	public void setTRASH_5(String tRASH_5) {
		TRASH_5 = tRASH_5;
	}

	public String getTRASH_6() {
		return TRASH_6;
	}

	public void setTRASH_6(String tRASH_6) {
		TRASH_6 = tRASH_6;
	}

	public String getTRASH_7() {
		return TRASH_7;
	}

	public void setTRASH_7(String tRASH_7) {
		TRASH_7 = tRASH_7;
	}

	public String getTRASH_8() {
		return TRASH_8;
	}

	public void setTRASH_8(String tRASH_8) {
		TRASH_8 = tRASH_8;
	}

	public String getTRASH_9() {
		return TRASH_9;
	}

	public void setTRASH_9(String tRASH_9) {
		TRASH_9 = tRASH_9;
	}

	public String getTRASH_10() {
		return TRASH_10;
	}

	public void setTRASH_10(String tRASH_10) {
		TRASH_10 = tRASH_10;
	}

	public String getTRASH_11() {
		return TRASH_11;
	}

	public void setTRASH_11(String tRASH_11) {
		TRASH_11 = tRASH_11;
	}

	public String getTRASH_12() {
		return TRASH_12;
	}

	public void setTRASH_12(String tRASH_12) {
		TRASH_12 = tRASH_12;
	}

}
