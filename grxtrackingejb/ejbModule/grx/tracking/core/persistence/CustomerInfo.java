package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class CustomerInfo implements Serializable{

	@EmbeddedId
	private CustomerId customerId;
	
	private String name;
	
	private String description;
}
