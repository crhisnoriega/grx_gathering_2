package grx.tracking.core.persistence;

import java.io.Serializable;

import grx.tracking.core.persistence.TruckInfo.TRUCK_TYPE;

public class OrderRestriction implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum ORDER_RESTRICTION_TYPE {
		TIME_DUE, TRUCK;
	};

	private ORDER_RESTRICTION_TYPE type;
	private TRUCK_TYPE truckType;
	private long maxTime;

	public ORDER_RESTRICTION_TYPE getType() {
		return type;
	}

	public void setType(ORDER_RESTRICTION_TYPE type) {
		this.type = type;
	}

	public TRUCK_TYPE getTruckType() {
		return truckType;
	}

	public void setTruckType(TRUCK_TYPE truckType) {
		this.truckType = truckType;
	}

	public long getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(long maxTime) {
		this.maxTime = maxTime;
	}

}
