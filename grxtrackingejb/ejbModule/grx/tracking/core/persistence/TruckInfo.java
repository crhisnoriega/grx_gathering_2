package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;

@Entity
public class TruckInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum TRUCK_TYPE {
		SELECIONE("Selecione"), TRUCK1("Camin�o Simples"), TRUCK2("Camin�o Duplo"), TRUCK3("Camin�o Triplo"), TRUCK4(
				"");

		private String name;

		private TRUCK_TYPE(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	};

	@EmbeddedId
	private TruckId truckId;

	private String placa;

	private String rntc;

	private Date registerDate;

	private TRUCK_TYPE carType;

	private String truckDescription;

	@Lob
	private Hashtable<ORDER_TYPE, Integer> dimensions;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_user")) })
	private UserId truckOwner;

	public TruckId getTruckId() {
		return truckId;
	}

	public void setTruckId(TruckId truckId) {
		this.truckId = truckId;
	}

	@OneToMany(fetch = FetchType.EAGER)
	private List<CarEventInfo> events;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRntc() {
		return rntc;
	}

	public void setRntc(String rntc) {
		this.rntc = rntc;
	}

	public TRUCK_TYPE getCarType() {
		return carType;
	}

	public void setCarType(TRUCK_TYPE carType) {
		this.carType = carType;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public List<CarEventInfo> getEvents() {
		return events;
	}

	public void setEvents(List<CarEventInfo> events) {
		this.events = events;
	}

	public String getTruckDescription() {
		return truckDescription;
	}

	public void setTruckDescription(String truckDescription) {
		this.truckDescription = truckDescription;
	}

	public UserId getTruckOwner() {
		return truckOwner;
	}

	public void setTruckOwner(UserId truckOwner) {
		this.truckOwner = truckOwner;
	}

	public Hashtable<ORDER_TYPE, Integer> getDimensions() {
		return dimensions;
	}

	public void setDimensions(Hashtable<ORDER_TYPE, Integer> dimensions) {
		this.dimensions = dimensions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((truckId == null) ? 0 : truckId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TruckInfo other = (TruckInfo) obj;
		if (truckId == null) {
			if (other.truckId != null)
				return false;
		} else if (!truckId.equals(other.truckId))
			return false;
		return true;
	}

}
