package grx.tracking.core.persistence;

import java.io.Serializable;

public class PermissionDescr implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum PERMISSION_TYPE {
		ALLOW_USER, ALLOW_SECTION, ALLOW_ALL, DENY
	}

	private UserId userId;
	private String sector_id;

	private PERMISSION_TYPE type;

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public String getSector_id() {
		return sector_id;
	}

	public void setSector_id(String sector_id) {
		this.sector_id = sector_id;
	}

	public PERMISSION_TYPE getType() {
		return type;
	}

	public void setType(PERMISSION_TYPE type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "[type: " + this.type + ", sector_id: " + this.sector_id + ", userId: " + userId + "]";
	}
}
