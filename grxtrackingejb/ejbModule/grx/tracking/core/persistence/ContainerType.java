package grx.tracking.core.persistence;

public enum ContainerType {
	SELECIONE("Selecionar"), 
	CONTAINER_1("Ca�amba de 4m� (4)"), 
	CONTAINER_2("Ca�amba de 5m� (5)"), 
	CONTAINER_3("Ca�amba de 7m� (7)"), 
	CONTAINER_4("Ca�amba de 12m3 (12)"),
	CONTAINER_5("Ca�amba de 37 m� (37)"),
	CONTAINER_6("Caminh�o Basculante de 10m� (10)"),
	CONTAINER_7("Caminh�o Basculante de 12m� (12)"),
	CONTAINER_8("Caminh�o Basculante de 15m� (15)"),
	CONTAINER_9("Caminh�o Basculante de 16m� (16)"),
	CONTAINER_10("Caminh�o Basculante de 16,64m� (16.6)"),
	CONTAINER_11("Caminh�o Basculante de 17m� (17)"),
	CONTAINER_12("Caminh�o Basculante de 17,5m� (17.5)"),
	CONTAINER_13("Caminh�o Basculante de 18m� (18)"),
	CONTAINER_14("Caminh�o Basculante de 18,5m� (18.5)"),
	CONTAINER_15("Caminh�o Basculante de 20m� (20)"),	
	CONTAINER_16("Caminh�o  Toco de 6m� (6)"),
	
	CONTAINER_17("Carreta 24m� (24)"),
	CONTAINER_18("Carreta 35m� (35)"),
	
	CONTAINER_19("Cavalo Mec�nico (24)"),
	CONTAINER_20("Cavalo Mec�nico (30)"),
	CONTAINER_21("Cavalo Mec�nico (35)"),
	
	CONTAINER_22("Roll-on Roll-off 20m� (20)"),
	CONTAINER_23("Roll-on Roll-off 22m� (22)"),
	CONTAINER_24("Roll-on Roll-off 24m� (24)"),
	;

	private String name;

	private ContainerType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
};
