package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class RouteStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum ROUTE_STATUS {
		RUNNING("Em andamento"), PENDING("Pendente"), COMPLETE("Completada"), CANCEL("Cancelada"), REJECTED(
				"Rejeitada"), TO_APROVAL("Para aprova��o"), SELECT("Selecione Status Rota");
		private String name;

		private ROUTE_STATUS(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	};

	private ROUTE_STATUS status;
	private Date timeDate;
	private OrderId currentOrder;

	public ROUTE_STATUS getStatus() {
		return status;
	}

	public void setStatus(ROUTE_STATUS status) {
		this.status = status;
	}

	public Date getTimeDate() {
		return timeDate;
	}

	public void setTimeDate(Date timeDate) {
		this.timeDate = timeDate;
	}

	public OrderId getCurrentOrder() {
		return currentOrder;
	}

	public void setCurrentOrder(OrderId currentOrder) {
		this.currentOrder = currentOrder;
	}

}
