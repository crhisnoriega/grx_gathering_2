package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class UserId implements Serializable {

	private static final long serialVersionUID = 1L;

	private String user_id;
	private String sector_id;

	public UserId() {

	}

	public UserId(String user_id, String sector_id) {
		super();
		this.user_id = user_id;
		this.sector_id = sector_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getSector_id() {
		return sector_id;
	}

	public void setSector_id(String sector_id) {
		this.sector_id = sector_id;
	}

	@Override
	public String toString() {
		return this.user_id + "|" + this.sector_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sector_id == null) ? 0 : sector_id.hashCode());
		result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserId other = (UserId) obj;
		if (sector_id == null) {
			if (other.sector_id != null)
				return false;
		} else if (!sector_id.equals(other.sector_id))
			return false;
		if (user_id == null) {
			if (other.user_id != null)
				return false;
		} else if (!user_id.equals(other.user_id))
			return false;
		return true;
	}

}
