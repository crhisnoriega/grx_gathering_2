package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Cacheable
public class UserInfo implements Serializable {

	public static enum USER_ROLE {
		ADMIN("Administrador"), APP_USER("Usuario app"), CONTROLLER("Controlador"), DRIVER("Motorista");

		private String name;

		private USER_ROLE(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	};

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserId userId;

	private String password;
	private String email;
	private String name;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	private String currentToken;

	@Temporal(TemporalType.TIMESTAMP)
	private Date tokenDate;

	@Lob
	private USER_ROLE[] roles;

	@Lob
	@Column(name = "perm")
	private Serializable permissions;

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getCurrentToken() {
		return currentToken;
	}

	public void setCurrentToken(String currentToken) {
		this.currentToken = currentToken;
	}

	public Date getTokenDate() {
		return tokenDate;
	}

	public void setTokenDate(Date tokenDate) {
		this.tokenDate = tokenDate;
	}

	public USER_ROLE[] getRoles() {
		return roles;
	}

	public void setRoles(USER_ROLE[] roles) {
		this.roles = roles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfo other = (UserInfo) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	public List<PermissionDescr> getPermissions() {
		if (permissions == null) {
			permissions = new ArrayList<>();
		}
		return (List<PermissionDescr>) permissions;
	}

	public void setPermissions(List<PermissionDescr> permissions) {
		this.permissions = (Serializable) permissions;
	}

}
