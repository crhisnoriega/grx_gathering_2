package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class GeoLocation implements Serializable {

	private static final long serialVersionUID = 1L;

	private double latitude;
	private double longitude;
	private double elevation;
	private double orientation;

	public GeoLocation() {
		super();
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getElevation() {
		return elevation;
	}

	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	public double getOrientation() {
		return orientation;
	}

	public void setOrientation(double orientation) {
		this.orientation = orientation;
	}

	@Override
	public String toString() {

		return this.latitude + ", " + this.longitude;
	}

}