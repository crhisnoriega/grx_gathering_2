package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class EquipmentId implements Serializable {

	private static final long serialVersionUID = 1L;
	private String equip_id;
	private String sector_id;

	public String getEquip_id() {
		return equip_id;
	}

	public void setEquip_id(String equip_id) {
		this.equip_id = equip_id;
	}

	public String getSector_id() {
		return sector_id;
	}

	public void setSector_id(String sector_id) {
		this.sector_id = sector_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((equip_id == null) ? 0 : equip_id.hashCode());
		result = prime * result + ((sector_id == null) ? 0 : sector_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EquipmentId other = (EquipmentId) obj;
		if (equip_id == null) {
			if (other.equip_id != null)
				return false;
		} else if (!equip_id.equals(other.equip_id))
			return false;
		if (sector_id == null) {
			if (other.sector_id != null)
				return false;
		} else if (!sector_id.equals(other.sector_id))
			return false;
		return true;
	}

}
