package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class CTRStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum CTR_STATUS {
		SELECT("Selecione Status"), NEW("Gerado"), ENVIADO("Enviado a Obra"), RECEBIDO("Recebido em Obra"), RETIRADO(
				"Retirado"), A_VALIDAR("Para Validar"), PENDENTS("Pendente de Recebimento");

		private String name;

		private CTR_STATUS(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	private CTR_STATUS status;
	private Date date;

	public CTR_STATUS getStatus() {
		return status;
	}

	public void setStatus(CTR_STATUS status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
