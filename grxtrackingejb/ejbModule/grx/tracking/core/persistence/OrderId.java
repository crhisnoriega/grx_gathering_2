package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class OrderId implements Serializable {

	private static final long serialVersionUID = 1L;

	private String order_id;
	private String sector_id;

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getSector_id() {
		return sector_id;
	}

	public void setSector_id(String sector_id) {
		this.sector_id = sector_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order_id == null) ? 0 : order_id.hashCode());
		result = prime * result + ((sector_id == null) ? 0 : sector_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderId other = (OrderId) obj;
		if (order_id == null) {
			if (other.order_id != null)
				return false;
		} else if (!order_id.equals(other.order_id))
			return false;
		if (sector_id == null) {
			if (other.sector_id != null)
				return false;
		} else if (!sector_id.equals(other.sector_id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.order_id + "|" + this.sector_id;
	}

}
