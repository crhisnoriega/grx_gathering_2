package grx.tracking.core.persistence;

public enum TrashType {
	
	SELECIONE("Selecionar", "none"), 
	TRASH_1("Classe A - Concreto, Argamassa, Alvenaria, Cer�micos", "M3"),
	TRASH_2("Classe A/B - Entulho, Argamassa, Cer�micos, Pl�stico ,Papel, Metal", "M3"),
	TRASH_3("Classe B - Emb. Lata de Tinta, Pl�stico, Papel, Papel�o, Metal, Vidro, Massa Verde, Emb. Lata de Solvente, Poda de Arvor�s, Cobre, Alum�nio, A�o, Ferro", "M3"),
	TRASH_4("Classe B1 - Madeira", "M3"),
	TRASH_5("Classe B2 - Gesso", "M3"),
	TRASH_6("Classe C - L� de Vidro", "M3"),
	TRASH_7("Classe D - Amianto", "M3"),
	TRASH_8("Classe II A", "M3"),
	TRASH_9("Classe II B - Inertes", "M3"),
	TRASH_10("Rejeitos", "M3"),
	TRASH_11("Solos - Solos Provenientes de Terraplanagem e Escava��o", "M3")
	;
	
	private String name;
	private String unid;

	
	private TrashType(String name, String unid) {
		this.name = name;
		this.unid = unid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}
	
	

}
