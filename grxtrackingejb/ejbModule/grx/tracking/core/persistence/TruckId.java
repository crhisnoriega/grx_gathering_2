package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TruckId implements Serializable {

	private static final long serialVersionUID = 1L;

	private String truck_id;
	private String sector_id;

	public String getTruck_id() {
		return truck_id;
	}

	public void setTruck_id(String truck_id) {
		this.truck_id = truck_id;
	}

	public String getSector_id() {
		return sector_id;
	}

	public void setSector_id(String sector_id) {
		this.sector_id = sector_id;
	}

	@Override
	public String toString() {
		return this.truck_id + "|" + this.sector_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sector_id == null) ? 0 : sector_id.hashCode());
		result = prime * result + ((truck_id == null) ? 0 : truck_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TruckId other = (TruckId) obj;
		if (sector_id == null) {
			if (other.sector_id != null)
				return false;
		} else if (!sector_id.equals(other.sector_id))
			return false;
		if (truck_id == null) {
			if (other.truck_id != null)
				return false;
		} else if (!truck_id.equals(other.truck_id))
			return false;
		return true;
	}

}
