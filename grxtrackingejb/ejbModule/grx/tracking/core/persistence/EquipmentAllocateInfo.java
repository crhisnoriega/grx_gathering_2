package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
public class EquipmentAllocateInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_user")) })
	private UserId userId;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_equip")) })
	private EquipmentId equipId;

	private int quantity;

	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public EquipmentId getEquipId() {
		return equipId;
	}

	public void setEquipId(EquipmentId equipId) {
		this.equipId = equipId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
