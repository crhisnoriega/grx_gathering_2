package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.TableGenerator;

import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;

@Entity
@Cacheable
public class RouteInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Lob
	private RoutePointInfo[] points;

	@Embedded
	private UserId userId;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "currentOrder.sector_id", column = @Column(name = "sector_id_status")),
			@AttributeOverride(name = "currentOrder.order_id", column = @Column(name = "order_id_status")) })
	private RouteStatus currentStatus;

	@Lob
	private Hashtable<OrderInfo.ORDER_TYPE, Integer> qtde;

	// @Lob
	// private List<RouteStatus> followup;
	private String k_str;

	private String startSectorId;
	private String endSectorId;

	public void addQtde(ORDER_TYPE type) {
		if (this.qtde == null) {
			this.qtde = new Hashtable<>();
		}

		Integer q = this.qtde.get(type);
		if (q == null) {
			q = 0;
		}
		q++;
		this.qtde.put(type, q);
	}

	public Hashtable<OrderInfo.ORDER_TYPE, Integer> getQtde() {
		return qtde;
	}

	public void setQtde(Hashtable<OrderInfo.ORDER_TYPE, Integer> qtde) {
		this.qtde = qtde;
	}

	public RoutePointInfo[] getPoints() {
		return points;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPoints(RoutePointInfo[] points) {
		this.points = points;
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public RouteStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(RouteStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getK_Str() {
		return k_str;
	}

	public void setK_Str(String k_str) {
		this.k_str = k_str;
	}

	public String getStartSectorId() {
		return startSectorId;
	}

	public void setStartSectorId(String startSectorId) {
		this.startSectorId = startSectorId;
	}

	public String getEndSectorId() {
		return endSectorId;
	}

	public void setEndSectorId(String endSectorId) {
		this.endSectorId = endSectorId;
	}

}
