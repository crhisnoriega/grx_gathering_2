package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

@Embeddable
public class OrderStatus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum ORDER_STATUS {
		SELECT("Selecione Status"), NEW("Nova"), TO_VALIDATE("A Validar"), PENDING("Pendente"), RESERVED(
				"Reservado"), ROUTING("Em Rota"), FINISHED("Concluida"), OCURRENCE("Ocorrencia"), ENVIADO(
						"Enviado a Obra"), RECEBIDO(
								"Recebido em Obra"), RETIRADO(
										"Retirado"), A_VALIDAR("Para Validar"), GENERATE("Gerado"),;

		private String name;

		private ORDER_STATUS(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	private ORDER_STATUS status;
	private Date date;

	public ORDER_STATUS getStatus() {
		return status;
	}

	public void setStatus(ORDER_STATUS status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
