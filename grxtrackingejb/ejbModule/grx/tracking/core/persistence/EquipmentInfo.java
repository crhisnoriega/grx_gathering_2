package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class EquipmentInfo implements Serializable {
	public static enum EQUIP_TYPE {
		CONTAINER_1, CONTAINER_2
	};

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EquipmentId equipId;

	private String description;

	private EQUIP_TYPE type;

	private int quantity;

	public EquipmentId getEquipId() {
		return equipId;
	}

	public void setEquipId(EquipmentId equipId) {
		this.equipId = equipId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EQUIP_TYPE getType() {
		return type;
	}

	public void setType(EQUIP_TYPE type) {
		this.type = type;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
