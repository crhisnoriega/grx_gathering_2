package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import grx.tracking.core.maps.directions.GoogleRouteInfo;

@Entity
@Cacheable
public class CalculatedRouteInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private GeoLocation start;

	@AttributeOverrides({ @AttributeOverride(name = "latitude", column = @Column(name = "latitude_end")),
			@AttributeOverride(name = "longitude", column = @Column(name = "longitude_end")),
			@AttributeOverride(name = "elevation", column = @Column(name = "elevation_end")),
			@AttributeOverride(name = "orientation", column = @Column(name = "orientation_end")), })
	private GeoLocation end;

	@Lob
	private byte[] route_bytes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GeoLocation getStart() {
		return start;
	}

	public void setStart(GeoLocation start) {
		this.start = start;
	}

	public GeoLocation getEnd() {
		return end;
	}

	public void setEnd(GeoLocation end) {
		this.end = end;
	}

	public byte[] getRoute_Bytes() {
		return route_bytes;
	}

	public void setRoute_Bytes(byte[] route_bytes) {
		this.route_bytes = route_bytes;
	}

}
