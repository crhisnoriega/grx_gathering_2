package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Lob;
import javax.persistence.Transient;

import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.OrderInfo.PRIOR_TYPE;

public class RoutePointInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private OrderId orderId;

	private PRIOR_TYPE prior;

	private Date estimateTime;

	private int sequence;

	private RouteStatus status;

	@Lob
	private GoogleRouteInfo googleRoute;

	public OrderId getOrderId() {
		return orderId;
	}

	public void setOrderId(OrderId orderId) {
		this.orderId = orderId;
	}

	public PRIOR_TYPE getPrior() {
		return prior;
	}

	public void setPrior(PRIOR_TYPE prior) {
		this.prior = prior;
	}

	public Date getEstimateTime() {
		return estimateTime;
	}

	public void setEstimateTime(Date estimateTime) {
		this.estimateTime = estimateTime;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	public RouteStatus getStatus() {
		return status;
	}

	public void setStatus(RouteStatus status) {
		this.status = status;
	}

	public GoogleRouteInfo getGoogleRoute() {
		return googleRoute;
	}

	public void setGoogleRoute(GoogleRouteInfo googleRoute) {
		this.googleRoute = googleRoute;
	}

}