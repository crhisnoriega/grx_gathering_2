package grx.tracking.core.persistence;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class UserTruckMap implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_user")) })
	private UserId userId;
	private TruckId truckId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public TruckId getTruckId() {
		return truckId;
	}

	public void setTruckId(TruckId truckId) {
		this.truckId = truckId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserTruckMap other = (UserTruckMap) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
