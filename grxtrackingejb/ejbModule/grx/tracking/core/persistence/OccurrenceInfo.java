package grx.tracking.core.persistence;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OccurrenceInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum OCCURRENCE_STATUS {
		RECEIVED("Recebida"), CLOSED("Fechada"), OTHER("OUTRA"), PENDENT("Pendente");

		private String name;

		private OCCURRENCE_STATUS(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	};

	public static enum OCCURRENCE_TYPE {
		TRUCK_BREAK("Problema no Caminhao"), ORDER_PROBLEM("Problema no Pedido"), OTHER(""), ROUTE_PROBLEM(
				"Problema na Rota");
		private String name;

		private OCCURRENCE_TYPE(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	};

	@Id
	private String id;

	@Embedded
	private UserId userId;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_order")) })
	private OrderId orderId;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "sector_id", column = @Column(name = "sector_id_truck")) })
	private TruckId truckId;

	private String route_id;

	private Date date;

	private OCCURRENCE_STATUS status;
	private OCCURRENCE_TYPE type;

	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserId getUserId() {
		return userId;
	}

	public void setUserId(UserId userId) {
		this.userId = userId;
	}

	public OrderId getOrderId() {
		return orderId;
	}

	public void setOrderId(OrderId orderId) {
		this.orderId = orderId;
	}

	public String getRoute_id() {
		return route_id;
	}

	public void setRoute_id(String route_id) {
		this.route_id = route_id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OCCURRENCE_STATUS getStatus() {
		return status;
	}

	public void setStatus(OCCURRENCE_STATUS status) {
		this.status = status;
	}

	public OCCURRENCE_TYPE getType() {
		return type;
	}

	public void setType(OCCURRENCE_TYPE type) {
		this.type = type;
	}

	public TruckId getTruckId() {
		return truckId;
	}

	public void setTruckId(TruckId truckId) {
		this.truckId = truckId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OccurrenceInfo other = (OccurrenceInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
