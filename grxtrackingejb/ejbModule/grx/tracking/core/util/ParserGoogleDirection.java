package grx.tracking.core.util;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import grx.tracking.core.maps.directions.Leg;
import grx.tracking.core.maps.directions.Response;
import grx.tracking.core.maps.directions.Route;
import grx.tracking.core.maps.directions.Step;

public class ParserGoogleDirection {

	public List<String> extractPolylines(String json) {
		List<String> lines = new ArrayList<>();

		Gson gson = new Gson();
		Response r = gson.fromJson(json, Response.class);
		for (Route route : r.getRoutes()) {
			Leg[] legs = route.getLegs();
			for (Leg leg : legs) {
				// System.out.println(leg);
				Step[] steps = leg.getSteps();
				for (Step step : steps) {
					// System.out.println(step);
					System.out.println(step.getPolyline().get("points").getAsString());
					lines.add(step.getPolyline().get("points").getAsString());
				}
			}
		}

		return lines;
	}

	public static void main(String[] args) throws Exception {
		List<String> lines = Files.readAllLines(Paths.get("C:/", "json.txt"));
		String json = "";

		for (String line : lines) {
			json = json + line + "\n";
		}

		Gson gson = new Gson();
		Response r = gson.fromJson(json, Response.class);
		for (Route route : r.getRoutes()) {
			Leg[] legs = route.getLegs();
			for (Leg leg : legs) {
				// System.out.println(leg);
				Step[] steps = leg.getSteps();
				for (Step step : steps) {
					// System.out.println(step);
					System.out.println(step.getPolyline().get("points").getAsString());
				}
			}
		}

	}
}
