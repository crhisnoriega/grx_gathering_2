package grx.tracking.core.util;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;

import grx.tracking.core.ejb.GRXTrackingService;
import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.persistence.UserId;

@ManagedBean
public class EJBLocator {

	@EJB
	private GRXTrackingService service;

	private GRXTrackingSession refUserSession(String userIdAsString) throws Exception {
		String[] parts = userIdAsString.split("|");

		String user_id = parts[0];
		String sector_id = parts[1];

		UserId userId = new UserId(user_id, sector_id);
		return this.service.getUserSessionEJB(userId);
	}
}
