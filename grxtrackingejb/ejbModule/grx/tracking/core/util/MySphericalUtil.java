package grx.tracking.core.util;

import static java.lang.Math.toRadians;

import com.google.maps.model.LatLng;

import grx.tracking.core.persistence.GeoLocation;

import static grx.tracking.core.util.MathUtil.*;

public class MySphericalUtil {

	private MySphericalUtil() {
	}

	/**
	 * Returns distance on the unit sphere; the arguments are in radians.
	 */
	private static double distanceRadians(double lat1, double lng1, double lat2, double lng2) {
		return arcHav(havDistance(lat1, lat2, lng1 - lng2));
	}

	/**
	 * Returns the angle between two LatLngs, in radians. This is the same as
	 * the distance on the unit sphere.
	 */
	static double computeAngleBetween(GeoLocation from, GeoLocation to) {
		return distanceRadians(toRadians(from.getLatitude()), toRadians(from.getLongitude()),
				toRadians(to.getLatitude()), toRadians(to.getLongitude()));
	}

	/**
	 * Returns the distance between two LatLngs, in meters.
	 */
	public static double computeDistanceBetween(GeoLocation from, GeoLocation to) {
		return computeAngleBetween(from, to) * EARTH_RADIUS;
	}

}