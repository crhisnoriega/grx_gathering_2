package grx.tracking.core.util;

import java.util.List;

import grx.tracking.core.events.GRXEvent;
import grx.tracking.core.persistence.PermissionDescr;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.PermissionDescr.PERMISSION_TYPE;

public class SecurityController {

	public static boolean checkPermission(GRXEvent event, List<PermissionDescr> userPerm) {

		boolean allow = false;
		switch (event.getTo()) {
		case TO_USER:
			for (PermissionDescr perm : userPerm) {
				UserId toUSerId = event.getToUserId();
				if (PERMISSION_TYPE.ALLOW_USER.equals(perm.getType()) && perm.getUserId().equals(toUSerId)) {
					allow = true;
				}
			}

			break;

		case TO_SECTION:
			for (PermissionDescr perm : userPerm) {
				UserId toUSerId = event.getToUserId();
				if (PERMISSION_TYPE.ALLOW_SECTION.equals(perm.getType())
						&& perm.getSector_id().equals(toUSerId.getSector_id())) {
					allow = true;
				}
			}

			break;

		case BROADCAST:
			allow = true;
			break;

		default:
			allow = false;
			break;
		}

		return allow;
	}
}
