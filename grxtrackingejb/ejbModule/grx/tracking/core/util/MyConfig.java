package grx.tracking.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

import org.apache.commons.lang3.SerializationUtils;

public class MyConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	private String confirmRoutes = "false";
	private String jsonConfig = "";

	private String configFile = System.getProperty("user.home") + File.separator + "." + "grxtracking" + File.separator
			+ "config";

	public MyConfig() {
		try {
			this.load();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void load_2_123() {
		File conf_dir = new File(this.configFile);
		if (!conf_dir.getParentFile().exists()) {
			conf_dir.getParentFile().mkdirs();
		}

		if (conf_dir.exists()) {
			try {

				// Reading the object from a file
				FileInputStream file = new FileInputStream(this.configFile);
				ObjectInputStream in = new ObjectInputStream(file);

				// Method for deserialization of object
				MyConfig mm = (MyConfig) in.readObject();

				in.close();
				file.close();

				this.confirmRoutes = mm.confirmRoutes;
				this.jsonConfig = mm.jsonConfig;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void load() throws Exception {
		File conf_dir = new File(this.configFile);
		if (!conf_dir.getParentFile().exists()) {
			conf_dir.getParentFile().mkdirs();
		}

		if (conf_dir.exists()) {
			MyConfig credendials = SerializationUtils.deserialize(new FileInputStream(new File(this.configFile)));
			this.confirmRoutes = credendials.confirmRoutes;
			this.jsonConfig = credendials.jsonConfig;
		}
	}

	public void save() throws Exception {
		File conf_dir = new File(this.configFile);
		if (!conf_dir.getParentFile().exists()) {
			conf_dir.getParentFile().mkdirs();
		}
		SerializationUtils.serialize(this, new FileOutputStream(new File(this.configFile)));
	}

	public String getConfirmRoutes() {
		return confirmRoutes;
	}

	public void setConfirmRoutes(String confirmRoutes) {
		this.confirmRoutes = confirmRoutes;
	}

	public String getJsonConfig() {
		return jsonConfig;
	}

	public void setJsonConfig(String jsonConfig) {
		this.jsonConfig = jsonConfig;
	}

}
