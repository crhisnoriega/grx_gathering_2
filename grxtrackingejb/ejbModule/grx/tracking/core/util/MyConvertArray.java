package grx.tracking.core.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MyConvertArray {

	public static <T> T[] convertArrayList(List<T> list, Class<T> c) {
		@SuppressWarnings("unchecked")
		T[] array_list = (T[]) Array.newInstance(c, list.size());
		list.toArray(array_list);
		return array_list;
	}

	public static <T> boolean cointais(T[] array, T obj) {
		for (T o : array) {
			if (o.equals(obj)) {
				return true;
			}
		}

		return false;
	}

	public static void main(String[] args) {
		List<String> ff = new ArrayList<>();
		ff.add("dd");
		String[] a = convertArrayList(ff, String.class);
		System.out.println(a);
	}
}
