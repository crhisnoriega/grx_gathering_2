package grx.tracking.core.util;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.jboss.security.auth.spi.Users.User;

import grx.tracking.core.maps.directions.GoogleMapsServices;
import grx.tracking.core.maps.directions.GoogleRouteInfo;
import grx.tracking.core.persistence.Address;
import grx.tracking.core.persistence.AddressInfo;
import grx.tracking.core.persistence.GeoLocation;
import grx.tracking.core.persistence.OrderId;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.OrderInfo.ORDER_TYPE;
import grx.tracking.core.persistence.OrderStatus;
import grx.tracking.core.persistence.OrderStatus.ORDER_STATUS;
import grx.tracking.core.persistence.PermissionDescr;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.RoutePointInfo;
import grx.tracking.core.persistence.SectorInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.TruckInfo;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.persistence.PermissionDescr.PERMISSION_TYPE;
import grx.tracking.core.persistence.UserInfo.USER_ROLE;

public class PopulateMockData {

	public static Logger logger = Logger.getLogger(PopulateMockData.class.getCanonicalName());

	private EntityManagerFactory fact;
	private EntityManager em;

	public PopulateMockData(EntityManagerFactory fact) {
		super();
		this.fact = fact;
		this.em = this.fact.createEntityManager();
	}

	public PopulateMockData(EntityManager em) {
		this.em = em;
	}

	public void populateRouteTest() {

		RouteInfo route = new RouteInfo();
		List<RoutePointInfo> points = new Vector<>();
		RoutePointInfo routePoint = new RoutePointInfo();

		GoogleMapsServices services = new GoogleMapsServices();

		try {
			GoogleRouteInfo googleRoute = services.extractRouteInfo("tatuape, sp, brazil", "penha, sp, brazil");
			routePoint.setGoogleRoute(googleRoute);
		} catch (Exception e) {
			e.printStackTrace();
		}
		points.add(routePoint);

		route.setPoints(MyConvertArray.convertArrayList(points, RoutePointInfo.class));

		this.em.persist(route);
		this.em.flush();

	}

	/*public void populateOrder() {
		GoogleMapsServices services = new GoogleMapsServices();
		{
			OrderId orderId = new OrderId();
			orderId.setOrder_id("120.345");
			orderId.setSector_id("section1");

			if (this.em.find(OrderInfo.class, orderId) == null) {

				OrderInfo order = new OrderInfo();
				order.setOrderId(orderId);
				order.setDescription("Cliente pediu para parar na frente do pr�dio do outro lado da rua");
				OrderStatus currentStatus = new OrderStatus();
				currentStatus.setStatus(ORDER_STATUS.WAITING);
				currentStatus.setDate(Calendar.getInstance().getTime());
				order.setCurrentStatus(currentStatus);
				order.setOrderType(ORDER_TYPE.DELIVERY);

				{
					AddressInfo address = new AddressInfo();

					Address addr = new Address();
					addr.setXLgr("RUA");
					addr.setEndereco("Rua Carlos Weber");
					addr.setNro("890");
					addr.setXCpl("BLOCO B APTO 32");
					addr.setXBairro("Vila Leopoldina");
					addr.setXMun("Sao Paulo");
					addr.setCidade("Sao Paulo");
					addr.setCEP("05303-000");

					address.setAddress(addr);

					order.setAddress(address);

					try {
						GeoLocation location = services.geoDecoding(addr);
						location.setElevation(0.1d);
						location.setOrientation(0.1d);

						address.setLocation(location);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				this.em.persist(order);
				this.em.flush();
			}
		}

		{
			OrderId orderId = new OrderId();
			orderId.setOrder_id("120.346");
			orderId.setSector_id("section1");

			if (this.em.find(OrderInfo.class, orderId) == null) {

				OrderInfo order = new OrderInfo();
				order.setOrderId(orderId);
				order.setDescription("Parar depois das 20h");
				OrderStatus currentStatus = new OrderStatus();
				currentStatus.setStatus(ORDER_STATUS.WAITING);
				currentStatus.setDate(Calendar.getInstance().getTime());
				order.setCurrentStatus(currentStatus);
				order.setOrderType(ORDER_TYPE.DELIVERY);

				{
					AddressInfo address = new AddressInfo();

					Address addr = new Address();
					addr.setXLgr("Av");
					addr.setEndereco("Celso Garcia");
					addr.setNro("3964");
					addr.setXBairro("Tatuape");
					addr.setXMun("Sao Paulo");
					addr.setCidade("Sao Paulo");
					addr.setCEP("03064-000");

					address.setAddress(addr);

					order.setAddress(address);

					try {
						GeoLocation location = services.geoDecoding(addr);
						location.setElevation(0.1d);
						location.setOrientation(0.1d);

						address.setLocation(location);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				this.em.persist(order);
				this.em.flush();
			}
		}

		{
			OrderId orderId = new OrderId();
			orderId.setOrder_id("120.347");
			orderId.setSector_id("section1");

			if (this.em.find(OrderInfo.class, orderId) == null) {

				OrderInfo order = new OrderInfo();
				order.setOrderId(orderId);
				order.setDescription("");
				OrderStatus currentStatus = new OrderStatus();
				currentStatus.setStatus(ORDER_STATUS.WAITING);
				currentStatus.setDate(Calendar.getInstance().getTime());
				order.setCurrentStatus(currentStatus);
				order.setOrderType(ORDER_TYPE.DELIVERY);

				{
					AddressInfo address = new AddressInfo();

					Address addr = new Address();
					addr.setXLgr("Rua");
					addr.setEndereco("Diogo do Couto");
					addr.setNro("8");
					addr.setXBairro("Jd bonfiglioli");
					addr.setXMun("Sao Paulo");
					addr.setCidade("Sao Paulo");
					addr.setCEP("03064-000");

					address.setAddress(addr);

					order.setAddress(address);

					try {
						GeoLocation location = services.geoDecoding(addr);
						location.setElevation(0.1d);
						location.setOrientation(0.1d);

						address.setLocation(location);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				this.em.persist(order);
				this.em.flush();
			}
		}
	}*/

	public void populateUsers() {
		{
			UserId userId = new UserId("admin", "base1");

			UserInfo user = this.em.find(UserInfo.class, userId);
			if (user == null) {
				user = new UserInfo();
				user.setUserId(userId);
				user.setName("administrador do sistema");
				user.setPassword("admin");
				user.setRoles(new USER_ROLE[] { USER_ROLE.CONTROLLER });

				PermissionDescr perm = new PermissionDescr();
				perm.setType(PERMISSION_TYPE.ALLOW_SECTION);
				perm.setSector_id("base1");
				user.getPermissions().add(perm);

				this.em.persist(user);
				this.em.flush();
				logger.info("usuario admin criado!");
			}
		}
		{
			List<UserInfo> users = this.em.createQuery( "select u from UserInfo as u",UserInfo.class).getResultList();
			users.forEach(user->{
				if(user.getRoles()==null) {
					user.setRoles(new UserInfo.USER_ROLE[] {UserInfo.USER_ROLE.DRIVER});
					em.merge(user);
					em.flush();
				}
			});
		}
		{
			UserId userId = new UserId("app", "base1");

			UserInfo user = this.em.find(UserInfo.class, userId);
			if (user == null) {
				user = new UserInfo();
				user.setUserId(userId);
				user.setName("usuario app");
				user.setPassword("app");
				user.setRoles(new USER_ROLE[] { USER_ROLE.DRIVER });

				//this.em.persist(user);
				// this.em.flush();
				logger.info("usuario app criado!");
			}
		}

	}

	public void populateSector() {
		{
			SectorInfo sector1 = new SectorInfo();
			sector1.setSectorId("base1");
			sector1.setName("Base Santo Andre");

			if (this.em.find(SectorInfo.class, sector1.getSectorId()) == null) {
				this.em.persist(sector1);
				this.em.flush();
			}
		}

		{
			SectorInfo sector1 = new SectorInfo();
			sector1.setSectorId("base2");
			sector1.setName("Base Tatuape");

			if (this.em.find(SectorInfo.class, sector1.getSectorId()) == null) {
				this.em.persist(sector1);
				this.em.flush();
			}
		}
	}

	public void pupulateTrucks() {
		{
			TruckId truckId = new TruckId();
			truckId.setSector_id("section1");
			truckId.setTruck_id("truck_1");

			if (this.em.find(TruckInfo.class, truckId) == null) {
				TruckInfo truck = new TruckInfo();

				truck.setTruckId(truckId);

				truck.setTruckOwner(new UserId("app", "section1"));
				truck.setPlaca("ARB-2312");
				truck.setRntc("4123512");
				truck.setTruckDescription("caminhao do joao");

				this.em.persist(truck);
				this.em.flush();
			}
		}

		{
			TruckId truckId = new TruckId();
			truckId.setSector_id("section1");
			truckId.setTruck_id("truck_2");

			if (this.em.find(TruckInfo.class, truckId) == null) {
				TruckInfo truck = new TruckInfo();

				truck.setTruckId(truckId);

				truck.setTruckOwner(new UserId("app", "section1"));
				truck.setPlaca("AXR-1234");
				truck.setRntc("4123512");
				truck.setTruckDescription("caminhao do irmao joao");

				this.em.persist(truck);
				this.em.flush();
			}
		}
	}
}
