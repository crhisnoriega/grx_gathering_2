package grx.tracking.core.util;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Future;

import grx.tracking.core.ejb.GRXTrackingSession;
import grx.tracking.core.events.EventChangeStatusOrder;
import grx.tracking.core.events.EventChangeStatusRoute;
import grx.tracking.core.events.EventOcurrence;
import grx.tracking.core.events.EventUpdateGeoLocation;
import grx.tracking.core.listener.GRXUIEventListener;
import grx.tracking.core.maps.MapObject;
import grx.tracking.core.persistence.CTRInfo;
import grx.tracking.core.persistence.EquipmentAllocateInfo;
import grx.tracking.core.persistence.LastKnowUserLocation;
import grx.tracking.core.persistence.OrderInfo;
import grx.tracking.core.persistence.RouteInfo;
import grx.tracking.core.persistence.TruckId;
import grx.tracking.core.persistence.UserId;
import grx.tracking.core.persistence.UserInfo;
import grx.tracking.core.transport.TokenInfo;

public class GRXSessionMock implements GRXTrackingSession {

	@Override
	public void reloadRoute(String route_id) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> T refreshEntity(T entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void checkListeners() {
		throw new RuntimeException("method dont implemented yet");
	}

	// @Override
	public void confirmRoute(RouteInfo routeInfo) throws Exception {
		throw new Exception("method dont implemented yet");
	}

	@Override
	public <T> T findEntity(Class<T> c, Object id) throws Exception {
		throw new Exception("method dont implemented yet");
	}

	@Override
	public MapObject getRouteAsMapObject() {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public <T> List<T> findEntities(String sql, Class<T> c, Hashtable<String, Object> params) throws Exception {
		throw new Exception("method dont implemented yet");
	}

	/*
	 * @Override public void cancelOrder(EventCancelOrder cancelOrder) throws
	 * Exception { throw new RuntimeException("method dont implemented yet"); }
	 * 
	 * @Override public void cancelRoute(EventCancelRoute cancelRoute) throws
	 * Exception { throw new RuntimeException("method dont implemented yet"); }
	 */

	@Override
	public void processLocation(EventUpdateGeoLocation update) throws Exception {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public void startScheduleTasks(boolean startBase, TruckId truckId, String sector_id, int deliverCapacity,
			int pickupCapacity, boolean confirmRoute, List<EquipmentAllocateInfo> allocate, Date date)
					throws Exception {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public void registerListener(GRXUIEventListener listener) throws Exception {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public void unRegisterListener(GRXUIEventListener listener) throws Exception {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public void killSession(String status) throws Exception {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public void registerFirebaseToken(TokenInfo token) throws Exception {
		throw new RuntimeException("method dont implemented yet");
	}

	@Override
	public void setUser(UserInfo user) {
		// TODO Auto-generated method stub

	}

	@Override
	public UserInfo getUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateRouteWithNewStatus(EventChangeStatusRoute newStatus) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void showNotificationInWindow(String title, String message, Hashtable<String, Object> props)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<MapObject> getMapRepresention() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void drawMapObjectInMap(MapObject mapObject) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public RouteInfo getCurrentRoute() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LastKnowUserLocation getLastLocation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<OrderInfo> getRouteOrders() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Future<List<OrderInfo>> processFile(String filepath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAlive() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean reloadSession(Hashtable<String, Object> props) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> void saveEntity(Object id, T obj) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void detachEntity(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public <T> void deleteEntity(T entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void recalculateRoute(RouteInfo route) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void processOcorrenceEvent(EventOcurrence event) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void showOcurrence(String title, String message, Hashtable<String, Object> props) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeMapObjectInMap(MapObject mapObject) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void addOrderToRoute(UserId userId, OrderInfo order) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateOrderWithNewStatus(EventChangeStatusOrder status) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMapObjectInfo(MapObject object) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMapObjectInfo(List<MapObject> objects) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void saverCTR(CTRInfo ctr, String status, Hashtable<String, String> params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void showCTROnMap(CTRInfo ctr) {
		// TODO Auto-generated method stub

	}
}