package grx.tracking.core.http;

import java.util.logging.Logger;

import grx.tracking.core.transport.EventMessage;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpClient {

	public static Logger logger = Logger.getLogger(HttpClient.class.getCanonicalName());

	public String SERVER_KEY = "AAAACyDoCs0:APA91bFgJwnsCTVRmXdCkPNjET_a6lKu-ZnPNWd8aFhDFNF744krtgJ-1CuSt499DXKc7GURNBx8Aatc1whH321G0ola12cV4fsmOvZOZ9pC-d45bf6GJffJ40nmFgkdeNbclQL1gVDk";
	public String TOKEN_TEST = "ed2TGr4Au3U:APA91bEDaJwr_OalRLZrZSqAUp3sSUqwP-4YYhMPT1YJjErV5-GrGdox7hggoznbunKx3pJ3uCgC1naVEkHu1-19LANebFtkQedZBOFxrxATkVDWzOfcq4tq17lrB5Pqsk_3HmgwxiGf";

	public boolean sendFirebaseMessage(String token, EventMessage message) throws Exception {

		JSONConverter conv = new JSONConverter();
		String jsonMessage = conv.toJson(message);

		String jsonGoogle = "{\"to\":\"" + token + "\",\"data\":" + jsonMessage + "}";

		OkHttpClient client = new OkHttpClient();
		RequestBody post = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonGoogle);

		Request request = new Request.Builder().url("https://fcm.googleapis.com/fcm/send").post(post)
				.addHeader("Authorization", "key=" + SERVER_KEY).build();

		Response response = client.newCall(request).execute();

		String result = response.body().string();

		logger.info(result);

		return result.contains("\"success\":1");
	}
}
